package id.co.halloarif.hagnews_webview;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.github.javiersantos.appupdater.AppUpdater;

public class MainActivity extends AppCompatActivity {

    private static final String url = "https://halloarif.co.id/news/";

    private ProgressBar pbMainActivityfvbi;
    private WebView wvMainActivityfvbi;
    private SwipeRefreshLayout srlMainActivityfvbi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();

        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();
    }

    private void initComponent() {
        pbMainActivityfvbi =(ProgressBar) findViewById(R.id.pbMainActivity);
        srlMainActivityfvbi =(SwipeRefreshLayout) findViewById(R.id.srlMainActivity);
        wvMainActivityfvbi =(WebView) findViewById(R.id.wvMainActivity);

    }

    private void initParam() {

    }

    private void initSession() {

    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initContent() {
        pbMainActivityfvbi.setVisibility(View.GONE);
        pbMainActivityfvbi.setMax(100);

        //WEBSETTING
        wvMainActivityfvbi.getSettings().setJavaScriptEnabled(true);
        wvMainActivityfvbi.getSettings().setAppCacheEnabled(true);
        wvMainActivityfvbi.getSettings().setLoadWithOverviewMode(true);
        wvMainActivityfvbi.getSettings().setSupportZoom(false);
        wvMainActivityfvbi.setWebViewClient(new KanzanWEbView());
        wvMainActivityfvbi.setWebChromeClient(new KanzanWEbChromeView());
        wvMainActivityfvbi.loadUrl(url);

        checkUpdate();
    }

    private void initListener() {
        wvMainActivityfvbi.getViewTreeObserver().addOnScrollChangedListener(() -> {
            if (wvMainActivityfvbi.getScrollY() == 0) {
                srlMainActivityfvbi.setEnabled(true);
            } else {
                srlMainActivityfvbi.setEnabled(false);
            }
        });
        srlMainActivityfvbi.setOnRefreshListener(() -> {
            srlMainActivityfvbi.setRefreshing(false);
            wvMainActivityfvbi.reload();
        });
    }

    private void checkUpdate() {
        AppUpdater appUpdater = new AppUpdater(this)
                .setTitleOnUpdateAvailable("Pembaruan Tersedia!")
                .setContentOnUpdateAvailable("Ada versi terbaru dari aplikasi Hallo Arif, Anda ingin melakukan pembaruan aplikasi sekarang?")
                .setButtonUpdate("Ya, Perbarui")
                .setButtonDismiss("Tidak, Terima Kasih")
                .setButtonDoNotShowAgain(null);
        appUpdater.start();
    }

    private class KanzanWEbChromeView extends WebChromeClient {

        @Override
        public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(MainActivity.this);
            builder.setMessage(message)
                    .setNeutralButton("OK", (arg0, arg1) -> result.confirm())
                    .setCancelable(true)
                    .show();
            return true;
        }

        @Override
        public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(MainActivity.this);
            builder.setMessage(message)
                    .setPositiveButton("Yes", (dialog, which) -> result.confirm())
                    .setNegativeButton("No", (dialog, which) -> result.cancel())
                    .setCancelable(true)
                    .show();
            return true;
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            pbMainActivityfvbi.setProgress(newProgress);
        }

    }

    private class KanzanWEbView extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            wvMainActivityfvbi.loadUrl(url);
            Log.d("Lihat", "shouldOverrideUrlLoading KanzanWEbView : " + url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            pbMainActivityfvbi.setVisibility(View.VISIBLE);
            Log.d("Lihat", "onPageStarted KanzanWEbView : " + url);
            Log.d("Lihat", "onPageStarted KanzanWEbView : " + favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            srlMainActivityfvbi.setRefreshing(false);
            pbMainActivityfvbi.setVisibility(View.GONE);
            Log.d("Lihat", "onPageFinished KanzanWEbView : " + url);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
        }
    }

    @Override
    public void onBackPressed() {
        if (wvMainActivityfvbi.canGoBack()){
            wvMainActivityfvbi.goBack();
        }else {
            finish();
        }
    }
}
