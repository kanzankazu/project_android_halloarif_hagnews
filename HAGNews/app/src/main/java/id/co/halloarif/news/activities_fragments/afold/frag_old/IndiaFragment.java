package id.co.halloarif.news.activities_fragments.afold.frag_old;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afold.activity_old.MainActivity;
import id.co.halloarif.news.activities_fragments.afold.activity_old.Open_Post_Activity;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_Adapter_news;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Sub_category_Adapter;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.utils.Recycleritemclicklistener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class IndiaFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    RecyclerView recyclerview_sub_cat_india, recyclerview_india_post;
    LinearLayoutManager linearLayoutManager;
    ArrayList<Viewpager_items> arrayList_sub_heading = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_all_post = new ArrayList<>();
    Sub_category_Adapter sub_category_adapter;
    SwipeRefreshLayout mSwipeRefreshLayout;
    TextView text_header;
    FragmentActivity fragmentActivity;
    View view1;
    Recyclerview_Adapter_news recyclerview_adapter_news;
    RequestQueue requestQueue;
    Handler handler1;

    public IndiaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view1 = inflater.inflate(R.layout.fragment_india, container, false);
        initComponent(view1);
        return view1;
    }

    private void initComponent(View view) {
        text_header = view.findViewById(R.id.text_header);
        recyclerview_india_post = view.findViewById(R.id.recyclerview_india_post);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerview_india_post.setLayoutManager(linearLayoutManager1);
        recyclerview_india_post.setItemAnimator(new DefaultItemAnimator());

        recyclerview_sub_cat_india = view.findViewById(R.id.recyclerview_sub_cat_india);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerview_sub_cat_india.setLayoutManager(linearLayoutManager);
        recyclerview_sub_cat_india.setItemAnimator(new DefaultItemAnimator());

        // SwipeRefreshLayout

        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_blue_dark);


        recyclerview_sub_cat_india.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Constants.Cat_Name = Constants.SLUG_TO_OPEN;
                Viewpager_items model = arrayList_sub_heading.get(position);
                Constants.Cat_Tag_Name = model.getName_sub();
                Constants.Tag_id = model.getPager_id();
                ((MainActivity) fragmentActivity).CreateFragment(new SubCategory_post_Fragment());
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        recyclerview_india_post.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        try {
                            Viewpager_items model = arrayList_all_post.get(position);
                            CLICK_METHOD(model);
                        } catch (Exception e) {
                            Log.e("EXCEPTION_CLICK", "" + e);
                        }
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                })
        );
    }

    public void CLICK_METHOD(Viewpager_items model) {
        String post_type = model.getPost_type();
        if (post_type.contentEquals("post")) {
            Intent i = new Intent(getActivity(), Open_Post_Activity.class);
            i.putExtra("POST_ID", model.getId());
            startActivity(i);
        } else if (post_type.contentEquals("audio")) {
            ((MainActivity) fragmentActivity).CreateFragment(Audio_post_Fragment.newInstance(model.getId()));
        } else {
            String embed_url = model.getEmbed_code();
            String video_title = model.getTitle();
            String video_id = model.getId();
            String created_at = model.getCreated_at();
            String video_views = model.getHit();
            String content = model.getContent();

            ((MainActivity) fragmentActivity).draggableView.setVisibility(View.VISIBLE);
            ((MainActivity) fragmentActivity).draggableView.maximize();
            ((MainActivity) fragmentActivity).Call_VIDEO_Details_API(video_id);
            ((MainActivity) fragmentActivity).CAll_API_ALL_COMMENTS(video_id);
            ((MainActivity) fragmentActivity).PLAYER_INITIALIZE(embed_url);
            ((MainActivity) fragmentActivity).setText_values(video_title, video_views, content, created_at);
        }
    }

    private void callAsynchronousTask() {
        handler1 = new Handler();
        final int delay = 3000; //milliseconds
        handler1.postDelayed(new Runnable() {
            public void run() {
                ///DO SOMETHING
                Log.e("CALL", "AGAIN");

                handler1.postDelayed(this, delay);
            }
        }, delay);
    }


    private void SET_SUBCATEGORY_API() {

        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(fragmentActivity);
        }

        if (!Constants.SLUG_TO_OPEN.equalsIgnoreCase("homepage")) {
            String URL = Constants.MAIN_URL + Constants.SLUG_TO_OPEN;
            Log.e("URL_CATEGORY_APIS", Constants.MAIN_URL + Constants.SLUG_TO_OPEN);
            StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("CATEGORY_APIS", "" + response.trim());
                            try {
                                arrayList_sub_heading.clear();
                                arrayList_all_post.clear();

                                JSONObject ojs = new JSONObject(response);
                                String abc = ojs.getString("status");
                                Log.e("Value", abc);
                                if (Integer.parseInt(abc) == 1) {
                                    JSONObject jsonArray = ojs.getJSONObject("get_json_data");

                                    JSONArray jsonArray1 = jsonArray.getJSONArray("cat_data");
                                    for (int i = 0; i < jsonArray1.length(); i++) {
                                        JSONObject obj = jsonArray1.getJSONObject(i);
                                        String id = obj.getString("id");
                                        String name = obj.getString("name");
                                        String name_slug = obj.getString("name_slug");

                                        arrayList_sub_heading.add(new Viewpager_items(id, name, name_slug, ""));
                                    }

                                    JSONArray jsonArray12 = jsonArray.getJSONArray("posts_data");
                                    for (int i = 0; i < jsonArray12.length(); i++) {
                                        JSONObject obj = jsonArray12.getJSONObject(i);

                                        String id = obj.getString("id");
                                        String title = obj.getString("title");
                                        String category_name = obj.getString("category_name_slug");
                                        String post_type = obj.getString("post_type");
                                        String content = obj.getString("content");
                                        String image_default;
                                        String embed_code = "";


                                        if (post_type.contentEquals("post")) {     ///Post

                                            if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                                image_default = obj.getString("image_url");
                                            } else {
                                                image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                                embed_code = "";
                                            }
                                        } else {    ////Videos
                                            if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                                image_default = obj.getString("video_image_url");
                                            } else {
                                                image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                            }
                                            embed_code = obj.getString("video_embed_code");
                                        }
                                        String created_at = obj.getString("created_at");
                                        String comment_count = obj.getString("comment_count");
                                        String hit = obj.getString("hit");

                                        arrayList_all_post.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                                        //   arrayList_all_post.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, comment_count));
                                    }

                                    if (arrayList_sub_heading.isEmpty()) {
                                        Log.e("EMPTY", "EMPTY");

                                    } else {
                                        sub_category_adapter = new Sub_category_Adapter(getActivity(), R.layout.sub_category_items, arrayList_sub_heading);
                                        recyclerview_sub_cat_india.setAdapter(sub_category_adapter);
                                        sub_category_adapter.notifyDataSetChanged();
                                    }

                                    if (arrayList_all_post.isEmpty()) {
                                        Log.e("EMPTY", "EMPTY");
                                    } else {
                                        recyclerview_adapter_news = new Recyclerview_Adapter_news(getActivity(),
                                                R.layout.recent_news_layout, arrayList_all_post);
                                        recyclerview_india_post.setAdapter(recyclerview_adapter_news);
                                        recyclerview_adapter_news.notifyDataSetChanged();
                                    }

                                } else {


                                    String error = ojs.getString("error_json");
                                    arrayList_sub_heading.clear();
                                    arrayList_all_post.clear();

                                    sub_category_adapter = new Sub_category_Adapter(getActivity(), R.layout.sub_category_items, arrayList_sub_heading);
                                    recyclerview_sub_cat_india.setAdapter(sub_category_adapter);
                                    sub_category_adapter.notifyDataSetChanged();

                                    recyclerview_adapter_news = new Recyclerview_Adapter_news(getActivity(),
                                            R.layout.recent_news_layout, arrayList_all_post);
                                    recyclerview_india_post.setAdapter(recyclerview_adapter_news);
                                    recyclerview_adapter_news.notifyDataSetChanged();


                                    //Snackbar.make(view_snack, error, Snackbar.LENGTH_SHORT).show();
                                    Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                                    Log.e("error", error);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            mSwipeRefreshLayout.setRefreshing(false);

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            mSwipeRefreshLayout.setRefreshing(false);


                            //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }) {
            };
            requestQueue.add(strRequest);
        }
    }


    @Override
    public void onRefresh() {
        try {
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                    SET_SUBCATEGORY_API();
                }
            });
        } catch (NullPointerException e) {
            Log.e("Error", "Error");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                    text_header.setText(Constants.TAG_TITLE);
                    SET_SUBCATEGORY_API();
                }
            });
        } catch (NullPointerException e) {
            Log.e("Error", "Error");
        }
    }

}
