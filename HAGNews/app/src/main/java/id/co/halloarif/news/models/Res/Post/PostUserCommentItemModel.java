package id.co.halloarif.news.models.Res.Post;

public class PostUserCommentItemModel {
	private String userReplyCount;
	private String commentPostId;
	private String commentUserImage;
	private String commentUserName;
	private String userLikeCount;
	private String commentUserId;
	private String userLikeComment;
	private String commentParentId;
	private String commentContent;
	private String commentId;
	private String commentCreatedAt;

	public void setUserReplyCount(String userReplyCount){
		this.userReplyCount = userReplyCount;
	}

	public String getUserReplyCount(){
		return userReplyCount;
	}

	public void setCommentPostId(String commentPostId){
		this.commentPostId = commentPostId;
	}

	public String getCommentPostId(){
		return commentPostId;
	}

	public void setCommentUserImage(String commentUserImage){
		this.commentUserImage = commentUserImage;
	}

	public String getCommentUserImage(){
		return commentUserImage;
	}

	public void setCommentUserName(String commentUserName){
		this.commentUserName = commentUserName;
	}

	public String getCommentUserName(){
		return commentUserName;
	}

	public void setUserLikeCount(String userLikeCount){
		this.userLikeCount = userLikeCount;
	}

	public String getUserLikeCount(){
		return userLikeCount;
	}

	public void setCommentUserId(String commentUserId){
		this.commentUserId = commentUserId;
	}

	public String getCommentUserId(){
		return commentUserId;
	}

	public void setUserLikeComment(String userLikeComment){
		this.userLikeComment = userLikeComment;
	}

	public String getUserLikeComment(){
		return userLikeComment;
	}

	public void setCommentParentId(String commentParentId){
		this.commentParentId = commentParentId;
	}

	public String getCommentParentId(){
		return commentParentId;
	}

	public void setCommentContent(String commentContent){
		this.commentContent = commentContent;
	}

	public String getCommentContent(){
		return commentContent;
	}

	public void setCommentId(String commentId){
		this.commentId = commentId;
	}

	public String getCommentId(){
		return commentId;
	}

	public void setCommentCreatedAt(String commentCreatedAt){
		this.commentCreatedAt = commentCreatedAt;
	}

	public String getCommentCreatedAt(){
		return commentCreatedAt;
	}

	@Override
 	public String toString(){
		return 
			"PostUserCommentItemModel{" +
			"user_reply_count = '" + userReplyCount + '\'' + 
			",comment_post_id = '" + commentPostId + '\'' + 
			",comment_user_image = '" + commentUserImage + '\'' + 
			",comment_user_name = '" + commentUserName + '\'' + 
			",user_like_count = '" + userLikeCount + '\'' + 
			",comment_user_id = '" + commentUserId + '\'' + 
			",user_like_comment = '" + userLikeComment + '\'' + 
			",comment_parent_id = '" + commentParentId + '\'' + 
			",comment_content = '" + commentContent + '\'' + 
			",comment_id = '" + commentId + '\'' + 
			",comment_created_at = '" + commentCreatedAt + '\'' + 
			"}";
		}
}
