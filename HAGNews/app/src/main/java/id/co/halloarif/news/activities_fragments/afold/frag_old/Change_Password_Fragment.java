package id.co.halloarif.news.activities_fragments.afold.frag_old;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afold.activity_old.MainActivity;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.utils.preference.Preference_saved;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ST_004 on 26-03-2018.
 */

public class Change_Password_Fragment extends Fragment implements View.OnClickListener {
    EditText edttxt_oldpassword, edttxt_newpassword, edttxt_confirm_password;
    Button btn_savechanges;
    ImageView Iv_back;
    FragmentActivity fragmentActivity;
    String str_oldpass, str_newpass, str_confrmpass;
    FrameLayout frameloader;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_change_pass, container, false);
        initComponent(view);

        return view;
    }

    private void initComponent(View view) {
        edttxt_oldpassword = view.findViewById(R.id.edttxt_oldpassword);
        edttxt_newpassword = view.findViewById(R.id.edttxt_newpassword);
        edttxt_confirm_password = view.findViewById(R.id.edttxt_confirm_password);
        btn_savechanges = view.findViewById(R.id.btn_savechanges);
        frameloader = view.findViewById(R.id.frameloader);
        Iv_back = view.findViewById(R.id.Iv_back);
        btn_savechanges.setOnClickListener(this);
        Iv_back.setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
    }

    @Override
    public void onClick(View v) {

        if (v == btn_savechanges) {
            if (edttxt_oldpassword.getText().toString().isEmpty()) {
                edttxt_oldpassword.setError("Enter old password");
            } else if (edttxt_newpassword.getText().toString().isEmpty()) {
                edttxt_newpassword.setError("Enter new password");
            } else if (edttxt_confirm_password.getText().toString().isEmpty()) {
                edttxt_confirm_password.setError("Enter confirm password");
            } else {
                str_oldpass = edttxt_oldpassword.getText().toString();
                str_newpass = edttxt_newpassword.getText().toString();
                str_confrmpass = edttxt_confirm_password.getText().toString();
                if (str_confrmpass.contentEquals(str_newpass)) {
                    CALL_API_CHANGE_PASS(str_oldpass, str_confrmpass);
                } else {
                    Toast.makeText(getActivity(), "Password not matched", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (v == Iv_back) {
            ((MainActivity) fragmentActivity).getSupportFragmentManager().popBackStack();
            //  ((MainActivity) fragmentActivity).SetFrameVisible(false);
        }

    }

    private void CALL_API_CHANGE_PASS(final String str_oldpass, final String str_confrmpass) {
        frameloader.setVisibility(View.VISIBLE);
        final String useremail = Preference_saved.getInstance(getActivity()).getUser_Email();

        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(getActivity());
        String URL = Constants.MAIN_URL + "change-pass";
        // String URL = Constants.MAIN_URL + "change-pass/" + useremail + "/" + str_oldpass + "/" + str_confrmpass;
        Log.e("URL_UPDATE_POST", URL);
        StringRequest strRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("URL_UPDATE_POST", "" + response.trim());
                        try {

                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                                String message = jsonObject.getString("change_pass");
                                frameloader.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                                edttxt_oldpassword.setText("");
                                edttxt_newpassword.setText("");
                                edttxt_confirm_password.setText("");

                            } else {
                                String error = ojs.getString("error_json");
                                frameloader.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        frameloader.setVisibility(View.GONE);
                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", useremail);
                params.put("old_pass", str_oldpass);
                params.put("new_pass", str_confrmpass);
                return params;
            }
        };
        requestQueue.add(strRequest);

    }
}
