package id.co.halloarif.news.activities_fragments.afnew.adapter_new;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import id.co.halloarif.news.R;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.utils.MediaPlayerSingleton;

public class AudioItemAdapter extends RecyclerView.Adapter<AudioItemAdapter.AudioItemsViewHolder> implements Handler.Callback {

    private static final int MSG_UPDATE_SEEK_BAR = 1845;

    private Handler uiUpdateHandler;

    private Activity activity;
    private List<Viewpager_items> audioItems = new ArrayList<>();
    private int playingPosition, media_length;
    private AudioItemAdapter.AudioItemsViewHolder playingHolder;

    public AudioItemAdapter(Activity activity, List<Viewpager_items> audioItems) {
        this.activity = activity;
        this.audioItems = audioItems;
        this.playingPosition = -1;
        uiUpdateHandler = new Handler(this);
    }

    @NonNull
    @Override
    public AudioItemAdapter.AudioItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AudioItemAdapter.AudioItemsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_audio_tracks, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AudioItemAdapter.AudioItemsViewHolder holder, int position) {
        Viewpager_items model = audioItems.get(position);

        if (position == playingPosition) {
            playingHolder = holder;
            Log.e("BINDVIEW", "SAME POSITION");
            // this view holder corresponds to the currently playing audio cell
            // update its view to show playing progress
            updatePlayingView();
        } else {
            Log.e("BINDVIEW", "POSITION CHANGED");
            // and this one corresponds to non playing
            updateNonPlayingView(holder);
        }

        Log.d("Lihat", "onBindViewHolder AudioItemAdapter : " + model.getAudio_name());
        Log.d("Lihat", "onBindViewHolder AudioItemAdapter : " + model.getMusician());

        holder.Tv_song_title.setText(model.getAudio_name());
        holder.Tv_author_name.setText(model.getMusician());

        //   holder.tvIndex.setText(String.format(Locale.US, "%d", position));
    }

    @Override
    public int getItemCount() {
        Log.d("Lihat", "getItemCount AudioItemAdapter : " + audioItems.size());
        return audioItems.size();
    }

    @Override
    public void onViewRecycled(@NonNull AudioItemAdapter.AudioItemsViewHolder holder) {
        super.onViewRecycled(holder);
        if (playingPosition == holder.getAdapterPosition()) {
            // view holder displaying playing audio cell is being recycled
            // change its state to non-playing
            Log.e("BINDVIEW", "POSITION Recycled");

            updateNonPlayingView(playingHolder);
            playingHolder = null;
        }
    }

    /**
     * Changes the view to non playing state
     * - icon is changed to play arrow
     * - seek bar disabled
     * - remove seek bar updater, if needed
     *
     * @param holder ViewHolder whose state is to be chagned to non playing
     */
    private void updateNonPlayingView(AudioItemAdapter.AudioItemsViewHolder holder) {
        if (holder == playingHolder) {
            uiUpdateHandler.removeMessages(MSG_UPDATE_SEEK_BAR);
        }
        Log.e("BINDVIEW", "updateNonPlayingView");
        holder.sbProgress.setEnabled(false);
        holder.Iv_now_playing.setVisibility(View.INVISIBLE);
        holder.Relative_lay_seekbar.setVisibility(View.GONE);
        holder.Iv_playpause.setImageResource(R.drawable.ic_play);
    }

    /**
     * Changes the view to playing state
     * - icon is changed to pause
     * - seek bar enabled
     * - start seek bar updater, if needed
     */
    private void updatePlayingView() {
        Log.e("BINDVIEW", "updatePlayingView");

        playingHolder.sbProgress.setMax(MediaPlayerSingleton.getInstance().getMediaPlayer().getDuration());
        playingHolder.sbProgress.setProgress(MediaPlayerSingleton.getInstance().getMediaPlayer().getCurrentPosition());
        playingHolder.Tv_progress_total.setText(milliSecondsToTimer(MediaPlayerSingleton.getInstance().getMediaPlayer().getDuration()));
        playingHolder.sbProgress.setEnabled(true);
        if (MediaPlayerSingleton.getInstance().getMediaPlayer().isPlaying()) {
            uiUpdateHandler.sendEmptyMessageDelayed(MSG_UPDATE_SEEK_BAR, 100);
            playingHolder.Iv_now_playing.setVisibility(View.VISIBLE);
            playingHolder.Relative_lay_seekbar.setVisibility(View.VISIBLE);
            playingHolder.Iv_playpause.setImageResource(R.drawable.ic_pause);
        } else {
            playingHolder.Iv_now_playing.setVisibility(View.INVISIBLE);
            playingHolder.Relative_lay_seekbar.setVisibility(View.GONE);
            uiUpdateHandler.removeMessages(MSG_UPDATE_SEEK_BAR);
            playingHolder.Iv_playpause.setImageResource(R.drawable.ic_play);
        }
    }

        /*void tO_save_state() {
            if (mediaPlayer != null) {
                mediaPlayer.pause();
                media_length = mediaPlayer.getCurrentPosition();
            }
        }

        void resume_state() {
            if (mediaPlayer != null) {
                mediaPlayer.seekTo(media_length);
                mediaPlayer.start();
            }
        }*/

    String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }
        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case MSG_UPDATE_SEEK_BAR: {
                playingHolder.sbProgress.setProgress(MediaPlayerSingleton.getInstance().getMediaPlayer().getCurrentPosition());
                playingHolder.Tv_progress_start.setText(milliSecondsToTimer(MediaPlayerSingleton.getInstance().getMediaPlayer().getCurrentPosition()));
                uiUpdateHandler.sendEmptyMessageDelayed(MSG_UPDATE_SEEK_BAR, 100);
                return true;
            }
        }
        return false;
    }

    // Interaction listeners e.g. click, seekBarChange etc are handled in the view holder itself. This eliminates
    // need for anonymous allocations.
    class AudioItemsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
        SeekBar sbProgress;
        TextView Tv_author_name, Tv_song_title, Tv_progress_start, Tv_progress_total;
        ImageView Iv_playpause, Iv_now_playing;
        RelativeLayout Relative_lay_seekbar;

        AudioItemsViewHolder(View itemView) {
            super(itemView);
            Tv_author_name = itemView.findViewById(R.id.Tv_author_name);
            Relative_lay_seekbar = itemView.findViewById(R.id.Relative_lay_seekbar);
            Tv_progress_start = itemView.findViewById(R.id.Tv_progress_start);
            Tv_progress_total = itemView.findViewById(R.id.Tv_progress_total);
            Tv_song_title = itemView.findViewById(R.id.Tv_song_title);
            Iv_now_playing = itemView.findViewById(R.id.Iv_now_playing);
            sbProgress = itemView.findViewById(R.id.sbProgress);
            Iv_playpause = itemView.findViewById(R.id.Iv_playpause);

            Iv_now_playing.setVisibility(View.INVISIBLE);
            Relative_lay_seekbar.setVisibility(View.GONE);

            Iv_playpause.setOnClickListener(this);
            sbProgress.setOnSeekBarChangeListener(this);

        }

        @Override
        public void onClick(View v) {
            EventBus.getDefault().post("Play_true");
            if (getAdapterPosition() == playingPosition) {
                // toggle between play/pause of audio
                if (MediaPlayerSingleton.getInstance().getMediaPlayer() != null) {
                    if (MediaPlayerSingleton.getInstance().getMediaPlayer().isPlaying()) {
                        MediaPlayerSingleton.getInstance().getMediaPlayer().pause();

                    } else {
                        MediaPlayerSingleton.getInstance().getMediaPlayer().start();
                    }
                } else {
                    playingPosition = getAdapterPosition();
                    if (MediaPlayerSingleton.getInstance().getMediaPlayer() != null) {
                        if (null != playingHolder) {
                            updateNonPlayingView(playingHolder);
                        }
                        // MediaPlayerSingleton.getInstance().getMediaPlayer().release();
                    }
                    playingHolder = this;
                    startMediaPlayer(audioItems.get(playingPosition).getAudio_path());
                }

            } else {
                // start another audio playback
                playingPosition = getAdapterPosition();
                if (MediaPlayerSingleton.getInstance().getMediaPlayer() != null) {
                    if (null != playingHolder) {
                        updateNonPlayingView(playingHolder);
                    }
                    //    MediaPlayerSingleton.getInstance().getMediaPlayer().release();
                }
                playingHolder = this;
                startMediaPlayer(audioItems.get(playingPosition).getAudio_path());

                //radioManager.playOrPause(audioItems.get(playingPosition).getAudio_path());
            }
            updatePlayingView();
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                if (MediaPlayerSingleton.getInstance().getMediaPlayer() != null)
                    media_length = progress;
                MediaPlayerSingleton.getInstance().getMediaPlayer().seekTo(progress);
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    }

    private void startMediaPlayer(String audioResId) {
        try {
            if (MediaPlayerSingleton.getInstance().getMediaPlayer() != null) {
                Log.e("MEDIA_PLAYER", "NOT NULL");
            } else {
                Log.e("MEDIA_PLAYER", "NULL");
                MediaPlayerSingleton.getInstance().getMediaPlayer();
            }
            Log.e("AUDIO_RESOURCE", "" + audioResId);
            MediaPlayerSingleton.getInstance().getMediaPlayer().reset();
            MediaPlayerSingleton.getInstance().getMediaPlayer().setDataSource(audioResId);
            MediaPlayerSingleton.getInstance().getMediaPlayer().setAudioStreamType(AudioManager.STREAM_MUSIC);
            MediaPlayerSingleton.getInstance().getMediaPlayer().prepare();
            MediaPlayerSingleton.getInstance().getMediaPlayer().start();

            MediaPlayerSingleton.getInstance().getMediaPlayer().setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    Log.d("Lihat", "onPrepared AudioItemAdapter : " + "PREPARE");
                }
            });
            MediaPlayerSingleton.getInstance().getMediaPlayer().setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    //  MediaPlayerSingleton.getInstance().getMediaPlayer().release();
                    Log.d("Lihat", "onCompletion AudioItemAdapter : " + "CALLED");
                    if (playingPosition < (audioItems.size() - 1) && playingPosition != -1) {
                        Log.d("Lihat", "onCompletion AudioItemAdapter : " + "NEXT");

                        playingPosition = playingPosition + 1;

                        // start another audio playback
                        if (MediaPlayerSingleton.getInstance().getMediaPlayer() != null) {
                            if (null != playingHolder) {
                                updateNonPlayingView(playingHolder);
                            }
                            // MediaPlayerSingleton.getInstance().getMediaPlayer().release();
                        }
                        notifyDataSetChanged();
                        startMediaPlayer(audioItems.get(playingPosition).getAudio_path());
                        //  updatePlayingView();

                    } else {
                        Log.d("Lihat", "onCompletion AudioItemAdapter : " + "END");
                        releaseMediaPlayer();
                        Intent intent = new Intent("KEY");
                        activity.sendBroadcast(intent);
                    }
                }
            });
            playingHolder.sbProgress.setProgress(0);
            playingHolder.sbProgress.setMax(100);

        } catch (Exception e) {
            Log.d("EXCEPTION", "initMedia_error", e);
        }
    }

    private void releaseMediaPlayer() {
        if (null != playingHolder) {
            updateNonPlayingView(playingHolder);
        }
        MediaPlayerSingleton.getInstance().getMediaPlayer().reset();
        playingPosition = -1;
    }

    public void setData(List<Viewpager_items> datas) {
        audioItems = datas;
        notifyDataSetChanged();
    }

    public void replaceData(List<Viewpager_items> datas) {
        audioItems.clear();
        audioItems.addAll(datas);
        notifyDataSetChanged();
    }

    public void addDatas(List<Viewpager_items> datas) {
        audioItems.addAll(datas);
        notifyItemRangeInserted(audioItems.size(), datas.size());
    }

    public void addDataFirst(Viewpager_items data) {
        int position = 0;
        audioItems.add(position, data);
        notifyItemInserted(position);
    }

    public void removeAt(int position) {
        audioItems.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, audioItems.size());
    }

    public void removeDataFirst() {
        int position = 0;
        audioItems.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, audioItems.size());
    }
}
