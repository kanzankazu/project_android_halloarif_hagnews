package id.co.halloarif.news.models.Res.Category;

public class CatMainModel {
    public CatDataMainModel get_json_data;
    public Integer status;
    public String error_json;

    public CatDataMainModel getGet_json_data() {
        return get_json_data;
    }

    public void setGet_json_data(CatDataMainModel get_json_data) {
        this.get_json_data = get_json_data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getError_json() {
        return error_json;
    }

    public void setError_json(String error_json) {
        this.error_json = error_json;
    }
}
