package id.co.halloarif.news.models.Res.ReadList;

import java.util.ArrayList;
import java.util.List;

public class ReadListMainModel {
    public List<ReadListDataMainModel> get_json_data = new ArrayList<>();
    public String error;
    public Integer status;

    public List<ReadListDataMainModel> getGet_json_data() {
        return get_json_data;
    }

    public void setGet_json_data(List<ReadListDataMainModel> get_json_data) {
        this.get_json_data = get_json_data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
