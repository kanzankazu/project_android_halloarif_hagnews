package id.co.halloarif.news.activities_fragments.afold.activity_old.logreg;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;

import id.co.halloarif.news.R;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.utils.cropimage.CropImage;
import id.co.halloarif.news.utils.cropimage.InternalStorageContentProvider;
import id.co.halloarif.news.utils.preference.Preference_saved;
import id.co.halloarif.news.utils.CircleImageView;
import id.co.halloarif.news.utils.Volley_multipart.MySingleton;
import id.co.halloarif.news.utils.Volley_multipart.VolleyMultipartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.bumptech.glide.request.RequestOptions.centerCropTransform;

/**
 * Created by ST_004 on 23-03-2018.
 */

public class Update_Profile_Activity extends AppCompatActivity implements View.OnClickListener {
    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    public static final int REQUEST_CODE_GALLERY = 0x1;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    public static final int REQUEST_CODE_CROP_IMAGE = 0x3;
    public static final String TAG = "Profile_Setting";
    ImageView ivProfileActionBarBackfvbi;
    Button bProfileChangePhotofvbi, bProfileSubmitfvbi;
    CircleImageView civProfileUserfvbi;
    EditText etProfileUsernamefvbi, etProfileEmailfvbi;
    Bitmap bitmap;
    String encodedImage;
    byte[] imageBytes;
    String image_path;
    private File mFileTemp;
    private String str_path = null;
    private String str_email, str_username;
    public static final int RequestPermissionCode = 1;

    public static void copyStream(InputStream input, OutputStream output) throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // requestWindowFeature(Window.FEATURE_NO_TITLE);
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.fragment_updateprofile);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
        }

    private void initComponent() {
        ivProfileActionBarBackfvbi = (ImageView) findViewById(R.id.ivProfileActionBarBack);
        civProfileUserfvbi =(CircleImageView) findViewById(R.id.civProfileUser);
        bProfileChangePhotofvbi =(Button) findViewById(R.id.bProfileChangePhoto);
        etProfileEmailfvbi =(EditText) findViewById(R.id.etProfileEmail);
        etProfileUsernamefvbi =(EditText) findViewById(R.id.etProfileUsername);
        bProfileSubmitfvbi =(Button) findViewById(R.id.bProfileSubmit);
    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {
        String image = Preference_saved.getInstance(getApplicationContext()).getuserPicture();
        String username = Preference_saved.getInstance(getApplicationContext()).getUsername();
        String useremail = Preference_saved.getInstance(getApplicationContext()).getUser_Email();
        if (image.contentEquals("")) {
            civProfileUserfvbi.setVisibility(View.VISIBLE);
            int image_array = R.drawable.ic_person;

            Glide.with(getApplicationContext()).load(image_array).apply(centerCropTransform().placeholder(R.drawable.ic_person)).into(civProfileUserfvbi);
            etProfileUsernamefvbi.setText(username);
            etProfileEmailfvbi.setText(useremail);
        } else {
            civProfileUserfvbi.setVisibility(View.VISIBLE);
            Glide.with(getApplicationContext()).load(Constants.IMAGE_URL + image).apply(centerCropTransform().placeholder(R.drawable.ic_person)).into(civProfileUserfvbi);
            etProfileUsernamefvbi.setText(username);
            etProfileEmailfvbi.setText(useremail);
        }

        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
            System.out.println("---path 3v---" + mFileTemp.getPath());
            str_path = mFileTemp.getPath();
        } else {
            mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
            System.out.println("---path 3---" + mFileTemp.getPath());
            str_path = mFileTemp.getPath();
        }
    }

    private void initListener() {
        etProfileEmailfvbi.setEnabled(false);
        etProfileEmailfvbi.setClickable(false);
        ivProfileActionBarBackfvbi.setOnClickListener(this);
        bProfileChangePhotofvbi.setOnClickListener(this);
        bProfileSubmitfvbi.setOnClickListener(this);
    }

    private void startCropImage() {
        Intent intent = new Intent(getApplicationContext(), CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 3);
        intent.putExtra(CropImage.ASPECT_Y, 3);

        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        imageBytes = baos.toByteArray();

        Log.e("imageBytes", "" + imageBytes);

        encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        Log.e("encodedImage", encodedImage);
        return encodedImage;
    }

    public boolean checkPermission() {

        int result = ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(this, CAMERA);
        int read_result1 = ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && read_result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE, CAMERA, READ_EXTERNAL_STORAGE}, RequestPermissionCode);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {

            case REQUEST_CODE_GALLERY:
                try {

                    InputStream inputStream = getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                    copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();

                    startCropImage();

                } catch (Exception e) {

                    Log.e(TAG, "Error while creating temp file", e);
                }

                break;
            case REQUEST_CODE_TAKE_PICTURE:

                startCropImage();
                break;
            case REQUEST_CODE_CROP_IMAGE:

                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                if (path == null) {

                    return;
                }
                bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());

                image_path = getStringImage(bitmap);

                civProfileUserfvbi.setImageBitmap(bitmap);

                System.out.println("---path 3---" + path);
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean camaraPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean readPermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    if (StoragePermission && camaraPermission && readPermission) {
                        // Toast.makeText(this, "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        //  Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        if (v == ivProfileActionBarBackfvbi) {
            onBackPressed();
            // ((MainActivity) fragmentActivity).getSupportFragmentManager().popBackStack();
            // ((MainActivity) fragmentActivity).SetFrameVisible(false);
        } else if (v == bProfileChangePhotofvbi) {
            if (checkPermission()) {
                camera();
                Log.e("PERMISSION", "CHECK");
            } else {
                requestPermission();
            }
        } else if (v == bProfileSubmitfvbi) {
            str_email = etProfileEmailfvbi.getText().toString();
            str_username = etProfileUsernamefvbi.getText().toString();
            if (str_username.isEmpty()) {
                etProfileUsernamefvbi.setError("Enter username");
            } else {
                CAll_UPDATE_API(str_email, str_username);
            }
        }
    }

    private void camera() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.YourDialogStyle).setIcon(R.drawable.ic_add_photo).setTitle("Add Photo!");

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    try {
                        Uri mImageCaptureUri = null;
                        String state = Environment.getExternalStorageState();
                        if (Environment.MEDIA_MOUNTED.equals(state)) {
                            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                                mImageCaptureUri = FileProvider.getUriForFile(getApplicationContext(), getResources().getString(R.string.provider), mFileTemp);
                            } else {
                                mImageCaptureUri = Uri.fromFile(mFileTemp);
                            }
                        } else {
                            mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
                        }
                        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                                mImageCaptureUri);
                        intent.putExtra("return-data", true);
                        startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
                    } catch (ActivityNotFoundException e) {

                        Log.d(TAG, "cannot take picture", e);
                    }
                } else if (items[item].equals("Choose from Library")) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void CAll_UPDATE_API(final String str_email, final String str_username) {
        String URL = Constants.MAIN_URL + "user-profile";
        Log.e("URL_UPDATE_POST", URL);
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, URL, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    Log.e("URL_UPDATE_POST", "" + resultResponse);
                    JSONObject ojs = new JSONObject(resultResponse);
                    String abc = ojs.getString("status");
                    Log.e("Value", abc);
                    if (Integer.parseInt(abc) == 1) {
                        JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                        JSONObject jsonObject1 = jsonObject.getJSONObject("change_user_profile");
                        String message = jsonObject1.getString("title_success");
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        JSONObject jsonObject2 = jsonObject1.getJSONObject("user_profile");

                        String id = jsonObject2.getString("id");
                        String username = jsonObject2.getString("username");
                        String email = jsonObject2.getString("email");
                        String avatar = jsonObject2.getString("avatar");

                        Preference_saved.getInstance(getApplicationContext()).setUsername(username);
                        Preference_saved.getInstance(getApplicationContext()).setUser_Email(email);
                        Preference_saved.getInstance(getApplicationContext()).setuserPicture(avatar);
                        Preference_saved.getInstance(getApplicationContext()).setUser_id(id);

                    } else {
                        Toast.makeText(getApplicationContext(), "Incorrect Data", Toast.LENGTH_SHORT).show();
                        //  Log.e("error", error);*//**//*
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");
                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", str_email);
                params.put("username", str_username);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                if (imageBytes != null) {
                    params.put("image_name", new DataPart("temp_photo.jpg", imageBytes, "image/jpeg"));
                } else {

                }
                //params.put("image_name", new VolleyMultipartRequest.DataPart("temp_photo.jpg",imageBytes, "image/jpeg"));
                return params;
            }
        };
        MySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);
    }

}
