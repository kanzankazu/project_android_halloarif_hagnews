package id.co.halloarif.news.services;

public class PlaybackStatus {

    static final String IDLE = "PlaybackStatus_IDLE";

    public static final String LOADING = "PlaybackStatus_LOADING";

    public static final String PLAYING = "PlaybackStatus_PLAYING";

    static final String PAUSED = "PlaybackStatus_PAUSED";

    static final String STOPPED = "PlaybackStatus_STOPPED";

    public static final String ERROR = "PlaybackStatus_ERROR";


    public static String RADIO_NAME = "";

}