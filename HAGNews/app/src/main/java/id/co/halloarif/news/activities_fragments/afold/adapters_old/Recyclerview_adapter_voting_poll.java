package id.co.halloarif.news.activities_fragments.afold.adapters_old;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afold.activity_old.logreg.Login_Activity;
import id.co.halloarif.news.activities_fragments.afold.activity_old.logreg.Register_Activity;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.utils.preference.Preference_saved;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ST_004 on 09-02-2018.
 */

public class Recyclerview_adapter_voting_poll extends RecyclerView.Adapter<Recyclerview_adapter_voting_poll.ViewHolder> implements Filterable {
    private PopupWindow popupWindow_selct, popupWindow_result;
    private String vote_id, option_selected;
    private ProgressBar progressBar;
    float[] yData;
    private AppCompatSeekBar seekbar1, seekbar2, seekbar3, seekbar4, seekbar5, seekbar6, seekbar7, seekbar8, seekbar9, seekbar10;
    private TextView txt_vote1, txt_vote2, txt_vote3, txt_vote4, txt_vote5, txt_vote6,
            txt_vote7, txt_vote8, txt_vote9, txt_vote10;
    private Context activity;
    private Viewpager_items objAllBean;
    private ArrayList<Viewpager_items> mFilteredList;
    private int row;
    private String[] mMonth = new String[]{
            "option1", "option2", "option3", "option4", "option5", "option6",
            "option7", "option8", "option9", "option10"
    };

    public Recyclerview_adapter_voting_poll(Context act, int resource, ArrayList<Viewpager_items> arrayList) {
        activity = act;
        row = resource;
        mFilteredList = arrayList;
        //itemsAllphotos = arrayList;
        //arraycat = new ArrayList<ItemAllVideos>();
        //this.arraycat.addAll(itemsAllpho+``tos);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view;
        view = LayoutInflater.from(viewGroup.getContext()).inflate(row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        objAllBean = mFilteredList.get(i);

        viewHolder.txt_question.setText(objAllBean.getQuestion());
      /*  if (objAllBean.getOption1() != null) {
            viewHolder.radioButton_answer_1.setText(objAllBean.getOption1());
        } else {
        }*/

        vote_id = objAllBean.getId();
        Log.e("VOTE_ID", vote_id);
        if (objAllBean.getOption1().isEmpty()) {
            viewHolder.radioButton_answer_1.setVisibility(View.GONE);
        } else {
            viewHolder.radioButton_answer_1.setText(objAllBean.getOption1());
        }

        if (objAllBean.getOption2().isEmpty()) {
            viewHolder.radioButton_answer_2.setVisibility(View.GONE);
        } else {
            viewHolder.radioButton_answer_2.setText(objAllBean.getOption2());
        }

        if (objAllBean.getOption3().isEmpty()) {
            viewHolder.radioButton_answer_3.setVisibility(View.GONE);
        } else {
            viewHolder.radioButton_answer_3.setText(objAllBean.getOption3());
        }

        if (objAllBean.getOption4().isEmpty()) {
            viewHolder.radioButton_answer_4.setVisibility(View.GONE);
        } else {
            viewHolder.radioButton_answer_4.setText(objAllBean.getOption4());
        }

        if (objAllBean.getOption5().isEmpty()) {
            viewHolder.radioButton_answer_5.setVisibility(View.GONE);
        } else {
            viewHolder.radioButton_answer_5.setText(objAllBean.getOption5());
        }


        if (objAllBean.getOption6().isEmpty()) {
            viewHolder.radioButton_answer_6.setVisibility(View.GONE);
        } else {
            viewHolder.radioButton_answer_6.setText(objAllBean.getOption6());
        }

        if (objAllBean.getOption7().isEmpty()) {
            viewHolder.radioButton_answer_7.setVisibility(View.GONE);
        } else {
            viewHolder.radioButton_answer_7.setText(objAllBean.getOption7());
        }


        if (objAllBean.getOption8().isEmpty()) {
            viewHolder.radioButton_answer_8.setVisibility(View.GONE);
        } else {
            viewHolder.radioButton_answer_8.setText(objAllBean.getOption8());
        }


        if (objAllBean.getOption9().isEmpty()) {
            viewHolder.radioButton_answer_9.setVisibility(View.GONE);
        } else {
            viewHolder.radioButton_answer_9.setText(objAllBean.getOption9());
        }

        if (objAllBean.getOption10().isEmpty()) {
            viewHolder.radioButton_answer_10.setVisibility(View.GONE);
        } else {
            viewHolder.radioButton_answer_10.setText(objAllBean.getOption10());
        }


        viewHolder.text_view_result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("POSITION", "" + i + "CHECK VOTE" + mFilteredList.get(i).getId());
                String check = Preference_saved.getInstance(activity).check_login();
                Log.d("checklogin", check);
                if (check != null) {
                    if (check.contentEquals("true")) {
                        initiatePopupWindow_view_Result(v, mFilteredList, i);
                    } else {
                        initiatePopupWindow_SelectActionPlaylist(v);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    // mFilteredList = mArrayList;
                } else {
                    ArrayList<Viewpager_items> filteredList = new ArrayList<>();

                    /* for (Viewpager_items androidVersion : mArrayList) {

                     *//* if (androidVersion.getTitle().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }*//*
                    }*/

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Viewpager_items>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    private void initiatePopupWindow_SelectActionPlaylist(final View view) {
        try {
// We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.popup_select_action_login, (ViewGroup) view.findViewById(R.id.popup));

            RelativeLayout mainlayout = layout.findViewById(R.id.popup);

            mainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  popupWindow_selct.dismiss();
                }
            });
            ImageView close_dialog = layout.findViewById(R.id.ivPopForgetPassClose);
            close_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow_selct.dismiss();
                }
            });


            Button Btn_delete_playlist = layout.findViewById(R.id.Btn_login);
            Btn_delete_playlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow_selct.dismiss();
                    Intent i = new Intent(activity, Login_Activity.class);
                    activity.startActivity(i);
                    // initiatePopupWindow_deletePlaylist(playlist_id, popupWindow_selct);
                }
            });

            Button Btn_edit_playlist = layout.findViewById(R.id.Btn_regiter);
            Btn_edit_playlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow_selct.dismiss();
                    Intent i = new Intent(activity, Register_Activity.class);
                    activity.startActivity(i);
                }
            });
            popupWindow_selct = new PopupWindow(layout, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT, true);
            popupWindow_selct.showAtLocation(layout, Gravity.CENTER, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Call_api_View_Result(String vote_id) {

        String id = Preference_saved.getInstance(activity).getUser_id();
        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(activity);
        String URL = Constants.MAIN_URL + "voting/" + id + "/" + vote_id;
        Log.e("URL_VOTE", URL);
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("URL_VOTE", "" + response.trim());
                        try {

                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                                String option1a, option2a, option3a, option4a, option5a, option6a,
                                        option7a, option8a, option9a, option10a;
                                option1a = (jsonObject.getString("option1_yes"));
                                option2a = (jsonObject.getString("option2_no"));
                                option3a = (jsonObject.getString("option3_yes"));
                                option4a = (jsonObject.getString("option4_no"));
                                option5a = (jsonObject.getString("option5_yes"));
                                option6a = (jsonObject.getString("option6_no"));
                                option7a = (jsonObject.getString("option7_yes"));
                                option8a = (jsonObject.getString("option8_no"));
                                option9a = (jsonObject.getString("option9_yes"));
                                option10a = (jsonObject.getString("option10_no"));


                                JSONObject jsonObject1 = jsonObject.getJSONObject("voting_parseint");

                                String option1, option2, option3, option4, option5, option6, option7, option8, option9, option10;
                                option1 = (jsonObject1.getString("option1_yes"));
                                option1 = RemoveChar(option1);
                                option2 = (jsonObject1.getString("option2_no"));
                                option2 = RemoveChar(option2);
                                option3 = (jsonObject1.getString("option3_yes"));
                                option3 = RemoveChar(option3);
                                option4 = (jsonObject1.getString("option4_no"));
                                option4 = RemoveChar(option4);
                                option5 = (jsonObject1.getString("option5_yes"));
                                option5 = RemoveChar(option5);
                                option6 = (jsonObject1.getString("option6_no"));
                                option6 = RemoveChar(option6);
                                option7 = (jsonObject1.getString("option7_yes"));
                                option7 = RemoveChar(option7);
                                option8 = (jsonObject1.getString("option8_no"));
                                option8 = RemoveChar(option8);
                                option9 = (jsonObject1.getString("option9_yes"));
                                option9 = RemoveChar(option9);
                                option10 = (jsonObject1.getString("option10_no"));
                                option10 = RemoveChar(option10);


                                txt_vote1.setText(option1 + "% " + "Vote : " + option1a);
                                txt_vote2.setText(option2 + "% " + "Vote : " + option2a);
                                txt_vote3.setText(option3 + "% " + "Vote : " + option3a);
                                txt_vote4.setText(option4 + "% " + "Vote : " + option4a);
                                txt_vote5.setText(option5 + "% " + "Vote : " + option5a);
                                txt_vote6.setText(option6 + "% " + "Vote : " + option6a);
                                txt_vote7.setText(option7 + "% " + "Vote : " + option7a);
                                txt_vote8.setText(option8 + "% " + "Vote : " + option8a);
                                txt_vote9.setText(option9 + "% " + "Vote : " + option9a);
                                txt_vote10.setText(option10 + "% " + "Vote : " + option10a);


                                seekbar1.setProgress(Integer.parseInt(option1));
                                seekbar2.setProgress(Integer.parseInt(option2));
                                seekbar3.setProgress(Integer.parseInt(option3));
                                seekbar4.setProgress(Integer.parseInt(option4));
                                seekbar5.setProgress(Integer.parseInt(option5));
                                seekbar6.setProgress(Integer.parseInt(option6));
                                seekbar7.setProgress(Integer.parseInt(option7));
                                seekbar8.setProgress(Integer.parseInt(option8));
                                seekbar9.setProgress(Integer.parseInt(option9));
                                seekbar10.setProgress(Integer.parseInt(option10));

                                progressBar.setVisibility(View.GONE);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);
    }

    private String RemoveChar(String option) {
        String rephrase = null;
        if (option != null && option.length() > 1) {
            rephrase = option.substring(0, option.length() - 1);
        }
        return rephrase;
    }

    private void Call_api_voting() {
        final String id = Preference_saved.getInstance(activity).getUser_id();
        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(activity);
        String URL = Constants.MAIN_URL + "add_voting";

        //  String URL = Constants.MAIN_URL + "add_voting/" + id + "/" + vote_id + "/" + option_selected;
        Log.e("URL_VOTE", URL);
        StringRequest strRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("URL_VOTE", "" + response.trim());
                        try {
                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonObject = ojs.getJSONObject("get_json_data");
                                String title = jsonObject.getString("title");
                                //Snackbar.make(activity, title, Snackbar.LENGTH_SHORT).show();
                                Toast.makeText(activity, title, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", id);
                params.put("poll_id", vote_id);
                params.put("voting_option", option_selected);
                return params;
            }
        };
        requestQueue.add(strRequest);
    }


    private void initiatePopupWindow_view_Result(final View view, ArrayList<Viewpager_items> list, int position) {
        try {

            Viewpager_items objAllBean1 = list.get(position);
// We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.popup_poll_result, (ViewGroup) view.findViewById(R.id.popup));

            RelativeLayout mainlayout = layout.findViewById(R.id.popup);

            mainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  popupWindow_selct.dismiss();
                }
            });
            TextView txt_option1, txt_option2, txt_option3, txt_option4, txt_option5,
                    txt_option6, txt_option7, txt_option8, txt_option9, txt_option10;

            progressBar = layout.findViewById(R.id.progressBar);

            txt_option1 = layout.findViewById(R.id.txt_option1);
            txt_option2 = layout.findViewById(R.id.txt_option2);
            txt_option3 = layout.findViewById(R.id.txt_option3);
            txt_option4 = layout.findViewById(R.id.txt_option4);
            txt_option5 = layout.findViewById(R.id.txt_option5);
            txt_option6 = layout.findViewById(R.id.txt_option6);
            txt_option7 = layout.findViewById(R.id.txt_option7);
            txt_option8 = layout.findViewById(R.id.txt_option8);
            txt_option9 = layout.findViewById(R.id.txt_option9);
            txt_option10 = layout.findViewById(R.id.txt_option10);


            txt_vote1 = layout.findViewById(R.id.txt_vote1);
            txt_vote2 = layout.findViewById(R.id.txt_vote2);
            txt_vote3 = layout.findViewById(R.id.txt_vote3);
            txt_vote4 = layout.findViewById(R.id.txt_vote4);
            txt_vote5 = layout.findViewById(R.id.txt_vote5);
            txt_vote6 = layout.findViewById(R.id.txt_vote6);
            txt_vote7 = layout.findViewById(R.id.txt_vote7);
            txt_vote8 = layout.findViewById(R.id.txt_vote8);
            txt_vote9 = layout.findViewById(R.id.txt_vote9);
            txt_vote10 = layout.findViewById(R.id.txt_vote10);

            seekbar1 = layout.findViewById(R.id.seekbar1);
            seekbar2 = layout.findViewById(R.id.seekbar2);
            seekbar3 = layout.findViewById(R.id.seekbar3);
            seekbar4 = layout.findViewById(R.id.seekbar4);
            seekbar5 = layout.findViewById(R.id.seekbar5);
            seekbar6 = layout.findViewById(R.id.seekbar6);
            seekbar7 = layout.findViewById(R.id.seekbar7);
            seekbar8 = layout.findViewById(R.id.seekbar8);
            seekbar9 = layout.findViewById(R.id.seekbar9);
            seekbar10 = layout.findViewById(R.id.seekbar10);
            seekbar1.setEnabled(false);
            seekbar2.setEnabled(false);
            seekbar3.setEnabled(false);
            seekbar4.setEnabled(false);
            seekbar5.setEnabled(false);
            seekbar6.setEnabled(false);
            seekbar7.setEnabled(false);
            seekbar8.setEnabled(false);
            seekbar9.setEnabled(false);
            seekbar10.setEnabled(false);

            if (objAllBean1.getOption1().isEmpty()) {
                txt_option1.setVisibility(View.GONE);
                txt_vote1.setVisibility(View.GONE);
                seekbar1.setVisibility(View.GONE);
            } else {
                txt_option1.setText(objAllBean1.getOption1());
                seekbar1.setVisibility(View.VISIBLE);
            }

            if (objAllBean1.getOption2().isEmpty()) {
                txt_option2.setVisibility(View.GONE);
                seekbar2.setVisibility(View.GONE);
                txt_vote2.setVisibility(View.GONE);

            } else {
                seekbar2.setVisibility(View.VISIBLE);
                txt_option2.setText(objAllBean1.getOption2());
            }

            if (objAllBean1.getOption3().isEmpty()) {
                txt_option3.setVisibility(View.GONE);
                seekbar3.setVisibility(View.GONE);
                txt_vote3.setVisibility(View.GONE);

            } else {
                seekbar3.setVisibility(View.VISIBLE);
                txt_option3.setText(objAllBean1.getOption3());
            }

            if (objAllBean1.getOption4().isEmpty()) {
                txt_option4.setVisibility(View.GONE);
                seekbar4.setVisibility(View.GONE);
                txt_vote4.setVisibility(View.GONE);
            } else {
                seekbar4.setVisibility(View.VISIBLE);
                txt_option4.setText(objAllBean1.getOption4());
            }

            if (objAllBean1.getOption5().isEmpty()) {
                txt_option5.setVisibility(View.GONE);
                seekbar5.setVisibility(View.GONE);
                txt_vote5.setVisibility(View.GONE);
            } else {
                seekbar5.setVisibility(View.VISIBLE);
                txt_option5.setText(objAllBean1.getOption5());
            }

            if (objAllBean1.getOption6().isEmpty()) {
                txt_option6.setVisibility(View.GONE);
                seekbar6.setVisibility(View.GONE);
                txt_vote6.setVisibility(View.GONE);
            } else {
                seekbar6.setVisibility(View.VISIBLE);
                txt_option6.setText(objAllBean1.getOption6());
            }

            if (objAllBean1.getOption7().isEmpty()) {
                seekbar7.setVisibility(View.GONE);
                txt_option7.setVisibility(View.GONE);
                txt_vote7.setVisibility(View.GONE);
            } else {
                seekbar7.setVisibility(View.VISIBLE);
                txt_option7.setText(objAllBean1.getOption7());
            }


            if (objAllBean1.getOption8().isEmpty()) {
                seekbar8.setVisibility(View.GONE);
                txt_option8.setVisibility(View.GONE);
                txt_vote8.setVisibility(View.GONE);
            } else {
                seekbar8.setVisibility(View.VISIBLE);
                txt_option8.setText(objAllBean1.getOption8());
            }


            if (objAllBean1.getOption9().isEmpty()) {
                txt_option9.setVisibility(View.GONE);
                seekbar9.setVisibility(View.GONE);
                txt_vote9.setVisibility(View.GONE);
            } else {
                seekbar9.setVisibility(View.VISIBLE);
                txt_option9.setText(objAllBean1.getOption9());
            }

            if (objAllBean1.getOption10().isEmpty()) {
                txt_option10.setVisibility(View.GONE);
                seekbar10.setVisibility(View.GONE);
                txt_vote10.setVisibility(View.GONE);
            } else {
                seekbar10.setVisibility(View.VISIBLE);
                txt_option10.setText(objAllBean1.getOption10());
            }


            Call_api_View_Result(objAllBean1.getId());

            ImageView close_dialog = layout.findViewById(R.id.ivPopForgetPassClose);
            close_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow_result.dismiss();
                }
            });
            popupWindow_result = new PopupWindow(layout, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT, true);
            popupWindow_result.showAtLocation(layout, Gravity.CENTER, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private SpannableString generateCenterSpannableText() {
        SpannableString s = new SpannableString("Results");
        s.setSpan(new RelativeSizeSpan(1.7f), 0, 7, 0);
        return s;
    }

 /*   private void setData(int count, float range, Chart chart) {

        //float[] yData = {33, 0, 67, 0, 0, 0, 0};
        String[] xData = {"Sony", "Huawei", "LG", "Apple", "Samsung", "Vivo", "Micromax"};

        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        //   for (int i = 0; i < yData.length; i++)
        //     entries.add(new PieEntry(yData[i], i));

        ArrayList<String> xVals = new ArrayList<String>();

        //  for (int i = 0; i < xData.length; i++)
        //      xVals.add(xData[i]);


        for (int i = 0; i < count; i++) {
            entries.add(new PieEntry(yData[i % yData.length],
                    xData[i % xData.length]
            ));
        }


        // create pie data set
        PieDataSet dataSet = new PieDataSet(entries, "Results");
        dataSet.setSliceSpace(3f);
       // dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add many colors
        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);


        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        chart.setData(data);

        // undo all highlights
        chart.highlightValues(null);

        chart.invalidate();


    }
*/

    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("JAN");
        xAxis.add("FEB");
        xAxis.add("MAR");
        xAxis.add("APR");
        xAxis.add("MAY");
        xAxis.add("JUN");
        return xAxis;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txt, txt_question, text_view_result;
        RadioButton radioButton_answer_1, radioButton_answer_2, radioButton_answer_3, radioButton_answer_4,
                radioButton_answer_5, radioButton_answer_6, radioButton_answer_7, radioButton_answer_8,
                radioButton_answer_9, radioButton_answer_10;
        Button btn_vote;
        RadioGroup radio_group, radioGroup2, radioGroup3, radioGroup4, radioGroup5;
        SeekBar seekbar1;

        ViewHolder(View view) {
            super(view);
            txt_question = (TextView) view.findViewById(R.id.txt_question);
            text_view_result = view.findViewById(R.id.text_view_result);
            radioButton_answer_1 = view.findViewById(R.id.radioButton_answer_1);
            radioButton_answer_2 = view.findViewById(R.id.radioButton_answer_2);
            radioButton_answer_3 = view.findViewById(R.id.radioButton_answer_3);
            radioButton_answer_4 = view.findViewById(R.id.radioButton_answer_4);
            radioButton_answer_5 = view.findViewById(R.id.radioButton_answer_5);
            radioButton_answer_6 = view.findViewById(R.id.radioButton_answer_6);
            radioButton_answer_7 = view.findViewById(R.id.radioButton_answer_7);
            radioButton_answer_8 = view.findViewById(R.id.radioButton_answer_8);
            radioButton_answer_9 = view.findViewById(R.id.radioButton_answer_9);
            radioButton_answer_10 = view.findViewById(R.id.radioButton_answer_10);
            seekbar1 = view.findViewById(R.id.seekbar1);
            //  seekBar.getThumb().mutate().setAlpha(0);
            btn_vote = view.findViewById(R.id.btn_vote);
            radio_group = view.findViewById(R.id.radio_group);

            radioButton_answer_1.setOnClickListener(this);
            radioButton_answer_2.setOnClickListener(this);
            radioButton_answer_3.setOnClickListener(this);
            radioButton_answer_4.setOnClickListener(this);
            radioButton_answer_5.setOnClickListener(this);
            radioButton_answer_6.setOnClickListener(this);
            radioButton_answer_7.setOnClickListener(this);
            radioButton_answer_8.setOnClickListener(this);
            radioButton_answer_9.setOnClickListener(this);
            radioButton_answer_10.setOnClickListener(this);
            btn_vote.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {

            if (v == btn_vote) {
                String check = Preference_saved.getInstance(activity).check_login();
                Log.d("checklogin", check);
                if (check != null) {
                    if (check.contentEquals("true")) {
                        // Call_api_voting();
                        if (radioButton_answer_1.isChecked()) {
                            option_selected = "option1";
                            Log.e("CHeck", "checkbox1");
                            Call_api_voting();
                        } else if (radioButton_answer_2.isChecked()) {
                            Log.e("CHeck", "checkbox2");
                            option_selected = "option2";
                            Call_api_voting();
                        } else if (radioButton_answer_3.isChecked()) {
                            Log.e("CHeck", "checkbox3");
                            option_selected = "option3";
                            Call_api_voting();
                        } else if (radioButton_answer_4.isChecked()) {
                            Log.e("CHeck", "checkbox4");
                            option_selected = "option4";
                            Call_api_voting();
                        } else if (radioButton_answer_5.isChecked()) {
                            Log.e("CHeck", "checkbox5");
                            option_selected = "option5";
                            Call_api_voting();
                        } else if (radioButton_answer_6.isChecked()) {
                            Log.e("CHeck", "checkbox6");
                            option_selected = "option6";
                            Call_api_voting();
                        } else if (radioButton_answer_7.isChecked()) {
                            Log.e("CHeck", "checkbox7");
                            option_selected = "option7";
                            Call_api_voting();
                        } else if (radioButton_answer_8.isChecked()) {
                            Log.e("CHeck", "checkbox8");
                            option_selected = "option8";
                            Call_api_voting();
                        } else if (radioButton_answer_9.isChecked()) {
                            Log.e("CHeck", "checkbox9");
                            option_selected = "option9";
                            Call_api_voting();
                        } else if (radioButton_answer_10.isChecked()) {
                            Log.e("CHeck", "checkbox10");
                            option_selected = "option10";
                            Call_api_voting();
                        } else {
                            Toast.makeText(activity, "select any option", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        initiatePopupWindow_SelectActionPlaylist(v);
                    }
                } else {

                }
            } else if (v == radioButton_answer_1) {
                radioButton_answer_1.setChecked(true);
                radioButton_answer_2.setChecked(false);
                radioButton_answer_3.setChecked(false);
                radioButton_answer_4.setChecked(false);
                radioButton_answer_5.setChecked(false);
                radioButton_answer_6.setChecked(false);
                radioButton_answer_7.setChecked(false);
                radioButton_answer_8.setChecked(false);
                radioButton_answer_9.setChecked(false);
                radioButton_answer_10.setChecked(false);
            } else if (v == radioButton_answer_2) {
                radioButton_answer_1.setChecked(false);
                radioButton_answer_2.setChecked(true);
                radioButton_answer_3.setChecked(false);
                radioButton_answer_4.setChecked(false);
                radioButton_answer_5.setChecked(false);
                radioButton_answer_6.setChecked(false);
                radioButton_answer_7.setChecked(false);
                radioButton_answer_8.setChecked(false);
                radioButton_answer_9.setChecked(false);
                radioButton_answer_10.setChecked(false);

            } else if (v == radioButton_answer_3) {
                radioButton_answer_1.setChecked(false);
                radioButton_answer_2.setChecked(false);
                radioButton_answer_3.setChecked(true);
                radioButton_answer_4.setChecked(false);
                radioButton_answer_5.setChecked(false);
                radioButton_answer_6.setChecked(false);
                radioButton_answer_7.setChecked(false);
                radioButton_answer_8.setChecked(false);
                radioButton_answer_9.setChecked(false);
                radioButton_answer_10.setChecked(false);
            } else if (v == radioButton_answer_4) {
                radioButton_answer_1.setChecked(false);
                radioButton_answer_2.setChecked(false);
                radioButton_answer_3.setChecked(false);
                radioButton_answer_4.setChecked(true);
                radioButton_answer_5.setChecked(false);
                radioButton_answer_6.setChecked(false);
                radioButton_answer_7.setChecked(false);
                radioButton_answer_8.setChecked(false);
                radioButton_answer_9.setChecked(false);
                radioButton_answer_10.setChecked(false);
            } else if (v == radioButton_answer_5) {
                radioButton_answer_1.setChecked(false);
                radioButton_answer_2.setChecked(false);
                radioButton_answer_3.setChecked(false);
                radioButton_answer_4.setChecked(false);
                radioButton_answer_5.setChecked(true);
                radioButton_answer_6.setChecked(false);
                radioButton_answer_7.setChecked(false);
                radioButton_answer_8.setChecked(false);
                radioButton_answer_9.setChecked(false);
                radioButton_answer_10.setChecked(false);
            } else if (v == radioButton_answer_6) {
                radioButton_answer_1.setChecked(false);
                radioButton_answer_2.setChecked(false);
                radioButton_answer_3.setChecked(false);
                radioButton_answer_4.setChecked(false);
                radioButton_answer_5.setChecked(false);
                radioButton_answer_6.setChecked(true);
                radioButton_answer_7.setChecked(false);
                radioButton_answer_8.setChecked(false);
                radioButton_answer_9.setChecked(false);
                radioButton_answer_10.setChecked(false);
            } else if (v == radioButton_answer_7) {
                radioButton_answer_1.setChecked(false);
                radioButton_answer_2.setChecked(false);
                radioButton_answer_3.setChecked(false);
                radioButton_answer_4.setChecked(false);
                radioButton_answer_5.setChecked(false);
                radioButton_answer_6.setChecked(false);
                radioButton_answer_7.setChecked(true);
                radioButton_answer_8.setChecked(false);
                radioButton_answer_9.setChecked(false);
                radioButton_answer_10.setChecked(false);
            } else if (v == radioButton_answer_8) {
                radioButton_answer_1.setChecked(false);
                radioButton_answer_2.setChecked(false);
                radioButton_answer_3.setChecked(false);
                radioButton_answer_4.setChecked(false);
                radioButton_answer_5.setChecked(false);
                radioButton_answer_6.setChecked(false);
                radioButton_answer_7.setChecked(false);
                radioButton_answer_8.setChecked(true);
                radioButton_answer_9.setChecked(false);
                radioButton_answer_10.setChecked(false);
            } else if (v == radioButton_answer_9) {
                radioButton_answer_1.setChecked(false);
                radioButton_answer_2.setChecked(false);
                radioButton_answer_3.setChecked(false);
                radioButton_answer_4.setChecked(false);
                radioButton_answer_5.setChecked(false);
                radioButton_answer_6.setChecked(false);
                radioButton_answer_7.setChecked(false);
                radioButton_answer_8.setChecked(false);
                radioButton_answer_9.setChecked(true);
                radioButton_answer_10.setChecked(false);
            } else if (v == radioButton_answer_10) {
                radioButton_answer_1.setChecked(false);
                radioButton_answer_2.setChecked(false);
                radioButton_answer_3.setChecked(false);
                radioButton_answer_4.setChecked(false);
                radioButton_answer_5.setChecked(false);
                radioButton_answer_6.setChecked(false);
                radioButton_answer_7.setChecked(false);
                radioButton_answer_8.setChecked(false);
                radioButton_answer_9.setChecked(false);
                radioButton_answer_10.setChecked(true);
            }

        }
    }

    public void setData(ArrayList<Viewpager_items> datas) {
        mFilteredList = datas;
        notifyDataSetChanged();

    }

    public void replaceData(List<Viewpager_items> datas) {
        mFilteredList.clear();
        mFilteredList.addAll(datas);
        notifyDataSetChanged();
    }

    public void addDatas(List<Viewpager_items> datas) {
        mFilteredList.addAll(datas);
        notifyItemRangeInserted(mFilteredList.size(), datas.size());
    }

    public void addDataFirst(Viewpager_items data) {
        int position = 0;
        mFilteredList.add(position, data);
        notifyItemInserted(position);
    }

    public void removeAt(int position) {
        mFilteredList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mFilteredList.size());
    }

    public void removeDataFirst() {
        int position = 0;
        mFilteredList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mFilteredList.size());
    }


}