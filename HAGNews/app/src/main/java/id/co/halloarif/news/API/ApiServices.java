package id.co.halloarif.news.API;

import id.co.halloarif.news.models.Req.ChangePassReqModel;
import id.co.halloarif.news.models.Req.LoginReqModel;
import id.co.halloarif.news.models.Req.RegReqModel;
import id.co.halloarif.news.models.Res.ChangePass.ChangePassMainModel;
import id.co.halloarif.news.models.Res.HomePage.HomepageMainModel;
import id.co.halloarif.news.models.Res.Profile.UserProfileMainModel;
import id.co.halloarif.news.models.Res.User.UserMainModel;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiServices {

    /*@POST("mail")
    Call<ReportRespondModel> Report(@Body ReportRequestModel model);

    @POST("appupdate")
    Call<UpdateRespondModel> Update(@Body UpdateRequestModel model);

    @GET("oui")
    Call<OuiResponseModel> Oui();

    @POST("uploadoui")
    Call<OuiUploadResponseModel> uploadOui(@Body OuiUploadRequestModel model);

    @GET("thing")
    Call<OuiResponseModel> OuiThing(@Query("param1") String param1, @Query("param2") String param2);

    @GET("thing")
    Call<OuiResponseModel> OuiThing(@QueryMap Map<String, String> paramsMap);

    @GET("{id}")
    Call<List<OuiResponseModel>> groupList(@Path("id") int id, @Query("format") String format);

    @GET("{id}")
    Call<List<OuiResponseModel>> groupList(@Path("id") int id, @QueryMap Map<String, String> format);

    @GET("group/{id}/users")
    Call<List<OuiResponseModel>> groupList2(@Path("id") int groupId);

    @GET("group/{id}/users")
    Call<List<OuiResponseModel>> groupList2(@Path("id") int groupId, @QueryMap Map<String, String> options);

    @FormUrlEncoded
    @POST("user/edit")
    Call<OuiResponseModel> updateUser(@Field("first_name") String first, @Field("last_name") String last);

    @Multipart
    @PUT("user/photo")
    Call<OuiResponseModel> updateUser(@Part("photo") RequestBody photo, @Part("description") RequestBody description);

    @GET("user")
    Call<OuiResponseModel> getUser1(@Header("Authorization") String authorization);

    @GET("user")
    Call<OuiResponseModel> getUser1(@HeaderMap Map<String, String> headers);

    @Headers({
            "Accept: application/vnd.github.v3.full+json",
            "User-Agent: Retrofit-Sample-App"
    })
    @GET("users/{username}")
    Call<OuiResponseModel> getUser2(@Path("username") String username);

    @Headers("Cache-Control: max-age=640000")
    @GET("widget/list")
    Call<List<OuiResponseModel>> widgetList();*/

    @POST("login")
    Call<UserMainModel> Login(@Body LoginReqModel model);

    @POST("register")
    Call<UserMainModel> Register(@Body RegReqModel model);

    @POST("change-pass")
    Call<ChangePassMainModel> ChangePass(@Body ChangePassReqModel model);

    //@POST("forgetpass") Call<ReportRespondModel> Report(@Body ReportRequestModel model);
    //@POST("resetpass") Call<ReportRespondModel> Report(@Body ReportRequestModel model);
    @GET("homepage")
    Call<HomepageMainModel> HomePage();

    //@POST("top-header") Call<ReportRespondModel> Report(@Body ReportRequestModel model);
    //@POST("video") Call<ReportRespondModel> Report(@Body ReportRequestModel model);
    //@POST("audio") Call<ReportRespondModel> Report(@Body ReportRequestModel model);
    //@POST("gallery") Call<ReportRespondModel> Report(@Body ReportRequestModel model);
    //@POST("tags_slug") Call<ReportRespondModel> Report(@Body ReportRequestModel model);
    //@POST("post") Call<ReportRespondModel> Report(@Body ReportRequestModel model);
    //@POST("add_post_comment") Call<ReportRespondModel> Report(@Body ReportRequestModel model);
    //@POST("post_comment") Call<ReportRespondModel> Report(@Body ReportRequestModel model);
    @Multipart
    @POST("user-profile")
    Call<UserProfileMainModel> UserProfile(@Part("file\"; filename=\"pp.png\" ") RequestBody file, @Part("FirstName") RequestBody fname, @Part("Id") RequestBody id);
    //@POST("add_voting") Call<ReportRespondModel> Report(@Body ReportRequestModel model);
    //@POST("voting") Call<ReportRespondModel> Report(@Body ReportRequestModel model);
    //@POST("reading-list") Call<ReportRespondModel> Report(@Body ReportRequestModel model);
    //@POST("add-remove-reading-list") Call<ReportRespondModel> Report(@Body ReportRequestModel model);
    //@POST("add_remove_like_comment_like_comment") Call<ReportRespondModel> Report(@Body ReportRequestModel model);
    //@POST("delete_comment") Call<ReportRespondModel> Report(@Body ReportRequestModel model);
}
