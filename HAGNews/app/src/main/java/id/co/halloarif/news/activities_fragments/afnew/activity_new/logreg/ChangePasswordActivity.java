package id.co.halloarif.news.activities_fragments.afnew.activity_new.logreg;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.HashMap;
import java.util.Map;

import id.co.halloarif.news.ISeasonConfig;
import id.co.halloarif.news.R;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Res.ChangePass.ChangePassDataItemModel;
import id.co.halloarif.news.models.Res.ChangePass.ChangePassMainModel;
import id.co.halloarif.news.support.SessionUtil;

public class ChangePasswordActivity extends AppCompatActivity {
    EditText edttxt_oldpassword, edttxt_newpassword, edttxt_confirm_password;
    Button btn_savechanges;
    ImageView Iv_back;
    FragmentActivity fragmentActivity;
    String str_oldpass, str_newpass, str_confrmpass;
    FrameLayout frameloader;

    private String userId;
    private String userEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        edttxt_oldpassword = findViewById(R.id.edttxt_oldpassword);
        edttxt_newpassword = findViewById(R.id.edttxt_newpassword);
        edttxt_confirm_password = findViewById(R.id.edttxt_confirm_password);
        btn_savechanges = findViewById(R.id.btn_savechanges);
        frameloader = findViewById(R.id.frameloader);
        Iv_back = findViewById(R.id.Iv_back);
    }

    private void initParam() {

    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_ID)) {
            userId = SessionUtil.getStringPreferences(ISeasonConfig.KEY_USER_ID, null);
        }
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_EMAIL)) {
            userEmail = SessionUtil.getStringPreferences(ISeasonConfig.KEY_USER_EMAIL, null);
        }
    }

    private void initContent() {

    }

    private void initListener() {
        btn_savechanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edttxt_oldpassword.getText().toString().isEmpty()) {
                    edttxt_oldpassword.setError("Enter old password");
                } else if (edttxt_newpassword.getText().toString().isEmpty()) {
                    edttxt_newpassword.setError("Enter new password");
                } else if (edttxt_confirm_password.getText().toString().isEmpty()) {
                    edttxt_confirm_password.setError("Enter confirm password");
                } else {
                    str_oldpass = edttxt_oldpassword.getText().toString();
                    str_newpass = edttxt_newpassword.getText().toString();
                    str_confrmpass = edttxt_confirm_password.getText().toString();
                    if (str_confrmpass.contentEquals(str_newpass)) {
                        CALL_API_CHANGE_PASS(str_oldpass, str_confrmpass);
                    } else {
                        Toast.makeText(ChangePasswordActivity.this, "Password not matched", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        Iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void CALL_API_CHANGE_PASS(final String str_oldpass, final String str_confrmpass) {
        frameloader.setVisibility(View.VISIBLE);

        String URL = Constants.MAIN_URL + "change-pass";

        RequestQueue requestQueue = Volley.newRequestQueue(ChangePasswordActivity.this);
        StringRequest strRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                frameloader.setVisibility(View.GONE);

                JsonParser parser = new JsonParser();
                JsonElement mJson = parser.parse(response);
                Gson gson = new Gson();
                ChangePassMainModel mainModel = gson.fromJson(mJson, ChangePassMainModel.class);

                Integer abc = mainModel.getStatus();
                if (abc == 1) {
                    ChangePassDataItemModel dataMainModel = mainModel.getGet_json_data();

                    String message = dataMainModel.getChange_pass();
                    Toast.makeText(ChangePasswordActivity.this, message, Toast.LENGTH_SHORT).show();

                    edttxt_oldpassword.setText("");
                    edttxt_newpassword.setText("");
                    edttxt_confirm_password.setText("");

                    finish();
                } else {
                    String error = mainModel.getError_json();
                    Toast.makeText(ChangePasswordActivity.this, error, Toast.LENGTH_SHORT).show();
                    Log.e("error", error);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                frameloader.setVisibility(View.GONE);
                Toast.makeText(ChangePasswordActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", userEmail);
                params.put("old_pass", str_oldpass);
                params.put("new_pass", str_confrmpass);
                return params;
            }
        };
        requestQueue.add(strRequest);
    }
}
