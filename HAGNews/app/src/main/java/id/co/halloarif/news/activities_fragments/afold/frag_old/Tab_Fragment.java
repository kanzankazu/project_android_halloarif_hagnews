package id.co.halloarif.news.activities_fragments.afold.frag_old;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.SectionsPagerAdapter;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Viewpager_items;

public class Tab_Fragment extends Fragment {

    FragmentActivity fragmentActivity;
    View view1;
    ViewPager viewPager;
    TabLayout tabLayout;
    ArrayList<Viewpager_items> arrayList_viewpager = new ArrayList<>();
    FrameLayout Frame_loader_main_screen;
    //Fragment List
    //Title List
    private final List<String> mFragmentTitleList = new ArrayList<>();
    SectionsPagerAdapter mSectionsPagerAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view1 = inflater.inflate(R.layout.fragment_tab_layout, container, false);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        initComponent(view1);
        return view1;
    }

    private void initComponent(View view1) {
        Frame_loader_main_screen = view1.findViewById(R.id.Frame_loader_main_screen);
        viewPager = view1.findViewById(R.id.tabanim_viewpager);
        tabLayout = view1.findViewById(R.id.tabanim_tabs);

        try {
            fragmentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SET_TAB_LAYOUT_ITEMS_NEW();
                    // Stuff that updates the UI

                }
            });
        } catch (Exception e) {
            Log.e("EXCEPTION", "" + e);
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.e("Positionstatescroll", "" + position + "offset" + positionOffset + "offpixel" + positionOffsetPixels);
            /*    if (!CHECK_FRAGMENT) {
                    CHECK_FRAGMENT = true;
                    assert (viewPager.getAdapter()) != null;
                    Fragment fragment = ((SectionsPagerAdapter) viewPager.getAdapter()).getItem(position);
                    Log.e("Positionslug_FIRST", ">>> Position_Fragment >>" + "" + fragment + " " + " Position>>> " + position);
                    String slug = arrayList_viewpager.get(position).getName_slug();
                    String tag_title = arrayList_viewpager.get(position).getPager_title();
                    if (fragment != null) {
                        Constants.TAG_TITLE = tag_title;
                        Constants.SLUG_TO_OPEN = slug;
                        fragment.onResume();
                    }
                }*/
            }

            @Override
            public void onPageSelected(int position) {
                assert (viewPager.getAdapter()) != null;
                Fragment fragment = ((SectionsPagerAdapter) viewPager.getAdapter()).getItem(position);
                Log.e("Positionslug", ">>> Position_Fragment >>" + "" + fragment + " " + " Position>>> " + position);
                String slug = arrayList_viewpager.get(position).getName_slug();
                String tag_title = arrayList_viewpager.get(position).getPager_title();
                if (fragment != null) {
                    Constants.TAG_TITLE = tag_title;
                    Constants.SLUG_TO_OPEN = slug;
                    fragment.onResume();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.e("Positionstate", "" + state);
            }
        });

    }

    private void SET_TAB_LAYOUT_ITEMS_NEW() {
        Frame_loader_main_screen.setVisibility(View.VISIBLE);

        RequestQueue queue = Volley.newRequestQueue(fragmentActivity);
        String URL = Constants.MAIN_URL + "top-header";
        Log.e("URL_TAB", Constants.MAIN_URL + "top-header");

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String title;
                String check_from;
                try {
                    mFragmentTitleList.clear();
                    arrayList_viewpager.clear();

                    JSONObject ojs = new JSONObject(response);
                    String abc = ojs.getString("status");
                    if (Integer.parseInt(abc) == 1) {
                        JSONArray jsonArray = ojs.getJSONArray("get_json_data");
                        arrayList_viewpager.add(new Viewpager_items("0", "HomePage", "homepage", "category", ""));
                        mFragmentTitleList.add("Home Page");
                        Log.d("test", String.valueOf(mFragmentTitleList));

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);
                            String id = obj.getString("id");
                            title = obj.getString("title");
                            String name_slug = obj.getString("slug");
                            check_from = obj.getString("page_type");

                            arrayList_viewpager.add(new Viewpager_items(id, title, name_slug, check_from, ""));
                            mFragmentTitleList.add(title);

                            Log.d("test", String.valueOf(mFragmentTitleList));
                        }

                        //create multiple titles, but use OneFragment() for every new tab
                        for (int i = 0; i < mFragmentTitleList.size(); i++) {

                            Log.e("FRAG_ADDED", "" + mFragmentTitleList.get(i));
                            if (mFragmentTitleList.get(i).equalsIgnoreCase("Home Page")) {
                                //  mFragmentList.add(new HomeFragment());
                                mSectionsPagerAdapter.addFragment(new HomeFragment(), mFragmentTitleList.get(i), i);
                            } else if (mFragmentTitleList.get(i).equalsIgnoreCase("Gallery")) {
                                // mFragmentList.add(new Gallery_Fragment());
                                mSectionsPagerAdapter.addFragment(new Gallery_Fragment(), mFragmentTitleList.get(i), i);
                            } else if (mFragmentTitleList.get(i).equalsIgnoreCase("video")) {
                                //mFragmentList.add(new Videos_Fragment());
                                mSectionsPagerAdapter.addFragment(new Videos_Fragment(), mFragmentTitleList.get(i), i);
                            } else {
                                mSectionsPagerAdapter.addFragment(new IndiaFragment(), mFragmentTitleList.get(i), i);
                                // mFragmentList.add(new IndiaFragment());
                            }
                        }

                        Log.e("COUNT", "" + mSectionsPagerAdapter.getCount());

                        if (mFragmentTitleList.isEmpty()) {
                            Frame_loader_main_screen.setVisibility(View.GONE);
                        } else {
                            Frame_loader_main_screen.setVisibility(View.GONE);

                            viewPager.setAdapter(mSectionsPagerAdapter);
                            // viewPager.setOffscreenPageLimit(3);
                            viewPager.setCurrentItem(0);
                            // adapter = new ViewPagerAdapter(getChildFragmentManager(), mFragmentList, mFragmentTitleList);
                            //  viewPager.setAdapter(adapter);
                            //   viewPager.setCurrentItem(0);
                            //   viewPager.setOffscreenPageLimit(mFragmentList.size());
                            tabLayout.setupWithViewPager(viewPager);
                            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

                        }
                    } else {
                        Log.e("data value", "0");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                int duration = Toast.LENGTH_SHORT;
                Toast.makeText(fragmentActivity, "Try Again", duration).show();
            }
        });
        queue.add(stringRequest);
    }

}
