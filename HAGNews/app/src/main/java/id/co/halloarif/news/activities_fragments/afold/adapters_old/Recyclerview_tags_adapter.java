package id.co.halloarif.news.activities_fragments.afold.adapters_old;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.co.halloarif.news.R;
import id.co.halloarif.news.models.Viewpager_items;

/**
 * Created by ST_004 on 09-02-2018.
 */

public class Recyclerview_tags_adapter extends RecyclerView.Adapter<Recyclerview_tags_adapter.ViewHolder> {
    private Context activity;
    private Viewpager_items objAllBean;
    private ArrayList<Viewpager_items> mArrayList = new ArrayList<>();

    public Recyclerview_tags_adapter(Context act, ArrayList<Viewpager_items> arrayList) {
        activity = act;
        mArrayList = arrayList;
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sub_category_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        objAllBean = mArrayList.get(i);
        String id = objAllBean.getId();
        String tag = objAllBean.getTag();
        String tag_slug = objAllBean.getTag_slug();
        String created_at = objAllBean.getCreated_at();

        viewHolder.txt.setText(objAllBean.getTag());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt;

        ViewHolder(View view) {
            super(view);
            txt = view.findViewById(R.id.title);
        }
    }

    public void setData(ArrayList<Viewpager_items> datas) {
        mArrayList = datas;
        notifyDataSetChanged();
    }

    public void replaceData(List<Viewpager_items> datas) {
        mArrayList.clear();
        mArrayList.addAll(datas);
        notifyDataSetChanged();
    }

    public void addDatas(List<Viewpager_items> datas) {
        mArrayList.addAll(datas);
        notifyItemRangeInserted(mArrayList.size(), datas.size());
    }

    public void addDataFirst(Viewpager_items data) {
        int position = 0;
        mArrayList.add(position, data);
        notifyItemInserted(position);
    }

    public void removeAt(int position) {
        mArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mArrayList.size());
    }

    public void removeDataFirst() {
        int position = 0;
        mArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mArrayList.size());
    }

}