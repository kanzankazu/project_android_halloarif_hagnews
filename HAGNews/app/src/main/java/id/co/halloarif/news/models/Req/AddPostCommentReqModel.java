package id.co.halloarif.news.models.Req;

public class AddPostCommentReqModel {
    private String parent_id;
    private String post_id;
    private String user_id;
    private String comment;

    public AddPostCommentReqModel(String parent_id, String post_id, String user_id, String comment) {
        this.parent_id = parent_id;
        this.post_id = post_id;
        this.user_id = user_id;
        this.comment = comment;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
