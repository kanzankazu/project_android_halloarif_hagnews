package id.co.halloarif.news.support;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ServiceUtil {

    public static void startService(Activity activity, Class<?> targetClass) {
        activity.startService(new Intent(activity, targetClass));
    }

    public static void stopSevice(Activity activity, Class<?> targetClass) {
        activity.stopService(new Intent(activity, targetClass));
    }

    public static boolean isMyServiceRunning(Activity activity, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.d("Lihat", "isMyServiceRunning ServiceUtil : " + "service running");
                return true;
            }
        }
        Log.d("Lihat", "isMyServiceRunning ServiceUtil : " + "service not running");
        return false;
    }

    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.d("Lihat", "isMyServiceRunning ServiceUtil : " + "service running");
                return true;
            }
        }
        Log.d("Lihat", "isMyServiceRunning ServiceUtil : " + "service not running");
        return false;
    }
}
