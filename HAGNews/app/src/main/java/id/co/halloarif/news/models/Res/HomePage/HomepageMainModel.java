package id.co.halloarif.news.models.Res.HomePage;

public class HomepageMainModel {

    public HomepageDataModel get_json_data;
    public String homes_tag;
    public String error_json;
    public Integer status;

    public String getError_json() {
        return error_json;
    }

    public void setError_json(String error_json) {
        this.error_json = error_json;
    }

    public HomepageDataModel getGet_json_data() {
        return get_json_data;
    }

    public void setGet_json_data(HomepageDataModel get_json_data) {
        this.get_json_data = get_json_data;
    }

    public String getHomes_tag() {
        return homes_tag;
    }

    public void setHomes_tag(String homes_tag) {
        this.homes_tag = homes_tag;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
