package id.co.halloarif.news.activities_fragments.afnew.frag_new;

import android.app.ProgressDialog;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mzelzoghbi.zgallery.ZGallery;
import com.mzelzoghbi.zgallery.adapters.GridImagesAdapter;
import com.mzelzoghbi.zgallery.entities.ZColor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.co.halloarif.news.ISeasonConfig;
import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_Adapter_news;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Sub_category_Adapter;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Res.GallerySub.GallerySubDataMainModel;
import id.co.halloarif.news.models.Res.GallerySub.GallerySubMainModel;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.support.SessionUtil;
import id.co.halloarif.news.utils.Recycleritemclicklistener;

import static id.co.halloarif.news.support.SystemUtil.dpToPx;

/**
 * A simple {@link Fragment} subclass.
 */
public class GalleryNewsFragment extends Fragment {

    RecyclerView recyclerview_sub_cat_india;
    RecyclerView recyclerview_india_post;
    LinearLayoutManager linearLayoutManager;
    ArrayList<Viewpager_items> arrayList_sub_heading = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_all_post = new ArrayList<>();
    ArrayList<String> list = new ArrayList<>();
    SwipeRefreshLayout mSwipeRefreshLayout;
    TextView text_header;
    FragmentActivity fragmentActivity;
    View view1, view_snack;

    Sub_category_Adapter sub_category_adapter;
    Recyclerview_Adapter_news recyclerview_adapter_news;

    private GridImagesAdapter adapter;

    private String userId;
    private Boolean loggedin;
    private String subCatId;

    public GalleryNewsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view1 = inflater.inflate(R.layout.fragment_gallery_news, container, false);

        initComponent(view1);
        initParam();
        initSession();
        initContent();
        initListener();

        return view1;
    }

    private void initComponent(View view) {
        text_header = view.findViewById(R.id.text_header);
        recyclerview_india_post = view.findViewById(R.id.recyclerview_india_post);
        recyclerview_sub_cat_india = view.findViewById(R.id.recyclerview_sub_cat_india);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
    }

    private void initParam() {

    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_ID)) {
            userId = SessionUtil.getStringPreferences(ISeasonConfig.KEY_USER_ID, null);
        }
        if (SessionUtil.checkIfExist(ISeasonConfig.LOGGEDIN)) {
            loggedin = SessionUtil.getBoolPreferences(ISeasonConfig.LOGGEDIN, false);
        }
    }

    private void initContent() {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerview_adapter_news = new Recyclerview_Adapter_news(getActivity(), R.layout.gallery_item_layout, arrayList_all_post);
        recyclerview_india_post.setAdapter(recyclerview_adapter_news);
        recyclerview_india_post.setLayoutManager(mLayoutManager);
        recyclerview_india_post.addItemDecoration(new GridSpacingItemDecoration(3, dpToPx(getActivity(), 1), true));
        recyclerview_india_post.setItemAnimator(new DefaultItemAnimator());

        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        sub_category_adapter = new Sub_category_Adapter(getActivity(), R.layout.sub_category_items, arrayList_sub_heading);
        recyclerview_sub_cat_india.setAdapter(sub_category_adapter);
        recyclerview_sub_cat_india.setLayoutManager(linearLayoutManager);
        recyclerview_sub_cat_india.setItemAnimator(new DefaultItemAnimator());

        // SwipeRefreshLayout
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_blue_dark);

        Call_API_Gallery(false);
    }

    private void initListener() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                if (TextUtils.isEmpty(subCatId)) {
                    Call_API_Gallery(false);
                } else {
                    Call_API_Gallery(true);
                }
            }
        });
        recyclerview_sub_cat_india.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                // Constants.Cat_Name = Constants.SLUG_TO_OPEN;
                Viewpager_items model = arrayList_sub_heading.get(position);
                /*Constants.Cat_Tag_Name = model.getName_sub();
                Constants.Tag_id = model.getPager_id();
                ((MainActivity) fragmentActivity).CreateFragment(new Subcategory_Gallery_Fragment());*/

                if (TextUtils.isEmpty(model.getPager_id())) {
                    subCatId = "";
                    Call_API_Gallery(false);
                } else {
                    subCatId = model.getPager_id();
                    Call_API_Gallery(true);
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        recyclerview_india_post.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ZGallery.with(getActivity(), list)
                        .setToolbarTitleColor(ZColor.WHITE)
                        .setGalleryBackgroundColor(ZColor.black_little_dark_trans)
                        .selectedImagePosition(position)
                        .setToolbarColorResId(R.color.colorPrimary)
                        .setTitle("Gallery")
                        .show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    private void Call_API_Gallery(boolean isSub) {
        String URL;

        if (isSub) {
            ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "Loading...", "Please Wait..", false, false);

            RequestQueue requestQueue;
            requestQueue = Volley.newRequestQueue(getActivity());

            URL = Constants.MAIN_URL + "gallery/" + subCatId;
            Log.d("Lihat", "Call_API_Gallery GalleryNewsFragment : " + Constants.MAIN_URL + "gallery/" + subCatId);
            StringRequest strRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    progressDialog.dismiss();

                    Log.e("URL_GALLERY", "" + response.trim());
                    arrayList_all_post.clear();
                    list.clear();

                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(response);
                    Gson gson = new Gson();
                    GallerySubMainModel ojs = gson.fromJson(mJson, GallerySubMainModel.class);

                    Integer abc = ojs.getStatus();
                    if (abc == 1) {
                        List<GallerySubDataMainModel> jsonArray = ojs.getGet_json_data();

                        for (int i = 0; i < jsonArray.size(); i++) {
                            GallerySubDataMainModel obj = jsonArray.get(i);

                            String id = obj.getId();
                            String title = obj.getTitle();
                            String image_default = Constants.IMAGE_URL + obj.getPath_big();
                            String created_at = obj.getCreated_at();
                            // String category_name = obj.getString("category_name_slug");
                            /// String post_type = obj.getString("post_type");
                            // String comment_count = obj.getString("comment_count");
                            // String hit = obj.getString("hit");

                            arrayList_all_post.add(new Viewpager_items(id, title, image_default, "", "", created_at, "", ""));
                        }

                        if (!arrayList_all_post.isEmpty()) {
                            for (int i = 0; i < arrayList_all_post.size(); i++) {
                                list.add(arrayList_all_post.get(i).getImage_default());
                            }
                            recyclerview_adapter_news.setData(arrayList_all_post);
                        }
                    } else {
                        String error_json = ojs.getError_json();
                        Toast.makeText(getActivity(), error_json, Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    progressDialog.dismiss();

                    //  Toast.makeText(getactivity(), error.toString(), Toast.LENGTH_SHORT).show();
                }
            }) {
            };
            requestQueue.add(strRequest);
        } else {
            RequestQueue requestQueue;
            requestQueue = Volley.newRequestQueue(getActivity());

            URL = Constants.MAIN_URL + "gallery/";
            Log.d("Lihat", "Call_API_Gallery GalleryNewsFragment : " + Constants.MAIN_URL + "gallery/");
            StringRequest strRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("URL_GALLERY", "" + response.trim());
                    try {
                        arrayList_sub_heading.clear();
                        arrayList_all_post.clear();
                        list.clear();
                        JSONObject ojs = new JSONObject(response);
                        String abc = ojs.getString("status");
                        Log.e("Value", abc);
                        if (Integer.parseInt(abc) == 1) {
                            JSONObject jsonArray = ojs.getJSONObject("get_json_data");

                            JSONArray jsonArray1 = jsonArray.getJSONArray("cat_data");
                            for (int i = 0; i < jsonArray1.length(); i++) {
                                JSONObject obj = jsonArray1.getJSONObject(i);
                                String id = obj.getString("id");
                                String name = obj.getString("name");
                                // String name_slug = obj.getString("name_slug");

                                arrayList_sub_heading.add(new Viewpager_items(id, name, "", ""));
                            }

                            JSONArray jsonArray12 = jsonArray.getJSONArray("posts_data");
                            for (int i = 0; i < jsonArray12.length(); i++) {
                                JSONObject obj = jsonArray12.getJSONObject(i);

                                String id = obj.getString("id");
                                String title = obj.getString("title");
                                String image_default = Constants.IMAGE_URL + obj.getString("path_big");
                                String created_at = obj.getString("created_at");
                                // String category_name = obj.getString("category_name_slug");
                                /// String post_type = obj.getString("post_type");
                                // String comment_count = obj.getString("comment_count");
                                // String hit = obj.getString("hit");

                                arrayList_all_post.add(new Viewpager_items(id, title, image_default, "", "", created_at, "", ""));
                            }

                            if (!arrayList_sub_heading.isEmpty()) {
                                sub_category_adapter.setData(arrayList_sub_heading);
                            }

                            if (!arrayList_all_post.isEmpty()) {
                                for (int i = 0; i < arrayList_all_post.size(); i++) {
                                    list.add(arrayList_all_post.get(i).getImage_default());
                                }
                                recyclerview_adapter_news.setData(arrayList_all_post);
                            }
                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mSwipeRefreshLayout.setRefreshing(false);

                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            mSwipeRefreshLayout.setRefreshing(false);

                            //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }) {
            };
            requestQueue.add(strRequest);
        }
    }

    class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;

        private boolean includeEdge;

        GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }
}
