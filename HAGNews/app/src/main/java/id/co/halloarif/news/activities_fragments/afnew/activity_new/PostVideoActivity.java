package id.co.halloarif.news.activities_fragments.afnew.activity_new;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import id.co.halloarif.news.IConfig;
import id.co.halloarif.news.ISeasonConfig;
import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afold.activity_old.logreg.Login_Activity;
import id.co.halloarif.news.activities_fragments.afold.activity_old.logreg.Register_Activity;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recycler_Comments_adapter;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.support.SessionUtil;
import id.co.halloarif.news.utils.MediaPlayerSingleton;
import id.co.halloarif.news.utils.Volley_multipart.MySingleton;
import id.co.halloarif.news.utils.Volley_multipart.VolleyMultipartRequest;
import id.co.halloarif.news.utils.YoutubeDeveloperKey;

public class PostVideoActivity extends YouTubeBaseActivity implements View.OnClickListener, YouTubePlayer.OnInitializedListener {

    private static final int RECOVERY_DIALOG_REQUEST = 1;
    public TextView Tv_video_name;
    public TextView Tv_video_views;
    public TextView txtview_video_content;
    public TextView txtview_title;
    public TextView Tv_no_commments;
    public TextView txt_No_internet_connection;
    public TextView text_header_username;
    public ImageView fab;
    RecyclerView recyclerview_comments;
    Recycler_Comments_adapter recycler_comments_adapter;
    ArrayList<Viewpager_items> arrayList_comments = new ArrayList<>();
    NavigationView mNavigationView;
    ArrayList<Viewpager_items> arrayList_viewpager = new ArrayList<>();
    YouTubePlayer youTubePlayer;
    YouTubePlayerFragment youTubePlayerFragment;
    String video_id1;
    String str_title_slug;
    ImageView Iv_share_video;
    ImageView Iv_Add_favorite;
    FrameLayout Frame_loader;
    PopupWindow popupWindow_selct;
    ImageButton IB_submit_comment;
    String txt_comment;
    String str_share_facebook;
    String str_share_twitter;
    String str_share_plus_google;
    String str_share_linkedin;
    String str_share_tumblr;
    String str_share_pinterest;
    EditText edttxt_text_comment;
    ViewGroup Frame_moveable_lay;
    float dX;
    float dY;
    int lastAction;
    String url_final;
    private GestureDetector gestureDetector;
    private MyPlaylistEventListener playlistEventListener;
    private MyPlayerStateChangeListener playerStateChangeListener;
    private MyPlaybackEventListener playbackEventListener;
    private String videoUrl;
    private String videoView;
    private String postId;
    private String postTitle;
    private String postCreateAt;
    private String postContent;
    private String userId;
    private boolean logged;

    private View lay_topfvbi;
    private View lay_bottomfvbi;
    private VideoView vvPVfvbi;
    private MediaController videoMediaController;
    private Uri videoUri;
    private int position = 1;

    private BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (MediaPlayerSingleton.getInstance().getMediaPlayer() != null) {
                Log.e("MEDIA_PLAYER", "NOT NULL");
                if (MediaPlayerSingleton.getInstance().getMediaPlayer().isPlaying()) {
                    fab.setVisibility(View.VISIBLE);
                    fab.setImageResource(R.drawable.ic_pause_two);
                    Log.e("MEDIA_PLAYER", "true");
                } else {
                    Log.e("MEDIA_PLAYER", "false");
                    fab.setVisibility(View.GONE);
                    fab.setImageResource(R.drawable.ic_play_two);
                }
            } else {
                Log.e("MEDIA_PLAYER", "NULL");
                MediaPlayerSingleton.getInstance().getMediaPlayer();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_video);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        lay_topfvbi = (View) findViewById(R.id.lay_top);
        lay_bottomfvbi = (View) findViewById(R.id.lay_bottom);

        youTubePlayerFragment = (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_fragment);
        vvPVfvbi = (VideoView) findViewById(R.id.vvPV);

        Iv_share_video = findViewById(R.id.Iv_share_video);
        Iv_Add_favorite = findViewById(R.id.Iv_Add_favorite);
        txt_No_internet_connection = findViewById(R.id.txt_No_internet_connection);

        Tv_video_name = findViewById(R.id.Tv_video_name);
        Tv_video_views = findViewById(R.id.Tv_video_views);
        Tv_no_commments = findViewById(R.id.Tv_no_commments);
        txtview_title = findViewById(R.id.txtview_title);
        txtview_video_content = findViewById(R.id.txtview_video_content);

        Frame_loader = findViewById(R.id.Frame_loader);

        recyclerview_comments = findViewById(R.id.recyclerview_comments);
        edttxt_text_comment = findViewById(R.id.edttxt_text_comment);
        IB_submit_comment = findViewById(R.id.IB_submit_comment);

        fab = findViewById(R.id.fab);
        Frame_moveable_lay = findViewById(R.id.Frame_moveable_lay);
    }

    private void initParam() {
        Intent bundle = getIntent();
        if (bundle.hasExtra(ISeasonConfig.POST_ID) && bundle.hasExtra(ISeasonConfig.VIDEO_EMBED_URL)) {
            videoUrl = bundle.getStringExtra(ISeasonConfig.VIDEO_EMBED_URL);
            videoView = bundle.getStringExtra(ISeasonConfig.VIDEO_VIEWS);
            postId = bundle.getStringExtra(ISeasonConfig.POST_ID);
            postTitle = bundle.getStringExtra(ISeasonConfig.POST_TITLE);
            postCreateAt = bundle.getStringExtra(ISeasonConfig.POST_CREAT_AT);
            postContent = bundle.getStringExtra(ISeasonConfig.POST_CONTENT);
        }
    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_ID)) {
            userId = SessionUtil.getStringPreferences(ISeasonConfig.KEY_USER_ID, null);
        }
        if (SessionUtil.checkIfExist(ISeasonConfig.LOGGEDIN)) {
            logged = SessionUtil.getBoolPreferences(ISeasonConfig.LOGGEDIN, false);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initContent() {
        if (videoUrl.contains("https://youtube.com/embed/")) {
            url_final = videoUrl.replaceAll("https://youtube.com/embed/*", "").trim();
            Log.d("Lihat", "playerInitialize PostVideoActivity : " + url_final);

            youTubePlayerFragment.initialize(YoutubeDeveloperKey.DEVELOPER_KEY, this);

            lay_topfvbi.setVisibility(View.VISIBLE);
            vvPVfvbi.setVisibility(View.GONE);
        } else {
            lay_topfvbi.setVisibility(View.GONE);
            vvPVfvbi.setVisibility(View.VISIBLE);

            videoMediaController = new MediaController(this);
            videoMediaController.setAnchorView(vvPVfvbi);
            String uriPath = IConfig.API_BASE_URL + videoUrl; //update package name
            videoUri = Uri.parse(uriPath);

            vvPVfvbi.setMediaController(videoMediaController);
            vvPVfvbi.setVideoURI(videoUri);
            vvPVfvbi.requestFocus();
            vvPVfvbi.start();

            vvPVfvbi.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    /*if(isContinuously){
                        vv.start();
                    }*/
                    vvPVfvbi.seekTo(1);
                }
            });

            vvPVfvbi.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                // Close the progress bar and play the video
                public void onPrepared(MediaPlayer mp) {
                    /* progressBar.setVisibility(View.GONE);*/
                }
            });

            vvPVfvbi.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent motionEvent) {
                    if (vvPVfvbi.isPlaying()) {
                        vvPVfvbi.pause();
                        /*if (!getActionBar().isShowing()) {
                            getActionBar().show();
                            videoMediaController.show(0);
                        }*/
                        position = vvPVfvbi.getCurrentPosition();
                        return false;
                    } else {
                        /*if (getActionBar().isShowing()) {
                            getActionBar().hide();
                            videoMediaController.hide();
                        }*/
                        vvPVfvbi.seekTo(position);
                        vvPVfvbi.start();
                        return false;
                    }
                }
            });
        }

        recycler_comments_adapter = new Recycler_Comments_adapter(PostVideoActivity.this, R.layout.item_layout_comments, arrayList_comments);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        recyclerview_comments.setAdapter(recycler_comments_adapter);
        recyclerview_comments.setLayoutManager(mLayoutManager);
        recyclerview_comments.setHasFixedSize(true);
        recyclerview_comments.setNestedScrollingEnabled(false);

        playerStateChangeListener = new MyPlayerStateChangeListener();
        playbackEventListener = new MyPlaybackEventListener();
        playlistEventListener = new MyPlaylistEventListener();

        setText_values(postTitle, videoView, postContent, postCreateAt);
        callVideoDetails();
        callApiAllComments();

    }

    @SuppressLint("ClickableViewAccessibility")
    private void initListener() {
        Iv_share_video.setOnClickListener(this);
        Iv_Add_favorite.setOnClickListener(this);

        IB_submit_comment.setOnClickListener(this);

        gestureDetector = new GestureDetector(this, new SingleTapConfirm());
        fab.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if (gestureDetector.onTouchEvent(event)) {
                    if (MediaPlayerSingleton.getInstance().getMediaPlayer() != null) {
                        Log.e("MEDIA_PLAYER", "NOT NULL");

                        if (MediaPlayerSingleton.getInstance().getMediaPlayer().isPlaying()) {
                            fab.setImageResource(R.drawable.ic_play_two);
                            MediaPlayerSingleton.getInstance().getMediaPlayer().pause();
                            //Snackbar.make(view, "Stream Paused", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        } else {
                            fab.setImageResource(R.drawable.ic_pause_two);
                            MediaPlayerSingleton.getInstance().getMediaPlayer().start();
                            //  Snackbar.make(view, "Stream Playing", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        }
                    } else {
                        Log.e("MEDIA_PLAYER", "NULL");
                        MediaPlayerSingleton.getInstance().getMediaPlayer();
                    }
                    //  Toast.makeText(getApplicationContext(), "hello sucessfull", Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    switch (event.getActionMasked()) {

                        case MotionEvent.ACTION_DOWN:
                            dX = view.getX() - event.getRawX();
                            dY = view.getY() - event.getRawY();
                            lastAction = MotionEvent.ACTION_DOWN;
                            break;
                        case MotionEvent.ACTION_MOVE:

                            view.setY(event.getRawY() + dY);
                            view.setX(event.getRawX() + dX);

                            lastAction = MotionEvent.ACTION_MOVE;
                            break;

                        case MotionEvent.ACTION_UP:
                            if (lastAction == MotionEvent.ACTION_DOWN)
                                //  Toast.makeText(MainActivity.this, "Clicked!", Toast.LENGTH_SHORT).show();
                                break;

                        default:
                            return false;
                    }
                }
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == Iv_share_video) {
            if (str_title_slug != null) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Read more at " + Constants.BASE_URL + "post/" + str_title_slug);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        } else if (v == Iv_Add_favorite) {
            if (logged) {
                callApiAddFavorite();
            } else {
                initiatePopupWindow_SelectActionLogin();
            }
        } else if (v == IB_submit_comment) {
            if (logged) {
                txt_comment = edttxt_text_comment.getText().toString();
                if (txt_comment.isEmpty()) {
                    edttxt_text_comment.setError("write some comment....");
                } else {
                    Frame_loader.setVisibility(View.VISIBLE);
                    callApiComment();
                }

            } else {
                initiatePopupWindow_SelectActionLogin();
            }
        }
    }

    public void setText_values(String video_title, String video_views, String content, String created_at) {
        Tv_video_name.setText(video_title);

        String current_datetime = Constants.Current_Time_Zone();
        long api_datetime = Constants.getDateInMillis(created_at);
        long current_timeZone = Constants.getDateInMillis(current_datetime);
        Tv_video_views.setText(" " + video_views + "   |   " + DateUtils.getRelativeTimeSpanString(api_datetime, current_timeZone, DateUtils.SECOND_IN_MILLIS));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txtview_video_content.setText(Html.fromHtml(content, Html.FROM_HTML_MODE_COMPACT));
        } else {
            txtview_video_content.setText(Html.fromHtml(content));
        }
        txtview_title.setText(video_title);
    }

    /*YOUTUBE*/
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean b) {
        youTubePlayer = player;

        //Toast.makeText(getApplicationContext(), "YouTubePlayer.onInitializationSuccess()", Toast.LENGTH_LONG).show();

        youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
        youTubePlayer.setPlaybackEventListener(playbackEventListener);
        youTubePlayer.setPlaylistEventListener(playlistEventListener);

        if (!b) {
            player.cueVideo(url_final);
        }
    }

    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(PostVideoActivity.this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format("There was an error initializing the YouTubePlayer (%1$s)", errorReason.toString());
            Toast.makeText(PostVideoActivity.this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    private void initiatePopupWindow_SelectActionLogin() {
        try {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert inflater != null;
            View layout = inflater.inflate(R.layout.popup_select_action_login, (ViewGroup) findViewById(R.id.popup));

            RelativeLayout mainlayout = layout.findViewById(R.id.popup);

            mainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  popupWindow_selct.dismiss();
                }
            });
            ImageView close_dialog = layout.findViewById(R.id.ivPopForgetPassClose);
            close_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow_selct.dismiss();
                }
            });

            Button Btn_delete_playlist = layout.findViewById(R.id.Btn_login);
            Btn_delete_playlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow_selct.dismiss();
                    Intent i = new Intent(getApplicationContext(), Login_Activity.class);
                    startActivity(i);
                }
            });

            Button Btn_edit_playlist = layout.findViewById(R.id.Btn_regiter);
            Btn_edit_playlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow_selct.dismiss();
                    Intent i = new Intent(getApplicationContext(), Register_Activity.class);
                    startActivity(i);
                }
            });
            popupWindow_selct = new PopupWindow(layout, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT, true);
            popupWindow_selct.showAtLocation(layout, Gravity.CENTER, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callVideoDetails() {
        String URL;
        if (!TextUtils.isEmpty(userId)) {
            URL = Constants.MAIN_URL + "video/" + postId + "/" + userId;
        } else {
            URL = Constants.MAIN_URL + "video/" + postId + "/";
        }
        Log.e("URL_OPEN_POST", URL);

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("detail_news", "" + response.trim());
                        try {
                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                                if (logged) {
                                    String user_reading_list = jsonObject.getString("user_reading_list");
                                    if (user_reading_list.contentEquals("true")) {
                                        Iv_Add_favorite.setImageResource(R.drawable.ic_star_filled);
                                    } else {
                                        Iv_Add_favorite.setImageResource(R.drawable.ic_star_balck_empty);
                                    }
                                }

                                JSONArray jsonArray12 = jsonObject.getJSONArray("video_post_data");
                                for (int i = 0; i < jsonArray12.length(); i++) {
                                    JSONObject obj = jsonArray12.getJSONObject(i);

                                    video_id1 = obj.getString("id");
                                    str_title_slug = obj.getString("title_slug");

                                    str_share_facebook = obj.getString("share_facebook");
                                    str_share_twitter = obj.getString("share_twitter");
                                    str_share_plus_google = obj.getString("share_plus_google");
                                    str_share_linkedin = obj.getString("share_linkedin");
                                    str_share_tumblr = obj.getString("share_tumblr");
                                    str_share_pinterest = obj.getString("share_pinterest");

                                }
                            } else {
                                Log.e("error", "0");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);
    }

    private void callApiComment() {
        final String userid = userId;

        String URL = Constants.MAIN_URL + "add_post_comment";
        Log.e("URL_ADD_COMMENT", URL);

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, URL, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    Log.e("RESPONSE_ADD_COMMENT", "" + resultResponse);
                    JSONObject ojs = new JSONObject(resultResponse);
                    String abc = ojs.getString("status");
                    Log.e("Value", abc);
                    if (Integer.parseInt(abc) == 1) {
                        JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                        JSONObject jsonObject1 = jsonObject.getJSONObject("post_comment");
                        String title = jsonObject1.getString("title");

                        Toast.makeText(getApplicationContext(), title, Toast.LENGTH_SHORT).show();
                        edttxt_text_comment.setText("");
                        callApiAllComments();
                        Frame_loader.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(getApplicationContext(), "Incorrect Data", Toast.LENGTH_SHORT).show();
                        //  Log.e("error", error);*//**//*
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");
                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("post_id", video_id1);
                params.put("user_id", userid);
                params.put("comment", txt_comment);
                return params;
            }

        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(multipartRequest);
    }

    /*YOUTUBE*/

    /*VIDEO*/

    /*VIDEO*/

    private void callApiAllComments() {
        String URL;
        if (!TextUtils.isEmpty(userId)) {
            URL = Constants.MAIN_URL + "post_comment/" + postId + "/" + userId;
        } else {
            URL = Constants.MAIN_URL + "post_comment/" + postId + "/";
        }
        Log.e("URL_OPEN_POST", URL);

        Frame_loader.setVisibility(View.VISIBLE);
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            arrayList_comments.clear();
                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                                Object check = jsonObject.get("user_comment");
                                if (check instanceof JSONObject) {

                                    JSONObject jsonArray123 = jsonObject.getJSONObject("user_comment");
                                    String error = jsonArray123.getString("error");
                                    Tv_no_commments.setVisibility(View.VISIBLE);
                                    // Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                    Frame_loader.setVisibility(View.GONE);

                                } else if (check instanceof JSONArray) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("user_comment");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject obj = jsonArray.getJSONObject(i);
                                        String comment_user_like_count = obj.getString("user_like_count");
                                        String user_reply_count = obj.getString("user_reply_count");
                                        String comment_id = obj.getString("comment_id");
                                        String comment_post_id = obj.getString("comment_post_id");
                                        String comment_user_id = obj.getString("comment_user_id");
                                        String user_like_comment = obj.getString("user_like_comment");
                                        String comment_user_image = Constants.IMAGE_URL + obj.getString("comment_user_image");
                                        String comment_user_name = obj.getString("comment_user_name");
                                        String comment_parent_id = obj.getString("comment_parent_id");
                                        String comment_content = obj.getString("comment_content");
                                        String comment_created_at = obj.getString("comment_created_at");

                                        arrayList_comments.add(new Viewpager_items(comment_user_like_count, user_reply_count, comment_id, comment_post_id, comment_user_id, user_like_comment, comment_user_image, comment_user_name, comment_parent_id, comment_content, comment_created_at, "", ""));

                                        if (i == (jsonArray.length() - 1)) {
                                            Frame_loader.setVisibility(View.GONE);
                                        }
                                    }

                                    if (arrayList_comments.isEmpty()) {
                                        Tv_no_commments.setVisibility(View.VISIBLE);
                                    } else {
                                        Tv_no_commments.setVisibility(View.GONE);

                                        recycler_comments_adapter.setData(arrayList_comments);
                                    }
                                }
                            } else {
                                Log.e("error", "0");
                                String error = ojs.getString("error");
                                Snackbar.make(findViewById(android.R.id.content), error, Snackbar.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Frame_loader.setVisibility(View.GONE);
                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);
    }

    private void callApiAddFavorite() {
        String userid = userId;

        String URL = Constants.MAIN_URL + "add-reading-list/" + userid + "/" + video_id1;
        Log.e("URL_FAVORITE", URL);

        Frame_loader.setVisibility(View.VISIBLE);

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("DATA_FAVORITE", "" + response.trim());
                        try {

                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                                JSONObject jsonObject1 = jsonObject.getJSONObject("added_reading_post");
                                String status = jsonObject1.getString("status");
                                if (status.contentEquals("1")) {
                                    Iv_Add_favorite.setImageResource(R.drawable.ic_star_filled);
                                } else {
                                    Iv_Add_favorite.setImageResource(R.drawable.ic_star_balck_empty);
                                }
                                Frame_loader.setVisibility(View.GONE);

                            } else {
                                Frame_loader.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Frame_loader.setVisibility(View.GONE);
                    }
                }) {
        };
        requestQueue.add(strRequest);

    }

    private String getTimesText() {
        int currentTimeMillis = youTubePlayer.getCurrentTimeMillis();
        int durationMillis = youTubePlayer.getDurationMillis();
        return String.format("(%s/%s)", formatTime(currentTimeMillis), formatTime(durationMillis));
    }

    private String formatTime(int millis) {
        int seconds = millis / 1000;
        int minutes = seconds / 60;
        int hours = minutes / 60;

        return (hours == 0 ? "" : hours + ":") + String.format("%02d:%02d", minutes % 60, seconds % 60);
    }

    private void log(String message) {
        Log.e("VIDEO_RESPONSE", "" + message);
    }

    private void moveToBaseHome() {
        finish();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private final class MyPlayerStateChangeListener implements YouTubePlayer.PlayerStateChangeListener {

        String playerState = "UNINITIALIZED";

        @Override
        public void onLoading() {
            playerState = "LOADING";
            log(playerState);
        }

        @Override
        public void onLoaded(String videoId) {
            playerState = String.format("LOADED %s", videoId);
            log(playerState);
        }

        @Override
        public void onAdStarted() {
            playerState = "AD_STARTED";
            log(playerState);
        }

        @Override
        public void onVideoStarted() {
            playerState = "VIDEO_STARTED";
            log(playerState);
            if (MediaPlayerSingleton.getInstance().getMediaPlayer() != null) {
                if (MediaPlayerSingleton.getInstance().getMediaPlayer().isPlaying()) {

                    MediaPlayerSingleton.getInstance().getMediaPlayer().pause();
                    if (fab != null) {
                        fab.setVisibility(View.GONE);
                    }
                    Log.e("MEDIA_PLAYER", "true");
                } else {
                    MediaPlayerSingleton.getInstance().getMediaPlayer().pause();
                    Log.e("MEDIA_PLAYER", "false");
                }
            } else {
                Log.e("MEDIA_PLAYER", "NULL");
                MediaPlayerSingleton.getInstance().getMediaPlayer();
            }
        }

        @Override
        public void onVideoEnded() {
            playerState = "VIDEO_ENDED";
            log(playerState);
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason reason) {
            playerState = "ERROR (" + reason + ")";
            if (reason == YouTubePlayer.ErrorReason.UNEXPECTED_SERVICE_DISCONNECTION) {
                // When this error occurs the youTubePlayer is released and can no longer be used.
                youTubePlayer = null;
            }
            log(playerState);
        }

    }

    private final class MyPlaybackEventListener implements YouTubePlayer.PlaybackEventListener {
        String playbackState = "NOT_PLAYING";

        String bufferingState = "";

        @Override
        public void onPlaying() {
            playbackState = "PLAYING";
            log("\tPLAYING " + getTimesText());
        }

        @Override
        public void onBuffering(boolean isBuffering) {
            bufferingState = isBuffering ? "(BUFFERING)" : "";
            log("\t\t" + (isBuffering ? "BUFFERING " : "NOT BUFFERING ") + getTimesText());
        }

        @Override
        public void onStopped() {
            playbackState = "STOPPED";
            log("\tSTOPPED");
        }

        @Override
        public void onPaused() {
            playbackState = "PAUSED";
            log("\tPAUSED " + getTimesText());
        }

        @Override
        public void onSeekTo(int endPositionMillis) {
            log(String.format("\tSEEKTO: (%s/%s)",
                    formatTime(endPositionMillis),
                    formatTime(youTubePlayer.getDurationMillis())));
        }

    }

    private final class MyPlaylistEventListener implements YouTubePlayer.PlaylistEventListener {

        @Override
        public void onNext() {
            Log.d("Lihat", "onNext MyPlaylistEventListener : " + "NEXT VIDEO");
        }

        @Override
        public void onPrevious() {
            Log.d("Lihat", "onPrevious MyPlaylistEventListener : " + "PREVIOUS VIDEO");
        }

        @Override
        public void onPlaylistEnded() {
            Log.d("Lihat", "onPlaylistEnded MyPlaylistEventListener : " + "PLAYLIST ENDED");
        }

    }

    private class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapUp(MotionEvent event) {
            return true;
        }
    }
}
