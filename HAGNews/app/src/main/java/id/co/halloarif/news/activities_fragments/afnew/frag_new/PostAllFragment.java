package id.co.halloarif.news.activities_fragments.afnew.frag_new;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.co.halloarif.news.ISeasonConfig;
import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_Adapter_news;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Sub_category_Adapter;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Res.Category.CatDataMainModel;
import id.co.halloarif.news.models.Res.Category.CatDataModel;
import id.co.halloarif.news.models.Res.Category.CatMainModel;
import id.co.halloarif.news.models.Res.Category.CatPostModel;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.support.SessionUtil;
import id.co.halloarif.news.utils.Recycleritemclicklistener;

public class PostAllFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    RecyclerView recyclerview_sub_cat_india, recyclerview_india_post;
    LinearLayoutManager linearLayoutManager;
    ArrayList<Viewpager_items> arrayList_sub_heading = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_all_post = new ArrayList<>();
    Sub_category_Adapter sub_category_adapter;
    SwipeRefreshLayout mSwipeRefreshLayout;
    TextView text_header;
    FragmentActivity fragmentActivity;
    View view1;
    Recyclerview_Adapter_news recyclerview_adapter_news;
    Handler handler1;

    private String tagSlug;
    private String title;
    private String userId;
    private boolean loggedin;
    private String subCatId;

    public PostAllFragment() {
        // Required empty public constructor
    }

    public static PostAllFragment newInstance(String param1, String param2) {
        PostAllFragment fragment = new PostAllFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tagSlug = getArguments().getString(ISeasonConfig.TAG_SLUG);
            title = getArguments().getString(ISeasonConfig.POST_TITLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post_all, container, false);

        initComponent(view);
        initParam();
        initSession();
        initContent();
        initListener();

        return view;

    }

    private void initComponent(View view) {
        text_header = view.findViewById(R.id.text_header);
        recyclerview_india_post = view.findViewById(R.id.recyclerview_india_post);
        recyclerview_sub_cat_india = view.findViewById(R.id.recyclerview_sub_cat_india);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
    }

    private void initParam() {

    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_ID)) {
            userId = SessionUtil.getStringPreferences(ISeasonConfig.KEY_USER_ID, null);
        }
        if (SessionUtil.checkIfExist(ISeasonConfig.LOGGEDIN)) {
            loggedin = SessionUtil.getBoolPreferences(ISeasonConfig.LOGGEDIN, false);
        }
    }

    private void initContent() {
        text_header.setText(title);

        sub_category_adapter = new Sub_category_Adapter(getActivity(), R.layout.sub_category_items, arrayList_sub_heading);
        recyclerview_sub_cat_india.setAdapter(sub_category_adapter);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerview_sub_cat_india.setLayoutManager(linearLayoutManager);
        recyclerview_sub_cat_india.setItemAnimator(new DefaultItemAnimator());

        recyclerview_adapter_news = new Recyclerview_Adapter_news(getActivity(), R.layout.recent_news_layout, arrayList_all_post);
        recyclerview_india_post.setAdapter(recyclerview_adapter_news);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerview_india_post.setLayoutManager(linearLayoutManager1);
        recyclerview_india_post.setItemAnimator(new DefaultItemAnimator());

        // SwipeRefreshLayout
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_blue_dark);

        SET_SUBCATEGORY_API(false);
    }

    private void initListener() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    mSwipeRefreshLayout.post(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.setRefreshing(true);
                            if (TextUtils.isEmpty(subCatId)) {
                                SET_SUBCATEGORY_API(false);
                            } else {
                                SET_SUBCATEGORY_API(true);
                            }
                        }
                    });
                } catch (NullPointerException e) {
                    Log.e("Error", "Error");
                }
            }
        });
        recyclerview_sub_cat_india.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Viewpager_items model = arrayList_sub_heading.get(position);
                /*Constants.Cat_Name = Constants.SLUG_TO_OPEN;
                Constants.Tag_id = model.getPager_id();
                Constants.Cat_Tag_Name = model.getName_sub();
                ((MainActivity) fragmentActivity).CreateFragment(new SubCategory_post_Fragment());*/

                if (TextUtils.isEmpty(model.getPager_id())) {
                    subCatId = "";
                    SET_SUBCATEGORY_API(false);
                } else {
                    subCatId = model.getPager_id();
                    SET_SUBCATEGORY_API(true);
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        recyclerview_india_post.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                try {
                    Viewpager_items model = arrayList_all_post.get(position);
                    BaseHomeFragment.CLICK_METHOD(getActivity(), model);
                } catch (Exception e) {
                    Log.e("EXCEPTION_CLICK", "" + e);
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void SET_SUBCATEGORY_API(boolean isSub) {

        String URL;
        if (!isSub) {
            URL = Constants.MAIN_URL + tagSlug;
            Log.d("Lihat", "SET_SUBCATEGORY_API PostAllFragment : " + URL);

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            StringRequest strRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("Lihat", "onResponse PostAllFragment : " + response);

                    arrayList_sub_heading.clear();
                    arrayList_all_post.clear();

                    try {
                        JsonParser parser = new JsonParser();
                        JsonElement mJson = parser.parse(response);
                        Gson gson = new Gson();
                        CatMainModel mainModel = gson.fromJson(mJson, CatMainModel.class);

                        Integer status = mainModel.getStatus();
                        if (status == 1) {
                            CatDataMainModel dataMainModel = mainModel.getGet_json_data();

                            List<CatDataModel> catModels = dataMainModel.getCat_data();
                            arrayList_sub_heading.add(new Viewpager_items("", "All", "", ""));
                            for (int i = 0; i < catModels.size(); i++) {
                                CatDataModel catModel = catModels.get(i);
                                String id = catModel.getId();
                                String name = catModel.getName();
                                String name_slug = catModel.getName_slug();

                                arrayList_sub_heading.add(new Viewpager_items(id, name, name_slug, ""));
                            }

                            List<CatPostModel> postModels = dataMainModel.getPosts_data();
                            for (int i = 0; i < postModels.size(); i++) {
                                CatPostModel postModel = postModels.get(i);

                                String id = postModel.getId();
                                String title = postModel.getTitle();
                                String category_name = postModel.getCategory_name_slug();
                                String post_type = postModel.getPost_type();
                                String content = postModel.getContent();

                                String image_default = "";
                                String embed_code = "";

                                if (post_type.contentEquals("post")) {///Post
                                    if (TextUtils.isEmpty(postModel.getImage_default())) {
                                        image_default = postModel.getImage_url();
                                    } else {
                                        image_default = Constants.IMAGE_URL + postModel.getImage_default();
                                        embed_code = "";
                                    }

                                } else {////Videos
                                    if (TextUtils.isEmpty(postModel.getImage_default())) {
                                        image_default = postModel.getImage_url();
                                    } else {
                                        image_default = Constants.IMAGE_URL + postModel.getImage_default();
                                    }

                                    if (TextUtils.isEmpty(postModel.getVideo_embed_code())) {
                                        embed_code = postModel.getVideo_path();
                                    } else {
                                        embed_code = postModel.getVideo_embed_code();
                                    }
                                }

                                String created_at = postModel.getCreated_at();
                                String comment_count = postModel.getComment_count();
                                String hit = postModel.getHit();

                                arrayList_all_post.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                            }

                            if (arrayList_sub_heading.isEmpty()) {
                                Log.e("EMPTY", "EMPTY");
                            } else {
                                sub_category_adapter.setData(arrayList_sub_heading);
                            }

                            if (arrayList_all_post.isEmpty()) {
                                Log.e("EMPTY", "EMPTY");
                            } else {
                                recyclerview_adapter_news.setData(arrayList_all_post);
                            }

                        } else {

                            String error = mainModel.getError_json();
                            arrayList_sub_heading.clear();
                            arrayList_all_post.clear();

                            sub_category_adapter.setData(arrayList_sub_heading);
                            recyclerview_adapter_news.setData(arrayList_all_post);

                            Snackbar.make(getActivity().findViewById(android.R.id.content), error.toString(), Snackbar.LENGTH_SHORT).show();
                            Log.e("error", error);
                        }
                    } catch (Exception e) {
                        Log.d("Lihat", "onResponse PostAllFragment : " + e.toString());
                    }

                    /*
                    try {
                        JSONObject ojs = new JSONObject(response);
                        String abc = ojs.getString("status");
                        Log.e("Value", abc);
                        if (Integer.parseInt(abc) == 1) {
                            // JSONObject jsonArray = ojs.getJSONObject("cat_json_data");
                            JSONObject jsonArray12 = ojs.getJSONObject("get_json_data");

                            Object check = jsonArray12.get("posts_data");
                            if (check instanceof JSONObject) {
                                JSONObject jsonArray123 = jsonArray12.getJSONObject("posts_data");
                                // JSONObject obj = (JSONObject) jsonArray12.get(key);
                                String error = jsonArray123.getString("error");

                                Log.e("CHECK_ERROR", "" + error);
                            } else if (check instanceof JSONArray) {

                                JSONArray jsonArray123 = jsonArray12.getJSONArray("posts_data");
                                for (int i = 0; i < jsonArray123.length(); i++) {
                                    JSONObject obj = jsonArray123.getJSONObject(i);

                                    String id = obj.getString("id");
                                    String title = obj.getString("title");
                                    String category_name = obj.getString("category_name_slug");
                                    String post_type = obj.getString("post_type");
                                    String content = obj.getString("content");

                                    String image_default = "";
                                    String embed_code = "";

                                    if (post_type.contentEquals("post")) {///Post
                                        if (TextUtils.isEmpty("image_default")) {
                                            image_default = obj.getString("image_url");
                                        } else {
                                            image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                            embed_code = "";
                                        }
                                    } else {////Videos
                                        if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                            image_default = obj.getString("image_url");
                                        } else {
                                            image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                        }

                                        if (TextUtils.isEmpty(obj.getString("video_embed_code"))) {
                                            embed_code = obj.getString("video_path");
                                        } else {
                                            embed_code = obj.getString("video_embed_code");
                                        }
                                    }

                                    String created_at = obj.getString("created_at");
                                    String comment_count = obj.getString("comment_count");

                                    String hit = obj.getString("hit");

                                    arrayList_all_post.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                                }
                            }

                            if (!arrayList_all_post.isEmpty()) {
                                recyclerview_adapter_news.setData(arrayList_all_post);
                            }

                        } else {
                            Log.e("error", "0");
                               */
                                /*String error = ojs.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);*//*

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    */

                    mSwipeRefreshLayout.setRefreshing(false);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mSwipeRefreshLayout.setRefreshing(false);

                    //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            }) {
            };
            requestQueue.add(strRequest);
        } else {
            URL = Constants.MAIN_URL + tagSlug + "/" + subCatId;

            ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "Loading...", "Please Wait..", false, false);

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            StringRequest strRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("Lihat", "onResponse PostAllFragment : " + response);

                    progressDialog.dismiss();

                    arrayList_all_post.clear();
                    recyclerview_adapter_news.removeDataAll();

                    try {
                        JSONObject ojs = new JSONObject(response);
                        String abc = ojs.getString("status");
                        if (Integer.parseInt(abc) == 1) {
                            JSONObject jsonArray12 = ojs.getJSONObject("get_json_data");

                            Object check = jsonArray12.get("posts_data");
                            if (check instanceof JSONObject) {
                                JSONObject postsData = jsonArray12.getJSONObject("posts_data");
                                String error = postsData.getString("error");

                            } else if (check instanceof JSONArray) {
                                JSONArray postsDatas = jsonArray12.getJSONArray("posts_data");
                                for (int i = 0; i < postsDatas.length(); i++) {
                                    JSONObject postsData = postsDatas.getJSONObject(i);

                                    String id = postsData.getString("id");
                                    String title = postsData.getString("title");
                                    String category_name = postsData.getString("category_name_slug");
                                    String post_type = postsData.getString("post_type");
                                    String content = postsData.getString("content");

                                    String image_default = "";
                                    String embed_code = "";

                                    if (post_type.contentEquals("post")) {///Post
                                        if (TextUtils.isEmpty(postsData.getString("image_default"))) {
                                            image_default = postsData.getString("image_url");
                                        } else {
                                            image_default = Constants.IMAGE_URL + postsData.getString("image_default");
                                            embed_code = "";
                                        }
                                    } else {////Videos
                                        if (TextUtils.isEmpty(postsData.getString("image_default"))) {
                                            image_default = postsData.getString("image_url");
                                        } else {
                                            image_default = Constants.IMAGE_URL + postsData.getString("image_default");
                                            embed_code = "";
                                        }

                                        if (TextUtils.isEmpty(postsData.getString("video_embed_code"))) {
                                            embed_code = postsData.getString("video_path");
                                        } else {
                                            embed_code = postsData.getString("video_embed_code");
                                        }
                                    }

                                    String created_at = postsData.getString("created_at");
                                    String comment_count = postsData.getString("comment_count");
                                    String hit = postsData.getString("hit");

                                    arrayList_all_post.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                                }
                            }

                            if (!arrayList_all_post.isEmpty()) {
                                recyclerview_adapter_news.setData(arrayList_all_post);
                            }

                        } else {
                            Log.e("error", "0");
                               /*String error = ojs.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);*/
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mSwipeRefreshLayout.setRefreshing(false);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    mSwipeRefreshLayout.setRefreshing(false);

                    //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            }) {
            };
            requestQueue.add(strRequest);
        }

    }

    /*public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }*/
}
