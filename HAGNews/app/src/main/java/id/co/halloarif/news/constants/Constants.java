package id.co.halloarif.news.constants;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by ST_004 on 02-02-2018.
 */

public class Constants {
    public static String BASE_URL = "http://halloarif.co.id/news/";
    public static String MAIN_URL = BASE_URL + "webapi/app/";
    public static String IMAGE_URL = BASE_URL;

    public static String SLUG_TO_OPEN = "homepage";
    public static String TAG_TITLE = "";
    public static String CHECK_OPEN_FRAG = "HOME";

    public static String ACTION_PLAY_STICKING = "play";
    public static String ACTION_PAUSE_STICKING = "pause";

    public static int VALUE_SHOW_ITEMS = 5;

    public static String Cat_Name = "";
    public static String Cat_Tag_Name = "";
    public static String Tag_slug = "";
    public static String Tag_id = "";
    public static String Content_Equals = "";
    public static String USER_EMAIL = "";

    public static final String IMAGES = "IMAGES";
    public static final String TITLE = "TITLE";
    public static final String SELECTED_IMG_POS = "SELECTED_IMG_POS";
    public static final String BACKGROUND_COLOR = "BACKGROUND_COLOR";
    public static final String PLACE_HOLDER = "PLACE_HOLDER";
    public static final String SELECTED_IMAGE_POSITION = "SELECTED_IMAGE_POSITION";


    public static String Current_Time_Zone() {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormatter1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        //dateFormatter1.setLenient(false);
        Date today1 = new Date();
        dateFormatter1.setTimeZone(TimeZone.getDefault());
        return dateFormatter1.format(today1);
    }


    public static long getDateInMillis(String srcDate) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat desiredFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        desiredFormat.setTimeZone(TimeZone.getDefault());
        long dateInMillis = 0;
        try {
            Date date = desiredFormat.parse(srcDate);
            dateInMillis = date.getTime();
            return dateInMillis;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
