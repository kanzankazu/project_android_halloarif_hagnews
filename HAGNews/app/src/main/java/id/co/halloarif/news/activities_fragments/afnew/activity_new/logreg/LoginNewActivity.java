package id.co.halloarif.news.activities_fragments.afnew.activity_new.logreg;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import id.co.halloarif.news.ISeasonConfig;
import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afnew.activity_new.BaseHomeActivity;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.database.SQLiteHelperMethod;
import id.co.halloarif.news.models.Res.User.UserJsonDataItem;
import id.co.halloarif.news.models.Res.User.UserMainModel;
import id.co.halloarif.news.support.NetworkUtil;
import id.co.halloarif.news.support.SessionUtil;
import id.co.halloarif.news.utils.Volley_multipart.MySingleton;
import id.co.halloarif.news.utils.Volley_multipart.VolleyMultipartRequest;

public class LoginNewActivity extends AppCompatActivity implements View.OnClickListener {
    SQLiteHelperMethod db = new SQLiteHelperMethod(LoginNewActivity.this);

    FrameLayout flLoginProgrfvbi;
    TextView tvLoginRegfvbi, tvLoginForgetPassfvbi;
    EditText etLoginEmailfvbi, etLoginPasswordfvbi;
    Button bLoginfvbi;

    String txt_email, txt_password;
    PopupWindow popupWindow_forgot;
    private VideoView vvLoginfvbi;
    private String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login1);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();

        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();
    }

    private void initComponent() {
        vvLoginfvbi = (VideoView) findViewById(R.id.vvLogin);
        etLoginEmailfvbi = (EditText) findViewById(R.id.etLoginEmail);
        etLoginPasswordfvbi = (EditText) findViewById(R.id.etLoginPassword);
        flLoginProgrfvbi = (FrameLayout) findViewById(R.id.flLoginProgr);
        tvLoginRegfvbi = (TextView) findViewById(R.id.tvLoginReg);
        tvLoginForgetPassfvbi = (TextView) findViewById(R.id.tvLoginForgetPass);
        bLoginfvbi = (Button) findViewById(R.id.bLogin);
    }

    private void initParam() {

    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_ID)) {
            userID = SessionUtil.getStringPreferences(ISeasonConfig.KEY_USER_ID, null);

            moveToBaseHome();
        }
    }

    private void initContent() {
        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.news);
        vvLoginfvbi.setVideoURI(uri);
        vvLoginfvbi.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
                mp.setLooping(true);
            }
        });
    }

    private void initListener() {
        bLoginfvbi.setOnClickListener(this);
        tvLoginRegfvbi.setOnClickListener(this);
        tvLoginForgetPassfvbi.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == tvLoginRegfvbi) {
            //dialogInfo("Info", "Silahkan daftar di website, jika sudah silahkan kembali ke aplikasi ini");
            moveToRegister();
        } else if (view == tvLoginForgetPassfvbi) {
            //dialogInfo("Info", "Silahkan ubah password di website, jika sudah silahkan kembali ke aplikasi ini");
            moveToForgotPass();
        } else if (view == bLoginfvbi) {
            if (etLoginEmailfvbi.getText().toString().isEmpty()) {
                etLoginEmailfvbi.setError("Enter Email");
            } else if (etLoginPasswordfvbi.getText().toString().isEmpty()) {
                etLoginPasswordfvbi.setError("Enter Password");
            } else {
                txt_email = etLoginEmailfvbi.getText().toString();
                txt_password = etLoginPasswordfvbi.getText().toString();
                callLoginApiCheck(txt_email, txt_password);
            }
        }
    }

    private void dialogInfo(String title,String message) {
        new AlertDialog.Builder(LoginNewActivity.this)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                })
                .show();
    }

    private void moveToForgotPass() {
        startActivity(new Intent(LoginNewActivity.this, WebviewNewForgotPassword.class));
    }

    private void moveToRegister() {
        startActivity(new Intent(LoginNewActivity.this, WebviewNewRegister.class));
        //startActivity(new Intent(LoginNewActivity.this, RegisterNewActivity.class));
        //finish();
    }

    /**/
    private void callLoginApiCheck(String txt_email, String txt_password) {
        if (NetworkUtil.isConnected(getApplicationContext())) {
            callLoginApi(txt_email, txt_password);
        } else {
            Snackbar.make(findViewById(android.R.id.content), R.string.no_internet_connection, Snackbar.LENGTH_INDEFINITE).setAction("Re-Login", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callLoginApiCheck(txt_email, txt_password);
                }
            }).show();
        }
    }

    private void callLoginApi(final String txt_email, final String txt_password) {
        String URL = Constants.MAIN_URL + "login";
        Log.d("Lihat", "callLoginApi LoginNewActivity : " + Constants.MAIN_URL + "login/" + txt_email + "/" + txt_password);

        ProgressDialog progressDialog = ProgressDialog.show(LoginNewActivity.this, "Loading...", "Please Wait..", false, false);

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest strRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();

                        Log.d("Lihat", "onResponse LoginNewActivity : " + response);

                        JsonParser parser = new JsonParser();
                        JsonElement mJson = parser.parse(response);
                        Gson gson = new Gson();
                        UserMainModel mainModel = gson.fromJson(mJson, UserMainModel.class);
                        if (mainModel.getStatus() == 1) {
                            UserJsonDataItem dataModel = mainModel.get_json_data;
                            String emailStatus = dataModel.getEmail_status();
                            if (emailStatus.equalsIgnoreCase("1")) {

                                if (db.userDataGetAll(dataModel.getId()).size() > 0) {
                                    db.userDataUpdate(dataModel, dataModel.getId());
                                } else {
                                    db.userDataSave(dataModel);
                                }

                                SessionUtil.setStringPreferences(ISeasonConfig.KEY_USER_ID, dataModel.getId());
                                SessionUtil.setStringPreferences(ISeasonConfig.KEY_USER_EMAIL, dataModel.getEmail());
                                SessionUtil.setBoolPreferences(ISeasonConfig.LOGGEDIN, true);

                                moveToBaseHome();
                            } else {
                                Snackbar.make(findViewById(android.R.id.content), "Please Confirm Email First", Snackbar.LENGTH_SHORT).show();
                            }
                        } else {
                            Snackbar.make(findViewById(android.R.id.content), mainModel.getError_json(), Snackbar.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        if (TextUtils.isEmpty(error.toString())) {
                            Toast.makeText(LoginNewActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", txt_email);
                params.put("pass", txt_password);
                return params;
            }
        };
        requestQueue.add(strRequest);

    }

    /**/
    private void ForgotPassPopUp() {
        try {
            // We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = LayoutInflater.from(LoginNewActivity.this).inflate(R.layout.popup_forgot_pass, null);
            RelativeLayout mainlayout = layout.findViewById(R.id.rlPopForgetPass);
            mainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  popupWindow_selct.dismiss();
                }
            });
            ImageView close_dialog = layout.findViewById(R.id.ivPopForgetPassClose);
            close_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow_forgot.dismiss();
                }
            });

            final EditText edttxt_text_comment = layout.findViewById(R.id.etPopForgetPassEmail);
            Button Btn_submit = layout.findViewById(R.id.bPopForgetPassSubmit);
            Btn_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String text_reply = edttxt_text_comment.getText().toString();
                    if (text_reply.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Enter your email", Toast.LENGTH_SHORT).show();
                        //edttxt_text_comment.setError("write something...");
                    } else {
                        callForgotPassApi(edttxt_text_comment, text_reply);
                    }
                }
            });

            popupWindow_forgot = new PopupWindow(layout, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT, true);
            popupWindow_forgot.showAtLocation(layout, Gravity.CENTER, 0, 0);

            // popupWindowlogin.showAtLocation(layout, Gravity.CENTER, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callForgotPassApi(final EditText edttxt_email, final String text_reply) {
        flLoginProgrfvbi.setVisibility(View.VISIBLE);

        String URL = Constants.MAIN_URL + "forgetpass";
        Log.e("URL_FORGOT_PASS", URL);

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, URL, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    Log.e("RESPONSE_Forgotpass", "" + resultResponse);
                    JSONObject ojs = new JSONObject(resultResponse);
                    String abc = ojs.getString("status");
                    Log.e("Value", abc);
                    if (Integer.parseInt(abc) == 1) {
                        JSONObject jsonObject = ojs.getJSONObject("get_json_data");
                        JSONObject jsonObject1 = jsonObject.getJSONObject("forget_pass");
                        String title = jsonObject1.getString("title");
                        flLoginProgrfvbi.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), title, Toast.LENGTH_SHORT).show();
                        edttxt_email.setText("");
                        popupWindow_forgot.dismiss();
                    } else {
                        flLoginProgrfvbi.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Incorrect Data", Toast.LENGTH_SHORT).show();
                        //  Log.e("error", error);*//**//*
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");
                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                flLoginProgrfvbi.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", text_reply);
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(multipartRequest);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void moveToBaseHome() {
        Intent intent = new Intent(LoginNewActivity.this, BaseHomeActivity.class);
        startActivity(intent);
        finish();
    }
}
