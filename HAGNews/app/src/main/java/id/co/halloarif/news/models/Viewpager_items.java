package id.co.halloarif.news.models;

import java.io.Serializable;

/**
 * Created by ST_004 on 01-02-2018.
 */

public class Viewpager_items implements Serializable {

    private String pager_title;
    private String pager_id;
    private String name_slug;
    private String name_sub;
    private String title;
    private String id;
    private String image_default;
    private String category_name;
    private String post_type;
    private String created_at;
    private String hit;
    private String question;
    private String option1;
    private String option2;
    private String option3;
    private String option4;
    private String option5;
    private String option6;
    private String option7;
    private String option8;
    private String option9;
    private String option10;
    private String tag;
    private String tag_slug;
    private String embed_code;
    private String content;
    private String check_from;
    private String comment_count;

    private String comment_user_like_count;
    private String user_reply_count;
    private String comment_id;
    private String comment_post_id;
    private String comment_user_id;
    private String comment_user_image;
    private String comment_user_name;
    private String comment_parent_id;
    private String comment_content;
    private String comment_created_at;
    private String user_like_comment;

    private String audio_path;
    private String audio_name;
    private String musician;
    private String download_button;
    private String post_id;
    private String audio_id;

    public  String message;

    public Viewpager_items(String message) {
        this.message = message;
    }

    public Viewpager_items(String id, String audio_path, String audio_name, String musician, String download_button, String post_id, String audio_id) {
        this.id = id;
        this.audio_path = audio_path;
        this.audio_name = audio_name;
        this.musician = musician;
        this.download_button = download_button;
        this.post_id = post_id;
        this.audio_id = post_id;
    }

    public Viewpager_items(String pager_id, String pager_title, String name_slug, String check_from, String my) {
        this.pager_id = pager_id;
        this.pager_title = pager_title;
        this.name_slug = name_slug;
        this.check_from = check_from;
    }

    public Viewpager_items(String id, String tag, String tag_slug, String created_at, String s, String s1) {
        this.id = id;
        this.tag = tag;
        this.tag_slug = tag_slug;
        this.created_at = created_at;
    }

    public Viewpager_items(String id, String name, String name_slug, String s) {
        this.pager_id = id;
        this.name_sub = name;
        this.name_slug = name_slug;
    }

    public Viewpager_items(String id, String title, String image_default, String category_name, String post_type, String created_at, String hit, String comment_count) {
        this.id = id;
        this.title = title;
        this.image_default = image_default;
        this.category_name = category_name;
        this.post_type = post_type;
        this.created_at = created_at;
        this.hit = hit;
        this.comment_count = comment_count;

    }

    public Viewpager_items(String id, String title, String image_default, String category_name, String post_type, String created_at, String hit, String embed_code, String content, String comment_count) {
        this.id = id;
        this.title = title;
        this.image_default = image_default;
        this.category_name = category_name;
        this.post_type = post_type;
        this.created_at = created_at;
        this.hit = hit;
        this.embed_code = embed_code;
        this.content = content;
        this.comment_count = comment_count;
    }

    public Viewpager_items(String id, String question, String option1, String option2, String option3, String option4, String option5, String option6, String option7, String option8, String option9, String option10) {
        this.id = id;
        this.question = question;
        this.option1 = option1;
        this.option2 = option2;
        this.option3 = option3;
        this.option4 = option4;
        this.option5 = option5;
        this.option6 = option6;
        this.option7 = option7;
        this.option8 = option8;
        this.option9 = option9;
        this.option10 = option10;
    }

    public Viewpager_items(String comment_user_like_count, String user_reply_count, String comment_id, String comment_post_id, String comment_user_id, String user_like_comment, String comment_user_image, String comment_user_name, String comment_parent_id, String comment_content, String comment_created_at, String s, String s1) {
        this.comment_user_like_count = comment_user_like_count;
        this.user_reply_count = user_reply_count;
        this.comment_id = comment_id;
        this.comment_post_id = comment_post_id;
        this.comment_user_id = comment_user_id;
        this.user_like_comment = user_like_comment;
        this.comment_user_image = comment_user_image;
        this.comment_user_name = comment_user_name;
        this.comment_parent_id = comment_parent_id;
        this.comment_content = comment_content;
        this.comment_created_at = comment_created_at;
    }

    public String getUser_like_comment() {
        return user_like_comment;
    }

    public String getUser_reply_count() {
        return user_reply_count;
    }

    public String getComment_user_like_count() {
        return comment_user_like_count;
    }

    public String getComment_id() {
        return comment_id;
    }

    public String getComment_post_id() {
        return comment_post_id;
    }

    public String getComment_user_id() {
        return comment_user_id;
    }

    public String getComment_user_image() {
        return comment_user_image;
    }

    public String getComment_user_name() {
        return comment_user_name;
    }

    public String getComment_parent_id() {
        return comment_parent_id;
    }

    public String getComment_content() {
        return comment_content;
    }

    public String getComment_created_at() {
        return comment_created_at;
    }

    public String getComment_count() {
        return comment_count;
    }

    public String getCheck_from() {
        return check_from;
    }

    public String getContent() {
        return content;
    }

    public String getEmbed_code() {
        return embed_code;
    }

    public String getTag() {
        return tag;
    }

    public String getTag_slug() {
        return tag_slug;
    }

    public String getQuestion() {
        return question;
    }

    public String getOption1() {
        return option1;
    }

    public String getOption2() {
        return option2;
    }


    public String getAudio_path() {
        return audio_path;
    }

    public String getAudio_name() {
        return audio_name;
    }

    public String getMusician() {
        return musician;
    }

    public String getDownload_button() {
        return download_button;
    }

    public String getPost_id() {
        return post_id;
    }

    public String getAudio_id() {
        return audio_id;
    }

    public String getOption3() {
        return option3;
    }

    public String getOption4() {
        return option4;
    }

    public String getOption5() {
        return option5;
    }

    public String getOption6() {
        return option6;
    }

    public String getOption7() {
        return option7;
    }

    public String getOption8() {
        return option8;
    }

    public String getOption9() {
        return option9;
    }

    public String getOption10() {
        return option10;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public String getImage_default() {
        return image_default;
    }

    public String getCategory_name() {
        return category_name;
    }

    public String getPost_type() {
        return post_type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getHit() {
        return hit;
    }

    public String getName_sub() {
        return name_sub;
    }

    public void setName_sub(String name_sub) {
        this.name_sub = name_sub;
    }

    public String getName_slug() {
        return name_slug;
    }

    public void setName_slug(String name_slug) {
        this.name_slug = name_slug;
    }

    public String getPager_id() {
        return pager_id;
    }

    public void setPager_id(String pager_id) {
        this.pager_id = pager_id;
    }

    public String getPager_title() {
        return pager_title;
    }

    public void setPager_title(String pager_title) {
        this.pager_title = pager_title;
    }

}
