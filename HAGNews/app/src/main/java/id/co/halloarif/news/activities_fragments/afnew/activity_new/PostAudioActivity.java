package id.co.halloarif.news.activities_fragments.afnew.activity_new;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.co.halloarif.news.ISeasonConfig;
import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afnew.activity_new.logreg.LoginNewActivity;
import id.co.halloarif.news.activities_fragments.afnew.activity_new.logreg.RegisterNewActivity;
import id.co.halloarif.news.activities_fragments.afnew.adapter_new.AudioItemAdapter;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recycler_Comments_adapter;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Res.Audio.AudioDataItemModel;
import id.co.halloarif.news.models.Res.Audio.AudioDataMainModel;
import id.co.halloarif.news.models.Res.Audio.AudioDataModel;
import id.co.halloarif.news.models.Res.Audio.AudioMainModel;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.support.SessionUtil;
import id.co.halloarif.news.utils.AlignTextView;
import id.co.halloarif.news.utils.CircleImageView;
import id.co.halloarif.news.utils.HomeWatcher;
import id.co.halloarif.news.utils.MediaPlayerSingleton;
import id.co.halloarif.news.utils.MyNestedScrollView;
import id.co.halloarif.news.utils.PinchImageView;
import id.co.halloarif.news.utils.Volley_multipart.MySingleton;
import id.co.halloarif.news.utils.Volley_multipart.VolleyMultipartRequest;

public class PostAudioActivity extends AppCompatActivity implements View.OnClickListener {

    HomeWatcher mHomeWatcher = new HomeWatcher(this);

    CollapsingToolbarLayout collapsingToolbar;
    AppBarLayout appBarLayout;
    CircleImageView Iv_centre_imageview;
    ImageView IV_back_news_image, Iv_back_button, Iv_share_info, Iv_Add_favorite, IV_linkedin, IV_tumblr, IV_Pinterest, IV_Googleplus, IV_Twitter, IV_facebook;
    String id, title, str_title_slug, image_default, category_name, post_type, created_at, hit, content, str_share_facebook, str_share_twitter, str_share_plus_google, str_share_linkedin, str_share_tumblr, str_share_pinterest;
    TextView Tv_news_heading, Tv_category, Tv_time, Tv_no_commments;
    AlignTextView txtview_content;
    FrameLayout Frame_loader;
    NestedScrollView Nested_View_Recyclerview;
    PopupWindow popupWindow_selct, popupWindowlogin;
    ArrayList<Viewpager_items> arrayList_comments = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_tracks = new ArrayList<>();
    String txt_comment;
    LinearLayout lay_bottom;
    RecyclerView recyclerview_comments, recyclerView_audio_tracks;
    Recycler_Comments_adapter recycler_comments_adapter;
    MyNestedScrollView nestedScrollView;
    ImageButton IB_submit_comment;
    EditText edttxt_text_comment;
    FloatingActionButton fabfloating_button;
    AudioItemAdapter audioItemAdapter;

    private boolean mIsAvatarShown = true;
    private int mMaxScrollSize;
    private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 20;

    private String postId;
    private String userId;
    private boolean logged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_audio);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        fabfloating_button = findViewById(R.id.fabfloating_button);
        collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        appBarLayout = findViewById(R.id.appbar);

        recyclerview_comments = findViewById(R.id.recyclerview_comments);

        recyclerView_audio_tracks = findViewById(R.id.recyclerView_audio_tracks);

        edttxt_text_comment = findViewById(R.id.edttxt_text_comment);
        nestedScrollView = findViewById(R.id.nestedscroll_view);
        lay_bottom = findViewById(R.id.lay_bottom);
        IV_back_news_image = findViewById(R.id.IV_back_news_image);
        Iv_back_button = findViewById(R.id.Iv_back_button);
        Iv_share_info = findViewById(R.id.Iv_share_info);
        Iv_centre_imageview = findViewById(R.id.Iv_centre_imageview);
        Tv_time = findViewById(R.id.Tv_time);
        Frame_loader = findViewById(R.id.Frame_loader);
        Iv_Add_favorite = findViewById(R.id.Iv_Add_favorite);

        IV_facebook = findViewById(R.id.IV_facebook);
        IV_Googleplus = findViewById(R.id.IV_Googleplus);
        IV_linkedin = findViewById(R.id.IV_linkedin);
        IV_tumblr = findViewById(R.id.IV_tumblr);
        IV_Twitter = findViewById(R.id.IV_Twitter);
        IV_Pinterest = findViewById(R.id.IV_Pinterest);

        Tv_news_heading = findViewById(R.id.Tv_news_heading);
        Tv_category = findViewById(R.id.Tv_category);
        txtview_content = findViewById(R.id.txtview_content);
        Nested_View_Recyclerview = findViewById(R.id.Nested_View_Recyclerview);
        IB_submit_comment = findViewById(R.id.IB_submit_comment);
        Tv_no_commments = findViewById(R.id.Tv_no_commments);

    }

    private void initParam() {
        Intent bundle = getIntent();
        if (bundle.hasExtra(ISeasonConfig.POST_ID)) {
            postId = bundle.getStringExtra(ISeasonConfig.POST_ID);
        }
    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_ID)) {
            userId = SessionUtil.getStringPreferences(ISeasonConfig.KEY_USER_ID, null);
        }
        if (SessionUtil.checkIfExist(ISeasonConfig.LOGGEDIN)) {
            logged = SessionUtil.getBoolPreferences(ISeasonConfig.LOGGEDIN, false);
        }
    }

    private void initContent() {
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
                if (mMaxScrollSize == 0)
                    mMaxScrollSize = appBarLayout.getTotalScrollRange();
                int scrollRange;
                int percentage = (Math.abs(i)) * 100 / mMaxScrollSize;

                if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
                    mIsAvatarShown = false;
                    //  ViewCompat.animate(mFab).scaleY(0).scaleX(0).start();
                    //   mProfileImage.animate().scaleY(0).scaleX(0).setDuration(200).start();
                }

                if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
                    mIsAvatarShown = true;
                    //  mProfileImage.animate().scaleY(1).scaleX(1).start();
                    //  ViewCompat.animate(mFab).scaleY(1).scaleX(1).start();
                    //  toolbar.setVisibility(View.GONE);
                }
                scrollRange = appBarLayout.getTotalScrollRange();
                Log.e("SCroll_Range", "RANGE" + scrollRange + "INT VALUE" + i);

                if (scrollRange + i == 0) {

                    if (title != null && !title.isEmpty()) {
                        collapsingToolbar.setTitle(title);
                    }
                } else {
                    if (title != null && !title.isEmpty()) {
                        collapsingToolbar.setTitle(title);
                    }
                }
            }
        });

        audioItemAdapter = new AudioItemAdapter(PostAudioActivity.this, arrayList_tracks);
        recyclerView_audio_tracks.setAdapter(audioItemAdapter);
        RecyclerView.LayoutManager mLayoutManager1 = new GridLayoutManager(PostAudioActivity.this, 1);
        recyclerView_audio_tracks.setLayoutManager(mLayoutManager1);
        recyclerView_audio_tracks.setNestedScrollingEnabled(false);

        recycler_comments_adapter = new Recycler_Comments_adapter(PostAudioActivity.this, R.layout.item_layout_comments, arrayList_comments);
        recyclerview_comments.setAdapter(recycler_comments_adapter);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(PostAudioActivity.this, 1);
        recyclerview_comments.setLayoutManager(mLayoutManager);
        recyclerview_comments.setNestedScrollingEnabled(false);

        mHomeWatcher.setOnHomePressedListener(new HomeWatcher.OnHomePressedListener() {
            @Override
            public void onHomePressed() {
                MediaPlayerSingleton.getInstance().getMediaPlayer().stop();
            }

            @Override
            public void onHomeLongPressed() {
            }
        });
        mHomeWatcher.startWatch();

        CAll_DETAILS_API();

        CAll_GET_API_COMMENT();
    }

    private void initListener() {
        Iv_back_button.setOnClickListener(this);
        Iv_Add_favorite.setOnClickListener(this);
        Tv_news_heading.setOnClickListener(this);
        IB_submit_comment.setOnClickListener(this);
        IV_back_news_image.setOnClickListener(this);

        IV_facebook.setOnClickListener(this);
        IV_Pinterest.setOnClickListener(this);
        Iv_share_info.setOnClickListener(this);
        IV_Twitter.setOnClickListener(this);
        IV_tumblr.setOnClickListener(this);
        IV_Googleplus.setOnClickListener(this);
        IV_linkedin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == Iv_back_button) {
            onBackPressed();
        } else if (view == IV_back_news_image) {
            initiatePopupWindow_image();
        } else if (view == Iv_Add_favorite) {
            if (logged) {
                CALL_ADD_API_FAVORITE();
            } else {
                initiatePopupWindow_SelectActionLogin();
            }
        } else if (view == Iv_share_info) {
            if (str_title_slug != null) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Read more at " + Constants.BASE_URL + "post/" + str_title_slug);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        } else if (view == IB_submit_comment) {
            if (logged) {
                txt_comment = edttxt_text_comment.getText().toString();
                if (txt_comment.isEmpty()) {
                    edttxt_text_comment.setError("write some comment....");
                } else {
                    Frame_loader.setVisibility(View.VISIBLE);
                    CALL_ADD_API_COMMENT();
                }
            } else {
                initiatePopupWindow_SelectActionLogin();
            }
        }
    }

    private void CAll_DETAILS_API() {
        String URL;
        if (!TextUtils.isEmpty(userId)) {
            URL = Constants.MAIN_URL + "audio/" + postId + "/" + userId;
        } else {
            URL = Constants.MAIN_URL + "audio/" + postId + "/";
        }
        Log.e("URL_AUDIO_POST", URL);

        Frame_loader.setVisibility(View.VISIBLE);

        RequestQueue requestQueue = Volley.newRequestQueue(PostAudioActivity.this);
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        arrayList_tracks.clear();

                        JsonParser parser = new JsonParser();
                        JsonElement mJson = parser.parse(response);
                        Gson gson = new Gson();
                        AudioMainModel mainModel = gson.fromJson(mJson, AudioMainModel.class);

                        if (mainModel.getStatus() == 1) {
                            AudioDataMainModel dataMainModel = mainModel.getGet_json_data();
                            if (logged) {
                                String user_reading_list = dataMainModel.getUser_reading_list();
                                if (user_reading_list.contentEquals("true")) {
                                    Iv_Add_favorite.setImageResource(R.drawable.ic_star_filled);
                                } else {
                                    Iv_Add_favorite.setImageResource(R.drawable.ic_star_empty);
                                }
                            }

                            List<AudioDataModel> dataModels = dataMainModel.getPosts_data();
                            for (int i = 0; i < dataModels.size(); i++) {
                                AudioDataModel dataModel = dataModels.get(i);

                                List<AudioDataItemModel> audioDatas = dataModel.getAudio_data();
                                for (int j = 0; j < audioDatas.size(); j++) {
                                    AudioDataItemModel audioData = audioDatas.get(j);
                                    String id = audioData.getId();
                                    String audio_path = "http://www.halloarif.co.id/news/" + audioData.getAudio_path();
                                    String audio_name = audioData.getAudio_name();
                                    String musician = audioData.getMusician();
                                    String download_button = audioData.getDownload_button();
                                    String post_id = audioData.getPost_id();
                                    String audio_id = audioData.getAudio_id();

                                    arrayList_tracks.add(new Viewpager_items(id, audio_path, audio_name, musician, download_button, post_id, audio_id));
                                }

                                id = dataModel.getId();
                                title = dataModel.getTitle();
                                str_title_slug = dataModel.getTitle_slug();

                                if (dataModel.getImage_default().contentEquals("null") || dataModel.getImage_default().isEmpty()) {
                                    image_default = dataModel.getImage_url();
                                } else {
                                    image_default = Constants.IMAGE_URL + dataModel.getImage_default();
                                }

                                Log.d("Lihat", "onResponse PostAudioActivity : " + arrayList_tracks.size());

                                category_name = dataModel.getCategory_name();
                                post_type = dataModel.getPost_type();
                                created_at = dataModel.getCreated_at();
                                hit = dataModel.getHit();
                                content = dataModel.getContent();
                                str_share_facebook = dataModel.getShare_facebook();
                                str_share_twitter = dataModel.getShare_twitter();
                                str_share_plus_google = dataModel.getShare_plus_google();
                                str_share_linkedin = dataModel.getShare_linkedin();
                                str_share_tumblr = dataModel.getShare_tumblr();
                                str_share_pinterest = dataModel.getShare_pinterest();
                            }

                            /* Audio_Track_Adapter recycler_comments_adapter = new Audio_Track_Adapter(PostAudioActivity.this, R.layout.item_list_audio_tracks, arrayList_tracks);
                            recyclerView_audio_tracks.setAdapter(recycler_comments_adapter);
                            recycler_comments_adapter.notifyDataSetChanged();*/

                            String current_datetime = Constants.Current_Time_Zone();
                            long api_datetime = Constants.getDateInMillis(created_at);
                            long current_timeZone = Constants.getDateInMillis(current_datetime);
                            Tv_news_heading.setText(title);
                            Tv_category.setText(category_name);
                            Tv_time.setText(DateUtils.getRelativeTimeSpanString(api_datetime, current_timeZone, DateUtils.SECOND_IN_MILLIS));

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                txtview_content.setText(Html.fromHtml(content, Html.FROM_HTML_MODE_COMPACT));
                            } else {
                                txtview_content.setText(Html.fromHtml(content));
                            }

                            audioItemAdapter.setData(arrayList_tracks);

                            Glide.with(PostAudioActivity.this).load(image_default).apply(RequestOptions.circleCropTransform()).into(Iv_centre_imageview);

                            Glide.with(PostAudioActivity.this).load(image_default)
                                    .listener(new RequestListener<Drawable>() {
                                        @Override
                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                            // log exception
                                            Log.e("TAG", "Error loading image", e);
                                            return false; // important to return false so the error placeholder can be placed
                                        }

                                        @Override
                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                            Frame_loader.setVisibility(View.GONE);
                                            return false;
                                        }
                                    }).into(IV_back_news_image);

                        } else {
                            Snackbar.make(findViewById(android.R.id.content), mainModel.getError_json(), Snackbar.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Lihat", "onErrorResponse PostAudioActivity : " + error.toString());
                    }
                }) {
        };
        requestQueue.add(strRequest);
    }

    private void CAll_GET_API_COMMENT() {
        String URL;
        if (!TextUtils.isEmpty(userId)) {
            URL = Constants.MAIN_URL + "post_comment/" + postId + "/" + userId;
        } else {
            URL = Constants.MAIN_URL + "post_comment/" + postId + "/";
        }
        Log.e("URL_OPEN_POST", URL);

        Frame_loader.setVisibility(View.VISIBLE);

        RequestQueue requestQueue = Volley.newRequestQueue(PostAudioActivity.this);
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("detail_news", "" + response.trim());
                        try {
                            arrayList_comments.clear();
                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                                Object check = jsonObject.get("user_comment");
                                if (check instanceof JSONObject) {

                                    // JSONObject jsonArray123 = jsonObject.getJSONObject("user_comment");
                                    // String error = jsonArray123.getString("error");
                                    //Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                    Tv_no_commments.setVisibility(View.VISIBLE);
                                    Frame_loader.setVisibility(View.GONE);
                                    Nested_View_Recyclerview.setVisibility(View.GONE);

                                } else if (check instanceof JSONArray) {

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("user_comment");
                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject obj = jsonObject1.getJSONObject(i);
                                        String comment_user_like_count = obj.getString("user_like_count");
                                        String user_reply_count = obj.getString("user_reply_count");
                                        String comment_id = obj.getString("comment_id");
                                        String comment_post_id = obj.getString("comment_post_id");
                                        String comment_user_id = obj.getString("comment_user_id");
                                        String user_like_comment = obj.getString("user_like_comment");
                                        String comment_user_image = Constants.IMAGE_URL + obj.getString("comment_user_image");
                                        String comment_user_name = obj.getString("comment_user_name");
                                        String comment_parent_id = obj.getString("comment_parent_id");
                                        String comment_content = obj.getString("comment_content");
                                        String comment_created_at = obj.getString("comment_created_at");

                                        arrayList_comments.add(new Viewpager_items(comment_user_like_count, user_reply_count, comment_id, comment_post_id, comment_user_id, user_like_comment, comment_user_image, comment_user_name, comment_parent_id, comment_content, comment_created_at, "", ""));
                                    }

                                    if (arrayList_comments.isEmpty()) {
                                        Frame_loader.setVisibility(View.GONE);
                                        Tv_no_commments.setVisibility(View.VISIBLE);
                                        Nested_View_Recyclerview.setVisibility(View.GONE);
                                    } else {
                                        Tv_no_commments.setVisibility(View.GONE);
                                        Frame_loader.setVisibility(View.GONE);
                                        Nested_View_Recyclerview.setVisibility(View.VISIBLE);

                                        recycler_comments_adapter.setData(arrayList_comments);
                                    }
                                }
                            } else {
                                Log.e("error", "");
                               /*String error = ojs.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);*/
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);
    }

    private void CALL_ADD_API_COMMENT() {
        String userid;
        if (!TextUtils.isEmpty(userId)) {
            userid = userId;
        } else {
            userid = "";
        }

        String URL = Constants.MAIN_URL + "add_post_comment";
        Log.e("URL_ADD_COMMENT", URL);

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, URL, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    Log.e("RESPONSE_ADD_COMMENT", "" + resultResponse);
                    JSONObject ojs = new JSONObject(resultResponse);
                    String abc = ojs.getString("status");
                    Log.e("Value", abc);
                    if (Integer.parseInt(abc) == 1) {
                        JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                        JSONObject jsonObject1 = jsonObject.getJSONObject("post_comment");
                        String title = jsonObject1.getString("title");

                        Toast.makeText(PostAudioActivity.this, title, Toast.LENGTH_SHORT).show();
                        edttxt_text_comment.setText("");
                        CAll_GET_API_COMMENT();
                        Frame_loader.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(PostAudioActivity.this, "Incorrect Data", Toast.LENGTH_SHORT).show();
                        //  Log.e("error", error);*//**//*
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");
                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Frame_loader.setVisibility(View.GONE);
                Toast.makeText(PostAudioActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("post_id", postId);
                params.put("user_id", userid);
                params.put("comment", txt_comment);
                return params;
            }
        };
        MySingleton.getInstance(PostAudioActivity.this).addToRequestQueue(multipartRequest);

    }

    private void CALL_ADD_API_FAVORITE() {
        String URL = Constants.MAIN_URL + "add-reading-list/" + userId + "/" + postId;
        Log.e("URL_FAVORITE", URL);

        Frame_loader.setVisibility(View.VISIBLE);

        RequestQueue requestQueue = Volley.newRequestQueue(PostAudioActivity.this);
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("DATA_FAVORITE", "" + response.trim());
                try {

                    JSONObject ojs = new JSONObject(response);
                    String abc = ojs.getString("status");
                    Log.e("Value", abc);
                    if (Integer.parseInt(abc) == 1) {
                        JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                        JSONObject jsonObject1 = jsonObject.getJSONObject("added_reading_post");
                        //  String title = jsonObject1.getString("title");
                        String status = jsonObject1.getString("status");
                        if (status.contentEquals("1")) {
                            Iv_Add_favorite.setImageResource(R.drawable.ic_star_filled);
                        } else {
                            Iv_Add_favorite.setImageResource(R.drawable.ic_star_empty);
                        }
                        Frame_loader.setVisibility(View.GONE);

                    } else {
                               /*String error = ojs.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);*/
                        Frame_loader.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Frame_loader.setVisibility(View.GONE);

                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);

    }

    private void initiatePopupWindow_SelectActionLogin() {
        try {
            // We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert inflater != null;
            View layout = inflater.inflate(R.layout.popup_select_action_login, (ViewGroup) findViewById(R.id.popup));

            RelativeLayout mainlayout = layout.findViewById(R.id.popup);

            mainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  popupWindow_selct.dismiss();
                }
            });
            ImageView close_dialog = layout.findViewById(R.id.ivPopForgetPassClose);
            close_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindowlogin.dismiss();
                }
            });

            Button Btn_delete_playlist = layout.findViewById(R.id.Btn_login);
            Btn_delete_playlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindowlogin.dismiss();
                    Intent i = new Intent(PostAudioActivity.this, LoginNewActivity.class);
                    startActivity(i);

                }
            });

            Button Btn_edit_playlist = layout.findViewById(R.id.Btn_regiter);
            Btn_edit_playlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindowlogin.dismiss();
                    Intent i = new Intent(PostAudioActivity.this, RegisterNewActivity.class);
                    startActivity(i);
                }
            });
            popupWindowlogin = new PopupWindow(layout, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT, true);
            popupWindowlogin.showAtLocation(layout, Gravity.CENTER, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initiatePopupWindow_image() {
        try {
            // We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert inflater != null;
            View layout = inflater.inflate(R.layout.popup_imagezoom, (ViewGroup) findViewById(R.id.popup));

            RelativeLayout mainlayout = layout.findViewById(R.id.popup);

            mainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  popupWindow_selct.dismiss();
                }
            });
            ImageView close_dialog = layout.findViewById(R.id.ivPopForgetPassClose);
            close_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow_selct.dismiss();
                }
            });

            PinchImageView pinchimage = layout.findViewById(R.id.pinchimage);

            Glide.with(PostAudioActivity.this).load(image_default).into(pinchimage);

            pinchimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            popupWindow_selct = new PopupWindow(layout, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT, true);
            popupWindow_selct.showAtLocation(layout, Gravity.CENTER, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        MediaPlayerSingleton.getInstance().getMediaPlayer().stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MediaPlayerSingleton.getInstance().getMediaPlayer().stop();
    }

    @Override
    protected void onStop() {
        super.onStop();
        MediaPlayerSingleton.getInstance().getMediaPlayer().stop();
    }
}
