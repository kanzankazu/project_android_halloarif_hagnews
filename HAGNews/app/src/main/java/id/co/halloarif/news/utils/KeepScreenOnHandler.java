package id.co.halloarif.news.utils;

import android.view.Window;
import android.view.WindowManager;

import com.google.android.youtube.player.YouTubePlayer;

/**
 * Sets the FLAG_KEEP_SCREEN_ON flag during playback - disables it when playback is stopped
 */
public class KeepScreenOnHandler implements YouTubePlayer.PlaybackEventListener {

    private Window mWindow;

    public KeepScreenOnHandler(YouTubePlayer player, Window window) {
        player.setPlaybackEventListener(this);
        mWindow = window;
    }

    private void updateWakeLock(boolean enable) {
        if (enable) {
            mWindow.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            mWindow.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }
    @Override
    public void onPlaying() {
        updateWakeLock(true);
    }

    @Override
    public void onPaused() {
        updateWakeLock(false);
    }

    @Override
    public void onStopped() {
        updateWakeLock(false);
    }

    @Override
    public void onBuffering(boolean b) {
        updateWakeLock(true);
    }

    @Override
    public void onSeekTo(int i) {
        updateWakeLock(true);
    }
}
