package id.co.halloarif.news.activities_fragments.afnew.activity_new;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.Timer;
import java.util.TimerTask;

import id.co.halloarif.news.ISeasonConfig;
import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afnew.activity_new.logreg.ChangePasswordActivity;
import id.co.halloarif.news.activities_fragments.afnew.activity_new.logreg.LoginNewActivity;
import id.co.halloarif.news.activities_fragments.afnew.activity_new.logreg.RegisterNewActivity;
import id.co.halloarif.news.activities_fragments.afnew.activity_new.logreg.UpdateProfileNewActivity;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.database.SQLiteHelperMethod;
import id.co.halloarif.news.models.Res.Header.HeaderMainModel;
import id.co.halloarif.news.models.Res.User.UserJsonDataItem;
import id.co.halloarif.news.support.DateTimeUtil;
import id.co.halloarif.news.support.InputValidUtil;
import id.co.halloarif.news.support.NetworkUtil;
import id.co.halloarif.news.support.SessionUtil;
import id.co.halloarif.news.support.SystemUtil;

import static com.bumptech.glide.request.RequestOptions.centerCropTransform;

public class BaseHomeActivity extends AppCompatActivity {
    SQLiteHelperMethod db = new SQLiteHelperMethod(BaseHomeActivity.this);

    private Toolbar tbBHAfvbi;
    private ActionBar abfvbi;
    private TabLayout tlBHAfvbi;
    private ViewPager vpBHA;
    private TextView tvBHAinfo;
    private ImageView ivBHANavHeadDrawerPhotofvbi;
    private TextView tvBHANavHeadDrawerNamefvbi;
    private TextView tvBHANavHeadDrawerGreetingfvbi;

    private String[] titleTab = new String[]{"Main", "Update", "SOP & Peraturan", "Box Uneg-uneg", "Motivadio", "Receh Time", "Gallery", "HAG Heroes"};
    private String[] slugTab = new String[]{"", "update", "sop-peraturan", "box-uneg-uneg", "motivadio", "receh-time", "", "hag-heroes"};
    private int[] iconTab = new int[]{R.drawable.ic_tab_home, R.drawable.ic_tab_update, R.drawable.ic_tab_regulate, R.drawable.ic_tab_uneg, R.drawable.ic_tab_motivadio, R.drawable.ic_tab_receh_time, R.drawable.ic_tab_gallery, R.drawable.ic_tab_heroes};
    private SlidePagerAdapter mPagerAdapter;
    private String userId;
    private boolean doubleBackToExitPressedOnce;

    private DrawerLayout dlBHA;
    private NavigationView nvBHA;
    private Menu nvBHAMenu;
    private MenuItem menuDrawerSettingLogout;

    public boolean isOnline;
    private UserJsonDataItem userData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_home);

        initToolBar();
        initComponent();
        initSession();
        initParam();
        initContent();
        initListener();
    }

    private void initToolBar() {
        tbBHAfvbi = (Toolbar) findViewById(R.id.tbBHA);
        setSupportActionBar(tbBHAfvbi);
        abfvbi = getSupportActionBar();

        tlBHAfvbi = (TabLayout) findViewById(R.id.tlBHA);

        dlBHA = (DrawerLayout) findViewById(R.id.dlBHA);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, dlBHA, tbBHAfvbi, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        dlBHA.addDrawerListener(toggle);
        toggle.syncState();

        nvBHA = (NavigationView) findViewById(R.id.nvBHA);
        nvBHA.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                Log.d("Lihat", "onNavigationItemSelected BaseHomeActivity : " + id);

                if (id == R.id.menuDrawerAddPost) {
                    moveToPost();
                } else if (id == R.id.menuDrawerReadingList) {
                    moveToReadingList();
                } else if (id == R.id.menuDrawerChangePass) {
                    moveToChangePass();
                } else if (id == R.id.menuDrawerSettingLogout) {
                    moveToLogin();

                    db.userDataDelete(userId);

                    SessionUtil.removeKeyPreferences(ISeasonConfig.KEY_USER_ID);
                    SessionUtil.removeKeyPreferences(ISeasonConfig.KEY_USER_EMAIL);
                    SessionUtil.removeKeyPreferences(ISeasonConfig.LOGGEDIN);
                } else if (id == R.id.menuDrawerSettingHelp) {
                    dialogAbout();
                }

                dlBHA.closeDrawer(GravityCompat.START);
                return true;
            }
        });
        nvBHAMenu = nvBHA.getMenu();
        /*COntoh*/
        menuDrawerSettingLogout = nvBHAMenu.findItem(R.id.menuDrawerSettingLogout);
        //item.setTitle("NewTitleForCamera");

        View headerLayout = nvBHA.inflateHeaderView(R.layout.activity_main_nav_drawer);
        ivBHANavHeadDrawerPhotofvbi = (ImageView) headerLayout.findViewById(R.id.ivBHANavHeadDrawerPhoto);
        tvBHANavHeadDrawerNamefvbi = (TextView) headerLayout.findViewById(R.id.tvBHANavHeadDrawerName);
        tvBHANavHeadDrawerGreetingfvbi = (TextView) headerLayout.findViewById(R.id.tvBHANavHeadDrawerGreeting);
    }

    private void initComponent() {
        vpBHA = (ViewPager) findViewById(R.id.vpBHA);
        tvBHAinfo = (TextView) findViewById(R.id.tvBHAinfo);
    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_ID)) {
            userId = SessionUtil.getStringPreferences(ISeasonConfig.KEY_USER_ID, null);
        } else {
            checkLogin(false, "Guest");
        }
    }

    private void initParam() {

    }

    private void initContent() {
        if (!TextUtils.isEmpty(userId)) {
            UserJsonDataItem oneUserData = db.userDataGetOne(userId);

            Glide.with(getApplicationContext())
                    .load(InputValidUtil.isLinkUrl(oneUserData.getAvatar()) ? oneUserData.getAvatar() : Constants.IMAGE_URL + oneUserData.getAvatar())
                    .apply(centerCropTransform().placeholder(R.drawable.ic_person))
                    .into(ivBHANavHeadDrawerPhotofvbi);
            checkLogin(true, oneUserData.getUsername());
        } else {
            checkLogin(false, "Guest");
        }

        initTablayoutViewpager();

        initLoopCheckNetwork();

        callCategoryHeaderCheck();
    }

    private void initListener() {
        ivBHANavHeadDrawerPhotofvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToProfile();
            }
        });

        vpBHA.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tlBHAfvbi));

        tlBHAfvbi.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vpBHA.setCurrentItem(tab.getPosition());
                tbBHAfvbi.setTitle(titleTab[tab.getPosition()]);
                int tabIconColor = ContextCompat.getColor(BaseHomeActivity.this, R.color.colorPrimaryDark);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int tabIconColor = ContextCompat.getColor(BaseHomeActivity.this, R.color.grey);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void initLoopCheckNetwork() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!NetworkUtil.isConnected(getApplicationContext())) {
                            tvBHAinfo.setVisibility(View.VISIBLE);
                            tvBHAinfo.setText(R.string.no_internet_connection);
                            isOnline = false;
                        } else {
                            tvBHAinfo.setVisibility(View.GONE);
                            isOnline = true;
                        }
                    }
                });
            }
        }, 0, 1000);
    }

    private void initTablayoutViewpager() {
        int tabIconColorSelect = ContextCompat.getColor(this, R.color.colorPrimary);
        int tabIconColorUnSelect = ContextCompat.getColor(this, R.color.grey);
        for (int i = 0; i < titleTab.length; i++) {
            tlBHAfvbi.addTab(tlBHAfvbi.newTab().setIcon(iconTab[i]));
            if (i == 0) {
                tlBHAfvbi.getTabAt(i).getIcon().setColorFilter(tabIconColorSelect, PorterDuff.Mode.SRC_IN);
            } else {
                tlBHAfvbi.getTabAt(i).getIcon().setColorFilter(tabIconColorUnSelect, PorterDuff.Mode.SRC_IN);
            }
        }
        tlBHAfvbi.setTabGravity(TabLayout.GRAVITY_FILL);
        tlBHAfvbi.setEnabled(false);

        mPagerAdapter = new SlidePagerAdapter(getSupportFragmentManager(), titleTab, slugTab);
        vpBHA.setAdapter(mPagerAdapter);
        vpBHA.setOffscreenPageLimit(titleTab.length);
        vpBHA.setCurrentItem(0);
    }

    @SuppressLint("SetTextI18n")
    public void checkLogin(boolean isLoggedin, String s) {
        if (isLoggedin) {
            //menuDrawerSettingLoginReg.setVisible(false);
            menuDrawerSettingLogout.setVisible(true);
        } else {
            //menuDrawerSettingLoginReg.setVisible(true);
            menuDrawerSettingLogout.setVisible(false);
        }
        tvBHANavHeadDrawerNamefvbi.setText("Hai " + s);
        tvBHANavHeadDrawerGreetingfvbi.setText(DateTimeUtil.getGreetingTime());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void callCategoryHeaderCheck() {
        if (NetworkUtil.isConnected(getApplicationContext())) {
            callCategoryHeader();
        } else {
            Snackbar.make(findViewById(android.R.id.content), R.string.no_internet_connection, Snackbar.LENGTH_INDEFINITE).setAction(R.string.reload, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callCategoryHeaderCheck();
                }
            }).show();
        }
    }

    private void callCategoryHeader() {
        String URL = Constants.MAIN_URL + "top-header";

        RequestQueue queue = Volley.newRequestQueue(BaseHomeActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Lihat", "onResponse BaseHomeActivity : " + response);

                JsonParser parser = new JsonParser();
                JsonElement mJson = parser.parse(response);
                Gson gson = new Gson();
                HeaderMainModel model = gson.fromJson(mJson, HeaderMainModel.class);

                Log.d("Lihat", "onResponse BaseHomeActivity : " + model.getStatus());
                Log.d("Lihat", "onResponse BaseHomeActivity : " + model.getGet_json_data());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (!TextUtils.isEmpty(error.toString())) {
                    SystemUtil.showToast(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT, Integer.parseInt(null));
                }
            }
        });
        queue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish();
        }
        this.doubleBackToExitPressedOnce = true;
        SystemUtil.showToast(this, "Tekan sekali lagi untuk keluar", Toast.LENGTH_SHORT, Gravity.TOP);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void dialogLogin() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BaseHomeActivity.this);
        alertDialogBuilder.setMessage("Login or Register?");
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setPositiveButton("Login", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                moveToLogin();
            }
        });
        alertDialogBuilder.setNegativeButton("Register", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                moveToRegister();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void dialogAbout() {
        try {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
            LayoutInflater inflater = (this).getLayoutInflater();
            View dialogLayout = inflater.inflate(R.layout.popup_about, null);
            final android.app.AlertDialog dialog = builder.create();
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            dialog.setView(dialogLayout, 0, 0, 0, 0);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;

            //ImageView imageView = dialogLayout.findViewById(R.id.Tv_popup_image);
            //Glide.with(this).load("http://www.halloarif.co.id/news/uploads/logo/logo_5bbb399226272.png").into(imageView);

            builder.setView(dialogLayout);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void moveToPost() {
        new AlertDialog.Builder(BaseHomeActivity.this)
                .setTitle("Info")
                .setMessage("Please Wait, Underconstruction!!!")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //code here
                    }
                })
                .show();
    }

    private void moveToLogin() {
        Intent intent = new Intent(BaseHomeActivity.this, LoginNewActivity.class);
        startActivity(intent);
        finish();
    }

    private void moveToRegister() {
        Intent intent = new Intent(BaseHomeActivity.this, RegisterNewActivity.class);
        startActivity(intent);
        finish();
    }

    private void moveToProfile() {
        Intent intent = new Intent(BaseHomeActivity.this, UpdateProfileNewActivity.class);
        startActivity(intent);
        finish();
    }

    private void moveToReadingList() {
        Intent intent = new Intent(BaseHomeActivity.this, PostReadingListActivity.class);
        startActivity(intent);
        //finish();
    }

    private void moveToChangePass() {
        Intent intent = new Intent(BaseHomeActivity.this, ChangePasswordActivity.class);
        startActivity(intent);
        //finish();
    }
}
