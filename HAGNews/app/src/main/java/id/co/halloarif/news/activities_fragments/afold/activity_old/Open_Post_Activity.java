package id.co.halloarif.news.activities_fragments.afold.activity_old;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afold.activity_old.logreg.Login_Activity;
import id.co.halloarif.news.activities_fragments.afold.activity_old.logreg.Register_Activity;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recycler_Comments_adapter;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.utils.preference.Preference_saved;
import id.co.halloarif.news.utils.AlignTextView;
import id.co.halloarif.news.utils.MyNestedScrollView;
import id.co.halloarif.news.utils.PinchImageView;
import id.co.halloarif.news.utils.ScrollViewListener;
import id.co.halloarif.news.utils.Volley_multipart.MySingleton;
import id.co.halloarif.news.utils.Volley_multipart.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Open_Post_Activity extends AppCompatActivity implements View.OnClickListener, ScrollViewListener {

    ImageView IV_back_news_image, Iv_back_button, Iv_share_info, Iv_Add_favorite, IV_linkedin, IV_tumblr, IV_Pinterest, IV_Googleplus, IV_Twitter, IV_facebook;
    String post_id, id, title, str_title_slug, image_default, category_name, post_type, created_at, hit, content, str_share_facebook, str_share_twitter, str_share_plus_google, str_share_linkedin, str_share_tumblr, str_share_pinterest;
    TextView Tv_news_heading, Tv_category, Tv_time, Tv_no_commments;
    AlignTextView txtview_content;
    FrameLayout Frame_loader, frame_gotop;
    NestedScrollView Nested_View_Recyclerview;
    PopupWindow popupWindow_selct, popupWindowlogin;
    ArrayList<Viewpager_items> arrayList_comments = new ArrayList<>();
    String txt_comment;
    LinearLayout lay_bottom;
    RecyclerView recyclerview_comments;
    Recycler_Comments_adapter recycler_comments_adapter;
    MyNestedScrollView nestedScrollView;
    AppBarLayout appbar;
    CoordinatorLayout coordinate_lay;
    ImageButton IB_submit_comment;
    EditText edttxt_text_comment;
    String userid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
       /* getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.activity_sub__cat_);

        initCollapsingToolbar();

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        recyclerview_comments = findViewById(R.id.recyclerview_comments);
        edttxt_text_comment = findViewById(R.id.edttxt_text_comment);
        nestedScrollView = findViewById(R.id.nestedscroll_view);
        appbar = findViewById(R.id.appbar);
        coordinate_lay = findViewById(R.id.coordinate_lay);
        lay_bottom = findViewById(R.id.lay_bottom);
        IV_back_news_image = findViewById(R.id.IV_back_news_image);
        Iv_back_button = findViewById(R.id.Iv_back_button);
        Iv_share_info = findViewById(R.id.Iv_share_info);
        Tv_time = findViewById(R.id.Tv_time);
        Frame_loader = findViewById(R.id.Frame_loader);
        frame_gotop = findViewById(R.id.frame_gotop);
        Iv_Add_favorite = findViewById(R.id.Iv_Add_favorite);

        IV_facebook = findViewById(R.id.IV_facebook);
        IV_Googleplus = findViewById(R.id.IV_Googleplus);
        IV_linkedin = findViewById(R.id.IV_linkedin);
        IV_tumblr = findViewById(R.id.IV_tumblr);
        IV_Twitter = findViewById(R.id.IV_Twitter);
        IV_Pinterest = findViewById(R.id.IV_Pinterest);

        IB_submit_comment = findViewById(R.id.IB_submit_comment);
        Tv_news_heading = findViewById(R.id.Tv_news_heading);
        Tv_category = findViewById(R.id.Tv_category);
        txtview_content = findViewById(R.id.txtview_content);
        Nested_View_Recyclerview = findViewById(R.id.Nested_View_Recyclerview);
        Tv_no_commments = findViewById(R.id.Tv_no_commments);
    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        recyclerview_comments.setLayoutManager(mLayoutManager);
        //LinearLayoutManager linearLayoutManager12 = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        // recyclerview_comments.setLayoutManager(linearLayoutManager12);
        recyclerview_comments.setHasFixedSize(true);
        recyclerview_comments.setNestedScrollingEnabled(false);

        /*recyclerview_comments.addOnScrollListener(new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            //dx horizontal distance scrolled in pixels
            //dy vertical distance scrolled in pixels
            super.onScrolled(recyclerView, dx, dy);

            if (dy > 0 && mSubmit.getVisibility() == View.VISIBLE) {
                mSubmit.setVisibility(View.GONE);
            } else if (dy < 0 && mSubmit.getVisibility() != View.VISIBLE) {
                mSubmit.setVisibility(View.VISIBLE);
            }
        }
        });*/
    }

    private void initListener() {
        IV_back_news_image.setOnClickListener(this);
        Iv_back_button.setOnClickListener(this);
        Iv_Add_favorite.setOnClickListener(this);
        Tv_news_heading.setOnClickListener(this);
        IB_submit_comment.setOnClickListener(this);
        nestedScrollView.setScrollViewListener(this);
        frame_gotop.setOnClickListener(this);

        IV_facebook.setOnClickListener(this);
        IV_Pinterest.setOnClickListener(this);
        IV_Twitter.setOnClickListener(this);
        IV_tumblr.setOnClickListener(this);
        IV_Googleplus.setOnClickListener(this);
        IV_linkedin.setOnClickListener(this);
        Iv_share_info.setOnClickListener(this);
    }

    private void CAll_DETAILS_API() {
        String userid = Preference_saved.getInstance(getApplicationContext()).getUser_id();
        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        Frame_loader.setVisibility(View.VISIBLE);
        String URL = Constants.MAIN_URL + "post/" + post_id + "/" + userid;
        Log.e("URL_OPEN_POST", URL);
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("detail_news", "" + response.trim());
                        try {

                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                                String check = Preference_saved.getInstance(getApplicationContext()).check_login();
                                Log.d("checklogin", check);
                                if (check != null) {
                                    if (check.contentEquals("true")) {
                                        String user_reading_list = jsonObject.getString("user_reading_list");
                                        if (user_reading_list.contentEquals("true")) {
                                            Iv_Add_favorite.setImageResource(R.drawable.ic_star_filled);
                                        } else {
                                            Iv_Add_favorite.setImageResource(R.drawable.ic_star_empty);
                                        }
                                    }
                                }
                                JSONArray jsonArray12 = jsonObject.getJSONArray("posts_data");
                                for (int i = 0; i < jsonArray12.length(); i++) {
                                    JSONObject obj = jsonArray12.getJSONObject(i);

                                    id = obj.getString("id");
                                    title = obj.getString("title");
                                    str_title_slug = obj.getString("title_slug");
                                    if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                        image_default = obj.getString("image_url");
                                    } else {
                                        image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                    }
                                    category_name = obj.getString("category_name");
                                    post_type = obj.getString("post_type");
                                    created_at = obj.getString("created_at");
                                    hit = obj.getString("hit");
                                    content = obj.getString("content");
                                    str_share_facebook = obj.getString("share_facebook");
                                    str_share_twitter = obj.getString("share_twitter");
                                    str_share_plus_google = obj.getString("share_plus_google");
                                    str_share_linkedin = obj.getString("share_linkedin");
                                    str_share_tumblr = obj.getString("share_tumblr");
                                    str_share_pinterest = obj.getString("share_pinterest");
                                }

                                String current_datetime = Constants.Current_Time_Zone();
                                long api_datetime = Constants.getDateInMillis(created_at);
                                long current_timeZone = Constants.getDateInMillis(current_datetime);
                                Tv_news_heading.setText(title);
                                Tv_category.setText(category_name);
                                Tv_time.setText(DateUtils.getRelativeTimeSpanString(api_datetime, current_timeZone, DateUtils.SECOND_IN_MILLIS));


                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    txtview_content.setText(Html.fromHtml(content, Html.FROM_HTML_MODE_COMPACT));
                                } else {
                                    txtview_content.setText(Html.fromHtml(content));
                                }


                             /*   Picasso.with(getApplicationContext()).load(image_default)
                                        .into(IV_back_news_image, new com.squareup.picasso.Callback() {
                                            @Override
                                            public void onSuccess() {
                                                // viewHolder.avi.hide();
                                                Frame_loader.setVisibility(View.GONE);
                                            }

                                            @Override
                                            public void onError() {
                                                // viewHolder.avi.show();
                                            }
                                        });*/

                                Glide.with(getApplicationContext())
                                        .load(image_default).apply(RequestOptions.centerCropTransform().error(R.drawable.tallywithback))
                                        .listener(new RequestListener<Drawable>() {
                                            @Override
                                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                                // log exception
                                                Log.e("TAG", "Error loading image", e);
                                                return false; // important to return false so the error placeholder can be placed
                                            }

                                            @Override
                                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                                Frame_loader.setVisibility(View.GONE);
                                                return false;
                                            }
                                        }).into(IV_back_news_image);


                            } else {
                               /*String error = ojs.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);*/
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);
    }

    private void CALL_API_COMMENT() {

        final String userid = Preference_saved.getInstance(getApplicationContext()).getUser_id();
        String URL = Constants.MAIN_URL + "add_post_comment";
        Log.e("URL_ADD_COMMENT", URL);
        // String url = "http://www.angga-ari.com/api/something/awesome";
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, URL, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    Log.e("RESPONSE_ADD_COMMENT", "" + resultResponse);
                    JSONObject ojs = new JSONObject(resultResponse);
                    String abc = ojs.getString("status");
                    Log.e("Value", abc);
                    if (Integer.parseInt(abc) == 1) {
                        JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                        JSONObject jsonObject1 = jsonObject.getJSONObject("post_comment");
                        String title = jsonObject1.getString("title");

                        Toast.makeText(getApplicationContext(), title, Toast.LENGTH_SHORT).show();
                        edttxt_text_comment.setText("");
                        CAll_API_COMMENTS();
                        Frame_loader.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(getApplicationContext(), "Incorrect Data", Toast.LENGTH_SHORT).show();
                        //  Log.e("error", error);*//**//*
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");
                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Frame_loader.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("post_id", post_id);
                params.put("user_id", userid);
                params.put("comment", txt_comment);
                return params;
            }

        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(multipartRequest);

    }

    private void CAll_API_COMMENTS() {

        String check = Preference_saved.getInstance(getApplicationContext()).check_login();
        Log.d("checklogin", check);
        if (check != null) {
            if (check.contentEquals("true")) {
                userid = Preference_saved.getInstance(getApplicationContext()).getUser_id();
            } else {
                userid = "";
            }
        }

        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        Frame_loader.setVisibility(View.VISIBLE);
        String URL = Constants.MAIN_URL + "post_comment/" + post_id + "/" + userid;
        Log.e("URL_OPEN_POST", URL);
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("detail_news", "" + response.trim());
                        try {
                            arrayList_comments.clear();
                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                                Object check = jsonObject.get("user_comment");
                                if (check instanceof JSONObject) {

                                    JSONObject jsonArray123 = jsonObject.getJSONObject("user_comment");
                                    String error = jsonArray123.getString("error");
                                    //Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                    Tv_no_commments.setVisibility(View.VISIBLE);
                                    Frame_loader.setVisibility(View.GONE);
                                    Nested_View_Recyclerview.setVisibility(View.GONE);

                                } else if (check instanceof JSONArray) {

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("user_comment");
                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject obj = jsonObject1.getJSONObject(i);
                                        String comment_user_like_count = obj.getString("user_like_count");
                                        String user_reply_count = obj.getString("user_reply_count");
                                        String comment_id = obj.getString("comment_id");
                                        String comment_post_id = obj.getString("comment_post_id");
                                        String comment_user_id = obj.getString("comment_user_id");
                                        String user_like_comment = obj.getString("user_like_comment");
                                        String comment_user_image = Constants.IMAGE_URL + obj.getString("comment_user_image");
                                        String comment_user_name = obj.getString("comment_user_name");
                                        String comment_parent_id = obj.getString("comment_parent_id");
                                        String comment_content = obj.getString("comment_content");
                                        String comment_created_at = obj.getString("comment_created_at");

                                        arrayList_comments.add(new Viewpager_items(comment_user_like_count, user_reply_count, comment_id, comment_post_id, comment_user_id, user_like_comment, comment_user_image, comment_user_name, comment_parent_id, comment_content, comment_created_at, "", ""));
                                    }

                                    if (arrayList_comments.isEmpty()) {
                                        Frame_loader.setVisibility(View.GONE);
                                        Tv_no_commments.setVisibility(View.VISIBLE);
                                        Nested_View_Recyclerview.setVisibility(View.GONE);
                                    } else {
                                        Tv_no_commments.setVisibility(View.GONE);
                                        Frame_loader.setVisibility(View.GONE);
                                        Nested_View_Recyclerview.setVisibility(View.VISIBLE);
                                        recycler_comments_adapter = new Recycler_Comments_adapter(Open_Post_Activity.this, R.layout.item_layout_comments, arrayList_comments);
                                        recyclerview_comments.setAdapter(recycler_comments_adapter);
                                        recycler_comments_adapter.notifyDataSetChanged();
                                    }
                                }

                            } else {
                               /*String error = ojs.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);*/
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);
    }

    private void initCollapsingToolbar() {

        final CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

      /*  Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.background_home);

        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
            @SuppressWarnings("ResourceType")
            @Override
            public void onGenerated(Palette palette) {
                mutedColor = palette.getMutedColor(R.color.colorPrimary);
                collapsingToolbar.setContentScrimColor(mutedColor);
                collapsingToolbar.setStatusBarScrimColor(R.color.black_trans80);
            }
        });*/
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    if (title != null) {
                        collapsingToolbar.setTitle(title);
                    } else {
                        collapsingToolbar.setTitle("");
                    }
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle("");
                    isShow = false;
                }
            }
        });
    }

    private void initiatePopupWindow_image() {
        try {
// We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.popup_imagezoom, (ViewGroup) findViewById(R.id.popup));

            RelativeLayout mainlayout = layout.findViewById(R.id.popup);

            mainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  popupWindow_selct.dismiss();
                }
            });
            ImageView close_dialog = layout.findViewById(R.id.ivPopForgetPassClose);
            close_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow_selct.dismiss();
                }
            });

            PinchImageView pinchimage = layout.findViewById(R.id.pinchimage);

            Glide.with(getApplicationContext()).load(image_default).into(pinchimage);

            pinchimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            popupWindow_selct = new PopupWindow(layout, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT, true);
            popupWindow_selct.showAtLocation(layout, Gravity.CENTER, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initiatePopupWindow_SelectActionLogin() {
        try {
// We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.popup_select_action_login, (ViewGroup) findViewById(R.id.popup));

            RelativeLayout mainlayout = layout.findViewById(R.id.popup);

            mainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  popupWindow_selct.dismiss();
                }
            });
            ImageView close_dialog = layout.findViewById(R.id.ivPopForgetPassClose);
            close_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindowlogin.dismiss();
                }
            });


            Button Btn_delete_playlist = layout.findViewById(R.id.Btn_login);
            Btn_delete_playlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindowlogin.dismiss();
                    Intent i = new Intent(getApplicationContext(), Login_Activity.class);
                    startActivity(i);
                }
            });

            Button Btn_edit_playlist = layout.findViewById(R.id.Btn_regiter);
            Btn_edit_playlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindowlogin.dismiss();
                    Intent i = new Intent(getApplicationContext(), Register_Activity.class);
                    startActivity(i);
                }
            });
            popupWindowlogin = new PopupWindow(layout, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT, true);
            popupWindowlogin.showAtLocation(layout, Gravity.CENTER, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == Iv_back_button) {
            onBackPressed();
        } else if (view == Tv_news_heading) {
            //camera();
        } else if (view == IV_back_news_image) {
            initiatePopupWindow_image();
        } else if (view == Iv_Add_favorite) {
            String check = Preference_saved.getInstance(getApplicationContext()).check_login();
            Log.d("checklogin", check);
            if (check != null) {
                if (check.contentEquals("true")) {
                    CALL_API_ADD_FAVORITE();
                } else {
                    initiatePopupWindow_SelectActionLogin();
                }
            }
        } else if (view == IB_submit_comment) {
            String check = Preference_saved.getInstance(getApplicationContext()).check_login();
            Log.d("checklogin", check);
            if (check != null) {
                if (check.contentEquals("true")) {
                    txt_comment = edttxt_text_comment.getText().toString();
                    if (txt_comment.isEmpty()) {
                        edttxt_text_comment.setError("write some comment....");
                    } else {
                        Frame_loader.setVisibility(View.VISIBLE);
                        CALL_API_COMMENT();
                    }
                } else {
                    initiatePopupWindow_SelectActionLogin();
                }
            }
        } else if (view == frame_gotop) {
            //  nestedScrollView.smoothScrollTo(0, 0);
            nestedScrollView.fullScroll(NestedScrollView.FOCUS_UP);
            //  nestedScrollView.fullScroll(View.FOCUS_UP);

        } else if (view == Iv_share_info) {
            if (str_title_slug != null) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Read more at " + Constants.BASE_URL + "post/" + str_title_slug);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        } else if (view == IV_facebook) {
            if (!str_share_facebook.isEmpty()) {
                String url = str_share_facebook;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        } else if (view == IV_Twitter) {
            if (!str_share_twitter.isEmpty()) {
                String url = str_share_twitter;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "https://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        } else if (view == IV_Googleplus) {
            if (!str_share_plus_google.isEmpty()) {
                String url = str_share_plus_google;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        } else if (view == IV_linkedin) {
            if (!str_share_linkedin.isEmpty()) {
                String url = str_share_linkedin;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        } else if (view == IV_tumblr) {
            if (!str_share_tumblr.isEmpty()) {
                String url = str_share_tumblr;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        } else if (view == IV_Pinterest) {
            if (!str_share_pinterest.isEmpty()) {
                String url = str_share_pinterest;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        }
    }

    private void CALL_API_ADD_FAVORITE() {
        String userid = Preference_saved.getInstance(getApplicationContext()).getUser_id();
        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        Frame_loader.setVisibility(View.VISIBLE);
        String URL = Constants.MAIN_URL + "add-reading-list/" + userid + "/" + post_id;
        Log.e("URL_FAVORITE", URL);
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("DATA_FAVORITE", "" + response.trim());
                        try {

                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                                JSONObject jsonObject1 = jsonObject.getJSONObject("added_reading_post");
                                String title = jsonObject1.getString("title");
                                String status = jsonObject1.getString("status");
                                if (status.contentEquals("1")) {
                                    Iv_Add_favorite.setImageResource(R.drawable.ic_star_filled);
                                } else {
                                    Iv_Add_favorite.setImageResource(R.drawable.ic_star_empty);
                                }
                                Frame_loader.setVisibility(View.GONE);

                            } else {
                               /*String error = ojs.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);*/
                                Frame_loader.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Frame_loader.setVisibility(View.GONE);

                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onScrollChanged(MyNestedScrollView scrollView, int x, int y, int oldx, int oldy) {
        // We take the last son in the scrollview
        View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
        int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));

        // if diff is zero, then the bottom has been reached
        if (diff == 0) {
            //lay_bottom.setVisibility(View.VISIBLE);
            // frame_gotop.setVisibility(View.VISIBLE);
            Log.e("CHECK_", "BOTTOM");
            // do stuff
        } else {
            // lay_bottom.setVisibility(View.GONE);
            // frame_gotop.setVisibility(View.GONE);
            Log.e("CHECK_", "UP");

        }
    }
}

