package id.co.halloarif.news.models.Res.SubCategory;

public class SubCatMainModel {
    public SubCatDataMainModel get_json_data;
    public Integer status;

    public SubCatDataMainModel getGet_json_data() {
        return get_json_data;
    }

    public void setGet_json_data(SubCatDataMainModel get_json_data) {
        this.get_json_data = get_json_data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
