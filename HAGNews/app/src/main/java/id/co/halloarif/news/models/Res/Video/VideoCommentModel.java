package id.co.halloarif.news.models.Res.Video;

class VideoCommentModel {
    public String user_like_count;
    public String user_reply_count;
    public String comment_id;
    public String comment_post_id;
    public String comment_user_id;
    public String user_like_comment;
    public String comment_user_image;
    public String comment_user_name;
    public String comment_parent_id;
    public String comment_content;
    public String comment_created_at;
}
