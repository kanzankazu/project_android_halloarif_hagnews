package id.co.halloarif.news.models.Req;

public class SendEmailForgetUrlReqModel {
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
