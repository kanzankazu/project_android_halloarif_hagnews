package id.co.halloarif.news.activities_fragments.afnew.frag_new;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.co.halloarif.news.ISeasonConfig;
import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afnew.activity_new.PostAllActivity;
import id.co.halloarif.news.activities_fragments.afnew.activity_new.PostAudioActivity;
import id.co.halloarif.news.activities_fragments.afnew.activity_new.PostImageActivity;
import id.co.halloarif.news.activities_fragments.afnew.activity_new.PostTagActivity;
import id.co.halloarif.news.activities_fragments.afnew.activity_new.PostVideoActivity;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Featured_Adapter;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recycler_adapter_banner;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_Adapter_Videos;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_Adapter_news;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_adapter_voting_poll;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_tags_adapter;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Res.HomePage.HomepageDataModel;
import id.co.halloarif.news.models.Res.HomePage.HomepageMainModel;
import id.co.halloarif.news.models.Res.HomePage.HomepageMainPostModel;
import id.co.halloarif.news.models.Res.HomePage.HomepageTagsSlugModel;
import id.co.halloarif.news.models.Res.HomePage.HomepageVoteRecentPostModel;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.support.NetworkUtil;
import id.co.halloarif.news.support.SessionUtil;
import id.co.halloarif.news.utils.CustomLinearLayoutManager;
import id.co.halloarif.news.utils.LinePagerIndicatorDecoration;
import id.co.halloarif.news.utils.Recycleritemclicklistener;
import id.co.halloarif.news.utils.ShimmerFrameLayout;

public class BaseHomeFragment extends Fragment implements View.OnClickListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    ArrayList<Viewpager_items> arraylist_slide = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_recent = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_popular = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_random = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_videos = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_voting_poll = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_tags = new ArrayList<>();
    TextView text_breaking_news;

    RecyclerView recyclerview_slide;
    RecyclerView recyclerview_recentpost;
    RecyclerView recyclerview_Popular_view;
    RecyclerView recyclerview_random_post;
    RecyclerView recyclerview_top_videos;
    RecyclerView recyclerview_voting_poll;
    RecyclerView recyclerview_tags;

    Featured_Adapter recyclerview_adapter_slide;
    Recycler_adapter_banner recyclerview_adapter_popular;
    Recyclerview_Adapter_news recyclerview_adapter_random, recyclerview_adapter_recent;
    Recyclerview_Adapter_Videos recyclerview_adapter_videos;
    Recyclerview_adapter_voting_poll recyclerview_adapter_voting_poll;
    Recyclerview_tags_adapter recyclerview_adapter_tags;

    SwipeRefreshLayout mSwipeRefreshLayout;
    private ShimmerFrameLayout mShimmerViewContainer;

    private String userId;
    private boolean logged;
    private TextView tvMoreRecentfvbi, tvMorePopularfvbi, tvMoreRandomfvbi, tvMoreTopVideofvbi, tvMoreVotingPollfvbi, tvMoreTagsfvbi;

    public BaseHomeFragment() {
        // Required empty public constructor
    }

    public static BaseHomeFragment newInstance(String param1, String param2) {
        BaseHomeFragment fragment = new BaseHomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_base_home, container, false);

        initComponent(view);
        initParam();
        initSession();
        initContent();
        initListener();

        return view;
    }

    private void initComponent(View view) {
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);

        text_breaking_news = view.findViewById(R.id.text_breaking_news);
        recyclerview_slide = view.findViewById(R.id.recyclerview_featured);
        recyclerview_recentpost = view.findViewById(R.id.recyclerview_recentpost);
        recyclerview_Popular_view = view.findViewById(R.id.recyclerview_Popular_view);
        recyclerview_random_post = view.findViewById(R.id.recyclerview_random_post);
        recyclerview_top_videos = view.findViewById(R.id.recyclerview_top_videos);
        recyclerview_voting_poll = view.findViewById(R.id.recyclerview_voting_poll);
        recyclerview_tags = view.findViewById(R.id.recyclerview_tags);

        tvMoreRecentfvbi = (TextView) view.findViewById(R.id.tvMoreRecent);
        tvMorePopularfvbi = (TextView) view.findViewById(R.id.tvMorePopular);
        tvMoreRandomfvbi = (TextView) view.findViewById(R.id.tvMoreRandom);
        tvMoreTopVideofvbi = (TextView) view.findViewById(R.id.tvMoreTopVideo);
        tvMoreVotingPollfvbi = (TextView) view.findViewById(R.id.tvMoreVotingPoll);
    }

    private void initParam() {

    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_ID)) {
            userId = SessionUtil.getStringPreferences(ISeasonConfig.KEY_USER_ID, null);
        }
        if (SessionUtil.checkIfExist(ISeasonConfig.LOGGEDIN)) {
            logged = SessionUtil.getBoolPreferences(ISeasonConfig.LOGGEDIN, false);
        }
    }

    private void initContent() {
        text_breaking_news.setSelected(true);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_blue_dark);

        if (mShimmerViewContainer != null) {
            mShimmerViewContainer.setDuration(1500);
            mShimmerViewContainer.setIntensity(0.35f);
            mShimmerViewContainer.setMaskShape(ShimmerFrameLayout.MaskShape.RADIAL);
            mShimmerViewContainer.startShimmerAnimation();
        }

        initRecycleView();

        callHomePageAPICheck();
    }

    private void initRecycleView() {
        recyclerview_adapter_slide = new Featured_Adapter(getActivity(), R.layout.banner_layout_item, arraylist_slide);
        recyclerview_slide.setAdapter(recyclerview_adapter_slide);
        //LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        CustomLinearLayoutManager customLinearLayoutManager = new CustomLinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerview_slide.setLayoutManager(customLinearLayoutManager);
        recyclerview_slide.setHasFixedSize(true);
        recyclerview_slide.setNestedScrollingEnabled(false);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerview_slide);
        recyclerview_slide.addItemDecoration(new LinePagerIndicatorDecoration());

        recyclerview_adapter_recent = new Recyclerview_Adapter_news(getActivity(), R.layout.recent_news_layout, arrayList_recent);
        recyclerview_recentpost.setAdapter(recyclerview_adapter_recent);
        LinearLayoutManager linearLayoutManager12 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerview_recentpost.setLayoutManager(linearLayoutManager12);
        recyclerview_recentpost.setHasFixedSize(true);
        recyclerview_recentpost.setNestedScrollingEnabled(false);

        recyclerview_adapter_random = new Recyclerview_Adapter_news(getActivity(), R.layout.recent_news_layout, arrayList_random);
        recyclerview_random_post.setAdapter(recyclerview_adapter_random);
        LinearLayoutManager linearLayoutManager1234 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerview_random_post.setLayoutManager(linearLayoutManager1234);
        recyclerview_random_post.setHasFixedSize(true);
        recyclerview_random_post.setNestedScrollingEnabled(false);

        recyclerview_adapter_videos = new Recyclerview_Adapter_Videos(getActivity(), R.layout.video_item_layout, arrayList_videos);
        recyclerview_top_videos.setAdapter(recyclerview_adapter_videos);
        LinearLayoutManager linearLayoutManager12345 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerview_top_videos.setLayoutManager(linearLayoutManager12345);
        recyclerview_top_videos.setHasFixedSize(true);
        recyclerview_top_videos.setNestedScrollingEnabled(false);

        recyclerview_adapter_popular = new Recycler_adapter_banner(getActivity(), R.layout.banner_layout_item, arrayList_popular);
        recyclerview_Popular_view.setAdapter(recyclerview_adapter_popular);
        LinearLayoutManager linearLayoutManager123 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerview_Popular_view.setLayoutManager(linearLayoutManager123);
        recyclerview_Popular_view.setHasFixedSize(true);
        recyclerview_Popular_view.setNestedScrollingEnabled(false);

        recyclerview_adapter_voting_poll = new Recyclerview_adapter_voting_poll(getActivity(), R.layout.voting_poll_item, arrayList_voting_poll);
        recyclerview_voting_poll.setAdapter(recyclerview_adapter_voting_poll);
        LinearLayoutManager linearLayoutManager123456 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerview_voting_poll.setLayoutManager(linearLayoutManager123456);
        recyclerview_voting_poll.setHasFixedSize(true);
        recyclerview_voting_poll.setNestedScrollingEnabled(false);
        SnapHelper snapHelper2 = new PagerSnapHelper();
        snapHelper2.attachToRecyclerView(recyclerview_voting_poll);

        recyclerview_adapter_tags = new Recyclerview_tags_adapter(getActivity(), arrayList_tags);
        recyclerview_tags.setAdapter(recyclerview_adapter_tags);
        LinearLayoutManager linearLayoutManager1234567 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerview_tags.setLayoutManager(linearLayoutManager1234567);
        recyclerview_tags.setHasFixedSize(true);
        recyclerview_tags.setNestedScrollingEnabled(false);

    }

    private void initListener() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    mSwipeRefreshLayout.setRefreshing(true);
                    callHomePageAPICheck();
                } catch (NullPointerException e) {
                    Log.d("Lihat", "onRefresh BaseHomeFragment : " + e.toString());
                }
            }
        });

        /*text_breaking_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (breaking_news_id != null) {
                    Intent i = new Intent(getActivity(), Open_Post_Activity.class);
                    i.putExtra(ISeasonConfig.POST_ID, breaking_news_id);
                    startActivity(i);
                } else {
                    Toast.makeText(getActivity(), "No info Available", Toast.LENGTH_SHORT).show();
                }
            }
        });*/

        recyclerview_slide.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Viewpager_items model = arraylist_slide.get(position);
                CLICK_METHOD(getActivity(), model);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        recyclerview_recentpost.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Viewpager_items model = arrayList_recent.get(position);
                CLICK_METHOD(getActivity(), model);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        recyclerview_Popular_view.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Viewpager_items model = arrayList_popular.get(position);
                CLICK_METHOD(getActivity(), model);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        recyclerview_random_post.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Viewpager_items model = arrayList_random.get(position);
                CLICK_METHOD(getActivity(), model);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        recyclerview_top_videos.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                /*Viewpager_items model = arrayList_videos.get(position);
                String video_id = model.getId();
                String video_url;
                if (!TextUtils.isEmpty(model.getEmbed_code())) {
                    video_url = model.getEmbed_code();
                } else {
                    video_url = model.getEmbed_code();
                }
                String video_title = model.getTitle();
                String created_at = model.getCreated_at();
                String video_views = model.getHit();
                String content = model.getContent();

                ((MainActivity) getActivity()).draggableView.setVisibility(View.VISIBLE);
                ((MainActivity) getActivity()).draggableView.maximize();
                ((MainActivity) getActivity()).Call_VIDEO_Details_API(video_id);
                ((MainActivity) getActivity()).CAll_API_ALL_COMMENTS(video_id);
                ((MainActivity) getActivity()).PLAYER_INITIALIZE(video_url);
                ((MainActivity) getActivity()).setText_values(video_title, video_views, content, created_at);*/

                Viewpager_items model = arrayList_videos.get(position);
                CLICK_METHOD(getActivity(), model);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        recyclerview_tags.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                /*Constants.CHECK_OPEN_FRAG = "Sell";
                Constants.Tag_slug = arrayList_tags.get(position).getTag_slug();
                Constants.Cat_Name = arrayList_tags.get(position).getTag();
                ((MainActivity) getActivity()).CreateFragment(new Tags_Post_Fragment());*/

                Viewpager_items viewpager_items = arrayList_tags.get(position);

                Intent intent = new Intent(getActivity(), PostTagActivity.class);
                intent.putExtra(ISeasonConfig.TAG_SLUG, viewpager_items.getTag_slug());
                intent.putExtra(ISeasonConfig.TAG, viewpager_items.getTag());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        tvMoreRecentfvbi.setOnClickListener(this);
        tvMorePopularfvbi.setOnClickListener(this);
        tvMoreRandomfvbi.setOnClickListener(this);
        tvMoreTopVideofvbi.setOnClickListener(this);
        tvMoreVotingPollfvbi.setOnClickListener(this);
    }

    public static void CLICK_METHOD(FragmentActivity activity, Viewpager_items model) {
        String post_type = model.getPost_type();
        if (post_type.contentEquals("post")) {
            Intent i = new Intent(activity, PostImageActivity.class);
            i.putExtra(ISeasonConfig.POST_ID, model.getId());
            Log.d("Lihat", "CLICK_METHOD BaseHomeFragment : " + model.getId());
            Log.d("Lihat", "CLICK_METHOD BaseHomeFragment : " + model.getPost_type());
            activity.startActivity(i);
        } else if (post_type.contentEquals("audio")) {
            Intent i = new Intent(activity, PostAudioActivity.class);
            i.putExtra(ISeasonConfig.POST_ID, model.getId());
            Log.d("Lihat", "CLICK_METHOD BaseHomeFragment : " + model.getId());
            Log.d("Lihat", "CLICK_METHOD BaseHomeFragment : " + model.getPost_type());
            activity.startActivity(i);
        } else {
            Intent i = new Intent(activity, PostVideoActivity.class);
            i.putExtra(ISeasonConfig.VIDEO_EMBED_URL, model.getEmbed_code());
            i.putExtra(ISeasonConfig.POST_TITLE, model.getTitle());
            i.putExtra(ISeasonConfig.POST_ID, model.getId());
            i.putExtra(ISeasonConfig.POST_CREAT_AT, model.getCreated_at());
            i.putExtra(ISeasonConfig.VIDEO_VIEWS, model.getHit());
            i.putExtra(ISeasonConfig.POST_CONTENT, model.getContent());
            Log.d("Lihat", "CLICK_METHOD BaseHomeFragment : " + model.getId());
            Log.d("Lihat", "CLICK_METHOD BaseHomeFragment : " + model.getPost_type());
            activity.startActivity(i);
        }
    }

    private void callHomePageAPICheck() {
        if (NetworkUtil.isConnected(getActivity())) {
            callHomePageAPI();
        } else {
            Snackbar.make(getActivity().findViewById(android.R.id.content), R.string.no_internet_connection, Snackbar.LENGTH_INDEFINITE).setAction(R.string.reload, null).show();
        }

    }

    private void callHomePageAPI()  {
        String URL = Constants.MAIN_URL + "homepage";
        Log.d("Lihat", "callHomePageAPI BaseHomeFragment : " + URL);

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Lihat", "onResponse BaseHomeFragment : " + response);

                        mSwipeRefreshLayout.setRefreshing(false);

                        arraylist_slide.clear();
                        arrayList_recent.clear();
                        arrayList_popular.clear();
                        arrayList_random.clear();
                        arrayList_videos.clear();
                        arrayList_voting_poll.clear();
                        arrayList_tags.clear();

                        try{
                            JsonParser parser = new JsonParser();
                            JsonElement mJson = parser.parse(response);
                            Gson gson = new Gson();
                            HomepageMainModel mainModel = gson.fromJson(mJson, HomepageMainModel.class);

                            if (mainModel.getStatus() == 1) {
                                HomepageDataModel dataModel = mainModel.getGet_json_data();

                                List<HomepageMainPostModel> breakingNewsModel = dataModel.getBreaking_news();
                                String text = "";
                                for (int i = 0; i < breakingNewsModel.size(); i++) {
                                    HomepageMainPostModel model = breakingNewsModel.get(i);
                                    String titleBreaking = model.getTitle();
                                    int pos = i + 1;
                                    text = "  <a>  " + "   " + text + " <font color='red'> " + "  " + pos + ".  " + " </font> " + titleBreaking + "  " + "   " + "  </a>  ";
                                    text_breaking_news.setText(Html.fromHtml(text));
                                }
                                ////////////////////
                                List<HomepageMainPostModel> sliderFeaturModel = dataModel.getSlider_featur();
                                Log.d("Lihat", "onResponse BaseHomeFragment : " + sliderFeaturModel.size());
                                for (int i = 0; i < sliderFeaturModel.size(); i++) {
                                    HomepageMainPostModel model = sliderFeaturModel.get(i);
                                    String id = model.getId();
                                    String title = model.getTitle();
                                    String category_name = model.getCategory_name_slug();
                                    String post_type = model.getPost_type();
                                    String content = model.getContent();
                                    String created_at = model.getCreated_at();
                                    String comment_count = model.getComment_count();
                                    String hit = model.getHit();
                                    String image_default = "";
                                    String embed_code = "";
                                    if (post_type.contentEquals("post")) {///Post
                                        if (TextUtils.isEmpty(model.getImage_default())) {
                                            image_default = model.getImage_url();
                                        } else {
                                            image_default = Constants.IMAGE_URL + model.getImage_default();
                                            embed_code = "";
                                        }
                                    } else {////Videos
                                        if (TextUtils.isEmpty(model.getImage_default())) {
                                            image_default = model.getImage_url();
                                        } else {
                                            image_default = Constants.IMAGE_URL + model.getImage_default();
                                        }

                                        if (TextUtils.isEmpty(model.getVideo_embed_code())) {
                                            embed_code = model.getVideo_path();
                                        } else {
                                            embed_code = model.getVideo_embed_code();
                                        }
                                    }

                                    arraylist_slide.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                                }
                                ////////////////////////
                                List<HomepageMainPostModel> latestPostModel = dataModel.getLatest_post();
                                for (int i = 0; i < (latestPostModel.size() > Constants.VALUE_SHOW_ITEMS ? Constants.VALUE_SHOW_ITEMS : latestPostModel.size()); i++) {
                                    HomepageMainPostModel model = latestPostModel.get(i);
                                    String id = model.getId();
                                    String title = model.getTitle();
                                    String category_name = model.getCategory_name_slug();
                                    String post_type = model.getPost_type();
                                    String content = model.getContent();
                                    String created_at = model.getCreated_at();
                                    String comment_count = model.getComment_count();
                                    String hit = model.getHit();

                                    String image_default = "";
                                    String embed_code = "";

                                    if (post_type.contentEquals("post")) {///Post
                                        if (TextUtils.isEmpty(model.getImage_default())) {
                                            image_default = model.getImage_url();
                                        } else {
                                            image_default = Constants.IMAGE_URL + model.getImage_default();
                                            embed_code = "";
                                        }

                                    } else {////Videos
                                        if (TextUtils.isEmpty(model.getImage_default())) {
                                            image_default = model.getImage_url();
                                        } else {
                                            image_default = Constants.IMAGE_URL + model.getImage_default();
                                        }

                                        if (TextUtils.isEmpty(model.getVideo_embed_code())) {
                                            embed_code = model.getVideo_path();
                                        } else {
                                            embed_code = model.getVideo_embed_code();
                                        }
                                    }

                                    arrayList_recent.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                                    //  arrayList_recent.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, comment_count));

                                }
                                ////////////////////
                                List<HomepageMainPostModel> popularPostsModel = dataModel.getPopular_posts();
                                for (int i = 0; i < (popularPostsModel.size() > Constants.VALUE_SHOW_ITEMS ? Constants.VALUE_SHOW_ITEMS : popularPostsModel.size()); i++) {
                                    HomepageMainPostModel model = popularPostsModel.get(i);
                                    String id = model.getId();
                                    String title = model.getTitle();
                                    String category_name = model.getCategory_name_slug();
                                    String post_type = model.getPost_type();
                                    String content = model.getContent();
                                    String created_at = model.getCreated_at();
                                    String comment_count = model.getComment_count();
                                    String hit = model.getHit();
                                    String image_default = "";
                                    String embed_code = "";

                                    if (post_type.contentEquals("post")) {///Post
                                        if (TextUtils.isEmpty(model.getImage_default())) {
                                            image_default = model.getImage_url();
                                        } else {
                                            image_default = Constants.IMAGE_URL + model.getImage_default();
                                            embed_code = "";
                                        }

                                    } else {////Videos
                                        if (TextUtils.isEmpty(model.getImage_default())) {
                                            image_default = model.getImage_url();
                                        } else {
                                            image_default = Constants.IMAGE_URL + model.getImage_default();
                                        }

                                        if (TextUtils.isEmpty(model.getVideo_embed_code())) {
                                            embed_code = model.getVideo_path();
                                        } else {
                                            embed_code = model.getVideo_embed_code();
                                        }
                                    }

                                    arrayList_popular.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                                }
                                /////////////////////////////
                                List<HomepageMainPostModel> randomPostModel = dataModel.getRandom_post();
                                for (int i = 0; i < (randomPostModel.size() > Constants.VALUE_SHOW_ITEMS ? Constants.VALUE_SHOW_ITEMS : randomPostModel.size()); i++) {
                                    HomepageMainPostModel model = randomPostModel.get(i);
                                    String id = model.getId();
                                    String title = model.getTitle();
                                    String category_name = model.getCategory_name_slug();
                                    String post_type = model.getPost_type();
                                    String content = model.getContent();
                                    String created_at = model.getCreated_at();
                                    String comment_count = model.getComment_count();
                                    String hit = model.getHit();
                                    String image_default = "";
                                    String embed_code = "";

                                    if (post_type.contentEquals("post")) {///Post
                                        if (TextUtils.isEmpty(model.getImage_default())) {
                                            image_default = model.getImage_url();
                                        } else {
                                            image_default = Constants.IMAGE_URL + model.getImage_default();
                                            embed_code = "";
                                        }

                                    } else {////Videos
                                        if (TextUtils.isEmpty(model.getImage_default())) {
                                            image_default = model.getImage_url();
                                        } else {
                                            image_default = Constants.IMAGE_URL + model.getImage_default();
                                        }

                                        if (TextUtils.isEmpty(model.getVideo_embed_code())) {
                                            embed_code = model.getVideo_path();
                                        } else {
                                            embed_code = model.getVideo_embed_code();
                                        }
                                    }

                                    arrayList_random.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                                }
                                /////////////////////
                            /*List<HomepageVideoModel> recentVideosModel = dataModel.getRecent_videos();
                            for (int i = 0; i < recentVideosModel.size(); i++) {
                                HomepageVideoModel model = recentVideosModel.get(i);
                                String id = model.getId();
                                String title = model.getTitle();
                                String category_name = model.getCategory_name_slug();
                                // image_default = obj.getString("video_image_url");
                                String post_type = model.getPost_type();
                                String created_at = model.getCreated_at();
                                String hit = model.getHit();
                                String image_default = "";
                                String embed_code = "";

                                if (TextUtils.isEmpty(model.getImage_default())) {
                                    image_default = model.getImage_url();
                                } else {
                                    image_default = Constants.IMAGE_URL + model.getImage_default();
                                }

                                if (TextUtils.isEmpty(model.getVideo_embed_code())) {
                                    embed_code = model.getVideo_path();
                                } else {
                                    embed_code = model.getVideo_embed_code();
                                }

                                String content = model.getContent();
                                String comment_count = model.getComment_count();

                                arrayList_videos.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                            }*/
                                ///////////////////////////
                                List<HomepageVoteRecentPostModel> voteRecentPostModels = dataModel.getVote_recent_post();
                                for (int i = 0; i < voteRecentPostModels.size(); i++) {
                                    HomepageVoteRecentPostModel model = voteRecentPostModels.get(i);
                                    String id = model.getId();
                                    String question = model.getQuestion();
                                    String option1 = model.getOption1();
                                    String option2 = model.getOption2();
                                    String option3 = model.getOption3();
                                    String option4 = model.getOption4();
                                    String option5 = model.getOption5();
                                    String option6 = model.getOption6();
                                    String option7 = model.getOption7();
                                    String option8 = model.getOption8();
                                    String option9 = model.getOption9();
                                    String option10 = model.getOption10();

                                    arrayList_voting_poll.add(new Viewpager_items(id, question, option1, option2, option3, option4, option5, option6, option7, option8, option9, option10));
                                }
                                /////////////////////////////
                                List<HomepageTagsSlugModel> tagsSlugModels = dataModel.getTags_slug();
                                for (int i = 0; i < tagsSlugModels.size(); i++) {
                                    HomepageTagsSlugModel mode = tagsSlugModels.get(i);

                                    String id = mode.getId();
                                    String tag = mode.getTag();
                                    String tag_slug = mode.getTag_slug();
                                    String created_at = mode.getCreated_at();

                                    arrayList_tags.add(new Viewpager_items(id, tag, tag_slug, created_at, "", ""));
                                }

                                Log.d("Lihat", "onResponse BaseHomeFragment : " + arraylist_slide.size() + "," + arrayList_popular.size() + "," + arrayList_random.size() + "," + arrayList_recent.size());

                                recyclerview_adapter_slide.setData(arraylist_slide);
                                recyclerview_adapter_recent.setData(arrayList_recent);
                                recyclerview_adapter_random.setData(arrayList_random);
                                recyclerview_adapter_videos.setData(arrayList_videos);
                                recyclerview_adapter_popular.setData(arrayList_popular);
                                recyclerview_adapter_voting_poll.setData(arrayList_voting_poll);
                                recyclerview_adapter_tags.setData(arrayList_tags);

                                recycleAutoScroll(3000, arraylist_slide, recyclerview_slide);

                            } else {
                                Snackbar.make(getActivity().findViewById(android.R.id.content), mainModel.getError_json(), Snackbar.LENGTH_SHORT).show();
                            }
                        }catch (Exception e){
                            Log.d("Lihat", "onResponse BaseHomeFragment : " + e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mSwipeRefreshLayout.setRefreshing(false);

                        Snackbar.make(getActivity().findViewById(android.R.id.content), error.toString(), Snackbar.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);

    }

    public static void recycleAutoScroll(int speedScroll, ArrayList<Viewpager_items> arrayList, final RecyclerView recyclerView) {
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            int count = 0;
            boolean flag = true;

            @Override
            public void run() {
                if (count < arrayList.size()) {
                    //loop back
                    if (count == arrayList.size() - 1) {
                        flag = false;
                    } else if (count == 0) {
                        flag = true;
                    }

                    if (flag) count++;
                    else count--;

                    recyclerView.smoothScrollToPosition(count);
                    handler.postDelayed(this, speedScroll);
                }
            }
        };

        handler.postDelayed(runnable, speedScroll);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(getActivity(), PostAllActivity.class);
        if (view == tvMoreRecentfvbi) {
            intent.putExtra(ISeasonConfig.TYPE_POST, "recent");
        } else if (view == tvMorePopularfvbi) {
            intent.putExtra(ISeasonConfig.TYPE_POST, "popular");
        } else if (view == tvMoreRandomfvbi) {
            intent.putExtra(ISeasonConfig.TYPE_POST, "random");
        } else if (view == tvMoreTopVideofvbi) {
            intent.putExtra(ISeasonConfig.TYPE_POST, "video");
        } else if (view == tvMoreVotingPollfvbi) {
            intent.putExtra(ISeasonConfig.TYPE_POST, "voting");
        }
        startActivity(intent);
        //finish();
    }

     /*@Override
    public void onResume() {
        super.onResume();

        try {
            log.d("ONRESUME_CALLED", "CALLED");
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                    callHomePageAPICheck();
                }
            });
        } catch (NullPointerException e) {
            log.d("Error", "Error");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mShimmerViewContainer != null) {
            mShimmerViewContainer.stopShimmerAnimation();
        }
    }*/
}
