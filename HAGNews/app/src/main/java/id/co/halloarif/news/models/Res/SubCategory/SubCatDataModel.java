package id.co.halloarif.news.models.Res.SubCategory;

class SubCatDataModel {
    public String id;
    public String lang_id;
    public String name;
    public String name_slug;
    public String parent_id;
    public String description;
    public String keywords;
    public String color;
    public String block_type;
    public String category_order;
    public String show_at_homepage;
    public String show_on_menu;
    public String created_at;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLang_id() {
        return lang_id;
    }

    public void setLang_id(String lang_id) {
        this.lang_id = lang_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName_slug() {
        return name_slug;
    }

    public void setName_slug(String name_slug) {
        this.name_slug = name_slug;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBlock_type() {
        return block_type;
    }

    public void setBlock_type(String block_type) {
        this.block_type = block_type;
    }

    public String getCategory_order() {
        return category_order;
    }

    public void setCategory_order(String category_order) {
        this.category_order = category_order;
    }

    public String getShow_at_homepage() {
        return show_at_homepage;
    }

    public void setShow_at_homepage(String show_at_homepage) {
        this.show_at_homepage = show_at_homepage;
    }

    public String getShow_on_menu() {
        return show_on_menu;
    }

    public void setShow_on_menu(String show_on_menu) {
        this.show_on_menu = show_on_menu;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
