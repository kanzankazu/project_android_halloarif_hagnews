package id.co.halloarif.news.activities_fragments.afnew.activity_new.logreg;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.HashMap;
import java.util.Map;

import id.co.halloarif.news.R;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Res.User.UserMainModel;
import id.co.halloarif.news.support.NetworkUtil;
import id.co.halloarif.news.utils.TextCaptcha;

public class RegisterNewActivity extends AppCompatActivity implements View.OnClickListener {
    private VideoView vvRegfvbi;
    ImageView ivRegCaptchafvbi;
    ImageView ivRegCaptchaReloadfvbi;
    EditText etRegCaptchafvbi;
    EditText etRegUsernamefvbi;
    EditText etRegEmailfvbi;
    EditText etRegPassfvbi;
    EditText etRegPassConfrmfvbi;
    Button bRegSubmitfvbi;
    private TextView tvRegLoginfvbi;
    private TextView tvRegForgetPassfvbi;

    TextCaptcha textCaptcha;
    String txt_username;
    String text_email;
    String txt_confirm_pas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();

    }

    private void initComponent() {
        vvRegfvbi = (VideoView) findViewById(R.id.vvReg);
        etRegUsernamefvbi = (EditText) findViewById(R.id.etRegUsername);
        etRegEmailfvbi = (EditText) findViewById(R.id.etRegEmail);
        etRegPassfvbi = (EditText) findViewById(R.id.etRegPass);
        etRegPassConfrmfvbi = (EditText) findViewById(R.id.etRegPassConfrm);
        ivRegCaptchafvbi = (ImageView) findViewById(R.id.ivRegCaptcha);
        ivRegCaptchaReloadfvbi = (ImageView) findViewById(R.id.ivRegCaptchaReload);
        etRegCaptchafvbi = (EditText) findViewById(R.id.etRegCaptcha);
        bRegSubmitfvbi = (Button) findViewById(R.id.bRegSubmit);
        tvRegLoginfvbi = (TextView) findViewById(R.id.tvRegLogin);
        tvRegForgetPassfvbi = (TextView) findViewById(R.id.tvRegForgetPass);
    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {
        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.news);
        vvRegfvbi.setVideoURI(uri);

        textCaptcha = new TextCaptcha(ivRegCaptchafvbi.getMaxWidth(), ivRegCaptchafvbi.getMaxHeight(), 4, TextCaptcha.TextOptions.NUMBERS_AND_LETTERS);
        ivRegCaptchafvbi.setImageBitmap(textCaptcha.getImage());

    }

    private void initListener() {
        vvRegfvbi.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
                mp.setLooping(true);
            }
        });
        ivRegCaptchaReloadfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textCaptcha = new TextCaptcha(ivRegCaptchafvbi.getMaxWidth(), ivRegCaptchafvbi.getMaxHeight(), 4, TextCaptcha.TextOptions.NUMBERS_AND_LETTERS);
                ivRegCaptchafvbi.setImageBitmap(textCaptcha.getImage());
            }
        });
        bRegSubmitfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etRegUsernamefvbi.getText().toString().isEmpty()) {
                    etRegUsernamefvbi.setError("Enter Username");
                } else if (etRegEmailfvbi.getText().toString().isEmpty()) {
                    etRegEmailfvbi.setError("Enter Email");
                } else if (etRegPassfvbi.getText().toString().isEmpty()) {
                    etRegPassfvbi.setError("Enter Password");
                } else if (etRegPassConfrmfvbi.getText().toString().isEmpty()) {
                    etRegPassConfrmfvbi.setError("Enter Confirm Password");
                } else if (etRegCaptchafvbi.getText().toString().isEmpty()) {
                    etRegCaptchafvbi.setError("Enter TextCaptchaAbstract");
                } else {
                    if (etRegPassfvbi.getText().toString().contentEquals(etRegPassConfrmfvbi.getText().toString())) {

                        if (!textCaptcha.checkAnswer(etRegCaptchafvbi.getText().toString().trim())) {
                            etRegCaptchafvbi.setError("TextCaptchaAbstract is not match");
                            etRegCaptchafvbi.setText("");
                            //  numberOfCaptchaFalse++;
                        } else {
                            Log.d("Main", "captcha is match!");
                            txt_username = etRegUsernamefvbi.getText().toString();
                            txt_confirm_pas = etRegPassConfrmfvbi.getText().toString();
                            text_email = etRegEmailfvbi.getText().toString();
                            callRegisterApiCheck(txt_username, text_email, txt_confirm_pas);
                        }
                    } else {
                        etRegPassConfrmfvbi.setError("Password not matched");
                    }
                }
            }
        });

        tvRegLoginfvbi.setOnClickListener(this);
    }

    private void callRegisterApiCheck(final String username, final String txt_email, final String txt_password) {
        if (NetworkUtil.isConnected(getApplicationContext())) {
            callRegisterApi(username, txt_email, txt_password);
        } else {
            Snackbar.make(findViewById(android.R.id.content), R.string.no_internet_connection, Snackbar.LENGTH_INDEFINITE).setAction("Re-Login", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callRegisterApi(username, txt_email, txt_password);
                }
            }).show();
        }
    }

    private void callRegisterApi(final String username, final String txt_email, final String txt_password) {
        String URL = Constants.MAIN_URL + "register";
        Log.d("Lihat", "callRegisterApi RegisterNewActivity : " + Constants.MAIN_URL + "register/" + username + "/" + txt_email + "/" + txt_password);

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest strRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        JsonParser parser = new JsonParser();
                        JsonElement mJson = parser.parse(response);
                        Gson gson = new Gson();
                        UserMainModel mainModel = gson.fromJson(mJson, UserMainModel.class);
                        if (mainModel.getStatus() == 1) {
                            new AlertDialog.Builder(RegisterNewActivity.this)
                                    .setTitle("Information")
                                    .setMessage("A confirmation email has been sent to your email address for activation. Please confirm your account\n" +
                                            "if there's no confirmation email, you can contact us or resend email from web(Login first and goto setting)")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            Intent intent = new Intent(Intent.ACTION_MAIN);
                                            intent.addCategory(Intent.CATEGORY_APP_EMAIL);
                                            RegisterNewActivity.this.startActivity(intent);
                                            finish();
                                        }
                                    })
                                    .show();
                        } else {
                            Snackbar.make(findViewById(android.R.id.content), mainModel.getError_json(), Snackbar.LENGTH_SHORT).show();
                        }

                        /*Log.e("tab_DATA_register", "" + response.trim());
                        try {

                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonobject = ojs.getJSONObject("get_json_data");
                                String id = jsonobject.getString("id");
                                String username = jsonobject.getString("username");
                                String email = jsonobject.getString("email");
                                String avatar = jsonobject.getString("avatar");
                                String about_me = jsonobject.getString("about_me");
                                if (avatar.equalsIgnoreCase("null")) {
                                    avatar = "";
                                }
                                Constants.USER_EMAIL = email;
                                Preference_saved.getInstance(getApplicationContext()).set_check_login("true");
                                Preference_saved.getInstance(getApplicationContext()).setUsername(username);
                                Preference_saved.getInstance(getApplicationContext()).setUser_Email(email);
                                Preference_saved.getInstance(getApplicationContext()).setuserPicture(avatar);
                                Preference_saved.getInstance(getApplicationContext()).setUser_id(id);
                                Toast.makeText(getApplicationContext(), "Successfully Registered", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                String error = ojs.getString("error");
                                Snackbar.make(findViewById(android.R.id.content), error, Snackbar.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            Log.d("Lihat", "onResponse RegisterNewActivity : " + e.getMessage());
                        }*/
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (!TextUtils.isEmpty(error.toString())) {
                            Snackbar.make(findViewById(android.R.id.content), error.toString(), Snackbar.LENGTH_SHORT).show();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", username);
                params.put("email", txt_email);
                params.put("password", txt_password);
                return params;
            }
        };
        requestQueue.add(strRequest);
    }

    @Override
    public void onClick(View view) {
        if (view == tvRegLoginfvbi) {
            moveToLogin();
        } else if (view == tvRegForgetPassfvbi) {
        }
    }

    private void moveToLogin() {
        Intent intent = new Intent(RegisterNewActivity.this, LoginNewActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        moveToLogin();
    }
}
