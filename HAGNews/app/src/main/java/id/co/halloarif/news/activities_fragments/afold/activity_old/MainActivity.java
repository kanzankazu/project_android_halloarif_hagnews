package id.co.halloarif.news.activities_fragments.afold.activity_old;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;

import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afold.activity_old.logreg.Login_Activity;
import id.co.halloarif.news.activities_fragments.afold.activity_old.logreg.Register_Activity;
import id.co.halloarif.news.activities_fragments.afold.activity_old.logreg.Update_Profile_Activity;
import id.co.halloarif.news.activities_fragments.afold.frag_old.All_Post_Fragment;
import id.co.halloarif.news.activities_fragments.afold.frag_old.Change_Password_Fragment;
import id.co.halloarif.news.activities_fragments.afold.frag_old.Reading_list_Fragment;
import id.co.halloarif.news.activities_fragments.afold.frag_old.Tab_Fragment;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recycler_Comments_adapter;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.utils.preference.Preference_saved;
import id.co.halloarif.news.utils.slideview.DraggableListener;
import id.co.halloarif.news.utils.slideview.DraggableView;
import id.co.halloarif.news.utils.CircleImageView;
import id.co.halloarif.news.utils.KeepScreenOnHandler;
import id.co.halloarif.news.utils.MediaPlayerSingleton;
import id.co.halloarif.news.utils.Volley_multipart.MySingleton;
import id.co.halloarif.news.utils.Volley_multipart.VolleyMultipartRequest;
import id.co.halloarif.news.utils.YouTubeFailureRecoveryActivity;
import id.co.halloarif.news.utils.YoutubeDeveloperKey;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.bumptech.glide.request.RequestOptions.centerCropTransform;

public class MainActivity extends YouTubeFailureRecoveryActivity implements View.OnClickListener {
    private static final String PREFERENCES_FILE = "mymaterialapp_settings";
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";
    private static final int DELAY_MILLIS = 10;
    private static final String LOG_TAG = "CheckNetworkStatus";
    public DraggableView draggableView;
    RecyclerView recyclerview_comments;
    Recycler_Comments_adapter recycler_comments_adapter;
    ArrayList<Viewpager_items> arrayList_comments = new ArrayList<>();
    public TextView Tv_video_name, Tv_video_views, txtview_video_content, txtview_title, Tv_no_commments, txt_No_internet_connection, text_header_username, textView_login_logout;
    NavigationView mNavigationView;
    private GestureDetector gestureDetector;
    //  ViewPager viewPager;
    // TabLayout tabLayout;
    ArrayList<Viewpager_items> arrayList_viewpager = new ArrayList<>();
    YouTubePlayer player;
    YouTubePlayerFragment youTubePlayerFragment;
    String url_final, video_id1, str_title_slug;
    String userid;
    ImageView Iv_share_video, Iv_Add_favorite, Iv_navigate_top_backgrd;
    boolean doubleBackToExitPressedOnce = false;
    boolean bool_draggableView_closed_left = false;
    CircleImageView circularimage_header_User;
    FrameLayout container_post, nav_contentframe, Frame_loader, Frame_loader_main_screen;
    LinearLayout linear_home, linear_recent, linear_popular, linear_random, linear_videos, linear_voting, linear_aboutus, linear_shareapp, linear_rateapp, linear_moreapps, linear_user_settings, linear_reading_list, linear_update_profile, linear_change_pass;
    PopupWindow popupWindow_selct;
    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private boolean mUserLearnedDrawer;
    private int mCurrentSelectedPosition;
    private MyPlaylistEventListener playlistEventListener;
    private MyPlayerStateChangeListener playerStateChangeListener;
    private MyPlaybackEventListener playbackEventListener;
    private NetworkChangeReceiver receiver;
    private boolean isConnected = false;
    ImageButton IB_submit_comment;
    String txt_comment, str_share_facebook, str_share_twitter, str_share_plus_google, str_share_linkedin, str_share_tumblr, str_share_pinterest;
    ImageView IV_linkedin, IV_tumblr, IV_Pinterest, IV_Googleplus, IV_Twitter, IV_facebook;
    EditText edttxt_text_comment;
    public static final int RequestPermissionCode = 1;
    public ImageView fab;
    ViewGroup Frame_moveable_lay;
    float dX;
    float dY;
    int lastAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init_Declare();
        if (checkPermission()) {
            Log.e("PERMISSION", "CHECK");
        } else {
            requestPermission();
        }
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkChangeReceiver();
        registerReceiver(receiver, filter);
        mUserLearnedDrawer = Boolean.valueOf(readSharedSetting(this, PREF_USER_LEARNED_DRAWER, "false"));
        Set_Navigation_Toolbar();
        initializeDraggableView();

        try {
            Tab_Fragment homeFragment = new Tab_Fragment();
            getSupportFragmentManager().beginTransaction().add(R.id.nav_contentframe, homeFragment).commit();
        } catch (Exception e) {
            Log.e("ERROR_CREATE_FRAGMENT", "" + e);
        }

        if (savedInstanceState == null) {
         /*   new Thread(new Runnable() {
                @Override
                public void run() {

                }
            }).start();*/
        } else {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
        }

        youTubePlayerFragment = (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_fragment);
        youTubePlayerFragment.initialize(YoutubeDeveloperKey.DEVELOPER_KEY, this);
        playlistEventListener = new MyPlaylistEventListener();
        playerStateChangeListener = new MyPlayerStateChangeListener();
        playbackEventListener = new MyPlaybackEventListener();

    }

    public static void saveSharedSetting(Context ctx, String settingName, String settingValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(settingName, settingValue);
        editor.apply();
    }

    public static String readSharedSetting(Context ctx, String settingName, String defaultValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        return sharedPref.getString(settingName, defaultValue);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE, CAMERA, READ_EXTERNAL_STORAGE}, RequestPermissionCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean camaraPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean readPermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    if (StoragePermission && camaraPermission && readPermission) {
                        // Toast.makeText(this, "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        //  Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    public boolean checkPermission() {

        int result = ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(this, CAMERA);
        int read_result1 = ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE);


        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && read_result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void init_Declare() {
        mToolbar = findViewById(R.id.toolbar);
        mDrawerLayout = findViewById(R.id.nav_drawer);
        mNavigationView = findViewById(R.id.nvBHA);
        // View hView = mNavigationView.getHeaderView(0);
        gestureDetector = new GestureDetector(this, new SingleTapConfirm());
        circularimage_header_User = findViewById(R.id.circularimage_header_User);
        Iv_navigate_top_backgrd = findViewById(R.id.Iv_navigate_top_backgrd);
        Glide.with(getApplicationContext()).load(R.drawable.nav_header_bg_small).apply(centerCropTransform()).into(Iv_navigate_top_backgrd);
        text_header_username = findViewById(R.id.text_header_username);
        container_post = findViewById(R.id.container_post);
        nav_contentframe = findViewById(R.id.nav_contentframe);
        Iv_share_video = findViewById(R.id.Iv_share_video);
        Iv_Add_favorite = findViewById(R.id.Iv_Add_favorite);
        txt_No_internet_connection = findViewById(R.id.txt_No_internet_connection);
        Tv_video_name = findViewById(R.id.Tv_video_name);
        Tv_video_views = findViewById(R.id.Tv_video_views);
        Tv_no_commments = findViewById(R.id.Tv_no_commments);
        txtview_video_content = findViewById(R.id.txtview_video_content);
        txtview_title = findViewById(R.id.txtview_title);
        linear_home = findViewById(R.id.linear_home);
        linear_recent = findViewById(R.id.linear_recent);
        linear_popular = findViewById(R.id.linear_popular);
        linear_random = findViewById(R.id.linear_random);
        linear_videos = findViewById(R.id.linear_videos);
        linear_voting = findViewById(R.id.linear_voting);
        linear_aboutus = findViewById(R.id.linear_aboutus);
        linear_shareapp = findViewById(R.id.linear_shareapp);
        linear_rateapp = findViewById(R.id.linear_rateapp);
        linear_moreapps = findViewById(R.id.linear_moreapps);
        Frame_loader = findViewById(R.id.Frame_loader);
        Frame_loader_main_screen = findViewById(R.id.Frame_loader_main_screen);
        textView_login_logout = findViewById(R.id.textView_login_logout);
        linear_user_settings = findViewById(R.id.linear_user_settings);
        linear_reading_list = findViewById(R.id.linear_reading_list);
        linear_update_profile = findViewById(R.id.linear_update_profile);
        linear_change_pass = findViewById(R.id.linear_change_pass);
        IB_submit_comment = findViewById(R.id.IB_submit_comment);
        edttxt_text_comment = findViewById(R.id.edttxt_text_comment);
        IB_submit_comment.setOnClickListener(this);

        IV_facebook = findViewById(R.id.IV_facebook);
        IV_Googleplus = findViewById(R.id.IV_Googleplus);
        IV_linkedin = findViewById(R.id.IV_linkedin);
        IV_tumblr = findViewById(R.id.IV_tumblr);
        IV_Twitter = findViewById(R.id.IV_Twitter);
        IV_Pinterest = findViewById(R.id.IV_Pinterest);

        draggableView = findViewById(R.id.draggable_view);
        recyclerview_comments = findViewById(R.id.recyclerview_comments);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        recyclerview_comments.setLayoutManager(mLayoutManager);
        recyclerview_comments.setHasFixedSize(true);
        recyclerview_comments.setNestedScrollingEnabled(false);

        Iv_share_video.setOnClickListener(this);
        IV_facebook.setOnClickListener(this);
        IV_Pinterest.setOnClickListener(this);
        IV_Twitter.setOnClickListener(this);
        IV_tumblr.setOnClickListener(this);
        IV_Googleplus.setOnClickListener(this);
        IV_linkedin.setOnClickListener(this);

        linear_update_profile.setOnClickListener(this);
        linear_reading_list.setOnClickListener(this);
        linear_change_pass.setOnClickListener(this);
        linear_home.setOnClickListener(this);
        linear_recent.setOnClickListener(this);
        linear_popular.setOnClickListener(this);
        linear_videos.setOnClickListener(this);
        linear_voting.setOnClickListener(this);
        linear_aboutus.setOnClickListener(this);
        linear_shareapp.setOnClickListener(this);
        linear_rateapp.setOnClickListener(this);
        linear_moreapps.setOnClickListener(this);
        linear_random.setOnClickListener(this);
        Iv_Add_favorite.setOnClickListener(this);
        textView_login_logout.setOnClickListener(this);

        //params = (AppBarLayout.LayoutParams) mToolbar.getLayoutParams();
        //  params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
        if (mToolbar != null) {
            mToolbar.setTitle("HAGNews");
            //setSupportActionBar(mToolbar);
        }

        fab = findViewById(R.id.fab);
        Frame_moveable_lay = findViewById(R.id.Frame_moveable_lay);
        fab.setOnTouchListener(onTouchListener());


    }

    private View.OnTouchListener onTouchListener() {
        return new View.OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if (gestureDetector.onTouchEvent(event)) {
                    if (MediaPlayerSingleton.getInstance().getMediaPlayer() != null) {
                        Log.e("MEDIA_PLAYER", "NOT NULL");

                        if (MediaPlayerSingleton.getInstance().getMediaPlayer().isPlaying()) {
                            fab.setImageResource(R.drawable.ic_play_two);
                            MediaPlayerSingleton.getInstance().getMediaPlayer().pause();
                            //Snackbar.make(view, "Stream Paused", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        } else {
                            //  Snackbar.make(view, "Stream Playing", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            fab.setImageResource(R.drawable.ic_pause_two);
                            MediaPlayerSingleton.getInstance().getMediaPlayer().start();
                        }
                    } else {
                        Log.e("MEDIA_PLAYER", "NULL");
                        MediaPlayerSingleton.getInstance().getMediaPlayer();
                    }
                    //  Toast.makeText(getApplicationContext(), "hello sucessfull", Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    switch (event.getActionMasked()) {

                        case MotionEvent.ACTION_DOWN:
                            dX = view.getX() - event.getRawX();
                            dY = view.getY() - event.getRawY();
                            lastAction = MotionEvent.ACTION_DOWN;
                            break;
                        case MotionEvent.ACTION_MOVE:

                            view.setY(event.getRawY() + dY);
                            view.setX(event.getRawX() + dX);

                            lastAction = MotionEvent.ACTION_MOVE;
                            break;

                        case MotionEvent.ACTION_UP:
                            if (lastAction == MotionEvent.ACTION_DOWN)
                                //  Toast.makeText(MainActivity.this, "Clicked!", Toast.LENGTH_SHORT).show();
                                break;

                        default:
                            return false;
                    }
                }
                return true;
            }
        };
    }

    public void PLAYER_INITIALIZE(String url) {
        if (url.contains("https://youtube.com/embed/")) {
            url_final = url.replaceAll("https://youtube.com/embed/*", "").trim();
            Log.e("URL_FINAL", url_final);
        }
        playVideoAtSelection(url_final);
    }

    public void CAll_API_ALL_COMMENTS(String video_id) {

        String check = Preference_saved.getInstance(getApplicationContext()).check_login();
        Log.d("checklogin", check);
        if (check != null) {
            if (check.contentEquals("true")) {
                userid = Preference_saved.getInstance(getApplicationContext()).getUser_id();
            } else {
                userid = "0";
            }
        }

        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        Frame_loader.setVisibility(View.VISIBLE);
        String URL = Constants.MAIN_URL + "post_comment/" + video_id + "/" + userid;
        Log.e("URL_OPEN_POST", URL);
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("detail_news", "" + response.trim());
                        try {
                            arrayList_comments.clear();
                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                                Object check = jsonObject.get("user_comment");
                                if (check instanceof JSONObject) {

                                    JSONObject jsonArray123 = jsonObject.getJSONObject("user_comment");
                                    String error = jsonArray123.getString("error");
                                    Tv_no_commments.setVisibility(View.VISIBLE);
                                    // Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                    Frame_loader.setVisibility(View.GONE);

                                } else if (check instanceof JSONArray) {
                                    JSONArray jsonObject1 = jsonObject.getJSONArray("user_comment");
                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject obj = jsonObject1.getJSONObject(i);
                                        String comment_user_like_count = obj.getString("user_like_count");
                                        String user_reply_count = obj.getString("user_reply_count");
                                        String comment_id = obj.getString("comment_id");
                                        String comment_post_id = obj.getString("comment_post_id");
                                        String comment_user_id = obj.getString("comment_user_id");
                                        String user_like_comment = obj.getString("user_like_comment");
                                        String comment_user_image = Constants.IMAGE_URL + obj.getString("comment_user_image");
                                        String comment_user_name = obj.getString("comment_user_name");
                                        String comment_parent_id = obj.getString("comment_parent_id");
                                        String comment_content = obj.getString("comment_content");
                                        String comment_created_at = obj.getString("comment_created_at");

                                        arrayList_comments.add(new Viewpager_items(comment_user_like_count, user_reply_count, comment_id, comment_post_id, comment_user_id, user_like_comment, comment_user_image, comment_user_name, comment_parent_id, comment_content, comment_created_at, "", ""));
                                        Frame_loader.setVisibility(View.GONE);
                                    }

                                    if (arrayList_comments.isEmpty()) {
                                        Tv_no_commments.setVisibility(View.VISIBLE);
                                    } else {
                                        Tv_no_commments.setVisibility(View.GONE);
                                        recycler_comments_adapter = new Recycler_Comments_adapter(MainActivity.this, R.layout.item_layout_comments, arrayList_comments);
                                        recyclerview_comments.setAdapter(recycler_comments_adapter);
                                        recycler_comments_adapter.notifyDataSetChanged();
                                    }
                                }
                            } else {
                                Log.e("error", "0");
                               /*String error = ojs.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);*/
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Frame_loader.setVisibility(View.GONE);
                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);
    }

    public void Call_VIDEO_Details_API(String video_id) {
        String userid = Preference_saved.getInstance(getApplicationContext()).getUser_id();
        String user_id_check;
        if (userid != null) {
            user_id_check = userid;
        } else {
            user_id_check = "";
        }
        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(getApplicationContext());

        String URL = Constants.MAIN_URL + "video/" + video_id + "/" + user_id_check;
        Log.e("URL_OPEN_POST", URL);
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("detail_news", "" + response.trim());
                        try {
                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonObject = ojs.getJSONObject("get_json_data");
                                String check = Preference_saved.getInstance(getApplicationContext()).check_login();
                                Log.d("checklogin", check);
                                if (check != null) {
                                    if (check.contentEquals("true")) {
                                        String user_reading_list = jsonObject.getString("user_reading_list");
                                        if (user_reading_list.contentEquals("true")) {
                                            Iv_Add_favorite.setImageResource(R.drawable.ic_star_filled);
                                        } else {
                                            Iv_Add_favorite.setImageResource(R.drawable.ic_star_balck_empty);
                                        }
                                    }
                                }
                                JSONArray jsonArray12 = jsonObject.getJSONArray("video_post_data");
                                for (int i = 0; i < jsonArray12.length(); i++) {
                                    JSONObject obj = jsonArray12.getJSONObject(i);

                                    video_id1 = obj.getString("id");
                                    str_title_slug = obj.getString("title_slug");

                                    str_share_facebook = obj.getString("share_facebook");
                                    str_share_twitter = obj.getString("share_twitter");
                                    str_share_plus_google = obj.getString("share_plus_google");
                                    str_share_linkedin = obj.getString("share_linkedin");
                                    str_share_tumblr = obj.getString("share_tumblr");
                                    str_share_pinterest = obj.getString("share_pinterest");

                                }
                            } else {
                                Log.e("error", "0");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);
    }

    @Override
    public void onBackPressed() {

        Log.e("CHECK_OPEN_FRAG", "" + Constants.CHECK_OPEN_FRAG);

        /*if (draggableView != null && draggableView.isClosedAtLeft()) {
            int count1 = getSupportFragmentManager().getBackStackEntryCount();
            Log.e("STACK_COUNT>>CLOSE_DRAG", "" + count1);
            if (count1 > 0) {
                getSupportFragmentManager().popBackStackImmediate();
            } else {
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    if (doubleBackToExitPressedOnce) {
                        //imageLoader.clearCache();
                        super.onBackPressed();
                        return;
                    }
                    doubleBackToExitPressedOnce = true;
                    Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            doubleBackToExitPressedOnce = false;
                        }
                    }, 2000);
                }
            }
        } else {
            int count = getSupportFragmentManager().getBackStackEntryCount();
            Log.e("STACK_COUNT>>OPEN_DRAG", "" + count);
            if (count > 0) {
                if (draggableView != null && (draggableView.isMaximized())) {
                    Log.e("DRAG_CHECK", " MINIMIZED");
                    draggableView.minimize();
                } else {
                    getSupportFragmentManager().popBackStackImmediate();
                }
            } else {

                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    if (draggableView != null && (draggableView.isMaximized())) {
                        Log.e("DRAG_CHECK", " MINIMIZED");
                        draggableView.minimize();
                    } else if (draggableView != null && draggableView.isMinimized()) {
                        draggableView.closeToLeft();
                    } else if (draggableView != null && draggableView.isClosedAtLeft()) {
                        getSupportFragmentManager().popBackStackImmediate();
                        //SetFrameVisible(false);
                    } else {
                        draggableView.closeToLeft();
                    }
                }
            }
        }*/


        if (Constants.CHECK_OPEN_FRAG.equalsIgnoreCase("HOME")) {
            Log.e("CHECK", "" + "IN THIS");

            if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            } else if (draggableView.isClosedAtLeft()) {
                Log.e("CHECK", "" + "DONE");
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }
                doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Constants.CHECK_OPEN_FRAG = "HOME";
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            } else {
                if (draggableView != null && (draggableView.isMaximized())) {
                    Log.e("DRAG_CHECK", " MINIMIZED");
                    draggableView.minimize();
                } else if (draggableView != null && draggableView.isMinimized()) {
                    draggableView.closeToLeft();
                }
            }
        } else {
            if (draggableView != null && draggableView.isClosedAtLeft()) {
                int count1 = getSupportFragmentManager().getBackStackEntryCount();
                Log.e("STACK_COUNT>>CLOSE_DRAG", "" + count1);
                if (count1 > 0) {
                    mToolbar.setTitle(getResources().getString(R.string.app_name));
                    Constants.CHECK_OPEN_FRAG = "HOME";
                    getSupportFragmentManager().popBackStackImmediate();
                } else {
                    if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                    } else {
                        if (doubleBackToExitPressedOnce) {
                            //imageLoader.clearCache();
                            super.onBackPressed();
                            return;
                        }
                        doubleBackToExitPressedOnce = true;
                        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                doubleBackToExitPressedOnce = false;
                            }
                        }, 2000);
                    }
                }
            } else {
                int count = getSupportFragmentManager().getBackStackEntryCount();
                Log.e("STACK_COUNT>>OPEN_DRAG", "" + count);
                if (count > 0) {
                    if (draggableView != null && (draggableView.isMaximized())) {
                        Log.e("DRAG_CHECK", " MINIMIZED");
                        draggableView.minimize();
                    } else {
                        mToolbar.setTitle(getResources().getString(R.string.app_name));
                        Constants.CHECK_OPEN_FRAG = "HOME";
                        getSupportFragmentManager().popBackStackImmediate();
                    }
                } else {
                    if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                    } else {
                        if (draggableView != null && (draggableView.isMaximized())) {
                            Log.e("DRAG_CHECK", " MINIMIZED");
                            draggableView.minimize();
                        } else if (draggableView != null && draggableView.isMinimized()) {
                            draggableView.closeToLeft();
                        } else if (draggableView != null && draggableView.isClosedAtLeft()) {
                            getSupportFragmentManager().popBackStackImmediate();
                        } else {
                            draggableView.closeToLeft();
                        }
                    }

                }
            }
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (receiver != null) {
            unregisterReceiver(receiver);
        }
        if (player != null) {
            player.release();
        }
    }

    @SuppressLint("SetTextI18n")
    public void setText_values(String video_title, String video_views, String content, String created_at) {
        Tv_video_name.setText(video_title);

        String current_datetime = Constants.Current_Time_Zone();
        long api_datetime = Constants.getDateInMillis(created_at);
        long current_timeZone = Constants.getDateInMillis(current_datetime);
        Tv_video_views.setText(" " + video_views + "   |   " + DateUtils.getRelativeTimeSpanString(api_datetime, current_timeZone, DateUtils.SECOND_IN_MILLIS));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txtview_video_content.setText(Html.fromHtml(content, Html.FROM_HTML_MODE_COMPACT));
        } else {
            txtview_video_content.setText(Html.fromHtml(content));
        }
        txtview_title.setText(video_title);
    }

    private void initializeDraggableView() {
        draggableView.setVisibility(View.GONE);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                draggableView.setTopViewHeight(metrics.widthPixels / 16 * 9);
                draggableView.minimize();
                draggableView.closeToLeft();
                hookDraggableViewListener();
            }
        }, DELAY_MILLIS);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        String s = null;
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            s = "Landscape Orientation \n";
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            s = "Portrait Orintation \n";
        }

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            try {
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                draggableView.setTopViewHeight(metrics.heightPixels);
                player.setFullscreen(true);
            } catch (NullPointerException e) {
                Log.e("Nullpoint", "" + e);
            }
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            try {
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                draggableView.setTopViewHeight(metrics.widthPixels / 16 * 9);
                player.setFullscreen(false);
            } catch (NullPointerException e) {
                Log.e("Nullpoint", "" + e);
            }
        }

        //  s += "onConfigurationChanged was Called" + ((YoutubePlay) getApplicationContext()).inc() + "times";
    }

    public void hookDraggableViewListener() {
        draggableView.setDraggableListener(new DraggableListener() {
            @Override
            public void onMaximized() {
                try {
                    draggableView.setClickToMinimizeEnabled(false);
                } catch (NullPointerException e) {
                    Log.e("Nullpoint", "" + e);
                }
            }

            @Override
            public void onMinimized() {
                try {

                    draggableView.setClickToMaximizeEnabled(true);
                } catch (NullPointerException e) {
                    Log.e("Nullpoint", "" + e);
                }
            }

            @Override
            public void onClosedToLeft() {
                bool_draggableView_closed_left = true;
                // check_dragview_maximize = false;
            }

            @Override
            public void onClosedToRight() {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void changeNavItemBG(String abc) {
        if (abc.equals("HAGNews")) {
            linear_home.setBackground(getResources().getDrawable(R.drawable.bg_nav_text_white));
            linear_recent.setBackground(null);
            linear_random.setBackground(null);
            linear_popular.setBackground(null);
            linear_videos.setBackground(null);
            linear_voting.setBackground(null);
        } else if (abc.equals("Latest Post")) {
            linear_recent.setBackground(getResources().getDrawable(R.drawable.bg_nav_text_white));
            linear_home.setBackground(null);
            linear_random.setBackground(null);
            linear_popular.setBackground(null);
            linear_videos.setBackground(null);
            linear_voting.setBackground(null);
        } else if (abc.equals("Popular Post")) {
            linear_popular.setBackground(getResources().getDrawable(R.drawable.bg_nav_text_white));
            linear_recent.setBackground(null);
            linear_home.setBackground(null);
            linear_random.setBackground(null);
            linear_videos.setBackground(null);
            linear_voting.setBackground(null);
        } else if (abc.equals("Random Post")) {
            linear_popular.setBackground(null);
            linear_recent.setBackground(null);
            linear_random.setBackground(getResources().getDrawable(R.drawable.bg_nav_text_white));
            linear_home.setBackground(null);
            linear_videos.setBackground(null);
            linear_voting.setBackground(null);
        } else if (abc.equals("Top Videos")) {
            linear_videos.setBackground(getResources().getDrawable(R.drawable.bg_nav_text_white));
            linear_popular.setBackground(null);
            linear_recent.setBackground(null);
            linear_random.setBackground(null);
            linear_home.setBackground(null);
            linear_voting.setBackground(null);
        } else if (abc.equals("Voting Poll")) {
            linear_voting.setBackground(getResources().getDrawable(R.drawable.bg_nav_text_white));
            linear_videos.setBackground(null);
            linear_popular.setBackground(null);
            linear_recent.setBackground(null);
            linear_random.setBackground(null);
            linear_home.setBackground(null);
        }
    }

    public void loadFrag(final Fragment fragment) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    getSupportFragmentManager().beginTransaction().setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).replace(R.id.nav_contentframe, fragment).addToBackStack(null).commit();

                    Log.e("CREATE_FRAGMENT", "" + "CLICK");
                } catch (Exception e) {
                    Log.e("ERROR_CREATE_FRAGMENT", "" + e);
                }
            }
        }).start();
    }

    public void CreateFragment(final Fragment fragment) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    getSupportFragmentManager().beginTransaction().setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).add(R.id.container_post, fragment).addToBackStack(null).commit();
                    Log.e("CREATE_FRAGMENT", "" + "CLICK");
                } catch (Exception e) {
                    Log.e("ERROR_CREATE_FRAGMENT", "" + e);
                }
            }
        }).start();
    }

    @Override
    public void onClick(View v) {
        if (v == linear_home) {
            mCurrentSelectedPosition = 0;
            Constants.CHECK_OPEN_FRAG = "HOME";
            Tab_Fragment f1 = new Tab_Fragment();
            loadFrag(f1);
            mToolbar.setTitle("HAGNews");
            // params.setScrollFlags(1);
            changeNavItemBG("HAGNews");
            Constants.Content_Equals = "HAGNews";
        } else if (v == Iv_share_video) {
            if (str_title_slug != null) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Read more at " + Constants.BASE_URL + "post/" + str_title_slug);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        } else if (v == linear_recent) {
            mCurrentSelectedPosition = 1;
            Constants.CHECK_OPEN_FRAG = "Sell";
            All_Post_Fragment f1 = new All_Post_Fragment();
            loadFrag(f1);
            mToolbar.setTitle("Latest Post");
            //  params.setScrollFlags(0);
            changeNavItemBG("Latest Post");
            Constants.Content_Equals = "Latest Post";
        } else if (v == linear_popular) {
            mCurrentSelectedPosition = 2;
            Constants.CHECK_OPEN_FRAG = "Sell";
            All_Post_Fragment f1 = new All_Post_Fragment();
            loadFrag(f1);
            mToolbar.setTitle("Popular Post");
            // params.setScrollFlags(0);
            changeNavItemBG("Popular Post");
            Constants.Content_Equals = "Popular Post";
        } else if (v == linear_random) {
            mCurrentSelectedPosition = 3;
            Constants.CHECK_OPEN_FRAG = "Sell";
            All_Post_Fragment f1 = new All_Post_Fragment();
            loadFrag(f1);
            mToolbar.setTitle("Random Post");
            //  params.setScrollFlags(0);
            changeNavItemBG("Random Post");
            Constants.Content_Equals = "Random Post";
        } else if (v == linear_videos) {
            mCurrentSelectedPosition = 4;
            Constants.CHECK_OPEN_FRAG = "Sell";
            All_Post_Fragment f1 = new All_Post_Fragment();
            loadFrag(f1);
            mToolbar.setTitle("Top Videos");
            //  params.setScrollFlags(0);
            changeNavItemBG("Top Videos");
            Constants.Content_Equals = "Top Videos";

        } else if (v == linear_voting) {
            mCurrentSelectedPosition = 5;
            Constants.CHECK_OPEN_FRAG = "Sell";
            All_Post_Fragment f1 = new All_Post_Fragment();
            loadFrag(f1);
            mToolbar.setTitle("Voting Poll");
            //  params.setScrollFlags(0);
            changeNavItemBG("Voting Poll");
            Constants.Content_Equals = "Voting Poll";

        } else if (v == linear_shareapp) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Keep yourself updated with what's happening around you and world. Download the " + getResources().getString(R.string.app_name) + " from the following link " + " https://play.google.com/store/apps/details?id=" + getPackageName());
            sendIntent.setType("text/plain");
            startActivity(sendIntent);

        } else if (v == linear_aboutus) {
            initiatePopupWindow_about();
        } else if (v == linear_rateapp) {
            final String appName = getPackageName();//your application package name i.e play store application url
            Log.e("package:", appName);
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appName)));
            }
        } else if (v == linear_moreapps) {
            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            String link = "https://play.google.com/store/apps/developer?id=%F0%9F%8D%80Sekhon+Technologies%F0%9F%8D%80";
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
            }
            //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("")));
        } else if (v == linear_reading_list) {
            Constants.CHECK_OPEN_FRAG = "Sell";
            Constants.Cat_Name = "Reading List";
            CreateFragment(new Reading_list_Fragment());
        } else if (v == linear_update_profile) {
            Intent i = new Intent(MainActivity.this, Update_Profile_Activity.class);
            startActivity(i);
            // CreateFragment(new UpdateProfileNewActivity());
        } else if (v == linear_change_pass) {
            Constants.CHECK_OPEN_FRAG = "Sell";
            CreateFragment(new Change_Password_Fragment());
        } else if (v == textView_login_logout) {
            String check = Preference_saved.getInstance(getApplicationContext()).check_login();
            Log.d("checklogin", check);
            if (check != null) {
                if (check.contentEquals("true")) {
                    Snackbar.make(nav_contentframe, "Successfully log out", Snackbar.LENGTH_SHORT).show();
                    Preference_saved.getInstance(getApplicationContext()).set_check_login("false");
                    Preference_saved.getInstance(getApplicationContext()).setUser_id("");
                    text_header_username.setText("Welcome Guest");
                    circularimage_header_User.setVisibility(View.GONE);
                    textView_login_logout.setText("Already member!  Login");
                    textView_login_logout.setVisibility(View.VISIBLE);
                    linear_user_settings.setVisibility(View.GONE);
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    initiatePopupWindow_SelectActionLogin();
                }
            }
        } else if (v == IB_submit_comment) {
            String check = Preference_saved.getInstance(getApplicationContext()).check_login();
            Log.d("checklogin", check);
            if (check != null) {
                if (check.contentEquals("true")) {
                    txt_comment = edttxt_text_comment.getText().toString();
                    if (txt_comment.isEmpty()) {
                        edttxt_text_comment.setError("write some comment....");
                    } else {
                        Frame_loader.setVisibility(View.VISIBLE);

                        CALL_API_COMMENT();
                    }

                } else {
                    initiatePopupWindow_SelectActionLogin();
                }
            }
        } else if (v == Iv_Add_favorite) {
            String check = Preference_saved.getInstance(getApplicationContext()).check_login();
            Log.d("checklogin", check);
            if (check != null) {
                if (check.contentEquals("true")) {
                    CALL_API_ADD_FAVORITE();
                } else {
                    initiatePopupWindow_SelectActionLogin();
                }
            }
        } else if (v == IV_facebook) {
            if (!str_share_facebook.isEmpty()) {
                String url = str_share_facebook;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        } else if (v == IV_Twitter) {
            if (!str_share_twitter.isEmpty()) {
                String url = str_share_twitter;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "https://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        } else if (v == IV_Googleplus) {
            if (!str_share_plus_google.isEmpty()) {
                String url = str_share_plus_google;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        } else if (v == IV_linkedin) {
            if (!str_share_linkedin.isEmpty()) {
                String url = str_share_linkedin;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        } else if (v == IV_tumblr) {
            if (!str_share_tumblr.isEmpty()) {
                String url = str_share_tumblr;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        } else if (v == IV_Pinterest) {
            if (!str_share_pinterest.isEmpty()) {
                String url = str_share_pinterest;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    private void initiatePopupWindow_about() {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater inflater = (this).getLayoutInflater();
            View dialogLayout = inflater.inflate(R.layout.popup_about, null);
            final AlertDialog dialog = builder.create();
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            dialog.setView(dialogLayout, 0, 0, 0, 0);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(true);
            //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager.LayoutParams wlmp = dialog.getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER;

            ImageView imageView = dialogLayout.findViewById(R.id.Tv_popup_image);

            Glide.with(this)
                    .load("https://halloarif.co.id/news/uploads/logo/logo_5c541797b5c33.png")
                    .into(imageView);

            builder.setView(dialogLayout);

            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void CALL_API_COMMENT() {
        final String userid = Preference_saved.getInstance(getApplicationContext()).getUser_id();
        String URL = Constants.MAIN_URL + "add_post_comment";
        Log.e("URL_ADD_COMMENT", URL);
        // String url = "http://www.angga-ari.com/api/something/awesome";
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, URL, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    Log.e("RESPONSE_ADD_COMMENT", "" + resultResponse);
                    JSONObject ojs = new JSONObject(resultResponse);
                    String abc = ojs.getString("status");
                    Log.e("Value", abc);
                    if (Integer.parseInt(abc) == 1) {
                        JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                        JSONObject jsonObject1 = jsonObject.getJSONObject("post_comment");
                        String title = jsonObject1.getString("title");

                        Toast.makeText(getApplicationContext(), title, Toast.LENGTH_SHORT).show();
                        edttxt_text_comment.setText("");
                        CAll_API_ALL_COMMENTS(video_id1);
                        Frame_loader.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(getApplicationContext(), "Incorrect Data", Toast.LENGTH_SHORT).show();
                        //  Log.e("error", error);*//**//*
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");
                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("post_id", video_id1);
                params.put("user_id", userid);
                params.put("comment", txt_comment);
                return params;
            }

        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(multipartRequest);
    }

    private void CALL_API_ADD_FAVORITE() {
        String userid = Preference_saved.getInstance(getApplicationContext()).getUser_id();
        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        Frame_loader.setVisibility(View.VISIBLE);
        String URL = Constants.MAIN_URL + "add-reading-list/" + userid + "/" + video_id1;
        Log.e("URL_FAVORITE", URL);
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("DATA_FAVORITE", "" + response.trim());
                        try {

                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                                JSONObject jsonObject1 = jsonObject.getJSONObject("added_reading_post");
                                String status = jsonObject1.getString("status");
                                if (status.contentEquals("1")) {
                                    Iv_Add_favorite.setImageResource(R.drawable.ic_star_filled);
                                } else {
                                    Iv_Add_favorite.setImageResource(R.drawable.ic_star_balck_empty);
                                }
                                Frame_loader.setVisibility(View.GONE);

                            } else {
                                Frame_loader.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Frame_loader.setVisibility(View.GONE);
                    }
                }) {
        };
        requestQueue.add(strRequest);

    }

    private void initiatePopupWindow_SelectActionLogin() {
        try {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert inflater != null;
            View layout = inflater.inflate(R.layout.popup_select_action_login, (ViewGroup) findViewById(R.id.popup));

            RelativeLayout mainlayout = layout.findViewById(R.id.popup);

            mainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  popupWindow_selct.dismiss();
                }
            });
            ImageView close_dialog = layout.findViewById(R.id.ivPopForgetPassClose);
            close_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow_selct.dismiss();
                }
            });


            Button Btn_delete_playlist = layout.findViewById(R.id.Btn_login);
            Btn_delete_playlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow_selct.dismiss();
                    Intent i = new Intent(getApplicationContext(), Login_Activity.class);
                    startActivity(i);
                }
            });

            Button Btn_edit_playlist = layout.findViewById(R.id.Btn_regiter);
            Btn_edit_playlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow_selct.dismiss();
                    Intent i = new Intent(getApplicationContext(), Register_Activity.class);
                    startActivity(i);
                }
            });
            popupWindow_selct = new PopupWindow(layout, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT, true);
            popupWindow_selct.showAtLocation(layout, Gravity.CENTER, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Set_Navigation_Toolbar() {
        if (mToolbar != null) {
            ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(MainActivity.this, mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close) {
                @Override
                public void onDrawerClosed(View v) {
                    super.onDrawerClosed(v);
                }

                @Override
                public void onDrawerOpened(View v) {
                    super.onDrawerOpened(v);
                }

            };
            mDrawerLayout.addDrawerListener(actionBarDrawerToggle);
            actionBarDrawerToggle.syncState();

            if (!mUserLearnedDrawer) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
                mUserLearnedDrawer = true;
                saveSharedSetting(this, PREF_USER_LEARNED_DRAWER, "true");
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION, 0);
        /// Menu menu = mNavigationView.getMenu();
        ///  menu.getItem(mCurrentSelectedPosition).setChecked(true);
    }

    @Override
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubePlayerFragment;
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        this.player = youTubePlayer;
        player.setPlaylistEventListener(playlistEventListener);
        player.setPlayerStateChangeListener(playerStateChangeListener);
        player.setPlaybackEventListener(playbackEventListener);

        /*if (!wasRestored) {
            //playVideoAtSelection();
        }*/
    }

    private void playVideoAtSelection(String url_final) {
        if (player != null) {
            player.cueVideo(url_final);
            new KeepScreenOnHandler(player, getWindow());
        }
    }

    private void log(String message) {
        Log.e("VIDEO_RESPONSE", "" + message);
    }

    private String getTimesText() {
        int currentTimeMillis = player.getCurrentTimeMillis();
        int durationMillis = player.getDurationMillis();
        return String.format("(%s/%s)", formatTime(currentTimeMillis), formatTime(durationMillis));
    }

    @SuppressLint("DefaultLocale")
    private String formatTime(int millis) {
        int seconds = millis / 1000;
        int minutes = seconds / 60;
        int hours = minutes / 60;

        return (hours == 0 ? "" : hours + ":")
                + String.format("%02d:%02d", minutes % 60, seconds % 60);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public class NetworkChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            Log.v(LOG_TAG, "Receieved notification about network status");
            isNetworkAvailable(context);
        }

        private void isNetworkAvailable(Context context) {
            ConnectivityManager connectivity = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for (NetworkInfo anInfo : info) {
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            if (!isConnected) {
                                Log.v(LOG_TAG, "Now you are connected to Internet!");
                                isConnected = true;
                                txt_No_internet_connection.setVisibility(View.GONE);
                            }
                            return;
                        }
                    }
                }
            }
            Log.v(LOG_TAG, "You are not connected to Internet!");
            isConnected = false;
            txt_No_internet_connection.setVisibility(View.VISIBLE);
        }
    }

    private final class MyPlaylistEventListener implements YouTubePlayer.PlaylistEventListener {
        @Override
        public void onNext() {
            log("NEXT VIDEO");
        }

        @Override
        public void onPrevious() {
            log("PREVIOUS VIDEO");
        }

        @Override
        public void onPlaylistEnded() {
            log("PLAYLIST ENDED");
        }
    }

    private final class MyPlaybackEventListener implements YouTubePlayer.PlaybackEventListener {
        String playbackState = "NOT_PLAYING";
        String bufferingState = "";

        @Override
        public void onPlaying() {
            playbackState = "PLAYING";
            log("\tPLAYING " + getTimesText());
        }

        @Override
        public void onBuffering(boolean isBuffering) {
            bufferingState = isBuffering ? "(BUFFERING)" : "";
            log("\t\t" + (isBuffering ? "BUFFERING " : "NOT BUFFERING ") + getTimesText());
        }

        @Override
        public void onStopped() {
            playbackState = "STOPPED";
            log("\tSTOPPED");
        }

        @Override
        public void onPaused() {
            playbackState = "PAUSED";
            log("\tPAUSED " + getTimesText());
        }

        @Override
        public void onSeekTo(int endPositionMillis) {
            log(String.format("\tSEEKTO: (%s/%s)",
                    formatTime(endPositionMillis),
                    formatTime(player.getDurationMillis())));
        }
    }

    private final class MyPlayerStateChangeListener implements YouTubePlayer.PlayerStateChangeListener {
        String playerState = "UNINITIALIZED";

        @Override
        public void onLoading() {
            playerState = "LOADING";
            log(playerState);
        }

        @Override
        public void onLoaded(String videoId) {
            playerState = String.format("LOADED %s", videoId);
            log(playerState);
        }

        @Override
        public void onAdStarted() {
            playerState = "AD_STARTED";
            log(playerState);
        }

        @Override
        public void onVideoStarted() {
            playerState = "VIDEO_STARTED";
            log(playerState);
            if (MediaPlayerSingleton.getInstance().getMediaPlayer() != null) {
                if (MediaPlayerSingleton.getInstance().getMediaPlayer().isPlaying()) {

                    MediaPlayerSingleton.getInstance().getMediaPlayer().pause();
                    if (fab != null) {
                        fab.setVisibility(View.GONE);
                    }
                    Log.e("MEDIA_PLAYER", "true");
                } else {
                    MediaPlayerSingleton.getInstance().getMediaPlayer().pause();
                    Log.e("MEDIA_PLAYER", "false");
                }
            } else {
                Log.e("MEDIA_PLAYER", "NULL");
                MediaPlayerSingleton.getInstance().getMediaPlayer();
            }
        }

        @Override
        public void onVideoEnded() {
            playerState = "VIDEO_ENDED";
            log(playerState);
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason reason) {
            playerState = "ERROR (" + reason + ")";
            if (reason == YouTubePlayer.ErrorReason.UNEXPECTED_SERVICE_DISCONNECTION) {
                // When this error occurs the player is released and can no longer be used.
                player = null;
            }
            log(playerState);
        }
    }

    private BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (MediaPlayerSingleton.getInstance().getMediaPlayer() != null) {
                Log.e("MEDIA_PLAYER", "NOT NULL");
                if (MediaPlayerSingleton.getInstance().getMediaPlayer().isPlaying()) {
                    fab.setVisibility(View.VISIBLE);
                    fab.setImageResource(R.drawable.ic_pause_two);
                    Log.e("MEDIA_PLAYER", "true");
                } else {
                    Log.e("MEDIA_PLAYER", "false");
                    fab.setVisibility(View.GONE);
                    fab.setImageResource(R.drawable.ic_play_two);
                }
            } else {
                Log.e("MEDIA_PLAYER", "NULL");
                MediaPlayerSingleton.getInstance().getMediaPlayer();
            }
        }
    };


    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mNotificationReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mNotificationReceiver, new IntentFilter("KEY"));

        String check = Preference_saved.getInstance(getApplicationContext()).check_login();
        Log.d("ONRESUME", check);
        if (check != null) {
            if (check.contentEquals("true")) {
                String image = Preference_saved.getInstance(getApplicationContext()).getuserPicture();
                String username = Preference_saved.getInstance(getApplicationContext()).getUsername();
                if (image.contentEquals("")) {
                    circularimage_header_User.setVisibility(View.VISIBLE);
                    linear_user_settings.setVisibility(View.VISIBLE);
                    int image_array = R.drawable.ic_person;
                    Glide.with(getApplicationContext()).load(image_array)
                            .apply(centerCropTransform()
                                    .placeholder(R.drawable.ic_person)
                                    .error(R.drawable.ic_person)).into(circularimage_header_User);
                    text_header_username.setText(username);
                } else {
                    linear_user_settings.setVisibility(View.VISIBLE);
                    circularimage_header_User.setVisibility(View.VISIBLE);
                    Glide.with(getApplicationContext()).load(Constants.IMAGE_URL + image)
                            .apply(centerCropTransform()
                                    .placeholder(R.drawable.ic_person)
                                    .error(R.drawable.ic_person)).into(circularimage_header_User);
                    text_header_username.setText(username);
                }
                textView_login_logout.setVisibility(View.VISIBLE);
                textView_login_logout.setText("Log out");
            } else {
                text_header_username.setText("Welcome Guest");
                linear_user_settings.setVisibility(View.GONE);
                circularimage_header_User.setVisibility(View.GONE);
                textView_login_logout.setText("Already member!  Login");
                textView_login_logout.setVisibility(View.VISIBLE);
            }
        }
    }

    private class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapUp(MotionEvent event) {
            return true;
        }
    }


}