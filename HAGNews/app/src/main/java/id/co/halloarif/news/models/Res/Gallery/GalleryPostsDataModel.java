package id.co.halloarif.news.models.Res.Gallery;

class GalleryPostsDataModel {
    public String id;
    public String lang_id;
    public String title;
    public String category_id;
    public String path_big;
    public String path_small;
    public String created_at;
    public String category_name;
}
