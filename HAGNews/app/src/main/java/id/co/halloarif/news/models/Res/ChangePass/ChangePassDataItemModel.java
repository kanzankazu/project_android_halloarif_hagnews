package id.co.halloarif.news.models.Res.ChangePass;

public class ChangePassDataItemModel {
    public String change_pass;
    public ChangePassUserProfile user_profile;

    public String getChange_pass() {
        return change_pass;
    }

    public void setChange_pass(String change_pass) {
        this.change_pass = change_pass;
    }

    public ChangePassUserProfile getUser_profile() {
        return user_profile;
    }

    public void setUser_profile(ChangePassUserProfile user_profile) {
        this.user_profile = user_profile;
    }
}
