package id.co.halloarif.news.activities_fragments.afold.frag_old;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afold.activity_old.MainActivity;
import id.co.halloarif.news.activities_fragments.afold.activity_old.Open_Post_Activity;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_Adapter_news;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.utils.Recycleritemclicklistener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ST_004 on 15-03-2018.
 */

public class SubCategory_post_Fragment extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    TextView text_header;
    RecyclerView recyclerview_post;
    ArrayList<Viewpager_items> arrayList_all_post = new ArrayList<>();
    String tag_slug;
    ImageView Iv_back;
    FragmentActivity fragmentActivity;
    SwipeRefreshLayout mSwipeRefreshLayout;
    FrameLayout frame_no_data;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_post_data, container, false);

        initComponent(view);
        if (Constants.Cat_Tag_Name != null) {
            text_header.setText(Constants.Cat_Tag_Name);
        }

        mSwipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                mSwipeRefreshLayout.setRefreshing(true);

                // Fetching data from server
                CALL_API_Category_POST();
            }
        });


        recyclerview_post.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Viewpager_items model = arrayList_all_post.get(position);
                        CLICK_METHOD(model);
                    }

                    @Override
                    public void onLongClick(View view, int position) {
                    }
                })
        );

        return view;
    }


    public void CLICK_METHOD(Viewpager_items model) {
        String post_type = model.getPost_type();
        if (post_type.contentEquals("post")) {
            Intent i = new Intent(getActivity(), Open_Post_Activity.class);
            i.putExtra("POST_ID", model.getId());
            startActivity(i);
        } else if (post_type.contentEquals("audio")) {
            ((MainActivity) fragmentActivity).CreateFragment(Audio_post_Fragment.newInstance(model.getId()));
        } else {

            String embed_url = model.getEmbed_code();
            String video_title = model.getTitle();
            String video_id = model.getId();
            String created_at = model.getCreated_at();
            String video_views = model.getHit();
            String content = model.getContent();

            ((MainActivity) fragmentActivity).draggableView.setVisibility(View.VISIBLE);
            ((MainActivity) fragmentActivity).draggableView.maximize();
            ((MainActivity) fragmentActivity).Call_VIDEO_Details_API(video_id);
            ((MainActivity) fragmentActivity).CAll_API_ALL_COMMENTS(video_id);
            ((MainActivity) fragmentActivity).PLAYER_INITIALIZE(embed_url);
            ((MainActivity) fragmentActivity).setText_values(video_title, video_views, content, created_at);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
    }

    private void initComponent(View view) {
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_blue_dark);
        Iv_back = view.findViewById(R.id.Iv_back);
        frame_no_data = view.findViewById(R.id.frame_no_data);
        Iv_back.setOnClickListener(this);
        text_header = view.findViewById(R.id.text_header);
        recyclerview_post = view.findViewById(R.id.recyclerview_post);
        LinearLayoutManager linearLayoutManager12 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerview_post.setLayoutManager(linearLayoutManager12);
        recyclerview_post.setHasFixedSize(true);
        recyclerview_post.setNestedScrollingEnabled(false);

    }

    @Override
    public void onClick(View v) {
        if (v == Iv_back) {
            (fragmentActivity).getSupportFragmentManager().popBackStack();
        }
    }


    private void CALL_API_Category_POST() {

        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(fragmentActivity);

        String URL = Constants.MAIN_URL + Constants.Cat_Name + "/" + Constants.Tag_id;
        Log.e("URL_TAB", URL);
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("tab_DATA", "" + response.trim());
                        try {
                            arrayList_all_post.clear();
                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                // JSONObject jsonArray = ojs.getJSONObject("cat_json_data");
                                JSONObject jsonArray12 = ojs.getJSONObject("get_json_data");

                                Object check = jsonArray12.get("posts_data");
                                if (check instanceof JSONObject) {
                                    JSONObject jsonArray123 = jsonArray12.getJSONObject("posts_data");
                                    // JSONObject obj = (JSONObject) jsonArray12.get(key);
                                    String error = jsonArray123.getString("error");

                                    frame_no_data.setVisibility(View.VISIBLE);

                                    Log.e("CHECK_ERROR", "" + error);
                                } else if (check instanceof JSONArray) {
                                    frame_no_data.setVisibility(View.GONE);

                                    JSONArray jsonArray123 = jsonArray12.getJSONArray("posts_data");
                                    for (int i = 0; i < jsonArray123.length(); i++) {
                                        JSONObject obj = jsonArray123.getJSONObject(i);

                                        String id = obj.getString("id");
                                        String title = obj.getString("title");
                                        String category_name = obj.getString("category_name_slug");
                                        String post_type = obj.getString("post_type");
                                        String content = obj.getString("content");

                                        String image_default = "";
                                        String embed_code = "";

                                        if (post_type.contentEquals("post")) {     ///Post

                                            if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                                image_default = obj.getString("image_url");
                                            } else {
                                                image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                                embed_code = "";
                                            }

                                        } else {    ////Videos
                                            if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                                image_default = obj.getString("video_image_url");
                                            } else {
                                                image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                            }
                                            embed_code = obj.getString("video_embed_code");
                                        }

                                        String created_at = obj.getString("created_at");
                                        String comment_count = obj.getString("comment_count");

                                        String hit = obj.getString("hit");

                                        arrayList_all_post.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                                    }
                                }

                                if (arrayList_all_post.isEmpty()) {

                                } else {
                                    Recyclerview_Adapter_news recyclerview_adapter_news = new Recyclerview_Adapter_news(getActivity(),
                                            R.layout.recent_news_layout, arrayList_all_post);
                                    recyclerview_post.setAdapter(recyclerview_adapter_news);
                                    recyclerview_adapter_news.notifyDataSetChanged();
                                }

                            } else {
                                Log.e("error", "0");
                               /*String error = ojs.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);*/
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);


    }

    @Override
    public void onRefresh() {
        CALL_API_Category_POST();
    }
}
