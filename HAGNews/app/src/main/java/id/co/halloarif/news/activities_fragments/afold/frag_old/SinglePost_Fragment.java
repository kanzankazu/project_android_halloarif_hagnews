package id.co.halloarif.news.activities_fragments.afold.frag_old;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Html;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Sub_category_Adapter;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Viewpager_items;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ST_004 on 05-03-2018.
 */

public class SinglePost_Fragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    LinearLayoutManager linearLayoutManager;
    ArrayList<Viewpager_items> arrayList_all_post = new ArrayList<>();
    Sub_category_Adapter sub_category_adapter;
    SwipeRefreshLayout mSwipeRefreshLayout;
    TextView text_header, txtview_content, Tv_time, Tv_news_heading;
    ImageView IV_back_news_image;

    public SinglePost_Fragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_singlepost, container, false);

        initComponent(view);
        IV_back_news_image.setVisibility(View.GONE);


        return view;
    }

    private void initComponent(View view) {
        //   text_header = view.findViewById(R.id.text_header);
        txtview_content = (TextView) view.findViewById(R.id.txtview_content);
        Tv_news_heading = view.findViewById(R.id.Tv_news_heading);
        Tv_time = view.findViewById(R.id.Tv_time);
        IV_back_news_image = view.findViewById(R.id.IV_back_news_image);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_blue_dark);

    }

    @Override
    public void onRefresh() {
        SET_SUBCATEGORY_API();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                    // text_header.setText(Constants.TAG_TITLE);
                    SET_SUBCATEGORY_API();
                }
            });
        } catch (NullPointerException e) {
            Log.e("Error", "Error");
        }
    }

    private void SET_SUBCATEGORY_API() {
        String URL = Constants.MAIN_URL + Constants.SLUG_TO_OPEN;
        Log.e("URL_TAB", Constants.MAIN_URL + Constants.SLUG_TO_OPEN);

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("tab_DATA", "" + response.trim());
                        try {
                            arrayList_all_post.clear();

                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonArray = ojs.getJSONObject("get_json_data");
                               // String id = jsonArray.getString("id");
                                String title = jsonArray.getString("title");
                                String page_content = jsonArray.getString("page_content");
                                String created_at = jsonArray.getString("created_at");

                                Tv_news_heading.setText(title);

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    txtview_content.setText(Html.fromHtml(page_content, Html.FROM_HTML_MODE_COMPACT));
                                } else {
                                    txtview_content.setText(Html.fromHtml(page_content));
                                }

                                String current_datetime = Constants.Current_Time_Zone();
                                long api_datetime = Constants.getDateInMillis(created_at);
                                long current_timeZone = Constants.getDateInMillis(current_datetime);

                                if (created_at != null) {
                                    Tv_time.setText(DateUtils.getRelativeTimeSpanString(api_datetime, current_timeZone, DateUtils.SECOND_IN_MILLIS));
                                }


                            } else {
                               /*String error = ojs.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);*/
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        mSwipeRefreshLayout.setRefreshing(false);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mSwipeRefreshLayout.setRefreshing(false);


                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);
    }

}
