package id.co.halloarif.news.models.Res.Audio;

import java.util.List;

public class AudioDataMainModel {
    public String user_reading_list;
    public List<AudioDataModel> posts_data = null;

    public String getUser_reading_list() {
        return user_reading_list;
    }

    public void setUser_reading_list(String user_reading_list) {
        this.user_reading_list = user_reading_list;
    }

    public List<AudioDataModel> getPosts_data() {
        return posts_data;
    }

    public void setPosts_data(List<AudioDataModel> posts_data) {
        this.posts_data = posts_data;
    }
}
