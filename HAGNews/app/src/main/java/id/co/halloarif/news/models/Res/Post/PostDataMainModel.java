package id.co.halloarif.news.models.Res.Post;

import java.util.List;

public class PostDataMainModel {
    public String user_reading_list;
    public List<PostDataModel> posts_data = null;

    public String getUser_reading_list() {
        return user_reading_list;
    }

    public void setUser_reading_list(String user_reading_list) {
        this.user_reading_list = user_reading_list;
    }

    public List<PostDataModel> getPosts_data() {
        return posts_data;
    }

    public void setPosts_data(List<PostDataModel> posts_data) {
        this.posts_data = posts_data;
    }
}
