package id.co.halloarif.news.activities_fragments.afold.activity_old.logreg;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import id.co.halloarif.news.R;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.utils.preference.Preference_saved;
import id.co.halloarif.news.utils.TextCaptcha;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Register_Activity extends AppCompatActivity {
    private VideoView vvRegfvbi;
    ImageView ivRegCaptchafvbi;
    ImageView ivRegCaptchaReloadfvbi;
    EditText etRegCaptchafvbi;
    EditText etRegUsernamefvbi;
    EditText etRegEmailfvbi;
    EditText etRegPassfvbi;
    EditText etRegPassConfrmfvbi;
    Button bRegSubmitfvbi;

    TextCaptcha textCaptcha;
    String txt_username;
    String text_email;
    String txt_confirm_pas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();

    }

    private void initComponent() {
        vvRegfvbi =(VideoView) findViewById(R.id.vvReg);
        etRegUsernamefvbi =(EditText) findViewById(R.id.etRegUsername);
        etRegEmailfvbi =(EditText) findViewById(R.id.etRegEmail);
        etRegPassfvbi =(EditText) findViewById(R.id.etRegPass);
        etRegPassConfrmfvbi =(EditText) findViewById(R.id.etRegPassConfrm);
        ivRegCaptchafvbi =(ImageView) findViewById(R.id.ivRegCaptcha);
        ivRegCaptchaReloadfvbi =(ImageView) findViewById(R.id.ivRegCaptchaReload);
        etRegCaptchafvbi =(EditText) findViewById(R.id.etRegCaptcha);
        bRegSubmitfvbi =(Button) findViewById(R.id.bRegSubmit);
    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {
        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.news);
        vvRegfvbi.setVideoURI(uri);

        textCaptcha = new TextCaptcha(ivRegCaptchafvbi.getMaxWidth(), ivRegCaptchafvbi.getMaxHeight(), 4, TextCaptcha.TextOptions.NUMBERS_AND_LETTERS);
        ivRegCaptchafvbi.setImageBitmap(textCaptcha.getImage());

    }

    private void initListener() {
        vvRegfvbi.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
                mp.setLooping(true);
            }
        });
        ivRegCaptchaReloadfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textCaptcha = new TextCaptcha(ivRegCaptchafvbi.getMaxWidth(), ivRegCaptchafvbi.getMaxHeight(), 4, TextCaptcha.TextOptions.NUMBERS_AND_LETTERS);
                ivRegCaptchafvbi.setImageBitmap(textCaptcha.getImage());
            }
        });
        bRegSubmitfvbi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etRegUsernamefvbi.getText().toString().isEmpty()) {
                    etRegUsernamefvbi.setError("Enter Username");
                } else if (etRegEmailfvbi.getText().toString().isEmpty()) {
                    etRegEmailfvbi.setError("Enter Email");
                } else if (etRegPassfvbi.getText().toString().isEmpty()) {
                    etRegPassfvbi.setError("Enter Password");
                } else if (etRegPassConfrmfvbi.getText().toString().isEmpty()) {
                    etRegPassConfrmfvbi.setError("Enter Confirm Password");
                } else if (etRegCaptchafvbi.getText().toString().isEmpty()) {
                    etRegCaptchafvbi.setError("Enter TextCaptchaAbstract");
                } else {
                    if (etRegPassfvbi.getText().toString().contentEquals(etRegPassConfrmfvbi.getText().toString())) {

                        if (!textCaptcha.checkAnswer(etRegCaptchafvbi.getText().toString().trim())) {
                            etRegCaptchafvbi.setError("TextCaptchaAbstract is not match");
                            etRegCaptchafvbi.setText("");
                            //  numberOfCaptchaFalse++;
                        } else {
                            Log.d("Main", "captcha is match!");
                            txt_username = etRegUsernamefvbi.getText().toString();
                            txt_confirm_pas = etRegPassConfrmfvbi.getText().toString();
                            text_email = etRegEmailfvbi.getText().toString();
                            Call_Register_API(txt_username, text_email, txt_confirm_pas);
                        }
                    } else {
                        etRegPassConfrmfvbi.setError("Password not matched");
                    }
                }
            }
        });
    }

    private void Call_Register_API(final String username, final String txt_email, final String txt_password) {
        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        String URL = Constants.MAIN_URL + "register";
        //String URL = Constants.MAIN_URL + "register/" + username + "/" + txt_email + "/" + txt_password;

        Log.e("tab_DATA_register_URL", Constants.MAIN_URL + "register/" + username + "/" + txt_email + "/" + txt_password);
        StringRequest strRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("tab_DATA_register", "" + response.trim());
                        try {

                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonobject = ojs.getJSONObject("get_json_data");
                                String id = jsonobject.getString("id");
                                String username = jsonobject.getString("username");
                                String email = jsonobject.getString("email");
                                String avatar = jsonobject.getString("avatar");
                                String about_me = jsonobject.getString("about_me");
                                if (avatar.equalsIgnoreCase("null")) {
                                    avatar = "";
                                }
                                Constants.USER_EMAIL = email;
                                Preference_saved.getInstance(getApplicationContext()).set_check_login("true");
                                Preference_saved.getInstance(getApplicationContext()).setUsername(username);
                                Preference_saved.getInstance(getApplicationContext()).setUser_Email(email);
                                Preference_saved.getInstance(getApplicationContext()).setuserPicture(avatar);
                                Preference_saved.getInstance(getApplicationContext()).setUser_id(id);
                                Toast.makeText(getApplicationContext(), "Successfully Registered", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), "Already registered with this email !", Toast.LENGTH_SHORT).show();
                                Preference_saved.getInstance(getApplicationContext()).set_check_login("false");
                               /*String error = ojs.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);*/
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", username);
                params.put("email", txt_email);
                params.put("password", txt_password);
                return params;
            }
        };
        requestQueue.add(strRequest);


    }
}
