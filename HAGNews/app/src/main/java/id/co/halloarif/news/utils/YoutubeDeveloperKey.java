// Copyright 2014 Google Inc. All Rights Reserved.

package id.co.halloarif.news.utils;

/**
 * Static container class for holding a reference to your YouTube Developer Key.
 */
public class YoutubeDeveloperKey {

  /**
   * Please replace this with a valid API key which is enabled for the
   * YouTube Data API v3 service. Go to the
   * <a href="https://console.developers.google.com/">Google Developers Console</a>
   * to register a new developer key.
   */
  public static final String DEVELOPER_KEY = "AIzaSyBNL_MoVfr47JCBXZECxOOP-mOpHPLofk8";
//  public static final String DEVELOPER_KEY = "AIzaSyA7PffiRIT4J4-d9v5qkyaQXY3LN4iFhV0";

}
