package id.co.halloarif.news.models.Res;

public class BaseResponseModel {

    private String error_json;
    private Integer status;

    public String getError_json() {
        return error_json;
    }

    public void setError_json(String error_json) {
        this.error_json = error_json;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
