package id.co.halloarif.news.models.Res.Audio;

import java.util.List;

public class AudioCommentModel {
    public List<AudioCommentUserModel> user_comment ;

    public List<AudioCommentUserModel> getUser_comment() {
        return user_comment;
    }

    public void setUser_comment(List<AudioCommentUserModel> user_comment) {
        this.user_comment = user_comment;
    }
}
