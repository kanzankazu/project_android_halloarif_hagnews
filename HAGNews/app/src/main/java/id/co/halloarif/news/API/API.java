package id.co.halloarif.news.API;

import id.co.halloarif.news.IConfig;
import id.co.halloarif.news.models.Req.ChangePassReqModel;
import id.co.halloarif.news.models.Req.LoginReqModel;
import id.co.halloarif.news.models.Req.RegReqModel;
import id.co.halloarif.news.models.Res.ChangePass.ChangePassMainModel;
import id.co.halloarif.news.models.Res.HomePage.HomepageMainModel;
import id.co.halloarif.news.models.Res.Profile.UserProfileMainModel;
import id.co.halloarif.news.models.Res.User.UserMainModel;
import okhttp3.RequestBody;
import retrofit2.Call;

public class API {

    private static ApiServices getAPIService() {
        return RetrofitClient.getClient(IConfig.API_BASE_URL).create(ApiServices.class);
    }

    public static Call<UserMainModel> doLogin(LoginReqModel request) {
        return getAPIService().Login(request);
    }

    public static Call<UserMainModel> doRegister(RegReqModel request) {
        return getAPIService().Register(request);
    }

    public static Call<UserProfileMainModel> doUpdateProfile(RequestBody file, RequestBody fname, RequestBody id) {
        return getAPIService().UserProfile(file, fname, id);
    }

    public static Call<ChangePassMainModel> doChangePass(ChangePassReqModel request) {
        return getAPIService().ChangePass(request);
    }

    public static Call<HomepageMainModel> doHomePage() {
        return getAPIService().HomePage();
    }
}
