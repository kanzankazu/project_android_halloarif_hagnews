package id.co.halloarif.news.activities_fragments.afold.frag_old;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afold.activity_old.logreg.Login_Activity;
import id.co.halloarif.news.activities_fragments.afold.activity_old.logreg.Register_Activity;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recycler_Comments_adapter;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.utils.preference.Preference_saved;
import id.co.halloarif.news.utils.AlignTextView;
import id.co.halloarif.news.utils.CircleImageView;
import id.co.halloarif.news.utils.MediaPlayerSingleton;
import id.co.halloarif.news.utils.MyNestedScrollView;
import id.co.halloarif.news.utils.PinchImageView;
import id.co.halloarif.news.utils.Volley_multipart.MySingleton;
import id.co.halloarif.news.utils.Volley_multipart.VolleyMultipartRequest;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Audio_post_Fragment extends Fragment implements View.OnClickListener, AppBarLayout.OnOffsetChangedListener {

    FragmentActivity fragmentActivity;
    View view1;
    CollapsingToolbarLayout collapsingToolbar;
    AppBarLayout appBarLayout;
    CircleImageView Iv_centre_imageview;
    ImageView IV_back_news_image, Iv_back_button, Iv_share_info, Iv_Add_favorite, IV_linkedin, IV_tumblr, IV_Pinterest, IV_Googleplus, IV_Twitter, IV_facebook;
    String id, title, str_title_slug, image_default, category_name, post_type, created_at, hit, content, str_share_facebook, str_share_twitter, str_share_plus_google, str_share_linkedin, str_share_tumblr, str_share_pinterest;
    TextView Tv_news_heading, Tv_category, Tv_time, Tv_no_commments;
    AlignTextView txtview_content;
    FrameLayout Frame_loader;
    NestedScrollView Nested_View_Recyclerview;
    PopupWindow popupWindow_selct, popupWindowlogin;
    ArrayList<Viewpager_items> arrayList_comments = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_tracks = new ArrayList<>();
    String txt_comment;
    LinearLayout lay_bottom;
    RecyclerView recyclerview_comments, recyclerView_audio_tracks;
    Recycler_Comments_adapter recycler_comments_adapter;
    MyNestedScrollView nestedScrollView;
    ImageButton IB_submit_comment;
    EditText edttxt_text_comment;
    String POST_ID, login_user_id;
    FloatingActionButton fabfloating_button;
    AudioItemAdapter audioItemAdapter;

    private boolean mIsAvatarShown = true;
    private int mMaxScrollSize;
    private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 20;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
    }

    public static Audio_post_Fragment newInstance(String POST_ID) {
        Audio_post_Fragment fragment = new Audio_post_Fragment();
        Bundle args = new Bundle();
        args.putString("POST_ID", POST_ID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();
        int scrollRange;
        int percentage = (Math.abs(i)) * 100 / mMaxScrollSize;

        if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
            mIsAvatarShown = false;
            //  ViewCompat.animate(mFab).scaleY(0).scaleX(0).start();
            //   mProfileImage.animate().scaleY(0).scaleX(0).setDuration(200).start();
        }

        if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
            mIsAvatarShown = true;
            //  mProfileImage.animate().scaleY(1).scaleX(1).start();
            //  ViewCompat.animate(mFab).scaleY(1).scaleX(1).start();
            //  toolbar.setVisibility(View.GONE);
        }
        scrollRange = appBarLayout.getTotalScrollRange();
        Log.e("SCroll_Range", "RANGE" + scrollRange + "INT VALUE" + i);

        if (scrollRange + i == 0) {

            if (title != null && !title.isEmpty()) {
                collapsingToolbar.setTitle(title);
            }
        } else {
            if (title != null && !title.isEmpty()) {
                collapsingToolbar.setTitle(title);
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view1 = inflater.inflate(R.layout.fragment_audio_frag, container, false);
        initComponent(view1);

        if (getArguments() != null) {
            POST_ID = getArguments().getString("POST_ID", "0");

            CAll_DETAILS_API();

            CAll_API_COMMENTS();
            Log.e("VALUES>>>", "ID >>" + POST_ID);
        }


        return view1;
    }

    private void initComponent(View view1) {
        fabfloating_button = view1.findViewById(R.id.fabfloating_button);
        collapsingToolbar = view1.findViewById(R.id.collapsing_toolbar);
        appBarLayout = view1.findViewById(R.id.appbar);
        appBarLayout.addOnOffsetChangedListener(this);

        recyclerview_comments = view1.findViewById(R.id.recyclerview_comments);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(fragmentActivity, 1);
        recyclerview_comments.setLayoutManager(mLayoutManager);
        recyclerview_comments.setNestedScrollingEnabled(false);

        recyclerView_audio_tracks = view1.findViewById(R.id.recyclerView_audio_tracks);
        RecyclerView.LayoutManager mLayoutManager1 = new GridLayoutManager(fragmentActivity, 1);
        recyclerView_audio_tracks.setLayoutManager(mLayoutManager1);
        recyclerView_audio_tracks.setNestedScrollingEnabled(false);

        edttxt_text_comment = view1.findViewById(R.id.edttxt_text_comment);
        nestedScrollView = view1.findViewById(R.id.nestedscroll_view);
        lay_bottom = view1.findViewById(R.id.lay_bottom);
        IV_back_news_image = view1.findViewById(R.id.IV_back_news_image);
        Iv_back_button = view1.findViewById(R.id.Iv_back_button);
        Iv_share_info = view1.findViewById(R.id.Iv_share_info);
        Iv_centre_imageview = view1.findViewById(R.id.Iv_centre_imageview);
        Tv_time = view1.findViewById(R.id.Tv_time);
        Frame_loader = view1.findViewById(R.id.Frame_loader);
        Iv_Add_favorite = view1.findViewById(R.id.Iv_Add_favorite);

        IV_facebook = view1.findViewById(R.id.IV_facebook);
        IV_Googleplus = view1.findViewById(R.id.IV_Googleplus);
        IV_linkedin = view1.findViewById(R.id.IV_linkedin);
        IV_tumblr = view1.findViewById(R.id.IV_tumblr);
        IV_Twitter = view1.findViewById(R.id.IV_Twitter);
        IV_Pinterest = view1.findViewById(R.id.IV_Pinterest);

        IB_submit_comment = view1.findViewById(R.id.IB_submit_comment);
        Tv_news_heading = view1.findViewById(R.id.Tv_news_heading);
        Tv_category = view1.findViewById(R.id.Tv_category);
        txtview_content = view1.findViewById(R.id.txtview_content);
        Nested_View_Recyclerview = view1.findViewById(R.id.Nested_View_Recyclerview);
        Iv_back_button.setOnClickListener(this);
        Tv_no_commments = view1.findViewById(R.id.Tv_no_commments);
        Iv_Add_favorite.setOnClickListener(this);
        Tv_news_heading.setOnClickListener(this);
        IB_submit_comment.setOnClickListener(this);
        IV_back_news_image.setOnClickListener(this);

        IV_facebook.setOnClickListener(this);
        IV_Pinterest.setOnClickListener(this);
        Iv_share_info.setOnClickListener(this);
        IV_Twitter.setOnClickListener(this);
        IV_tumblr.setOnClickListener(this);
        IV_Googleplus.setOnClickListener(this);
        IV_linkedin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == Iv_back_button) {
            (fragmentActivity).getSupportFragmentManager().popBackStack();
        } else if (view == IV_back_news_image) {
            initiatePopupWindow_image();
        } else if (view == Iv_Add_favorite) {

            String check = Preference_saved.getInstance(fragmentActivity).check_login();
            Log.d("checklogin", check);
            if (check != null) {
                if (check.contentEquals("true")) {
                    CALL_API_ADD_FAVORITE();
                } else {
                    initiatePopupWindow_SelectActionLogin();
                }
            }
        } else if (view == Iv_share_info) {

            if (str_title_slug != null) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Read more at " + Constants.BASE_URL + "post/" + str_title_slug);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        } else if (view == IB_submit_comment) {
            String check = Preference_saved.getInstance(fragmentActivity).check_login();
            Log.d("checklogin", check);
            if (check != null) {
                if (check.contentEquals("true")) {
                    txt_comment = edttxt_text_comment.getText().toString();
                    if (txt_comment.isEmpty()) {
                        edttxt_text_comment.setError("write some comment....");
                    } else {
                        Frame_loader.setVisibility(View.VISIBLE);
                        CALL_API_COMMENT();
                    }
                } else {
                    initiatePopupWindow_SelectActionLogin();
                }
            }
        } else if (view == IV_facebook) {
            if (!str_share_facebook.isEmpty()) {
                String url = str_share_facebook;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        } else if (view == IV_Twitter) {
            if (!str_share_twitter.isEmpty()) {
                String url = str_share_twitter;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "https://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        } else if (view == IV_Googleplus) {
            if (!str_share_plus_google.isEmpty()) {
                String url = str_share_plus_google;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        } else if (view == IV_linkedin) {
            if (!str_share_linkedin.isEmpty()) {
                String url = str_share_linkedin;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        } else if (view == IV_tumblr) {
            if (!str_share_tumblr.isEmpty()) {
                String url = str_share_tumblr;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        } else if (view == IV_Pinterest) {
            if (!str_share_pinterest.isEmpty()) {
                String url = str_share_pinterest;
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        }
    }

    private void CAll_API_COMMENTS() {
        String check = Preference_saved.getInstance(fragmentActivity).check_login();
        Log.d("checklogin", check);
        if (check != null) {
            if (check.contentEquals("true")) {
                login_user_id = Preference_saved.getInstance(fragmentActivity).getUser_id();
            } else {
                login_user_id = "";
            }
        }

        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(fragmentActivity);
        Frame_loader.setVisibility(View.VISIBLE);
        String URL = Constants.MAIN_URL + "post_comment/" + POST_ID + "/" + login_user_id;
        Log.e("URL_OPEN_POST", URL);
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("detail_news", "" + response.trim());
                        try {
                            arrayList_comments.clear();
                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonObject = ojs.getJSONObject("get_json_data");


                                Object check = jsonObject.get("user_comment");
                                if (check instanceof JSONObject) {

                                    // JSONObject jsonArray123 = jsonObject.getJSONObject("user_comment");
                                    // String error = jsonArray123.getString("error");
                                    //Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                    Tv_no_commments.setVisibility(View.VISIBLE);
                                    Frame_loader.setVisibility(View.GONE);
                                    Nested_View_Recyclerview.setVisibility(View.GONE);

                                } else if (check instanceof JSONArray) {

                                    JSONArray jsonObject1 = jsonObject.getJSONArray("user_comment");
                                    for (int i = 0; i < jsonObject1.length(); i++) {
                                        JSONObject obj = jsonObject1.getJSONObject(i);
                                        String comment_user_like_count = obj.getString("user_like_count");
                                        String user_reply_count = obj.getString("user_reply_count");
                                        String comment_id = obj.getString("comment_id");
                                        String comment_post_id = obj.getString("comment_post_id");
                                        String comment_user_id = obj.getString("comment_user_id");
                                        String user_like_comment = obj.getString("user_like_comment");
                                        String comment_user_image = Constants.IMAGE_URL + obj.getString("comment_user_image");
                                        String comment_user_name = obj.getString("comment_user_name");
                                        String comment_parent_id = obj.getString("comment_parent_id");
                                        String comment_content = obj.getString("comment_content");
                                        String comment_created_at = obj.getString("comment_created_at");

                                        arrayList_comments.add(new Viewpager_items(comment_user_like_count, user_reply_count, comment_id, comment_post_id, comment_user_id, user_like_comment, comment_user_image, comment_user_name, comment_parent_id, comment_content, comment_created_at, "", ""));
                                    }

                                    if (arrayList_comments.isEmpty()) {
                                        Frame_loader.setVisibility(View.GONE);
                                        Tv_no_commments.setVisibility(View.VISIBLE);
                                        Nested_View_Recyclerview.setVisibility(View.GONE);
                                    } else {
                                        Tv_no_commments.setVisibility(View.GONE);
                                        Frame_loader.setVisibility(View.GONE);
                                        Nested_View_Recyclerview.setVisibility(View.VISIBLE);
                                        recycler_comments_adapter = new Recycler_Comments_adapter(fragmentActivity, R.layout.item_layout_comments, arrayList_comments);
                                        recyclerview_comments.setAdapter(recycler_comments_adapter);
                                        recycler_comments_adapter.notifyDataSetChanged();
                                    }
                                }
                            } else {
                                Log.e("error", "");
                               /*String error = ojs.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);*/
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);
    }

    private void CAll_DETAILS_API() {
        String userid = Preference_saved.getInstance(fragmentActivity).getUser_id();
        if (userid != null) {
            login_user_id = userid;
        } else {
            login_user_id = "";
        }
        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(fragmentActivity);
        Frame_loader.setVisibility(View.VISIBLE);
        String URL = Constants.MAIN_URL + "audio/" + POST_ID + "/" + login_user_id;
        Log.e("URL_AUDIO_POST", URL);
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("detail_news", "" + response.trim());
                        try {
                            arrayList_tracks.clear();

                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                                String check = Preference_saved.getInstance(fragmentActivity).check_login();
                                Log.d("checklogin", check);
                                if (check != null) {
                                    if (check.contentEquals("true")) {
                                        String user_reading_list = jsonObject.getString("user_reading_list");
                                        if (user_reading_list.contentEquals("true")) {
                                            Iv_Add_favorite.setImageResource(R.drawable.ic_star_filled);
                                        } else {
                                            Iv_Add_favorite.setImageResource(R.drawable.ic_star_empty);
                                        }
                                    }
                                }
                                JSONArray jsonArray12 = jsonObject.getJSONArray("posts_data");
                                for (int i = 0; i < jsonArray12.length(); i++) {
                                    JSONObject obj = jsonArray12.getJSONObject(i);

                                    JSONArray jsonArray_tracks = obj.getJSONArray("audio_data");
                                    for (int j = 0; j < jsonArray_tracks.length(); j++) {
                                        JSONObject jsonObject_tracks = jsonArray_tracks.getJSONObject(j);
                                        String id = jsonObject_tracks.getString("id");
                                        String audio_path = "http://www.halloarif.co.id/news/" + jsonObject_tracks.getString("audio_path");
                                        String audio_name = jsonObject_tracks.getString("audio_name");
                                        String musician = jsonObject_tracks.getString("musician");
                                        String download_button = jsonObject_tracks.getString("download_button");
                                        String post_id = jsonObject_tracks.getString("post_id");
                                        String audio_id = jsonObject_tracks.getString("audio_id");

                                        arrayList_tracks.add(new Viewpager_items(id, audio_path, audio_name, musician, download_button, post_id, audio_id));
                                    }


                                    id = obj.getString("id");
                                    title = obj.getString("title");
                                    str_title_slug = obj.getString("title_slug");

                                    if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                        image_default = obj.getString("image_url");
                                    } else {
                                        image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                    }
                                    category_name = obj.getString("category_name");
                                    post_type = obj.getString("post_type");
                                    created_at = obj.getString("created_at");
                                    hit = obj.getString("hit");
                                    content = obj.getString("content");
                                    str_share_facebook = obj.getString("share_facebook");
                                    str_share_twitter = obj.getString("share_twitter");
                                    str_share_plus_google = obj.getString("share_plus_google");
                                    str_share_linkedin = obj.getString("share_linkedin");
                                    str_share_tumblr = obj.getString("share_tumblr");
                                    str_share_pinterest = obj.getString("share_pinterest");
                                }


                               /* Audio_Track_Adapter recycler_comments_adapter = new
                                        Audio_Track_Adapter(fragmentActivity, R.layout.item_list_audio_tracks, arrayList_tracks);
                                recyclerView_audio_tracks.setAdapter(recycler_comments_adapter);
                                recycler_comments_adapter.notifyDataSetChanged();*/


                                audioItemAdapter = new AudioItemAdapter(arrayList_tracks);
                                recyclerView_audio_tracks.setAdapter(audioItemAdapter);
                                audioItemAdapter.notifyDataSetChanged();

                                String current_datetime = Constants.Current_Time_Zone();
                                long api_datetime = Constants.getDateInMillis(created_at);
                                long current_timeZone = Constants.getDateInMillis(current_datetime);
                                Tv_news_heading.setText(title);
                                Tv_category.setText(category_name);
                                Tv_time.setText(DateUtils.getRelativeTimeSpanString(api_datetime, current_timeZone, DateUtils.SECOND_IN_MILLIS));


                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    txtview_content.setText(Html.fromHtml(content, Html.FROM_HTML_MODE_COMPACT));
                                } else {
                                    txtview_content.setText(Html.fromHtml(content));
                                }


                             /*   Picasso.with(getApplicationContext()).load(image_default)
                                        .into(IV_back_news_image, new com.squareup.picasso.Callback() {
                                            @Override
                                            public void onSuccess() {
                                                // viewHolder.avi.hide();
                                                Frame_loader.setVisibility(View.GONE);
                                            }

                                            @Override
                                            public void onError() {
                                                // viewHolder.avi.show();
                                            }
                                        });*/

                                Glide.with(fragmentActivity).load(image_default).apply(RequestOptions.circleCropTransform()).into(Iv_centre_imageview);


                                Glide.with(fragmentActivity).load(image_default)
                                        .listener(new RequestListener<Drawable>() {
                                            @Override
                                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                                // log exception
                                                Log.e("TAG", "Error loading image", e);
                                                return false; // important to return false so the error placeholder can be placed
                                            }

                                            @Override
                                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                                Frame_loader.setVisibility(View.GONE);
                                                return false;
                                            }
                                        }).into(IV_back_news_image);


                                //  JSONArray jsonArray123 = jsonObject.getJSONArray("posts_data");


                            } else {
                                Log.e("error", "");
                               /*String error = ojs.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);*/
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);
    }

    private void CALL_API_COMMENT() {
        final String userid = Preference_saved.getInstance(fragmentActivity).getUser_id();
        String URL = Constants.MAIN_URL + "add_post_comment";
        Log.e("URL_ADD_COMMENT", URL);
        // String url = "http://www.angga-ari.com/api/something/awesome";
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, URL, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    Log.e("RESPONSE_ADD_COMMENT", "" + resultResponse);
                    JSONObject ojs = new JSONObject(resultResponse);
                    String abc = ojs.getString("status");
                    Log.e("Value", abc);
                    if (Integer.parseInt(abc) == 1) {
                        JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                        JSONObject jsonObject1 = jsonObject.getJSONObject("post_comment");
                        String title = jsonObject1.getString("title");

                        Toast.makeText(fragmentActivity, title, Toast.LENGTH_SHORT).show();
                        edttxt_text_comment.setText("");
                        CAll_API_COMMENTS();
                        Frame_loader.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(fragmentActivity, "Incorrect Data", Toast.LENGTH_SHORT).show();
                        //  Log.e("error", error);*//**//*
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");
                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Frame_loader.setVisibility(View.GONE);
                Toast.makeText(fragmentActivity, errorMessage, Toast.LENGTH_SHORT).show();
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("post_id", POST_ID);
                params.put("user_id", userid);
                params.put("comment", txt_comment);
                return params;
            }
        };
        MySingleton.getInstance(fragmentActivity).addToRequestQueue(multipartRequest);


    }

    private void CALL_API_ADD_FAVORITE() {
        String userid = Preference_saved.getInstance(fragmentActivity).getUser_id();
        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(fragmentActivity);
        Frame_loader.setVisibility(View.VISIBLE);
        String URL = Constants.MAIN_URL + "add-reading-list/" + userid + "/" + POST_ID;
        Log.e("URL_FAVORITE", URL);
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("DATA_FAVORITE", "" + response.trim());
                try {

                    JSONObject ojs = new JSONObject(response);
                    String abc = ojs.getString("status");
                    Log.e("Value", abc);
                    if (Integer.parseInt(abc) == 1) {
                        JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                        JSONObject jsonObject1 = jsonObject.getJSONObject("added_reading_post");
                        //  String title = jsonObject1.getString("title");
                        String status = jsonObject1.getString("status");
                        if (status.contentEquals("1")) {
                            Iv_Add_favorite.setImageResource(R.drawable.ic_star_filled);
                        } else {
                            Iv_Add_favorite.setImageResource(R.drawable.ic_star_empty);
                        }
                        Frame_loader.setVisibility(View.GONE);

                    } else {
                               /*String error = ojs.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);*/
                        Frame_loader.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Frame_loader.setVisibility(View.GONE);

                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);


    }

    private void initiatePopupWindow_SelectActionLogin() {
        try {
// We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) fragmentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert inflater != null;
            View layout = inflater.inflate(R.layout.popup_select_action_login, (ViewGroup) fragmentActivity.findViewById(R.id.popup));

            RelativeLayout mainlayout = layout.findViewById(R.id.popup);

            mainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  popupWindow_selct.dismiss();
                }
            });
            ImageView close_dialog = layout.findViewById(R.id.ivPopForgetPassClose);
            close_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindowlogin.dismiss();
                }
            });


            Button Btn_delete_playlist = layout.findViewById(R.id.Btn_login);
            Btn_delete_playlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindowlogin.dismiss();
                    Intent i = new Intent(fragmentActivity, Login_Activity.class);
                    startActivity(i);

                }
            });

            Button Btn_edit_playlist = layout.findViewById(R.id.Btn_regiter);
            Btn_edit_playlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindowlogin.dismiss();
                    Intent i = new Intent(fragmentActivity, Register_Activity.class);
                    startActivity(i);
                }
            });
            popupWindowlogin = new PopupWindow(layout, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT, true);
            popupWindowlogin.showAtLocation(layout, Gravity.CENTER, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initiatePopupWindow_image() {
        try {
// We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) fragmentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert inflater != null;
            View layout = inflater.inflate(R.layout.popup_imagezoom, (ViewGroup) fragmentActivity.findViewById(R.id.popup));

            RelativeLayout mainlayout = layout.findViewById(R.id.popup);

            mainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  popupWindow_selct.dismiss();
                }
            });
            ImageView close_dialog = layout.findViewById(R.id.ivPopForgetPassClose);
            close_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow_selct.dismiss();
                }
            });

            PinchImageView pinchimage = layout.findViewById(R.id.pinchimage);

            Glide.with(fragmentActivity).load(image_default).into(pinchimage);

            pinchimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            popupWindow_selct = new PopupWindow(layout, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT, true);
            popupWindow_selct.showAtLocation(layout, Gravity.CENTER, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class AudioItemAdapter extends RecyclerView.Adapter<AudioItemAdapter.AudioItemsViewHolder> implements Handler.Callback {

        private static final int MSG_UPDATE_SEEK_BAR = 1845;

        // private MediaPlayer mediaPlayer;

        private Handler uiUpdateHandler;

        private List<Viewpager_items> audioItems;
        private int playingPosition, media_length;
        private AudioItemsViewHolder playingHolder;

        AudioItemAdapter(List<Viewpager_items> audioItems) {
            this.audioItems = audioItems;
            this.playingPosition = -1;
            uiUpdateHandler = new Handler(this);
        }

        @NonNull
        @Override
        public AudioItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new AudioItemsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_audio_tracks, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull AudioItemsViewHolder holder, int position) {
            if (position == playingPosition) {
                playingHolder = holder;
                // this view holder corresponds to the currently playing audio cell
                // update its view to show playing progress
                Log.e("BINDVIEW", "SAME POSITION");
                updatePlayingView();
            } else {
                Log.e("BINDVIEW", "POSITION CHANGED");
                // and this one corresponds to non playing
                updateNonPlayingView(holder);
            }

            holder.Tv_song_title.setText(audioItems.get(position).getAudio_name());
            holder.Tv_author_name.setText(audioItems.get(position).getMusician());

            //   holder.tvIndex.setText(String.format(Locale.US, "%d", position));
        }

        @Override
        public int getItemCount() {
            return audioItems.size();
        }

        @Override
        public void onViewRecycled(@NonNull AudioItemsViewHolder holder) {
            super.onViewRecycled(holder);
            if (playingPosition == holder.getAdapterPosition()) {
                // view holder displaying playing audio cell is being recycled
                // change its state to non-playing
                Log.e("BINDVIEW", "POSITION Recycled");

                updateNonPlayingView(playingHolder);
                playingHolder = null;
            }
        }

        /**
         * Changes the view to non playing state
         * - icon is changed to play arrow
         * - seek bar disabled
         * - remove seek bar updater, if needed
         *
         * @param holder ViewHolder whose state is to be chagned to non playing
         */
        private void updateNonPlayingView(AudioItemsViewHolder holder) {
            if (holder == playingHolder) {
                uiUpdateHandler.removeMessages(MSG_UPDATE_SEEK_BAR);
            }
            Log.e("BINDVIEW", "updateNonPlayingView");
            holder.sbProgress.setEnabled(false);
            holder.Iv_now_playing.setVisibility(View.INVISIBLE);
            holder.Relative_lay_seekbar.setVisibility(View.GONE);
            holder.Iv_playpause.setImageResource(R.drawable.ic_play);
        }

        /**
         * Changes the view to playing state
         * - icon is changed to pause
         * - seek bar enabled
         * - start seek bar updater, if needed
         */
        private void updatePlayingView() {
            Log.e("BINDVIEW", "updatePlayingView");

            playingHolder.sbProgress.setMax(MediaPlayerSingleton.getInstance().getMediaPlayer().getDuration());
            playingHolder.sbProgress.setProgress(MediaPlayerSingleton.getInstance().getMediaPlayer().getCurrentPosition());
            playingHolder.Tv_progress_total.setText(milliSecondsToTimer(MediaPlayerSingleton.getInstance().getMediaPlayer().getDuration()));
            playingHolder.sbProgress.setEnabled(true);
            if (MediaPlayerSingleton.getInstance().getMediaPlayer().isPlaying()) {
                uiUpdateHandler.sendEmptyMessageDelayed(MSG_UPDATE_SEEK_BAR, 100);
                playingHolder.Iv_now_playing.setVisibility(View.VISIBLE);
                playingHolder.Relative_lay_seekbar.setVisibility(View.VISIBLE);
                playingHolder.Iv_playpause.setImageResource(R.drawable.ic_pause);
            } else {
                playingHolder.Iv_now_playing.setVisibility(View.INVISIBLE);
                playingHolder.Relative_lay_seekbar.setVisibility(View.GONE);
                uiUpdateHandler.removeMessages(MSG_UPDATE_SEEK_BAR);
                playingHolder.Iv_playpause.setImageResource(R.drawable.ic_play);
            }
        }




      /*  void tO_save_state() {
            if (mediaPlayer != null) {
                mediaPlayer.pause();
                media_length = mediaPlayer.getCurrentPosition();
            }
        }


        void resume_state() {
            if (mediaPlayer != null) {
                mediaPlayer.seekTo(media_length);
                mediaPlayer.start();
            }
        }
*/

        String milliSecondsToTimer(long milliseconds) {
            String finalTimerString = "";
            String secondsString = "";

            // Convert total duration into time
            int hours = (int) (milliseconds / (1000 * 60 * 60));
            int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
            int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
            // Add hours if there
            if (hours > 0) {
                finalTimerString = hours + ":";
            }
            // Prepending 0 to seconds if it is one digit
            if (seconds < 10) {
                secondsString = "0" + seconds;
            } else {
                secondsString = "" + seconds;
            }

            finalTimerString = finalTimerString + minutes + ":" + secondsString;

            // return timer string
            return finalTimerString;
        }

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_UPDATE_SEEK_BAR: {
                    playingHolder.sbProgress.setProgress(MediaPlayerSingleton.getInstance().getMediaPlayer().getCurrentPosition());
                    playingHolder.Tv_progress_start.setText(milliSecondsToTimer(MediaPlayerSingleton.getInstance().getMediaPlayer().getCurrentPosition()));
                    uiUpdateHandler.sendEmptyMessageDelayed(MSG_UPDATE_SEEK_BAR, 100);
                    return true;
                }
            }
            return false;
        }

        // Interaction listeners e.g. click, seekBarChange etc are handled in the view holder itself. This eliminates
        // need for anonymous allocations.
        class AudioItemsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
            SeekBar sbProgress;
            TextView Tv_author_name, Tv_song_title, Tv_progress_start, Tv_progress_total;
            ImageView Iv_playpause, Iv_now_playing;
            RelativeLayout Relative_lay_seekbar;

            AudioItemsViewHolder(View itemView) {
                super(itemView);
                Tv_author_name = itemView.findViewById(R.id.Tv_author_name);
                Relative_lay_seekbar = itemView.findViewById(R.id.Relative_lay_seekbar);
                Tv_progress_start = itemView.findViewById(R.id.Tv_progress_start);
                Tv_progress_total = itemView.findViewById(R.id.Tv_progress_total);
                Tv_song_title = itemView.findViewById(R.id.Tv_song_title);
                Iv_now_playing = itemView.findViewById(R.id.Iv_now_playing);
                sbProgress = itemView.findViewById(R.id.sbProgress);
                Iv_playpause = itemView.findViewById(R.id.Iv_playpause);


                Iv_now_playing.setVisibility(View.INVISIBLE);
                Relative_lay_seekbar.setVisibility(View.GONE);

                Iv_playpause.setOnClickListener(this);
                sbProgress.setOnSeekBarChangeListener(this);


            }

            @Override
            public void onClick(View v) {
                EventBus.getDefault().post("Play_true");
                if (getAdapterPosition() == playingPosition) {
                    // toggle between play/pause of audio
                    if (MediaPlayerSingleton.getInstance().getMediaPlayer() != null) {
                        if (MediaPlayerSingleton.getInstance().getMediaPlayer().isPlaying()) {
                            MediaPlayerSingleton.getInstance().getMediaPlayer().pause();

                        } else {
                            MediaPlayerSingleton.getInstance().getMediaPlayer().start();
                        }
                    } else {
                        playingPosition = getAdapterPosition();
                        if (MediaPlayerSingleton.getInstance().getMediaPlayer() != null) {
                            if (null != playingHolder) {
                                updateNonPlayingView(playingHolder);
                            }
                            // MediaPlayerSingleton.getInstance().getMediaPlayer().release();
                        }
                        playingHolder = this;
                        startMediaPlayer(audioItems.get(playingPosition).getAudio_path());
                    }

                } else {
                    // start another audio playback
                    playingPosition = getAdapterPosition();
                    if (MediaPlayerSingleton.getInstance().getMediaPlayer() != null) {
                        if (null != playingHolder) {
                            updateNonPlayingView(playingHolder);
                        }
                        //    MediaPlayerSingleton.getInstance().getMediaPlayer().release();
                    }
                    playingHolder = this;
                    startMediaPlayer(audioItems.get(playingPosition).getAudio_path());

                    //radioManager.playOrPause(audioItems.get(playingPosition).getAudio_path());
                }
                updatePlayingView();
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    if (MediaPlayerSingleton.getInstance().getMediaPlayer() != null)
                        media_length = progress;
                    MediaPlayerSingleton.getInstance().getMediaPlayer().seekTo(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        }


        private void startMediaPlayer(String audioResId) {
            try {

                if (MediaPlayerSingleton.getInstance().getMediaPlayer() != null) {
                    Log.e("MEDIA_PLAYER", "NOT NULL");
                } else {
                    Log.e("MEDIA_PLAYER", "NULL");
                    MediaPlayerSingleton.getInstance().getMediaPlayer();
                }
                Log.e("AUDIO_RESOURCE", "" + audioResId);

                MediaPlayerSingleton.getInstance().getMediaPlayer().reset();
                MediaPlayerSingleton.getInstance().getMediaPlayer().setDataSource(audioResId);
                MediaPlayerSingleton.getInstance().getMediaPlayer().setAudioStreamType(AudioManager.STREAM_MUSIC);
                MediaPlayerSingleton.getInstance().getMediaPlayer().prepare();
                MediaPlayerSingleton.getInstance().getMediaPlayer().start();

                MediaPlayerSingleton.getInstance().getMediaPlayer().setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {

                    }
                });
                MediaPlayerSingleton.getInstance().getMediaPlayer().setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        //  MediaPlayerSingleton.getInstance().getMediaPlayer().release();
                        Log.e("ONCOMPLETE", "CALLED");
                        if (playingPosition < (audioItems.size() - 1) && playingPosition != -1) {
                            Log.e("ONCOMPLETE", "NEXT");

                            playingPosition = playingPosition + 1;

                            // start another audio playback
                            if (MediaPlayerSingleton.getInstance().getMediaPlayer() != null) {
                                if (null != playingHolder) {
                                    updateNonPlayingView(playingHolder);
                                }
                                // MediaPlayerSingleton.getInstance().getMediaPlayer().release();
                            }
                            notifyDataSetChanged();
                            startMediaPlayer(audioItems.get(playingPosition).getAudio_path());
                            //  updatePlayingView();

                        } else {
                            Log.e("ONCOMPLETE", "END");
                            releaseMediaPlayer();
                            Intent intent = new Intent("KEY");
                            fragmentActivity.sendBroadcast(intent);
                        }
                    }
                });

                playingHolder.sbProgress.setProgress(0);
                playingHolder.sbProgress.setMax(100);

            } catch (Exception e) {
                Log.d("EXCEPTION", "initMedia_error", e);
            }
        }

        private void releaseMediaPlayer() {
            if (null != playingHolder) {
                updateNonPlayingView(playingHolder);
            }
            MediaPlayerSingleton.getInstance().getMediaPlayer().reset();
            playingPosition = -1;
        }
    }

    /*  @Override
    public void onPause() {
        super.onPause();
        audioItemAdapter.stopPlayer();
    }
    */


    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        //  if (audioItemAdapter != null)
        //     audioItemAdapter.resume_state();


    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        Intent intent = new Intent("KEY");
        fragmentActivity.sendBroadcast(intent);

        //  audioItemAdapter.tO_save_state();

    }


}
