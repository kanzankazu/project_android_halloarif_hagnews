package id.co.halloarif.news.activities_fragments.afnew.activity_new;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.text.TextUtils;

import id.co.halloarif.news.ISeasonConfig;
import id.co.halloarif.news.activities_fragments.afnew.frag_new.BaseHomeFragment;
import id.co.halloarif.news.activities_fragments.afnew.frag_new.GalleryNewsFragment;
import id.co.halloarif.news.activities_fragments.afnew.frag_new.PostAllFragment;
import id.co.halloarif.news.activities_fragments.afold.frag_old.Gallery_Fragment;

public class SlidePagerAdapter extends FragmentStatePagerAdapter {

    private final String[] titleTab;
    private final String[] slugTab;

    public SlidePagerAdapter(FragmentManager fm, String[] titleTab, String[] slugTab) {
        super(fm);
        this.titleTab = titleTab;
        this.slugTab = slugTab;
    }

    @Override
    public Fragment getItem(int position) {
        String title = this.titleTab[position];
        String slug = slugTab[position];

        if (position == 0) {
            return new BaseHomeFragment();
        } else if (position == 6) {
            return new GalleryNewsFragment();
        } else {
            if (!TextUtils.isEmpty(slug)) {
                Bundle bundle = new Bundle();
                bundle.putString(ISeasonConfig.TAG_SLUG, slug);
                bundle.putString(ISeasonConfig.POST_TITLE, title);

                PostAllFragment postAllFragment = new PostAllFragment();
                postAllFragment.setArguments(bundle);
                return postAllFragment;
            }
        }
        return null;
    }

    @Override
    public int getCount() {
        return titleTab.length;
    }
}
