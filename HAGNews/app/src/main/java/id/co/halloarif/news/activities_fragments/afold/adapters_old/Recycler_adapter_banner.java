package id.co.halloarif.news.activities_fragments.afold.adapters_old;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import id.co.halloarif.news.R;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Viewpager_items;

/**
 * Created by ST_004 on 08-12-2017.
 */

public class Recycler_adapter_banner extends RecyclerView.Adapter<Recycler_adapter_banner.ViewHolder1> implements Filterable {
    private Context act1;
    private ArrayList<Viewpager_items> mArrayList;
    private ArrayList<Viewpager_items> mFilteredList;
    private int row;

    public Recycler_adapter_banner(Context act, int resource, ArrayList<Viewpager_items> arrayList) {
        act1 = act;
        row = resource;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public ViewHolder1 onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(row, viewGroup, false);
        return new ViewHolder1(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder1 viewHolder, final int i) {

        Viewpager_items objAllBean = mFilteredList.get(i);

        viewHolder.txt_category.setText(objAllBean.getCategory_name());
        String current_datetime = Constants.Current_Time_Zone();

        long api_datetime = Constants.getDateInMillis(objAllBean.getCreated_at());
        long current_timeZone = Constants.getDateInMillis(current_datetime);
        viewHolder.txt_date.setText(DateUtils.getRelativeTimeSpanString(api_datetime, current_timeZone, DateUtils.SECOND_IN_MILLIS));
        viewHolder.txt_title.setText(objAllBean.getTitle());
        viewHolder.txt_views.setText(objAllBean.getHit());

        Glide.with(act1).load(objAllBean.getImage_default()).transition(new DrawableTransitionOptions().crossFade()).apply(RequestOptions.centerCropTransform().error(R.drawable.tallywithback)).into(viewHolder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<Viewpager_items> filteredList = new ArrayList<>();

                    for (Viewpager_items androidVersion : mArrayList) {

                        if (androidVersion.getTitle().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Viewpager_items>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class ViewHolder1 extends RecyclerView.ViewHolder {
        TextView txt_category, txt_title, txt_date, txt_views;
        ImageView thumbnail;

        ViewHolder1(View view) {
            super(view);
            txt_category = view.findViewById(R.id.txt_category);
            txt_title = view.findViewById(R.id.txt_title);
            txt_date = view.findViewById(R.id.txt_date);
            txt_views = view.findViewById(R.id.txt_views);
            thumbnail = view.findViewById(R.id.thumbnail);
        }
    }

    public void setData(ArrayList<Viewpager_items> datas) {
        mFilteredList = datas;
        notifyDataSetChanged();

    }

    public void replaceData(List<Viewpager_items> datas) {
        mFilteredList.clear();
        mFilteredList.addAll(datas);
        notifyDataSetChanged();
    }

    public void addDatas(List<Viewpager_items> datas) {
        mFilteredList.addAll(datas);
        notifyItemRangeInserted(mFilteredList.size(), datas.size());
    }

    public void addDataFirst(Viewpager_items data) {
        int position = 0;
        mFilteredList.add(position, data);
        notifyItemInserted(position);
    }

    public void removeAt(int position) {
        mFilteredList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mFilteredList.size());
    }

    public void removeDataFirst() {
        int position = 0;
        mFilteredList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mFilteredList.size());
    }
}
