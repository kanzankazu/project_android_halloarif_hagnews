package id.co.halloarif.news.models.Req;

public class PassResetReqModel {
    private String email;
    private String confirmation_password;

    public PassResetReqModel(String email, String confirmation_password) {
        this.email = email;
        this.confirmation_password = confirmation_password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getConfirmation_password() {
        return confirmation_password;
    }

    public void setConfirmation_password(String confirmation_password) {
        this.confirmation_password = confirmation_password;
    }
}
