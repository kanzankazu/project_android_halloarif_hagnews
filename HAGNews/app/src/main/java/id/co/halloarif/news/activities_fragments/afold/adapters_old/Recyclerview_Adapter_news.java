package id.co.halloarif.news.activities_fragments.afold.adapters_old;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import id.co.halloarif.news.R;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Viewpager_items;

/**
 * Created by ST_004 on 08-02-2018.
 */

public class Recyclerview_Adapter_news extends RecyclerView.Adapter<Recyclerview_Adapter_news.ViewHolder1> implements Filterable {
    private Context act1;
    private ArrayList<Viewpager_items> mArrayList;
    private ArrayList<Viewpager_items> mFilteredList;
    private int row;
    private Viewpager_items objAllBean;
    private ArrayList<Viewpager_items> copy_arrlist;

    public Recyclerview_Adapter_news(Context act, int resource, ArrayList<Viewpager_items> arrayList) {
        act1 = act;
        row = resource;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public ViewHolder1 onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(row, viewGroup, false);
        return new ViewHolder1(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder1 viewHolder, final int i) {

        objAllBean = mFilteredList.get(i);

        final String Created_at = objAllBean.getCreated_at();
        String current_datetime = Constants.Current_Time_Zone();
        long api_datetime = Constants.getDateInMillis(Created_at);
        long current_timeZone = Constants.getDateInMillis(current_datetime);

        final String title = objAllBean.getTitle();
        final String image = objAllBean.getImage_default();

        if (title != null) {
            viewHolder.textv_title.setText(title);
        }
        if (image != null) {
            Glide.with(act1).load(image).transition(new DrawableTransitionOptions().crossFade()).apply(RequestOptions.centerCropTransform().error(R.drawable.tallywithback)).into(viewHolder.Iv_image_recent);
        }

        if (Created_at != null) {
            viewHolder.textv_time.setText(DateUtils.getRelativeTimeSpanString(api_datetime, current_timeZone, DateUtils.SECOND_IN_MILLIS));
        }

    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<Viewpager_items> filteredList = new ArrayList<>();

                    for (Viewpager_items androidVersion : mArrayList) {

                        if (androidVersion.getTitle().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Viewpager_items>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder1 extends RecyclerView.ViewHolder {
        TextView textv_title, textv_time;
        ImageView Iv_image_recent;

        ViewHolder1(View view) {
            super(view);
            textv_title = (TextView) itemView.findViewById(R.id.textv_title);
            textv_time = (TextView) itemView.findViewById(R.id.textv_time);
            Iv_image_recent = (ImageView) itemView.findViewById(R.id.Iv_image_recent);
        }
    }

    public void setData(ArrayList<Viewpager_items> datas) {
        mFilteredList = datas;
        notifyDataSetChanged();

    }

    public void replaceData(List<Viewpager_items> datas) {
        mFilteredList.clear();
        mFilteredList.addAll(datas);
        notifyDataSetChanged();
    }

    public void addDatas(List<Viewpager_items> datas) {
        mFilteredList.addAll(datas);
        notifyItemRangeInserted(mFilteredList.size(), datas.size());
    }

    public void addDataFirst(Viewpager_items data) {
        int position = 0;
        mFilteredList.add(position, data);
        notifyItemInserted(position);
    }

    public void removeAt(int position) {
        mFilteredList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mFilteredList.size());
    }

    public void removeDataFirst() {
        int position = 0;
        mFilteredList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mFilteredList.size());
    }

    public void removeDataAll() {
        mFilteredList.clear();
        notifyDataSetChanged();
    }
}