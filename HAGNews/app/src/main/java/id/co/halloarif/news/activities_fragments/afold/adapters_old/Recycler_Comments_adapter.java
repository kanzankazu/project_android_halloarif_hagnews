package id.co.halloarif.news.activities_fragments.afold.adapters_old;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import id.co.halloarif.news.ISeasonConfig;
import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afold.activity_old.logreg.Login_Activity;
import id.co.halloarif.news.activities_fragments.afold.activity_old.logreg.Register_Activity;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.support.SessionUtil;
import id.co.halloarif.news.utils.Volley_multipart.MySingleton;
import id.co.halloarif.news.utils.Volley_multipart.VolleyMultipartRequest;

import static com.bumptech.glide.request.RequestOptions.centerCropTransform;

public class Recycler_Comments_adapter extends RecyclerView.Adapter<Recycler_Comments_adapter.ViewHolder1> implements Filterable {
    private Context act1;
    private ArrayList<Viewpager_items> mArrayList;
    private ArrayList<Viewpager_items> mFilteredList;
    private int row;
    PopupWindow popupWindowlogin, popupWindow_reply;
    View view_pop;

    private String userId;
    private boolean loggedin;

    public Recycler_Comments_adapter(Context act, int resource, ArrayList<Viewpager_items> arrayList) {
        act1 = act;
        row = resource;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }

    @Override
    public ViewHolder1 onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(row, viewGroup, false);
        view_pop = view;
        return new ViewHolder1(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder1 viewHolder, final int i) {

        Viewpager_items objAllBean = mFilteredList.get(i);
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_ID)) {
            userId = SessionUtil.getStringPreferences(ISeasonConfig.KEY_USER_ID, null);
        }
        if (SessionUtil.checkIfExist(ISeasonConfig.LOGGEDIN)) {
            loggedin = SessionUtil.getBoolPreferences(ISeasonConfig.LOGGEDIN,false);
        }

        final String Created_at = objAllBean.getComment_created_at();
        String current_datetime = Constants.Current_Time_Zone();
        long api_datetime = Constants.getDateInMillis(Created_at);
        long current_timeZone = Constants.getDateInMillis(current_datetime);

        String comment_text = objAllBean.getComment_content();
        final String image = objAllBean.getComment_user_image();

        String user_like_comment = objAllBean.getUser_like_comment();

        final String comment_parent_id = objAllBean.getComment_parent_id();
        final String comment_post_id = objAllBean.getComment_post_id();
        final String comment_id = objAllBean.getComment_id();
        String user_reply_count = objAllBean.getUser_reply_count();

        if (user_like_comment.contentEquals("0")) {
            viewHolder.Iv_like.setImageResource(R.drawable.ic_thumb_up);
            viewHolder.Iv_like1.setImageResource(R.drawable.ic_thumb_up);
        } else if (user_like_comment.contentEquals("true")) {
            viewHolder.Iv_like.setImageResource(R.drawable.ic_thumb_up_blue);
            viewHolder.Iv_like1.setImageResource(R.drawable.ic_thumb_up_blue);
        } else {
            viewHolder.Iv_like.setImageResource(R.drawable.ic_thumb_up);
            viewHolder.Iv_like1.setImageResource(R.drawable.ic_thumb_up);
        }

        if (comment_parent_id.contentEquals("0")) {
            viewHolder.layout_reply.setVisibility(View.VISIBLE);
            viewHolder.layout_reply1.setVisibility(View.VISIBLE);
            viewHolder.cardlist_item.setVisibility(View.VISIBLE);
            viewHolder.cardlist_item1.setVisibility(View.GONE);

            viewHolder.textview_reply_count.setText("Reply" + "(" + user_reply_count + ")");
            viewHolder.textv_username.setText(objAllBean.getComment_user_name());
            viewHolder.txtv_comment.setText(comment_text);
            viewHolder.textview_likes_count.setText("(" + objAllBean.getComment_user_like_count() + ")");
            if (image != null) {
                Glide.with(act1).load(image).transition(new DrawableTransitionOptions().crossFade())
                        .apply(centerCropTransform()).into(viewHolder.Iv_image_recent);
            }
            if (Created_at != null) {
                viewHolder.textv_time.setText(DateUtils.getRelativeTimeSpanString(api_datetime, current_timeZone, DateUtils.FORMAT_SHOW_TIME));
            }

            viewHolder.layout_likes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (loggedin) {
                        CALL_API_LIKE_COMMENTS(comment_parent_id, viewHolder, comment_id, comment_post_id);
                    } else {
                        initiatePopupWindow_SelectActionLogin(v);
                        // showPopup(v);
                        // showDiag();
                    }
                }
            });

            viewHolder.layout_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (v.equals(viewHolder.layout_delete)) {
                        CALL_API_DELETE_COMMENTS(comment_id, comment_post_id, userId);
                        removeAt(i);
                    }
                }
            });

            viewHolder.layout_reply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (loggedin) {
                        initiatePopupWindow_Reply(v, comment_id, comment_post_id);
                    } else {
                        initiatePopupWindow_SelectActionLogin(v);
                    }
                }
            });

        } else {
            viewHolder.cardlist_item.setVisibility(View.GONE);
            viewHolder.cardlist_item1.setVisibility(View.VISIBLE);

            viewHolder.textview_reply_count1.setText("Reply" + "(" + user_reply_count + ")");
            viewHolder.textv_username1.setText(objAllBean.getComment_user_name());
            viewHolder.txtv_comment1.setText(comment_text);
            viewHolder.textview_likes_count1.setText("(" + objAllBean.getComment_user_like_count() + ")");
            if (image != null) {

                Glide.with(act1).load(image).transition(new DrawableTransitionOptions().crossFade())
                        .apply(centerCropTransform()).into(viewHolder.Iv_image_recent1);

            }
            if (Created_at != null) {
                viewHolder.textv_time1.setText(DateUtils.getRelativeTimeSpanString(api_datetime, current_timeZone, DateUtils.FORMAT_SHOW_TIME));
            }

            viewHolder.layout_likes1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (loggedin) {
                        CALL_API_LIKE_COMMENTS(comment_parent_id, viewHolder, comment_id, comment_post_id);
                    } else {
                        initiatePopupWindow_SelectActionLogin(v);
                        // showPopup(v);
                        //showDiag();
                    }
                }
            });

            viewHolder.layout_delete1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (v.equals(viewHolder.layout_delete1)) {
                        CALL_API_DELETE_COMMENTS(comment_id, comment_post_id, userId);
                        removeAt(i);
                    }
                }
            });

            viewHolder.layout_reply1.setVisibility(View.GONE);
            viewHolder.layout_reply.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(userId)) {
            viewHolder.layout_delete.setVisibility(View.GONE);
            viewHolder.layout_delete1.setVisibility(View.GONE);
        } else {
            if (userId.contentEquals(objAllBean.getComment_user_id())) {
                viewHolder.layout_delete1.setVisibility(View.VISIBLE);
                viewHolder.layout_delete.setVisibility(View.VISIBLE);
            } else {
                viewHolder.layout_delete1.setVisibility(View.GONE);
                viewHolder.layout_delete.setVisibility(View.GONE);
            }
        }
    }

    private void initiatePopupWindow_Reply(View v, final String comment_id, final String comment_post_id) {
        try {
// We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) act1.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = LayoutInflater.from(act1).inflate(R.layout.popup_reply_layout, null);
            //  final View layout = inflater.inflate(R.layout.popup_select_action_login, null);
            RelativeLayout mainlayout = layout.findViewById(R.id.popup);
            mainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  popupWindow_selct.dismiss();
                }
            });
            ImageView close_dialog = layout.findViewById(R.id.ivPopForgetPassClose);
            close_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow_reply.dismiss();
                }
            });

            final EditText edttxt_text_comment = layout.findViewById(R.id.edttxt_text_comment);
            Button Btn_submit = layout.findViewById(R.id.bPopForgetPassSubmit);
            Btn_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String text_reply = edttxt_text_comment.getText().toString();
                    if (text_reply.isEmpty()) {
                        Toast.makeText(act1, "write something...", Toast.LENGTH_SHORT).show();
                        //edttxt_text_comment.setError("write something...");
                    } else {
                        Call_API_Reply_submit(edttxt_text_comment, text_reply, comment_id, comment_post_id);
                    }
                }
            });

            popupWindow_reply = new PopupWindow(layout, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT, true);
            popupWindow_reply.showAtLocation(layout, Gravity.CENTER, 0, 0);

            // popupWindowlogin.showAtLocation(layout, Gravity.CENTER, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initiatePopupWindow_SelectActionLogin(View v) {
        try {
            // We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) act1.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = LayoutInflater.from(act1).inflate(R.layout.popup_select_action_login, null);
            //  final View layout = inflater.inflate(R.layout.popup_select_action_login, (ViewGroup) view.findViewById(R.id.popup));

            RelativeLayout mainlayout = layout.findViewById(R.id.popup);

            mainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  popupWindow_selct.dismiss();
                }
            });
            ImageView close_dialog = layout.findViewById(R.id.ivPopForgetPassClose);
            close_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindowlogin.dismiss();
                }
            });

            Button Btn_delete_playlist = layout.findViewById(R.id.Btn_login);
            Btn_delete_playlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindowlogin.dismiss();
                    Intent i = new Intent(act1, Login_Activity.class);
                    act1.startActivity(i);
                }
            });

            Button Btn_edit_playlist = layout.findViewById(R.id.Btn_regiter);
            Btn_edit_playlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindowlogin.dismiss();
                    Intent i = new Intent(act1, Register_Activity.class);
                    act1.startActivity(i);
                }
            });

            popupWindowlogin = new PopupWindow(layout, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT, true);
            popupWindowlogin.showAtLocation(layout, Gravity.CENTER, 0, 0);

            // popupWindowlogin.showAtLocation(layout, Gravity.CENTER, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void CALL_API_DELETE_COMMENTS(String comment_id, String comment_post_id, final String userid) {

        String URL = Constants.MAIN_URL + "delete_comment/" + comment_id + "/" + comment_post_id + "/" + userid;
        Log.e("URL_DELETE_COMMENT", URL);

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.GET, URL, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    Log.e("RESPONSE_DELETE_COMMENT", "" + resultResponse);
                    JSONObject ojs = new JSONObject(resultResponse);
                    String abc = ojs.getString("status");
                    Log.e("Value", abc);
                    if (Integer.parseInt(abc) == 1) {
                        JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                        JSONObject jsonObject1 = jsonObject.getJSONObject("user_comment");

                        String title = jsonObject1.getString("title");

                        Toast.makeText(act1, title, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(act1, "Incorrect Data", Toast.LENGTH_SHORT).show();
                        //  Log.e("error", error);*//**//*
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");
                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Toast.makeText(act1, errorMessage, Toast.LENGTH_SHORT).show();
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            /*@Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("comment_id", comment_id);
                params.put("user_id", userId);
                return params;
            }*/

        };
        MySingleton.getInstance(act1).addToRequestQueue(multipartRequest);

    }

    private void CALL_API_LIKE_COMMENTS(final String comment_parent_id, final ViewHolder1 viewHolder, final String commentId, final String post_id) {

        String URL = Constants.MAIN_URL + "add_like_comment/" + commentId + "/" + post_id + "/" + userId;
        Log.e("URL_LIKE_COMMENT", URL);

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.GET, URL, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    Log.e("RESPONSE_LIKE_COMMENT", "" + resultResponse);
                    JSONObject ojs = new JSONObject(resultResponse);
                    String abc = ojs.getString("status");
                    Log.e("Value", abc);
                    if (Integer.parseInt(abc) == 1) {
                        JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                        String like_comment = jsonObject.getString("like_comment");
                        String user_like_count = jsonObject.getString("user_like_count");
                        if (comment_parent_id.contentEquals("0")) {
                            if (like_comment.contentEquals("false")) {
                                viewHolder.Iv_like.setImageResource(R.drawable.ic_thumb_up);
                            } else {
                                viewHolder.Iv_like.setImageResource(R.drawable.ic_thumb_up_blue);
                            }
                            viewHolder.textview_likes_count.setText("(" + user_like_count + ")");
                        } else {
                            if (like_comment.contentEquals("false")) {
                                viewHolder.Iv_like1.setImageResource(R.drawable.ic_thumb_up);
                            } else {
                                viewHolder.Iv_like1.setImageResource(R.drawable.ic_thumb_up_blue);
                            }
                            viewHolder.textview_likes_count1.setText("(" + user_like_count + ")");
                        }

                    } else {
                        Toast.makeText(act1, "Incorrect Data", Toast.LENGTH_SHORT).show();
                        //  Log.e("error", error);*//**//*
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");
                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Toast.makeText(act1, errorMessage, Toast.LENGTH_SHORT).show();
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
         /*   @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("comment_id", commentId);
                params.put("post_id",post_id);
                params.put("user_id", userId);
                return params;
            }*/

        };
        MySingleton.getInstance(act1).addToRequestQueue(multipartRequest);

    }

    private void Call_API_Reply_submit(final EditText edttxt_text_comment, final String text_reply, final String comment_id, final String comment_post_id) {

        final String userid = userId;
        String URL = Constants.MAIN_URL + "add_post_comment";
        Log.e("URL_ADD_REPLY", URL);

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, URL, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    Log.e("RESPONSE_ADD_Reply", "" + resultResponse);
                    JSONObject ojs = new JSONObject(resultResponse);
                    String abc = ojs.getString("status");
                    Log.e("Value", abc);
                    if (Integer.parseInt(abc) == 1) {
                        JSONObject jsonObject = ojs.getJSONObject("get_json_data");

                        JSONObject jsonObject1 = jsonObject.getJSONObject("post_comment");

                        String title = jsonObject1.getString("title");

                        edttxt_text_comment.setText("");
                        Toast.makeText(act1, title, Toast.LENGTH_SHORT).show();
                        if (popupWindow_reply != null) {
                            popupWindow_reply.dismiss();
                        }
                        //  CAll_API_COMMENTS();

                    } else {
                        Toast.makeText(act1, "Incorrect Data", Toast.LENGTH_SHORT).show();
                        //  Log.e("error", error);*//**//*
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");
                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Toast.makeText(act1, errorMessage, Toast.LENGTH_SHORT).show();
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("parent_id", comment_id);
                params.put("post_id", comment_post_id);
                params.put("user_id", userid);
                params.put("comment", text_reply);
                return params;
            }

        };
        MySingleton.getInstance(act1).addToRequestQueue(multipartRequest);

    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<Viewpager_items> filteredList = new ArrayList<>();

                    for (Viewpager_items androidVersion : mArrayList) {

                        if (androidVersion.getTitle().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Viewpager_items>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder1 extends RecyclerView.ViewHolder {
        TextView textv_username, txtv_comment, textv_time, textview_likes_count, textview_reply_count;
        LinearLayout layout_reply, layout_likes, layout_delete;
        ImageView Iv_image_recent, Iv_like;

        TextView textv_username1, txtv_comment1, textv_time1, textview_likes_count1, textview_reply_count1;
        LinearLayout layout_reply1, layout_likes1, layout_delete1;
        ImageView Iv_image_recent1, Iv_like1;
        CardView cardlist_item, cardlist_item1;

        ViewHolder1(View view) {
            super(view);
            layout_reply = itemView.findViewById(R.id.layout_reply);
            layout_delete = itemView.findViewById(R.id.layout_delete);
            textv_username = (TextView) itemView.findViewById(R.id.textv_username);
            txtv_comment = itemView.findViewById(R.id.txtv_comment);
            Iv_like = itemView.findViewById(R.id.Iv_like);
            layout_likes = itemView.findViewById(R.id.layout_likes);
            textv_time = itemView.findViewById(R.id.textv_time);
            textview_likes_count = itemView.findViewById(R.id.textview_likes_count);
            textview_reply_count = itemView.findViewById(R.id.textview_reply_count);
            Iv_image_recent = itemView.findViewById(R.id.Iv_image_recent);
            cardlist_item = itemView.findViewById(R.id.cardlist_item);

            layout_reply1 = itemView.findViewById(R.id.layout_reply1);
            layout_delete1 = itemView.findViewById(R.id.layout_delete1);
            textv_username1 = (TextView) itemView.findViewById(R.id.textv_username1);
            txtv_comment1 = itemView.findViewById(R.id.txtv_comment1);
            Iv_like1 = itemView.findViewById(R.id.Iv_like1);
            layout_likes1 = itemView.findViewById(R.id.layout_likes1);
            textv_time1 = (TextView) itemView.findViewById(R.id.textv_time1);
            textview_likes_count1 = itemView.findViewById(R.id.textview_likes_count1);
            textview_reply_count1 = itemView.findViewById(R.id.textview_reply_count1);
            Iv_image_recent1 = (ImageView) itemView.findViewById(R.id.Iv_image_recent1);
            cardlist_item1 = itemView.findViewById(R.id.cardlist_item1);

        }
    }

    private void showDiag() {

        AlertDialog.Builder builder = new AlertDialog.Builder(act1);
        builder.setTitle("Are you sure?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();










  /*      AlertDialog.Builder builder = new AlertDialog.Builder(act1);
        builder.setMessage("Login first to perform action")
                .setCancelable(false)
                .setPositiveButton("Login", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //finish();
                        dialog.dismiss();
                        Intent i = new Intent(act1, LoginNewActivity.class);
                        act1.startActivity(i);
                    }
                })
                .setNegativeButton("Register", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.dismiss();
                        Intent i = new Intent(act1, RegisterNewActivity.class);
                        act1.startActivity(i);
                    }
                });

        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Alert");
        alert.show();*/
    }

    public void setData(ArrayList<Viewpager_items> datas) {
        mFilteredList = datas;
        notifyDataSetChanged();
    }

    public void replaceData(List<Viewpager_items> datas) {
        mFilteredList.clear();
        mFilteredList.addAll(datas);
        notifyDataSetChanged();
    }

    public void addDatas(List<Viewpager_items> datas) {
        mFilteredList.addAll(datas);
        notifyItemRangeInserted(mFilteredList.size(), datas.size());
    }

    public void addDataFirst(Viewpager_items data) {
        int position = 0;
        mFilteredList.add(position, data);
        notifyItemInserted(position);
    }

    public void removeAt(int position) {
        mFilteredList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mFilteredList.size());
    }

    public void removeDataFirst() {
        int position = 0;
        mFilteredList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mFilteredList.size());
    }

}