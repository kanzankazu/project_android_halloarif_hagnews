package id.co.halloarif.news.models.Req;

public class UpdateUserReqModel {
    private String email;
    private String username;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UpdateUserReqModel(String email, String username) {
        this.email = email;
        this.username = username;
    }

}
