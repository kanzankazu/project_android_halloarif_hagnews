package id.co.halloarif.news.models.Res.Post;

public class PostMainModel {
	public PostDataMainModel get_json_data;
	public String error_json = null;
	public Integer status;

	public PostDataMainModel getGet_json_data() {
		return get_json_data;
	}

	public void setGet_json_data(PostDataMainModel get_json_data) {
		this.get_json_data = get_json_data;
	}

	public String getError_json() {
		return error_json;
	}

	public void setError_json(String error_json) {
		this.error_json = error_json;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}