package id.co.halloarif.news.models.Res.Comment;

public class CommentMainModel {
    public CommentJsonModel get_json_data;
    public Integer status;

    public CommentJsonModel getGet_json_data() {
        return get_json_data;
    }

    public void setGet_json_data(CommentJsonModel get_json_data) {
        this.get_json_data = get_json_data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
