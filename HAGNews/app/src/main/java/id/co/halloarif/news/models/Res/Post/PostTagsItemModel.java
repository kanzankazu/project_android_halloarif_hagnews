package id.co.halloarif.news.models.Res.Post;

public class PostTagsItemModel {
	private String postId;
	private String tagSlug;
	private String createdAt;
	private String id;
	private String tag;

	public void setPostId(String postId){
		this.postId = postId;
	}

	public String getPostId(){
		return postId;
	}

	public void setTagSlug(String tagSlug){
		this.tagSlug = tagSlug;
	}

	public String getTagSlug(){
		return tagSlug;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setTag(String tag){
		this.tag = tag;
	}

	public String getTag(){
		return tag;
	}

	@Override
 	public String toString(){
		return 
			"VideoTagsItemModel{" +
			"post_id = '" + postId + '\'' + 
			",tag_slug = '" + tagSlug + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			",tag = '" + tag + '\'' + 
			"}";
		}
}
