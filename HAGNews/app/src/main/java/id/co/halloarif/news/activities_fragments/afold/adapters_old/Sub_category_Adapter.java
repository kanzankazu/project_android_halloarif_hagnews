package id.co.halloarif.news.activities_fragments.afold.adapters_old;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.co.halloarif.news.R;
import id.co.halloarif.news.models.Viewpager_items;

/**
 * Created by ST_004 on 02-02-2018.
 */

public class Sub_category_Adapter extends RecyclerView.Adapter<Sub_category_Adapter.ViewHolder> implements Filterable {
    private Context activity;
    private Viewpager_items objAllBean;
    private ArrayList<Viewpager_items> mArrayList;
    private ArrayList<Viewpager_items> mFilteredList;
    private int row;

    public Sub_category_Adapter(Context act, int resource, ArrayList<Viewpager_items> arrayList) {
        activity = act;
        row = resource;
        mArrayList = arrayList;
        mFilteredList = arrayList;
        //itemsAllphotos = arrayList;
        //arraycat = new ArrayList<ItemAllVideos>();
        //this.arraycat.addAll(itemsAllpho+``tos);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view;
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sub_category_items, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        objAllBean = mFilteredList.get(i);

        viewHolder.txt.setText(objAllBean.getName_sub());
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt;

        ViewHolder(View view) {
            super(view);
            txt = (TextView) view.findViewById(R.id.title);
        }
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<Viewpager_items> filteredList = new ArrayList<>();

                    for (Viewpager_items androidVersion : mArrayList) {

                       /* if (androidVersion.getTitle().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }*/
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Viewpager_items>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public void setData (ArrayList<Viewpager_items> datas) {
        mFilteredList = datas;
        notifyDataSetChanged();
    }

    public void replaceData(List<Viewpager_items> datas) {
        mFilteredList.clear();
        mFilteredList.addAll(datas);
        notifyDataSetChanged();
    }

    public void addDatas(List<Viewpager_items> datas) {
        mFilteredList.addAll(datas);
        notifyItemRangeInserted(mFilteredList.size(), datas.size());
    }

    public void addDataFirst(Viewpager_items data) {
        int position = 0;
        mFilteredList.add(position, data);
        notifyItemInserted(position);
    }

    public void removeAt(int position) {
        mFilteredList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mFilteredList.size());
    }

    public void removeDataFirst() {
        int position = 0;
        mFilteredList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mFilteredList.size());
    }

}