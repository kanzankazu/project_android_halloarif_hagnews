package id.co.halloarif.news.activities_fragments.afnew.activity_new;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.co.halloarif.news.ISeasonConfig;
import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afnew.frag_new.BaseHomeFragment;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_Adapter_news;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Res.Post.PostMainModel;
import id.co.halloarif.news.models.Res.Tag.TagMainDataModel;
import id.co.halloarif.news.models.Res.Tag.TagMainModel;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.support.SessionUtil;
import id.co.halloarif.news.utils.Recycleritemclicklistener;

public class PostTagActivity extends AppCompatActivity {

    TextView text_header;
    RecyclerView recyclerview_post;
    ArrayList<Viewpager_items> arrayList_all_post = new ArrayList<>();
    ImageView Iv_back;
    FragmentActivity fragmentActivity;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private Recyclerview_Adapter_news recyclerview_adapter_news;

    private String tagSlug;
    private String tag;
    private String userId;
    private boolean logged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_tag);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        Iv_back = (ImageView) findViewById(R.id.Iv_back);
        text_header = findViewById(R.id.text_header);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        recyclerview_post = findViewById(R.id.recyclerview_post);
    }

    private void initParam() {
        Intent bundle = getIntent();
        if (bundle.hasExtra(ISeasonConfig.TAG_SLUG)) {
            tagSlug = bundle.getStringExtra(ISeasonConfig.TAG_SLUG);
        }
        if (bundle.hasExtra(ISeasonConfig.TAG)) {
            tag = bundle.getStringExtra(ISeasonConfig.TAG);
        }
    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_ID)) {
            userId = SessionUtil.getStringPreferences(ISeasonConfig.KEY_USER_ID, null);
        }
        if (SessionUtil.checkIfExist(ISeasonConfig.LOGGEDIN)) {
            logged = SessionUtil.getBoolPreferences(ISeasonConfig.LOGGEDIN, false);
        }
    }

    private void initContent() {
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_blue_dark);

        recyclerview_adapter_news = new Recyclerview_Adapter_news(PostTagActivity.this, R.layout.recent_news_layout, arrayList_all_post);
        recyclerview_post.setAdapter(recyclerview_adapter_news);
        LinearLayoutManager linearLayoutManager12 = new LinearLayoutManager(PostTagActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerview_post.setLayoutManager(linearLayoutManager12);
        recyclerview_post.setHasFixedSize(true);
        recyclerview_post.setNestedScrollingEnabled(false);

        CALL_API_TAG_POST();
    }

    private void initListener() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    mSwipeRefreshLayout.post(new Runnable() {
                        @Override
                        public void run() {
                            mSwipeRefreshLayout.setRefreshing(true);
                            CALL_API_TAG_POST();
                        }
                    });
                } catch (NullPointerException e) {
                    Log.e("Error", "Error");
                }
            }
        });
        Iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (view == Iv_back) {
                    (fragmentActivity).getSupportFragmentManager().popBackStack();
                }*/
                finish();
            }
        });
        recyclerview_post.addOnItemTouchListener(new Recycleritemclicklistener(PostTagActivity.this, new Recycleritemclicklistener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Viewpager_items model = arrayList_all_post.get(position);
                        BaseHomeFragment.CLICK_METHOD(PostTagActivity.this, model);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                })
        );

    }

    private void CALL_API_TAG_POST() {

        String URL = Constants.MAIN_URL + "tags_slug/" + tagSlug;
        Log.e("URL_TAB", Constants.MAIN_URL + "tags_slug/" + tagSlug);

        ProgressDialog progressDialog = ProgressDialog.show(PostTagActivity.this, "Loading...", "Please Wait..", false, false);

        RequestQueue requestQueue = Volley.newRequestQueue(PostTagActivity.this);
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();

                Log.e("tab_DATA", "" + response.trim());

                arrayList_all_post.clear();

                JsonParser parser = new JsonParser();
                JsonElement mJson = parser.parse(response);
                Gson gson = new Gson();
                TagMainModel mainModel = gson.fromJson(mJson, TagMainModel.class);
                Integer statusbc = mainModel.getStatus();
                if (statusbc == 1) {
                    List<TagMainDataModel> dataModels = mainModel.getGet_json_data();
                    for (int i = 0; i < dataModels.size(); i++) {
                        TagMainDataModel dataModel = dataModels.get(i);

                        String id = dataModel.getId();
                        String title = dataModel.getTitle();
                        String post_type = dataModel.getPost_type();
                        String category_name = dataModel.getCategory_name_slug();
                        String content = dataModel.getContent();

                        String image_default = "";
                        String embed_code = "";

                        if (post_type.contentEquals("post")) {///Post
                            if (TextUtils.isEmpty(dataModel.getImage_default())) {
                                image_default = dataModel.getImage_url();
                            } else {
                                image_default = Constants.IMAGE_URL + dataModel.getImage_default();
                                embed_code = "";
                            }

                        } else {////Videos
                            if (TextUtils.isEmpty(dataModel.getImage_default())) {
                                image_default = dataModel.getImage_url();
                            } else {
                                image_default = Constants.IMAGE_URL + dataModel.getImage_default();
                            }

                            if (TextUtils.isEmpty(dataModel.getVideo_embed_code())) {
                                embed_code = dataModel.getVideo_path();
                            } else {
                                embed_code = dataModel.getVideo_embed_code();
                            }
                        }

                        //String category_name = dataModel.getCategory_name();
                        //String comment_count = dataModel.getComment_count();
                        String created_at = dataModel.getCreated_at();
                        String hit = dataModel.getHit();

                        arrayList_all_post.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, ""));
                    }

                    if (!arrayList_all_post.isEmpty()) {
                        recyclerview_adapter_news.setData(arrayList_all_post);
                    }

                } else {
                    String error = mainModel.getError_json();
                    Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                    Log.e("error_json", error);
                }

                mSwipeRefreshLayout.setRefreshing(false);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        mSwipeRefreshLayout.setRefreshing(false);
                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);
    }

}
