package id.co.halloarif.news.activities_fragments.afnew.activity_new;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

import id.co.halloarif.news.ISeasonConfig;
import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recycler_adapter_banner;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_Adapter_Videos;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_Adapter_news;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_adapter_voting_poll;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Res.HomePage.HomepageDataModel;
import id.co.halloarif.news.models.Res.HomePage.HomepageMainModel;
import id.co.halloarif.news.models.Res.HomePage.HomepageMainPostModel;
import id.co.halloarif.news.models.Res.HomePage.HomepageVoteRecentPostModel;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.support.NetworkUtil;
import id.co.halloarif.news.support.SessionUtil;
import id.co.halloarif.news.utils.Recycleritemclicklistener;

import static id.co.halloarif.news.activities_fragments.afnew.frag_new.BaseHomeFragment.CLICK_METHOD;

public class PostAllActivity extends AppCompatActivity {
    SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView recyclerview_main;

    Recycler_adapter_banner recyclerview_adapter_popular;
    Recyclerview_Adapter_news recyclerview_adapter_random, recyclerview_adapter_recent;
    Recyclerview_Adapter_Videos recyclerview_adapter_videos;
    Recyclerview_adapter_voting_poll recyclerview_adapter_voting_poll;

    ArrayList<Viewpager_items> arrayList_recent = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_popular = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_random = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_videos = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_voting_poll = new ArrayList<>();

    private String userId;
    private boolean loggedin;
    private String typePost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_all);

        initComponent();
        initParam();
        initSession();
        initActionBar();
        initContent();
        initListener();
    }

    private void initComponent() {
        mSwipeRefreshLayout = findViewById(R.id.swipe_container);

        recyclerview_main = (RecyclerView) findViewById(R.id.recyclerview_main);
    }

    private void initParam() {
        Intent bundle = getIntent();
        if (bundle.hasExtra(ISeasonConfig.TYPE_POST)) {
            typePost = bundle.getStringExtra(ISeasonConfig.TYPE_POST);
        }
    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_ID)) {
            userId = SessionUtil.getStringPreferences(ISeasonConfig.KEY_USER_ID, null);
        }
        if (SessionUtil.checkIfExist(ISeasonConfig.LOGGEDIN)) {
            loggedin = SessionUtil.getBoolPreferences(ISeasonConfig.LOGGEDIN, false);
        }
    }

    private void initActionBar() {
        String output = typePost.substring(0, 1).toUpperCase() + typePost.substring(1);
    }

    private void initContent() {
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_blue_dark);

        initRecycleView();

        callHomePageAPICheck();
    }

    private void initRecycleView() {
        if (typePost.equalsIgnoreCase("recent")) {
            recyclerview_adapter_recent = new Recyclerview_Adapter_news(PostAllActivity.this, R.layout.recent_news_layout, arrayList_recent);
            recyclerview_main.setAdapter(recyclerview_adapter_recent);
            LinearLayoutManager linearLayoutManager12 = new LinearLayoutManager(PostAllActivity.this, LinearLayoutManager.VERTICAL, false);
            recyclerview_main.setLayoutManager(linearLayoutManager12);
            recyclerview_main.setHasFixedSize(true);
            recyclerview_main.setNestedScrollingEnabled(false);
        } else if (typePost.equalsIgnoreCase("popular")) {
            recyclerview_adapter_popular = new Recycler_adapter_banner(PostAllActivity.this, R.layout.banner_layout_item, arrayList_popular);
            recyclerview_main.setAdapter(recyclerview_adapter_popular);
            LinearLayoutManager linearLayoutManager123 = new LinearLayoutManager(PostAllActivity.this, LinearLayoutManager.VERTICAL, false);
            recyclerview_main.setLayoutManager(linearLayoutManager123);
            recyclerview_main.setHasFixedSize(true);
            recyclerview_main.setNestedScrollingEnabled(false);
        } else if (typePost.equalsIgnoreCase("random")) {
            recyclerview_adapter_random = new Recyclerview_Adapter_news(PostAllActivity.this, R.layout.recent_news_layout, arrayList_random);
            recyclerview_main.setAdapter(recyclerview_adapter_random);
            LinearLayoutManager linearLayoutManager1234 = new LinearLayoutManager(PostAllActivity.this, LinearLayoutManager.VERTICAL, false);
            recyclerview_main.setLayoutManager(linearLayoutManager1234);
            recyclerview_main.setHasFixedSize(true);
            recyclerview_main.setNestedScrollingEnabled(false);
        } else if (typePost.equalsIgnoreCase("video")) {
            recyclerview_adapter_videos = new Recyclerview_Adapter_Videos(PostAllActivity.this, R.layout.video_item_layout, arrayList_videos);
            recyclerview_main.setAdapter(recyclerview_adapter_videos);
            LinearLayoutManager linearLayoutManager12345 = new LinearLayoutManager(PostAllActivity.this, LinearLayoutManager.VERTICAL, false);
            recyclerview_main.setLayoutManager(linearLayoutManager12345);
            recyclerview_main.setHasFixedSize(true);
            recyclerview_main.setNestedScrollingEnabled(false);
        } else if (typePost.equalsIgnoreCase("voting")) {
            recyclerview_adapter_voting_poll = new Recyclerview_adapter_voting_poll(PostAllActivity.this, R.layout.voting_poll_item, arrayList_voting_poll);
            recyclerview_main.setAdapter(recyclerview_adapter_voting_poll);
            LinearLayoutManager linearLayoutManager123456 = new LinearLayoutManager(PostAllActivity.this, LinearLayoutManager.VERTICAL, false);
            recyclerview_main.setLayoutManager(linearLayoutManager123456);
            recyclerview_main.setHasFixedSize(true);
            recyclerview_main.setNestedScrollingEnabled(false);
            SnapHelper snapHelper2 = new PagerSnapHelper();
            snapHelper2.attachToRecyclerView(recyclerview_main);
        }
    }

    private void initListener() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    mSwipeRefreshLayout.setRefreshing(true);
                    callHomePageAPICheck();
                } catch (NullPointerException e) {
                    Log.d("Lihat", "onRefresh BaseHomeFragment : " + e.toString());
                }
            }
        });

        /*text_breaking_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (breaking_news_id != null) {
                    Intent i = new Intent(PostAllActivity.this, Open_Post_Activity.class);
                    i.putExtra(ISeasonConfig.POST_ID, breaking_news_id);
                    startActivity(i);
                } else {
                    Toast.makeText(PostAllActivity.this, "No info Available", Toast.LENGTH_SHORT).show();
                }
            }
        });*/

        recyclerview_main.addOnItemTouchListener(new Recycleritemclicklistener(PostAllActivity.this, new Recycleritemclicklistener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (typePost.equalsIgnoreCase("recent")) {
                    Viewpager_items model = arrayList_recent.get(position);
                    CLICK_METHOD(PostAllActivity.this, model);
                } else if (typePost.equalsIgnoreCase("popular")) {
                    Viewpager_items model = arrayList_popular.get(position);
                    CLICK_METHOD(PostAllActivity.this, model);
                } else if (typePost.equalsIgnoreCase("random")) {
                    Viewpager_items model = arrayList_random.get(position);
                    CLICK_METHOD(PostAllActivity.this, model);
                } else if (typePost.equalsIgnoreCase("video")) {
                    Viewpager_items model = arrayList_videos.get(position);
                    CLICK_METHOD(PostAllActivity.this, model);
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void callHomePageAPICheck() {
        if (NetworkUtil.isConnected(PostAllActivity.this)) {
            callHomePageAPI();
        } else {
            Snackbar.make(PostAllActivity.this.findViewById(android.R.id.content), R.string.no_internet_connection, Snackbar.LENGTH_INDEFINITE).setAction(R.string.reload, null).show();
        }

    }

    private void callHomePageAPI() {
        String URL = Constants.MAIN_URL + "homepage";
        Log.d("Lihat", "callHomePageAPI BaseHomeFragment : " + URL);

        RequestQueue requestQueue = Volley.newRequestQueue(PostAllActivity.this);
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Lihat", "onResponse BaseHomeFragment : " + response);

                        mSwipeRefreshLayout.setRefreshing(false);

                        arrayList_recent.clear();
                        arrayList_random.clear();
                        arrayList_videos.clear();
                        arrayList_popular.clear();
                        arrayList_voting_poll.clear();

                        JsonParser parser = new JsonParser();
                        JsonElement mJson = parser.parse(response);
                        Gson gson = new Gson();
                        HomepageMainModel mainModel = gson.fromJson(mJson, HomepageMainModel.class);

                        if (mainModel.getStatus() == 1) {
                            HomepageDataModel dataModel = mainModel.getGet_json_data();

                            /*List<HomepageMainPostModel> breakingNewsModel = dataModel.getBreaking_news();
                            String text = "";
                            for (int i = 0; i < breakingNewsModel.size(); i++) {
                                HomepageMainPostModel model = breakingNewsModel.get(i);
                                String titleBreaking = model.getTitle();
                                int pos = i + 1;
                                text = "  <a>  " + "   " + text + " <font color='red'> " + "  " + pos + ".  " + " </font> " + titleBreaking + "  " + "   " + "  </a>  ";
                                text_breaking_news.setText(Html.fromHtml(text));
                            }*/
                            ////////////////////
                            /*List<HomepageMainPostModel> sliderFeaturModel = dataModel.getSlider_featur();
                            Log.d("Lihat", "onResponse BaseHomeFragment : " + sliderFeaturModel.size());
                            for (int i = 0; i < sliderFeaturModel.size(); i++) {
                                HomepageMainPostModel model = sliderFeaturModel.get(i);
                                String id = model.getId();
                                String title = model.getTitle();
                                String category_name = model.getCategory_name_slug();
                                String post_type = model.getPost_type();
                                String content = model.getContent();
                                String created_at = model.getCreated_at();
                                String comment_count = model.getComment_count();
                                String hit = model.getHit();
                                String image_default = "";
                                String embed_code = "";
                                if (post_type.contentEquals("post")) {///Post
                                    if (TextUtils.isEmpty(model.getImage_default())) {
                                        image_default = model.getImage_url();
                                    } else {
                                        image_default = Constants.IMAGE_URL + model.getImage_default();
                                        embed_code = "";
                                    }
                                } else {////Videos
                                    if (TextUtils.isEmpty(model.getImage_default())) {
                                        image_default = model.getImage_url();
                                    } else {
                                        image_default = Constants.IMAGE_URL + model.getImage_default();
                                    }

                                    if (TextUtils.isEmpty(model.getVideo_embed_code())) {
                                        embed_code = model.getVideo_path();
                                    } else {
                                        embed_code = model.getVideo_embed_code();
                                    }
                                }

                                arraylist_slide.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                            }*/
                            ////////////////////////
                            List<HomepageMainPostModel> latestPostModel = dataModel.getLatest_post();
                            for (int i = 0; i < latestPostModel.size(); i++) {
                                HomepageMainPostModel model = latestPostModel.get(i);
                                String id = model.getId();
                                String title = model.getTitle();
                                String category_name = model.getCategory_name_slug();
                                String post_type = model.getPost_type();
                                String content = model.getContent();
                                String created_at = model.getCreated_at();
                                String comment_count = model.getComment_count();
                                String hit = model.getHit();

                                String image_default = "";
                                String embed_code = "";

                                if (post_type.contentEquals("post")) {///Post
                                    if (TextUtils.isEmpty(model.getImage_default())) {
                                        image_default = model.getImage_url();
                                    } else {
                                        image_default = Constants.IMAGE_URL + model.getImage_default();
                                        embed_code = "";
                                    }

                                } else {////Videos
                                    if (TextUtils.isEmpty(model.getImage_default())) {
                                        image_default = model.getImage_url();
                                    } else {
                                        image_default = Constants.IMAGE_URL + model.getImage_default();
                                    }

                                    if (TextUtils.isEmpty(model.getVideo_embed_code())) {
                                        embed_code = model.getVideo_path();
                                    } else {
                                        embed_code = model.getVideo_embed_code();
                                    }
                                }

                                arrayList_recent.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));

                                //  arrayList_recent.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, comment_count));

                            }
                            ////////////////////
                            List<HomepageMainPostModel> popularPostsModel = dataModel.getPopular_posts();
                            for (int i = 0; i < popularPostsModel.size(); i++) {
                                HomepageMainPostModel model = popularPostsModel.get(i);
                                String id = model.getId();
                                String title = model.getTitle();
                                String category_name = model.getCategory_name_slug();
                                String post_type = model.getPost_type();
                                String content = model.getContent();
                                String created_at = model.getCreated_at();
                                String comment_count = model.getComment_count();
                                String hit = model.getHit();
                                String image_default = "";
                                String embed_code = "";

                                if (post_type.contentEquals("post")) {///Post
                                    if (TextUtils.isEmpty(model.getImage_default())) {
                                        image_default = model.getImage_url();
                                    } else {
                                        image_default = Constants.IMAGE_URL + model.getImage_default();
                                        embed_code = "";
                                    }

                                } else {////Videos
                                    if (TextUtils.isEmpty(model.getImage_default())) {
                                        image_default = model.getImage_url();
                                    } else {
                                        image_default = Constants.IMAGE_URL + model.getImage_default();
                                    }

                                    if (TextUtils.isEmpty(model.getVideo_embed_code())) {
                                        embed_code = model.getVideo_path();
                                    } else {
                                        embed_code = model.getVideo_embed_code();
                                    }
                                }

                                arrayList_popular.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                            }
                            /////////////////////////////
                            List<HomepageMainPostModel> randomPostModel = dataModel.getRandom_post();
                            for (int i = 0; i < randomPostModel.size(); i++) {
                                HomepageMainPostModel model = randomPostModel.get(i);
                                String id = model.getId();
                                String title = model.getTitle();
                                String category_name = model.getCategory_name_slug();
                                String post_type = model.getPost_type();
                                String content = model.getContent();
                                String created_at = model.getCreated_at();
                                String comment_count = model.getComment_count();
                                String hit = model.getHit();
                                String image_default = "";
                                String embed_code = "";

                                if (post_type.contentEquals("post")) {///Post
                                    if (TextUtils.isEmpty(model.getImage_default())) {
                                        image_default = model.getImage_url();
                                    } else {
                                        image_default = Constants.IMAGE_URL + model.getImage_default();
                                        embed_code = "";
                                    }

                                } else {////Videos
                                    if (TextUtils.isEmpty(model.getImage_default())) {
                                        image_default = model.getImage_url();
                                    } else {
                                        image_default = Constants.IMAGE_URL + model.getImage_default();
                                    }

                                    if (TextUtils.isEmpty(model.getVideo_embed_code())) {
                                        embed_code = model.getVideo_path();
                                    } else {
                                        embed_code = model.getVideo_embed_code();
                                    }
                                }

                                arrayList_random.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                            }
                            /////////////////////
                            /*List<HomepageVideoModel> recentVideosModel = dataModel.getRecent_videos();
                            for (int i = 0; i < recentVideosModel.size(); i++) {
                                HomepageVideoModel model = recentVideosModel.get(i);
                                String id = model.getId();
                                String title = model.getTitle();
                                String category_name = model.getCategory_name_slug();
                                // image_default = obj.getString("video_image_url");
                                String post_type = model.getPost_type();
                                String created_at = model.getCreated_at();
                                String hit = model.getHit();
                                String image_default = "";
                                String embed_code = "";

                                if (TextUtils.isEmpty(model.getImage_default())) {
                                    image_default = model.getImage_url();
                                } else {
                                    image_default = Constants.IMAGE_URL + model.getImage_default();
                                }

                                if (TextUtils.isEmpty(model.getVideo_embed_code())) {
                                    embed_code = model.getVideo_path();
                                } else {
                                    embed_code = model.getVideo_embed_code();
                                }

                                String content = model.getContent();
                                String comment_count = model.getComment_count();

                                arrayList_videos.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                            }*/
                            ///////////////////////////
                            List<HomepageVoteRecentPostModel> voteRecentPostModels = dataModel.getVote_recent_post();
                            for (int i = 0; i < voteRecentPostModels.size(); i++) {
                                HomepageVoteRecentPostModel model = voteRecentPostModels.get(i);
                                String id = model.getId();
                                String question = model.getQuestion();
                                String option1 = model.getOption1();
                                String option2 = model.getOption2();
                                String option3 = model.getOption3();
                                String option4 = model.getOption4();
                                String option5 = model.getOption5();
                                String option6 = model.getOption6();
                                String option7 = model.getOption7();
                                String option8 = model.getOption8();
                                String option9 = model.getOption9();
                                String option10 = model.getOption10();

                                arrayList_voting_poll.add(new Viewpager_items(id, question, option1, option2, option3, option4, option5, option6, option7, option8, option9, option10));
                            }
                            /////////////////////////////
                            /*List<HomepageTagsSlugModel> tagsSlugModels = dataModel.getTags_slug();
                            for (int i = 0; i < tagsSlugModels.size(); i++) {
                                HomepageTagsSlugModel mode = tagsSlugModels.get(i);

                                String id = mode.getId();
                                String tag = mode.getTag();
                                String tag_slug = mode.getTag_slug();
                                String created_at = mode.getCreated_at();

                                arrayList_tags.add(new Viewpager_items(id, tag, tag_slug, created_at, "", ""));
                            }*/

                            if (typePost.equalsIgnoreCase("recent")) {
                                recyclerview_adapter_recent.setData(arrayList_recent);
                            } else if (typePost.equalsIgnoreCase("popular")) {
                                recyclerview_adapter_popular.setData(arrayList_popular);
                            } else if (typePost.equalsIgnoreCase("random")) {
                                recyclerview_adapter_random.setData(arrayList_random);
                            } else if (typePost.equalsIgnoreCase("video")) {
                                recyclerview_adapter_videos.setData(arrayList_videos);
                            } else if (typePost.equalsIgnoreCase("voting")) {
                                recyclerview_adapter_voting_poll.setData(arrayList_voting_poll);
                            }

                        } else {
                            Snackbar.make(PostAllActivity.this.findViewById(android.R.id.content), mainModel.getError_json(), Snackbar.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mSwipeRefreshLayout.setRefreshing(false);

                        Snackbar.make(PostAllActivity.this.findViewById(android.R.id.content), error.toString(), Snackbar.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
