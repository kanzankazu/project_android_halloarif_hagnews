package id.co.halloarif.news.activities_fragments.afold.activity_old.logreg;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import id.co.halloarif.news.R;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.utils.preference.Preference_saved;
import id.co.halloarif.news.utils.Volley_multipart.MySingleton;
import id.co.halloarif.news.utils.Volley_multipart.VolleyMultipartRequest;

public class Login_Activity extends AppCompatActivity implements View.OnClickListener {
    TextView tvLoginRegfvbi, tvLoginForgetPassfvbi;
    EditText etLoginEmailfvbi, etLoginPasswordfvbi;
    Button bLoginfvbi;
    FrameLayout flLoginProgrfvbi;
    String txt_email, txt_password;
    PopupWindow popupWindow_forgot;
    private VideoView vvLoginfvbi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login1);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();

    }

    private void initComponent() {
        vvLoginfvbi = (VideoView) findViewById(R.id.vvLogin);
        etLoginEmailfvbi = (EditText) findViewById(R.id.etLoginEmail);
        etLoginPasswordfvbi = (EditText) findViewById(R.id.etLoginPassword);
        flLoginProgrfvbi = (FrameLayout) findViewById(R.id.flLoginProgr);
        tvLoginRegfvbi = (TextView) findViewById(R.id.tvLoginReg);
        tvLoginForgetPassfvbi = (TextView) findViewById(R.id.tvLoginForgetPass);
        bLoginfvbi = (Button) findViewById(R.id.bLogin);
    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {
        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.news);
        vvLoginfvbi.setVideoURI(uri);
        vvLoginfvbi.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
                mp.setLooping(true);
            }
        });
    }

    private void initListener() {
        bLoginfvbi.setOnClickListener(this);
        tvLoginRegfvbi.setOnClickListener(this);
        tvLoginForgetPassfvbi.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == tvLoginRegfvbi) {
            moveToRegister();
        } else if (view == tvLoginForgetPassfvbi) {
            moveToForgotPass();
        } else if (view == bLoginfvbi) {
            if (etLoginEmailfvbi.getText().toString().isEmpty()) {
                etLoginEmailfvbi.setError("Enter Email");
            } else if (etLoginPasswordfvbi.getText().toString().isEmpty()) {
                etLoginPasswordfvbi.setError("Enter Password");
            } else {
                txt_email = etLoginEmailfvbi.getText().toString();
                txt_password = etLoginPasswordfvbi.getText().toString();
                Call_Login_API(txt_email, txt_password);
            }
        }
    }

    private void moveToForgotPass() {
        startActivity(new Intent(Login_Activity.this, Webview_Forgot_Password.class));
    }

    private void moveToRegister() {
        startActivity(new Intent(Login_Activity.this, Register_Activity.class));
    }

    private void Call_Login_API(final String txt_email, final String txt_password) {
        flLoginProgrfvbi.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        // String URL_POST = "http://www.halloarif.co.id/news/webapi/app/login";
        String URL = Constants.MAIN_URL + "login";
        Log.e("tab_DATA_login_URL", Constants.MAIN_URL + "login/" + txt_email + "/" + txt_password);
        StringRequest strRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("tab_DATA_login", "" + response.trim());
                        try {
                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonobject = ojs.getJSONObject("get_json_data");
                                String id = jsonobject.getString("id");
                                String username = jsonobject.getString("username");
                                String email = jsonobject.getString("email");
                                String avatar = jsonobject.getString("avatar");
                                String about_me = jsonobject.getString("about_me");
                                if (avatar.equalsIgnoreCase("null")) {
                                    avatar = "";
                                }

                                Constants.USER_EMAIL = email;
                                Preference_saved.getInstance(getApplicationContext()).set_check_login("true");
                                Preference_saved.getInstance(getApplicationContext()).setUsername(username);
                                Preference_saved.getInstance(getApplicationContext()).setUser_Email(email);

                                Preference_saved.getInstance(getApplicationContext()).setuserPicture(avatar);
                                Preference_saved.getInstance(getApplicationContext()).setUser_id(id);
                                flLoginProgrfvbi.setVisibility(View.GONE);

                                Log.e("USer_Login", "id" + id + "username" + username);
                                Toast.makeText(getApplicationContext(), "Successfully Login", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                flLoginProgrfvbi.setVisibility(View.GONE);

                                Toast.makeText(getApplicationContext(), "Invalid Credential !", Toast.LENGTH_SHORT).show();
                                Preference_saved.getInstance(getApplicationContext()).set_check_login("false");

                                String error = ojs.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        flLoginProgrfvbi.setVisibility(View.GONE);

                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", txt_email);
                params.put("pass", txt_password);
                return params;
            }
        };
        requestQueue.add(strRequest);

    }

    private void ForgotPassPopUp() {
        try {
            // We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = LayoutInflater.from(Login_Activity.this).inflate(R.layout.popup_forgot_pass, null);
            RelativeLayout mainlayout = layout.findViewById(R.id.rlPopForgetPass);
            mainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  popupWindow_selct.dismiss();
                }
            });
            ImageView close_dialog = layout.findViewById(R.id.ivPopForgetPassClose);
            close_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow_forgot.dismiss();
                }
            });

            final EditText edttxt_text_comment = layout.findViewById(R.id.etPopForgetPassEmail);
            Button Btn_submit = layout.findViewById(R.id.bPopForgetPassSubmit);
            Btn_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String text_reply = edttxt_text_comment.getText().toString();
                    if (text_reply.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Enter your email", Toast.LENGTH_SHORT).show();
                        //edttxt_text_comment.setError("write something...");
                    } else {
                        CALL_Forgot_Pass_API(edttxt_text_comment, text_reply);
                    }
                }
            });

            popupWindow_forgot = new PopupWindow(layout, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT, true);
            popupWindow_forgot.showAtLocation(layout, Gravity.CENTER, 0, 0);

            // popupWindowlogin.showAtLocation(layout, Gravity.CENTER, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void CALL_Forgot_Pass_API(final EditText edttxt_email, final String text_reply) {
        flLoginProgrfvbi.setVisibility(View.VISIBLE);

        String URL = Constants.MAIN_URL + "forgetpass";
        // String URL = "http://www.halloarif.co.id/news/webforget/forget-pass/" + text_reply;
        Log.e("URL_FORGOT_PASS", URL);
        // String url = "http://www.angga-ari.com/api/something/awesome";
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, URL, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    Log.e("RESPONSE_Forgotpass", "" + resultResponse);
                    JSONObject ojs = new JSONObject(resultResponse);
                    String abc = ojs.getString("status");
                    Log.e("Value", abc);
                    if (Integer.parseInt(abc) == 1) {
                        JSONObject jsonObject = ojs.getJSONObject("get_json_data");
                        JSONObject jsonObject1 = jsonObject.getJSONObject("forget_pass");
                        String title = jsonObject1.getString("title");
                        flLoginProgrfvbi.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), title, Toast.LENGTH_SHORT).show();
                        edttxt_email.setText("");
                        popupWindow_forgot.dismiss();
                    } else {
                        flLoginProgrfvbi.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Incorrect Data", Toast.LENGTH_SHORT).show();
                        //  Log.e("error", error);*//**//*
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");
                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                flLoginProgrfvbi.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", text_reply);
                return params;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(multipartRequest);
    }
}
