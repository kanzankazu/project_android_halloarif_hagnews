package id.co.halloarif.news.models.Res.Comment;

import java.util.ArrayList;
import java.util.List;

public class CommentJsonModel {
    public List<CommentUserModel> user_comment = new ArrayList<>();

    public List<CommentUserModel> getUser_comment() {
        return user_comment;
    }

    public void setUser_comment(List<CommentUserModel> user_comment) {
        this.user_comment = user_comment;
    }
}
