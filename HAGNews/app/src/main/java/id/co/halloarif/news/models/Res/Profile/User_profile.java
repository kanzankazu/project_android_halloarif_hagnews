package id.co.halloarif.news.models.Res.Profile;

class User_profile {
    public String id;
    public String username;
    public String slug;
    public String email;
    public String email_status;
    public String token;
    public String role;
    public String user_type;
    public String google_id;
    public String facebook_id;
    public String avatar;
    public String status;
    public String about_me;
    public String facebook_url;
    public String twitter_url;
    public String google_url;
    public String instagram_url;
    public String pinterest_url;
    public String linkedin_url;
    public String vk_url;
    public String youtube_url;
    public String last_seen;
    public String created_at;
}
