package id.co.halloarif.news.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import id.co.halloarif.news.support.SessionUtil;

public class SQLiteHelper extends SQLiteOpenHelper {
    // Databases information
    public static final String DB_NM = "hagnews.db";
    public static final int DB_VER = 3;

    public static String TableUserData = "tabUserData";
    private static final String query_delete_table_UserData = "DROP TABLE IF EXISTS " + TableUserData;
    public static String KEY_UserData_id = "id";
    public static String KEY_UserData_username = "username";
    public static String KEY_UserData_slug = "slug";
    public static String KEY_UserData_email = "email";
    public static String KEY_UserData_email_status = "email_status";
    public static String KEY_UserData_token = "token";
    public static String KEY_UserData_role = "role";
    public static String KEY_UserData_user_type = "user_type";
    public static String KEY_UserData_google_id = "google_id";
    public static String KEY_UserData_facebook_id = "facebook_id";
    public static String KEY_UserData_avatar = "avatar";
    public static String KEY_UserData_status = "status";
    public static String KEY_UserData_about_me = "about_me";
    public static String KEY_UserData_facebook_url = "facebook_url";
    public static String KEY_UserData_twitter_url = "twitter_url";
    public static String KEY_UserData_google_url = "google_url";
    public static String KEY_UserData_instagram_url = "instagram_url";
    public static String KEY_UserData_pinterest_url = "pinterest_url";
    public static String KEY_UserData_linkedin_url = "linkedin_url";
    public static String KEY_UserData_vk_url = "vk_url";
    public static String KEY_UserData_youtube_url = "youtube_url";
    public static String KEY_UserData_last_seen = "last_seen";
    public static String KEY_UserData_created_at = "created_at";

    private static final String query_add_table_UserData = "CREATE TABLE IF NOT EXISTS " + TableUserData + "("
            + KEY_UserData_id + " INTEGER, "
            + KEY_UserData_username + " TEXT, "
            + KEY_UserData_slug + " TEXT, "
            + KEY_UserData_email + " TEXT, "
            + KEY_UserData_email_status + " TEXT, "
            + KEY_UserData_token + " TEXT, "
            + KEY_UserData_role + " TEXT, "
            + KEY_UserData_user_type + " TEXT, "
            + KEY_UserData_google_id + " TEXT, "
            + KEY_UserData_facebook_id + " TEXT, "
            + KEY_UserData_avatar + " TEXT, "
            + KEY_UserData_status + " TEXT, "
            + KEY_UserData_about_me + " TEXT, "
            + KEY_UserData_facebook_url + " TEXT, "
            + KEY_UserData_twitter_url + " TEXT, "
            + KEY_UserData_google_url + " TEXT, "
            + KEY_UserData_instagram_url + " TEXT, "
            + KEY_UserData_pinterest_url + " TEXT, "
            + KEY_UserData_linkedin_url + " TEXT, "
            + KEY_UserData_vk_url + " TEXT, "
            + KEY_UserData_youtube_url + " TEXT, "
            + KEY_UserData_last_seen + " TEXT, "
            + KEY_UserData_created_at + " TEXT) ";

    public static String TableCategoriesHeader = "tabCategoriesHeader";
    private static final String query_delete_table_CategoriesHeader = "DROP TABLE IF EXISTS " + TableCategoriesHeader;
    public static String KEY_CategoriesHeader_id = "id";
    public static String KEY_CategoriesHeader_title = "title";
    public static String KEY_CategoriesHeader_slug = "slug";
    public static String KEY_CategoriesHeader_parent_id = "parent_id";
    public static String KEY_CategoriesHeader_page_type = "page_type";
    public static String KEY_CategoriesHeader_menu_title = "menu_title";
    public static String KEY_CategoriesHeader_status = "status";

    private static final String query_add_table_CategoriesHeader = "CREATE TABLE IF NOT EXISTS " + TableCategoriesHeader + "("
            + KEY_CategoriesHeader_id + " INTEGER, "
            + KEY_CategoriesHeader_title + " TEXT, "
            + KEY_CategoriesHeader_slug + " TEXT, "
            + KEY_CategoriesHeader_parent_id + " TEXT, "
            + KEY_CategoriesHeader_page_type + " TEXT, "
            + KEY_CategoriesHeader_menu_title + " TEXT, "
            + KEY_CategoriesHeader_status + " TEXT) ";

    public SQLiteHelper(Context context) {
        super(context, DB_NM, null, DB_VER);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(query_add_table_UserData);
        db.execSQL(query_add_table_CategoriesHeader);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        SessionUtil.removeAllSharedPreferences();

        db.execSQL(query_delete_table_UserData);
        db.execSQL(query_delete_table_CategoriesHeader);

        onCreate(db);

    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        SessionUtil.removeAllSharedPreferences();

        db.execSQL(query_delete_table_UserData);
        db.execSQL(query_delete_table_CategoriesHeader);

        onCreate(db);

    }
}