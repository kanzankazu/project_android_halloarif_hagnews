package id.co.halloarif.news.models.Res.HomePage;

public class HomepageVideoModel {
    private String id;
    private String title;
    private String title_slug;
    private String keywords;
    private String summary;
    private String content;
    private String category_id;
    private String subcategory_id;
    private String image_big;
    private String image_default;
    private String image_slider;
    private String image_mid;
    private String image_small;
    private String hit;
    private String optional_url;
    private String need_auth;
    private String is_slider;
    private String slider_order;
    private String is_featured;
    private String featured_order;
    private String is_recommended;
    private String is_breaking;
    private String visibility;
    private String post_type;
    private String video_path;
    private String video_image_url;
    private String video_embed_code;
    private String embed_id;
    private String user_id;
    private String status;
    private String feed_id;
    private String post_url;
    private String created_at;
    private String username;
    private String user_slug;
    private String user_image;
    private String comment_count;
    private String post_comment;
    private String tags;
    private String category_name;
    private String category_name_slug;
    private String sub_category_name;
    private String sub_category_name_slug;
    private String share_facebook;
    private String share_twitter;
    private String share_plus_google;
    private String share_linkedin;
    private String share_tumblr;
    private String share_pinterest;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle_slug() {
        return title_slug;
    }

    public void setTitle_slug(String title_slug) {
        this.title_slug = title_slug;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getSubcategory_id() {
        return subcategory_id;
    }

    public void setSubcategory_id(String subcategory_id) {
        this.subcategory_id = subcategory_id;
    }

    public String getImage_big() {
        return image_big;
    }

    public void setImage_big(String image_big) {
        this.image_big = image_big;
    }

    public String getImage_default() {
        return image_default;
    }

    public void setImage_default(String image_default) {
        this.image_default = image_default;
    }

    public String getImage_slider() {
        return image_slider;
    }

    public void setImage_slider(String image_slider) {
        this.image_slider = image_slider;
    }

    public String getImage_mid() {
        return image_mid;
    }

    public void setImage_mid(String image_mid) {
        this.image_mid = image_mid;
    }

    public String getImage_small() {
        return image_small;
    }

    public void setImage_small(String image_small) {
        this.image_small = image_small;
    }

    public String getHit() {
        return hit;
    }

    public void setHit(String hit) {
        this.hit = hit;
    }

    public String getOptional_url() {
        return optional_url;
    }

    public void setOptional_url(String optional_url) {
        this.optional_url = optional_url;
    }

    public String getNeed_auth() {
        return need_auth;
    }

    public void setNeed_auth(String need_auth) {
        this.need_auth = need_auth;
    }

    public String getIs_slider() {
        return is_slider;
    }

    public void setIs_slider(String is_slider) {
        this.is_slider = is_slider;
    }

    public String getSlider_order() {
        return slider_order;
    }

    public void setSlider_order(String slider_order) {
        this.slider_order = slider_order;
    }

    public String getIs_featured() {
        return is_featured;
    }

    public void setIs_featured(String is_featured) {
        this.is_featured = is_featured;
    }

    public String getFeatured_order() {
        return featured_order;
    }

    public void setFeatured_order(String featured_order) {
        this.featured_order = featured_order;
    }

    public String getIs_recommended() {
        return is_recommended;
    }

    public void setIs_recommended(String is_recommended) {
        this.is_recommended = is_recommended;
    }

    public String getIs_breaking() {
        return is_breaking;
    }

    public void setIs_breaking(String is_breaking) {
        this.is_breaking = is_breaking;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getVideo_path() {
        return video_path;
    }

    public void setVideo_path(String video_path) {
        this.video_path = video_path;
    }

    public String getVideo_image_url() {
        return video_image_url;
    }

    public void setVideo_image_url(String video_image_url) {
        this.video_image_url = video_image_url;
    }

    public String getVideo_embed_code() {
        return video_embed_code;
    }

    public void setVideo_embed_code(String video_embed_code) {
        this.video_embed_code = video_embed_code;
    }

    public String getEmbed_id() {
        return embed_id;
    }

    public void setEmbed_id(String embed_id) {
        this.embed_id = embed_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFeed_id() {
        return feed_id;
    }

    public void setFeed_id(String feed_id) {
        this.feed_id = feed_id;
    }

    public String getPost_url() {
        return post_url;
    }

    public void setPost_url(String post_url) {
        this.post_url = post_url;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUser_slug() {
        return user_slug;
    }

    public void setUser_slug(String user_slug) {
        this.user_slug = user_slug;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getComment_count() {
        return comment_count;
    }

    public void setComment_count(String comment_count) {
        this.comment_count = comment_count;
    }

    public String getPost_comment() {
        return post_comment;
    }

    public void setPost_comment(String post_comment) {
        this.post_comment = post_comment;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCategory_name_slug() {
        return category_name_slug;
    }

    public void setCategory_name_slug(String category_name_slug) {
        this.category_name_slug = category_name_slug;
    }

    public String getSub_category_name() {
        return sub_category_name;
    }

    public void setSub_category_name(String sub_category_name) {
        this.sub_category_name = sub_category_name;
    }

    public String getSub_category_name_slug() {
        return sub_category_name_slug;
    }

    public void setSub_category_name_slug(String sub_category_name_slug) {
        this.sub_category_name_slug = sub_category_name_slug;
    }

    public String getShare_facebook() {
        return share_facebook;
    }

    public void setShare_facebook(String share_facebook) {
        this.share_facebook = share_facebook;
    }

    public String getShare_twitter() {
        return share_twitter;
    }

    public void setShare_twitter(String share_twitter) {
        this.share_twitter = share_twitter;
    }

    public String getShare_plus_google() {
        return share_plus_google;
    }

    public void setShare_plus_google(String share_plus_google) {
        this.share_plus_google = share_plus_google;
    }

    public String getShare_linkedin() {
        return share_linkedin;
    }

    public void setShare_linkedin(String share_linkedin) {
        this.share_linkedin = share_linkedin;
    }

    public String getShare_tumblr() {
        return share_tumblr;
    }

    public void setShare_tumblr(String share_tumblr) {
        this.share_tumblr = share_tumblr;
    }

    public String getShare_pinterest() {
        return share_pinterest;
    }

    public void setShare_pinterest(String share_pinterest) {
        this.share_pinterest = share_pinterest;
    }
}
