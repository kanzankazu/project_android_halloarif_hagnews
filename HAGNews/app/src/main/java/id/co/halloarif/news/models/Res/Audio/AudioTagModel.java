package id.co.halloarif.news.models.Res.Audio;

public class AudioTagModel {
    public String id;
    public String post_id;
    public String tag;
    public String tag_slug;
    public String created_at;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTag_slug() {
        return tag_slug;
    }

    public void setTag_slug(String tag_slug) {
        this.tag_slug = tag_slug;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
