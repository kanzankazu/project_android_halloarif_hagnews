package id.co.halloarif.news.models.Res.Video;

import android.nfc.Tag;

import java.util.List;

class VideoPostsDataModel {
    public String id;
    public String title;
    public String title_slug;
    public String keywords;
    public String summary;
    public String content;
    public String category_id;
    public String subcategory_id;
    public String image_big;
    public String image_default;
    public String image_slider;
    public String image_mid;
    public String image_small;
    public String hit;
    public String optional_url;
    public String need_auth;
    public String is_slider;
    public String slider_order;
    public String is_featured;
    public String featured_order;
    public String is_recommended;
    public String is_breaking;
    public String visibility;
    public String post_type;
    public String video_path;
    public String video_image_url;
    public String video_embed_code;
    public String embed_id;
    public String user_id;
    public String status;
    public String feed_id;
    public String post_url;
    public String created_at;
    public String username;
    public String user_slug;
    public String user_image;
    public String comment_count;
    public VideoCommentModel post_comment;
    public List<VideoTagModel> tags = null;
    public String category_name;
    public String category_name_slug;
    public String sub_category_name;
    public String sub_category_name_slug;
    public String share_facebook;
    public String share_twitter;
    public String share_plus_google;
    public String share_linkedin;
    public String share_tumblr;
    public String share_pinterest;
}
