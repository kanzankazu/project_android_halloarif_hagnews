package id.co.halloarif.news.activities_fragments.afold.frag_old;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afold.activity_old.MainActivity;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_Adapter_Videos;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.utils.Recycleritemclicklistener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Videos_Fragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    RecyclerView recyclerview_all_videos;
    ArrayList<Viewpager_items> arrayList_videos = new ArrayList<>();
    FragmentActivity fragmentActivity;
    SwipeRefreshLayout mSwipeRefreshLayout;

    public Videos_Fragment() {
        // Required empty public constructor
    }

    public static Videos_Fragment newInstance() {
        Videos_Fragment fragment = new Videos_Fragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_videos, container, false);

        initComponent(view);


        // SwipeRefreshLayout
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_blue_dark);

        mSwipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                mSwipeRefreshLayout.setRefreshing(true);

                // Fetching data from server
                CALL_API_ALL_VIDEOS();
            }
        });


        recyclerview_all_videos.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        Viewpager_items model = arrayList_videos.get(position);
                        String embed_url = model.getEmbed_code();
                        String video_title = model.getTitle();
                        String video_id = model.getId();
                        String created_at = model.getCreated_at();
                        String video_views = model.getHit();
                        String content = model.getContent();

                        ((MainActivity) fragmentActivity).draggableView.setVisibility(View.VISIBLE);
                        ((MainActivity) fragmentActivity).draggableView.maximize();
                        ((MainActivity) fragmentActivity).Call_VIDEO_Details_API(video_id);
                        ((MainActivity) fragmentActivity).CAll_API_ALL_COMMENTS(video_id);
                        ((MainActivity) fragmentActivity).PLAYER_INITIALIZE(embed_url);
                        ((MainActivity) fragmentActivity).setText_values(video_title, video_views, content, created_at);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                })
        );


        return view;
    }

    private void CALL_API_ALL_VIDEOS() {

        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(getActivity());

        String URL = Constants.MAIN_URL + "video";
        Log.e("URL_TAB", Constants.MAIN_URL + "video");
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("tab_DATA", "" + response.trim());
                        try {
                            arrayList_videos.clear();
                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                // JSONArray jsonArray = ojs.getJSONArray("get_json_data");
                                JSONObject jsonArray = ojs.getJSONObject("get_json_data");


                                JSONArray jsonArray12 = jsonArray.getJSONArray("posts_data");
                                for (int i = 0; i < jsonArray12.length(); i++) {

                                    JSONObject obj = jsonArray12.getJSONObject(i);
                                    String image_default = "";
                                    String id = obj.getString("id");
                                    String title = obj.getString("title");
                                    String category_name = obj.getString("category_name_slug");
                                    //String image_default = obj.getString("video_image_url");
                                    String post_type = obj.getString("post_type");
                                    String created_at = obj.getString("created_at");
                                    String hit = obj.getString("hit");

                                    if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                        image_default = obj.getString("video_image_url");
                                    } else {
                                        image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                    }


                                  /*  if (obj.getString("video_image_url").contentEquals("") || obj.getString("video_image_url").isEmpty()) {
                                        image_default = Constants.IMAGE_URL + obj.getString("image_mid");
                                    } else {
                                        image_default = obj.getString("video_image_url");
                                    }*/
                                    String embed_code = obj.getString("video_embed_code");
                                    String comment_count = obj.getString("comment_count");
                                    String content = obj.getString("content");

                                    arrayList_videos.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                                }

                                if (arrayList_videos.isEmpty()) {

                                } else {
                                    Recyclerview_Adapter_Videos recyclerview_adapter_videos = new Recyclerview_Adapter_Videos(getActivity(), R.layout.all_videos_main_item, arrayList_videos);
                                    recyclerview_all_videos.setAdapter(recyclerview_adapter_videos);
                                    recyclerview_adapter_videos.notifyDataSetChanged();
                                }
                            } else {
                               /*String error = ojs.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);*/
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        mSwipeRefreshLayout.setRefreshing(false);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mSwipeRefreshLayout.setRefreshing(false);

                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);
    }

    private void initComponent(View view) {

        recyclerview_all_videos = view.findViewById(R.id.recyclerview_all_videos);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerview_all_videos.setLayoutManager(mLayoutManager);
        recyclerview_all_videos.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(1), true));
        recyclerview_all_videos.setItemAnimator(new DefaultItemAnimator());

    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public void onRefresh() {
        CALL_API_ALL_VIDEOS();
    }

    private class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }


}
