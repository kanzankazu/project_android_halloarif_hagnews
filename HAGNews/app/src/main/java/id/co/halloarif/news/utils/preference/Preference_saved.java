package id.co.halloarif.news.utils.preference;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ST_004 on 28-09-2017.
 */

public class Preference_saved {

    private static final String VALUES_STORE = "values";
    @SuppressLint("StaticFieldLeak")
    private static Preference_saved instance = null;
    private Context context;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor editor;

    private Preference_saved(Context context) {
        if (context == null)
            return;
        mPreferences = context.getSharedPreferences(VALUES_STORE, Context.MODE_PRIVATE);
        // mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.context = context;
    }

    public static Preference_saved getInstance(Context context) {
        if (instance == null || instance.context == null) {
            instance = new Preference_saved(context);
        }
        return instance;
    }

    public String getUser_Email() {
        return mPreferences.getString("useremail", "");
    }

    public void setUser_Email(String user_email) {
        editor = mPreferences.edit();
        editor.putString("useremail", user_email);
        editor.apply();
    }

    public String getUsername() {
        return mPreferences.getString("username", "");
    }

    public void setUsername(String username) {
        editor = mPreferences.edit();
        editor.putString("username", username);
        editor.apply();
    }

    public String getUser_id() {
        return mPreferences.getString("user_id", "");
    }

    public void setUser_id(String user_id) {
        editor = mPreferences.edit();
        editor.putString("user_id", user_id);
        editor.apply();
    }

    public String getAPIToken() {
        return mPreferences.getString("api_token", "");
    }

    public void setAPIToken(String api_token) {
        editor = mPreferences.edit();
        editor.putString("api_token", api_token);
        editor.apply();
    }

    public String get_Base_url() {
        return mPreferences.getString("base_url", "");
    }

    public void setBase_url(String api_token) {
        editor = mPreferences.edit();
        editor.putString("base_url", api_token);
        editor.apply();
    }

    public String get_Check_From() {
        return mPreferences.getString("check_from", "");
    }

    public void set_Check_From(String from) {
        editor = mPreferences.edit();
        editor.putString("check_from", from);
        editor.apply();
    }

    public String getName() {
        return mPreferences.getString("Name", "");
    }

    public void setName(String Name) {
        editor = mPreferences.edit();
        editor.putString("Name", Name);
        editor.apply();
    }

    public String getFirstName() {
        return mPreferences.getString("firstName", "");
    }

    public void setFirstName(String firstName) {
        editor = mPreferences.edit();
        editor.putString("firstName", firstName);
        editor.apply();
    }

    public String getuserPicture() {
        return mPreferences.getString("userPicture", "");
    }

    public void setuserPicture(String userPicture) {
        editor = mPreferences.edit();
        editor.putString("userPicture", userPicture);
        editor.apply();
    }

    public String getloginType() {
        return mPreferences.getString("loginType", "");
    }

    public void setloginType(String loginType) {
        editor = mPreferences.edit();
        editor.putString("loginType", loginType);
        editor.apply();
    }

    public String check_login() {
        return mPreferences.getString(VALUES_STORE, "");
    }

    public void set_check_login(String check_login) {
        editor = mPreferences.edit();
        editor.putString(VALUES_STORE, check_login);
        editor.apply();
    }

    public void clear() {
        editor = mPreferences.edit();
        editor.remove(VALUES_STORE);
        // SharedPreferences.Editor.clear();
        editor.apply();
    }

}
