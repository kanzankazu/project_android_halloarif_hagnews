package id.co.halloarif.news.activities_fragments.afnew.activity_new;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import id.co.halloarif.news.ISeasonConfig;
import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afnew.frag_new.BaseHomeFragment;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_Adapter_news;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.support.SessionUtil;
import id.co.halloarif.news.utils.Recycleritemclicklistener;

public class PostReadingListActivity extends AppCompatActivity {

    TextView text_header;
    RecyclerView recyclerview_post;
    ArrayList<Viewpager_items> arrayList_all_post = new ArrayList<>();
    ImageView Iv_back;
    SwipeRefreshLayout mSwipeRefreshLayout;

    private String userId;
    private Recyclerview_Adapter_news recyclerview_adapter_news;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_reading_list);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();
    }

    private void initComponent() {
        mSwipeRefreshLayout = findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_blue_dark);
        Iv_back = findViewById(R.id.Iv_back);
        text_header = findViewById(R.id.text_header);
        recyclerview_post = findViewById(R.id.recyclerview_post);
        LinearLayoutManager linearLayoutManager12 = new LinearLayoutManager(PostReadingListActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerview_post.setLayoutManager(linearLayoutManager12);
        recyclerview_post.setHasFixedSize(true);
        recyclerview_post.setNestedScrollingEnabled(false);
    }

    private void initParam() {

    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_ID)) {
            userId = SessionUtil.getStringPreferences(ISeasonConfig.KEY_USER_ID, null);
        }
    }

    private void initContent() {
        recyclerview_adapter_news = new Recyclerview_Adapter_news(PostReadingListActivity.this, R.layout.recent_news_layout, arrayList_all_post);
        recyclerview_post.setAdapter(recyclerview_adapter_news);
        LinearLayoutManager linearLayoutManager12 = new LinearLayoutManager(PostReadingListActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerview_post.setLayoutManager(linearLayoutManager12);
        recyclerview_post.setHasFixedSize(true);
        recyclerview_post.setNestedScrollingEnabled(false);

        CALL_API_READ_LIST();
    }

    private void initListener() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);

                CALL_API_READ_LIST();
            }
        });
        Iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        recyclerview_post.addOnItemTouchListener(new Recycleritemclicklistener(PostReadingListActivity.this, new Recycleritemclicklistener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        try {
                            Viewpager_items model = arrayList_all_post.get(position);
                            BaseHomeFragment.CLICK_METHOD(PostReadingListActivity.this, model);
                        } catch (Exception e) {
                            Log.e("ERROE", "" + e);
                        }

                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                })
        );

    }

    private void CALL_API_READ_LIST() {
        ProgressDialog progressDialog = ProgressDialog.show(PostReadingListActivity.this, "Loading...", "Please Wait..", false, false);

        String URL = Constants.MAIN_URL + "reading-list/" + userId;
        Log.e("URL_READING_LIST", URL);

        RequestQueue requestQueue = Volley.newRequestQueue(PostReadingListActivity.this);
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mSwipeRefreshLayout.setRefreshing(false);
                progressDialog.dismiss();
                arrayList_all_post.clear();

                Log.d("Lihat", "onResponse PostReadingListActivity : " + response.trim());

                try {
                    arrayList_all_post.clear();
                    JSONObject ojs = new JSONObject(response);
                    String abc = ojs.getString("status");
                    Log.e("Value", abc);
                    if (Integer.parseInt(abc) == 1) {
                        // JSONObject jsonArray = ojs.getJSONObject("cat_json_data");
                        JSONArray jsonArray12 = ojs.getJSONArray("get_json_data");
                        for (int i = 0; i < jsonArray12.length(); i++) {
                            JSONObject obj = jsonArray12.getJSONObject(i);

                            String id = obj.getString("id");
                            String title = obj.getString("title");
                            String category_name = obj.getString("category_name_slug");
                            String post_type = obj.getString("post_type");
                            String content = obj.getString("content");

                            String image_default;
                            String embed_code = "";

                            if (post_type.contentEquals("post")) {///Post
                                if (TextUtils.isEmpty("image_default")) {
                                    image_default = obj.getString("image_url");
                                } else {
                                    image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                    embed_code = "";
                                }
                            } else {////Videos
                                if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                    image_default = obj.getString("image_url");
                                } else {
                                    image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                }

                                if (TextUtils.isEmpty(obj.getString("video_embed_code"))) {
                                    embed_code = obj.getString("video_path");
                                } else {
                                    embed_code = obj.getString("video_embed_code");
                                }
                            }
                            // String category_name = obj.getString("category_name");
                            String created_at = obj.getString("created_at");
                            // String comment_count =obj.getString("comment_count");
                            String hit = obj.getString("hit");

                            arrayList_all_post.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, ""));
                        }

                        if (!arrayList_all_post.isEmpty()) {
                            recyclerview_adapter_news.setData(arrayList_all_post);
                        } else {
                            Log.e("EMPTY", "EMPTY");
                        }

                    } else {
                        JSONObject jsonArray13 = ojs.getJSONObject("get_json_data");
                        String error = jsonArray13.getString("error");
                        Toast.makeText(PostReadingListActivity.this, error, Toast.LENGTH_SHORT).show();
                        Log.e("error", error);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        progressDialog.dismiss();
                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);

    }
}
