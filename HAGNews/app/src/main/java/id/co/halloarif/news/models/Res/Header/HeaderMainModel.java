package id.co.halloarif.news.models.Res.Header;

import java.util.List;

public class HeaderMainModel {
    public List<HeaderDataModel> get_json_data = null;
    public String error_json;
    public Integer status;

    public List<HeaderDataModel> getGet_json_data() {
        return get_json_data;
    }

    public void setGet_json_data(List<HeaderDataModel> get_json_data) {
        this.get_json_data = get_json_data;
    }

    public String getError_json() {
        return error_json;
    }

    public void setError_json(String error_json) {
        this.error_json = error_json;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
