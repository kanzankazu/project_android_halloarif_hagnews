package id.co.halloarif.news.models.Res.Gallery;

import java.util.List;

class GalleryJsonDataItem {
    public List<GalleryCatDataModel> cat_data = null;
    public List<GalleryPostsDataModel> posts_data = null;
}
