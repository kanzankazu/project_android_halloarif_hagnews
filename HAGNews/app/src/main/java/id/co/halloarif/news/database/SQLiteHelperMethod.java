package id.co.halloarif.news.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;

import id.co.halloarif.news.models.Res.Header.HeaderDataModel;
import id.co.halloarif.news.models.Res.User.UserJsonDataItem;

public class SQLiteHelperMethod extends SQLiteHelper {

    public SQLiteHelperMethod(Context context) {
        super(context);
    }

    /**/
    public void userDataSave(UserJsonDataItem model) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_UserData_id, model.getId());
        contentValues.put(KEY_UserData_username, model.getUsername());
        contentValues.put(KEY_UserData_slug, model.getSlug());
        contentValues.put(KEY_UserData_email, model.getEmail());
        contentValues.put(KEY_UserData_email_status, model.getEmail_status());
        contentValues.put(KEY_UserData_token, model.getToken());
        contentValues.put(KEY_UserData_role, model.getRole());
        contentValues.put(KEY_UserData_user_type, model.getUser_type());
        contentValues.put(KEY_UserData_google_id, model.getGoogle_id());
        contentValues.put(KEY_UserData_facebook_id, model.getFacebook_id());
        contentValues.put(KEY_UserData_avatar, model.getAvatar());
        contentValues.put(KEY_UserData_status, model.getStatus());
        contentValues.put(KEY_UserData_about_me, model.getAbout_me());
        contentValues.put(KEY_UserData_facebook_url, model.getFacebook_url());
        contentValues.put(KEY_UserData_twitter_url, model.getTwitter_url());
        contentValues.put(KEY_UserData_google_url, model.getGoogle_url());
        contentValues.put(KEY_UserData_instagram_url, model.getInstagram_url());
        contentValues.put(KEY_UserData_pinterest_url, model.getPinterest_url());
        contentValues.put(KEY_UserData_linkedin_url, model.getLinkedin_url());
        contentValues.put(KEY_UserData_vk_url, model.getVk_url());
        contentValues.put(KEY_UserData_youtube_url, model.getYoutube_url());
        contentValues.put(KEY_UserData_last_seen, model.getLast_seen());
        contentValues.put(KEY_UserData_created_at, model.getCreated_at());
        db.insert(TableUserData, null, contentValues);
        db.close();
    }

    public void userDataUpdate(UserJsonDataItem model, String accountId) {
        Log.d("Lihat", "userDataUpdate SQLiteHelperMethod : " + accountId);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_UserData_username, model.getUsername());
        contentValues.put(KEY_UserData_slug, model.getSlug());
        contentValues.put(KEY_UserData_email, model.getEmail());
        contentValues.put(KEY_UserData_email_status, model.getEmail_status());
        contentValues.put(KEY_UserData_token, model.getToken());
        contentValues.put(KEY_UserData_role, model.getRole());
        contentValues.put(KEY_UserData_user_type, model.getUser_type());
        contentValues.put(KEY_UserData_google_id, model.getGoogle_id());
        contentValues.put(KEY_UserData_facebook_id, model.getFacebook_id());
        contentValues.put(KEY_UserData_avatar, model.getAvatar());
        contentValues.put(KEY_UserData_status, model.getStatus());
        contentValues.put(KEY_UserData_about_me, model.getAbout_me());
        contentValues.put(KEY_UserData_facebook_url, model.getFacebook_url());
        contentValues.put(KEY_UserData_twitter_url, model.getTwitter_url());
        contentValues.put(KEY_UserData_google_url, model.getGoogle_url());
        contentValues.put(KEY_UserData_instagram_url, model.getInstagram_url());
        contentValues.put(KEY_UserData_pinterest_url, model.getPinterest_url());
        contentValues.put(KEY_UserData_linkedin_url, model.getLinkedin_url());
        contentValues.put(KEY_UserData_vk_url, model.getVk_url());
        contentValues.put(KEY_UserData_youtube_url, model.getYoutube_url());
        contentValues.put(KEY_UserData_last_seen, model.getLast_seen());
        contentValues.put(KEY_UserData_created_at, model.getCreated_at());
        db.update(TableUserData, contentValues, KEY_UserData_id + " = ? ", new String[]{accountId});
        db.close();
    }

    public ArrayList<UserJsonDataItem> userDataGetAll(@Nullable String id) {
        ArrayList<UserJsonDataItem> modelList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor;
        if (TextUtils.isEmpty(id)) {
            cursor = db.query(TableUserData, null, null, null, null, null, null);
        } else {
            cursor = db.query(TableUserData, null, KEY_UserData_id + " = ? ", new String[]{id}, null, null, null);
        }

        if (cursor.moveToFirst()) {
            do {
                UserJsonDataItem model = new UserJsonDataItem();
                model.setId(cursor.getString(cursor.getColumnIndexOrThrow(KEY_UserData_id)));
                model.setUsername(cursor.getString(cursor.getColumnIndexOrThrow(KEY_UserData_username)));
                model.setSlug(cursor.getString(cursor.getColumnIndex(KEY_UserData_slug)));
                model.setEmail(cursor.getString(cursor.getColumnIndex(KEY_UserData_email)));
                model.setEmail_status(cursor.getString(cursor.getColumnIndex(KEY_UserData_email_status)));
                model.setToken(cursor.getString(cursor.getColumnIndex(KEY_UserData_token)));
                model.setRole(cursor.getString(cursor.getColumnIndex(KEY_UserData_role)));
                model.setUser_type(cursor.getString(cursor.getColumnIndex(KEY_UserData_user_type)));
                model.setGoogle_id(cursor.getString(cursor.getColumnIndex(KEY_UserData_google_id)));
                model.setFacebook_id(cursor.getString(cursor.getColumnIndex(KEY_UserData_facebook_id)));
                model.setAvatar(cursor.getString(cursor.getColumnIndex(KEY_UserData_avatar)));
                model.setStatus(cursor.getString(cursor.getColumnIndex(KEY_UserData_status)));
                model.setAbout_me(cursor.getString(cursor.getColumnIndex(KEY_UserData_about_me)));
                model.setFacebook_url(cursor.getString(cursor.getColumnIndex(KEY_UserData_facebook_url)));
                model.setTwitter_url(cursor.getString(cursor.getColumnIndex(KEY_UserData_twitter_url)));
                model.setGoogle_url(cursor.getString(cursor.getColumnIndex(KEY_UserData_google_url)));
                model.setInstagram_url(cursor.getString(cursor.getColumnIndex(KEY_UserData_instagram_url)));
                model.setPinterest_url(cursor.getString(cursor.getColumnIndex(KEY_UserData_pinterest_url)));
                model.setLinkedin_url(cursor.getString(cursor.getColumnIndex(KEY_UserData_linkedin_url)));
                model.setVk_url(cursor.getString(cursor.getColumnIndex(KEY_UserData_vk_url)));
                model.setYoutube_url(cursor.getString(cursor.getColumnIndex(KEY_UserData_youtube_url)));
                model.setLast_seen(cursor.getString(cursor.getColumnIndex(KEY_UserData_last_seen)));
                model.setCreated_at(cursor.getString(cursor.getColumnIndex(KEY_UserData_created_at)));
                modelList.add(model);
            } while (cursor.moveToNext());
        }

        return modelList;
    }

    public UserJsonDataItem userDataGetOne(String accountid) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TableUserData, null, KEY_UserData_id + " = ? ", new String[]{accountid}, KEY_UserData_id, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        UserJsonDataItem model = new UserJsonDataItem();
        model.setId(cursor.getString(cursor.getColumnIndex(KEY_UserData_id)));
        model.setUsername(cursor.getString(cursor.getColumnIndex(KEY_UserData_username)));
        model.setSlug(cursor.getString(cursor.getColumnIndex(KEY_UserData_slug)));
        model.setEmail(cursor.getString(cursor.getColumnIndex(KEY_UserData_email)));
        model.setEmail_status(cursor.getString(cursor.getColumnIndex(KEY_UserData_email_status)));
        model.setToken(cursor.getString(cursor.getColumnIndex(KEY_UserData_token)));
        model.setRole(cursor.getString(cursor.getColumnIndex(KEY_UserData_role)));
        model.setUser_type(cursor.getString(cursor.getColumnIndex(KEY_UserData_user_type)));
        model.setGoogle_id(cursor.getString(cursor.getColumnIndex(KEY_UserData_google_id)));
        model.setFacebook_id(cursor.getString(cursor.getColumnIndex(KEY_UserData_facebook_id)));
        model.setAvatar(cursor.getString(cursor.getColumnIndex(KEY_UserData_avatar)));
        model.setStatus(cursor.getString(cursor.getColumnIndex(KEY_UserData_status)));
        model.setAbout_me(cursor.getString(cursor.getColumnIndex(KEY_UserData_about_me)));
        model.setFacebook_url(cursor.getString(cursor.getColumnIndex(KEY_UserData_facebook_url)));
        model.setTwitter_url(cursor.getString(cursor.getColumnIndex(KEY_UserData_twitter_url)));
        model.setGoogle_url(cursor.getString(cursor.getColumnIndex(KEY_UserData_google_url)));
        model.setInstagram_url(cursor.getString(cursor.getColumnIndex(KEY_UserData_instagram_url)));
        model.setPinterest_url(cursor.getString(cursor.getColumnIndex(KEY_UserData_pinterest_url)));
        model.setLinkedin_url(cursor.getString(cursor.getColumnIndex(KEY_UserData_linkedin_url)));
        model.setVk_url(cursor.getString(cursor.getColumnIndex(KEY_UserData_vk_url)));
        model.setYoutube_url(cursor.getString(cursor.getColumnIndex(KEY_UserData_youtube_url)));
        model.setLast_seen(cursor.getString(cursor.getColumnIndex(KEY_UserData_last_seen)));
        model.setCreated_at(cursor.getString(cursor.getColumnIndex(KEY_UserData_created_at)));
        // return model
        return model;
    }

    public void userDataDelete(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TableUserData, KEY_UserData_id + " = ?", new String[]{id});
        db.close();
    }

    /*catHeader*/
    public void catHeaderSave(HeaderDataModel model) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_CategoriesHeader_id, model.getId());
        contentValues.put(KEY_CategoriesHeader_title, model.getTitle());
        contentValues.put(KEY_CategoriesHeader_slug, model.getSlug());
        contentValues.put(KEY_CategoriesHeader_parent_id, model.getParent_id());
        contentValues.put(KEY_CategoriesHeader_page_type, model.getPage_type());
        contentValues.put(KEY_CategoriesHeader_menu_title, model.getMenu_title());
        contentValues.put(KEY_CategoriesHeader_status, model.getStatus());

        db.insert(TableCategoriesHeader, null, contentValues);
        db.close();
    }

    public void catHeaderUpdate(HeaderDataModel model, String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_CategoriesHeader_id, model.getId());
        contentValues.put(KEY_CategoriesHeader_title, model.getTitle());
        contentValues.put(KEY_CategoriesHeader_slug, model.getSlug());
        contentValues.put(KEY_CategoriesHeader_parent_id, model.getParent_id());
        contentValues.put(KEY_CategoriesHeader_page_type, model.getPage_type());
        contentValues.put(KEY_CategoriesHeader_menu_title, model.getMenu_title());
        contentValues.put(KEY_CategoriesHeader_status, model.getStatus());

        db.update(TableCategoriesHeader, contentValues, KEY_CategoriesHeader_id + " = ? ", new String[]{id});
        db.close();
    }

    public ArrayList<HeaderDataModel> catHeadersGet(@Nullable String id) {
        ArrayList<HeaderDataModel> modelList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor;
        if (TextUtils.isEmpty(id)) {
            cursor = db.query(TableCategoriesHeader, null, null, null, null, null, null);
        } else {
            cursor = db.query(TableCategoriesHeader, null, KEY_CategoriesHeader_id + " = ? ", new String[]{id}, null, null, null);
        }

        if (cursor.moveToFirst()) {
            do {
                HeaderDataModel model = new HeaderDataModel();
                model.setId(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CategoriesHeader_id)));
                model.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CategoriesHeader_title)));
                model.setSlug(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CategoriesHeader_slug)));
                model.setParent_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CategoriesHeader_parent_id)));
                model.setPage_type(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CategoriesHeader_page_type)));
                model.setMenu_title(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CategoriesHeader_menu_title)));
                model.setStatus(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CategoriesHeader_status)));
                modelList.add(model);
            } while (cursor.moveToNext());
        }

        return modelList;
    }

    public HeaderDataModel catHeaderGetOne(String id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TableCategoriesHeader, null, KEY_CategoriesHeader_id + " = ? ", new String[]{id}, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        HeaderDataModel model = new HeaderDataModel();
        model.setId(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CategoriesHeader_id)));
        model.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CategoriesHeader_title)));
        model.setSlug(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CategoriesHeader_slug)));
        model.setParent_id(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CategoriesHeader_parent_id)));
        model.setPage_type(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CategoriesHeader_page_type)));
        model.setMenu_title(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CategoriesHeader_menu_title)));
        model.setStatus(cursor.getString(cursor.getColumnIndexOrThrow(KEY_CategoriesHeader_status)));
        // return model
        return model;
    }

    public void catHeaderDeleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TableCategoriesHeader);
        db.close();
    }

    public void catHeaderDelete(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TableCategoriesHeader, KEY_CategoriesHeader_id + " = ?", new String[]{id});
        db.close();
    }
    /*catHeader*/
}
