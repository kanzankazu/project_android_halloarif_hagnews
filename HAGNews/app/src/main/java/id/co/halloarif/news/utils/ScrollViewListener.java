package id.co.halloarif.news.utils;

public interface ScrollViewListener {
    void onScrollChanged(MyNestedScrollView scrollView,
                         int x, int y, int oldx, int oldy);
}
