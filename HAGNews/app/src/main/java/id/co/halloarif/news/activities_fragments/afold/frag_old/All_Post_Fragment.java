package id.co.halloarif.news.activities_fragments.afold.frag_old;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afold.activity_old.MainActivity;
import id.co.halloarif.news.activities_fragments.afold.activity_old.Open_Post_Activity;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_Adapter_news;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_adapter_voting_poll;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.utils.MediaPlayerSingleton;
import id.co.halloarif.news.utils.Recycleritemclicklistener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ST_004 on 23-02-2018.
 */

public class All_Post_Fragment extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    TextView text_header;
    RecyclerView recyclerview_post;
    ArrayList<Viewpager_items> arrayList_all_post = new ArrayList<>();
    String tag_slug;
    ImageView Iv_back;
    FragmentActivity fragmentActivity;
    ArrayList<Viewpager_items> arrayList_recent = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_popular = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_random = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_videos = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_voting_poll = new ArrayList<>();
    Recyclerview_Adapter_news adapter_news;
    Recyclerview_adapter_voting_poll recyclerview_adapter_voting_poll;
    RelativeLayout rel1;
    SwipeRefreshLayout mSwipeRefreshLayout;

    public void CLICK_METHOD(Viewpager_items model) {
        String post_type = model.getPost_type();
        if (post_type.contentEquals("post")) {
            Intent i = new Intent(getActivity(), Open_Post_Activity.class);
            i.putExtra("POST_ID", model.getId());
            startActivity(i);
        } else if (post_type.contentEquals("audio")) {
            ((MainActivity) fragmentActivity).CreateFragment(Audio_post_Fragment.newInstance(model.getId()));
        } else {
            String embed_url = model.getEmbed_code();
            String video_title = model.getTitle();
            String video_id = model.getId();
            String created_at = model.getCreated_at();
            String video_views = model.getHit();
            String content = model.getContent();

            ((MainActivity) fragmentActivity).draggableView.setVisibility(View.VISIBLE);
            ((MainActivity) fragmentActivity).draggableView.maximize();
            ((MainActivity) fragmentActivity).Call_VIDEO_Details_API(video_id);
            ((MainActivity) fragmentActivity).CAll_API_ALL_COMMENTS(video_id);
            ((MainActivity) fragmentActivity).PLAYER_INITIALIZE(embed_url);
            ((MainActivity) fragmentActivity).setText_values(video_title, video_views, content, created_at);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_post_data, container, false);

        initComponent(view);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_blue_dark);

        mSwipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                mSwipeRefreshLayout.setRefreshing(true);

                // Fetching data from server
                HOME_API();
            }
        });


        recyclerview_post.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        if (Constants.Content_Equals.contentEquals("Latest Post")) {
                            Viewpager_items model = arrayList_recent.get(position);
                            CLICK_METHOD(model);
                        } else if (Constants.Content_Equals.contentEquals("Popular Post")) {
                            Viewpager_items model = arrayList_popular.get(position);
                            CLICK_METHOD(model);
                        } else if (Constants.Content_Equals.contentEquals("Random Post")) {
                            Viewpager_items model = arrayList_random.get(position);
                            CLICK_METHOD(model);
                        } else if (Constants.Content_Equals.contentEquals("Top Videos")) {
                            if (MediaPlayerSingleton.getInstance().getMediaPlayer() != null) {
                                if (MediaPlayerSingleton.getInstance().getMediaPlayer().isPlaying()) {
                                    MediaPlayerSingleton.getInstance().getMediaPlayer().pause();
                                    if (((MainActivity) fragmentActivity).fab != null) {
                                        ((MainActivity) fragmentActivity).fab.setVisibility(View.GONE);
                                    }
                                    Log.e("MEDIA_PLAYER", "true");
                                } else {
                                    MediaPlayerSingleton.getInstance().getMediaPlayer().pause();
                                    Log.e("MEDIA_PLAYER", "false");
                                }
                            } else {
                                Log.e("MEDIA_PLAYER", "NULL");
                                MediaPlayerSingleton.getInstance().getMediaPlayer();
                            }

                            Viewpager_items model = arrayList_videos.get(position);
                            String video_id = model.getId();
                            String embed_url = model.getEmbed_code();
                            String video_title = model.getTitle();
                            String created_at = model.getCreated_at();
                            String video_views = model.getHit();
                            String content = model.getContent();

                            ((MainActivity) fragmentActivity).draggableView.setVisibility(View.VISIBLE);
                            ((MainActivity) fragmentActivity).draggableView.maximize();
                            ((MainActivity) fragmentActivity).Call_VIDEO_Details_API(video_id);
                            ((MainActivity) fragmentActivity).CAll_API_ALL_COMMENTS(video_id);
                            ((MainActivity) fragmentActivity).PLAYER_INITIALIZE(embed_url);
                            ((MainActivity) fragmentActivity).setText_values(video_title, video_views, content, created_at);
                        } else if (Constants.Content_Equals.contentEquals("Voting Poll")) {

                        }
/*
                        Viewpager_items model = arrayList_all_post.get(position);
                        String post_type = model.getPost_type();
                        if (post_type.contentEquals("post")) {

                            Intent i = new Intent(getActivity(), Open_Post_Activity.class);
                            i.putExtra("POST_ID", model.getId());
                            startActivity(i);

                        } else {
                            String embed_url = model.getEmbed_code();
                            String video_title = model.getTitle();
                            String created_at = model.getCreated_at();
                            String video_views = model.getHit();
                            String content = model.getContent();

                            ((MainActivity) fragmentActivity).draggableView.setVisibility(View.VISIBLE);
                            ((MainActivity) fragmentActivity).draggableView.maximize();
                            ((MainActivity) fragmentActivity).PLAYER_INITIALIZE(embed_url);
                            ((MainActivity) fragmentActivity).setText_values(video_title, video_views, content, created_at);
                        }*/
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                })
        );
        return view;
    }


    private void initComponent(View view) {
        Iv_back = view.findViewById(R.id.Iv_back);
        rel1 = view.findViewById(R.id.rel1);
        rel1.setVisibility(View.GONE);
        Iv_back.setOnClickListener(this);
        text_header = view.findViewById(R.id.text_header);
        recyclerview_post = view.findViewById(R.id.recyclerview_post);
        LinearLayoutManager linearLayoutManager12 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerview_post.setLayoutManager(linearLayoutManager12);
        recyclerview_post.setHasFixedSize(true);
        recyclerview_post.setNestedScrollingEnabled(false);

    }


    private void HOME_API() {
        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(fragmentActivity);

        String URL = Constants.MAIN_URL + "homepage";
        Log.e("URL_TAB", Constants.MAIN_URL + "homepage");
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("tab_DATA", "" + response.trim());
                        try {
                            arrayList_recent.clear();
                            arrayList_popular.clear();
                            arrayList_random.clear();
                            arrayList_videos.clear();
                            arrayList_voting_poll.clear();

                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONObject jsonOBJ = ojs.getJSONObject("get_json_data");

                                JSONArray jsonArray_recent_post = jsonOBJ.getJSONArray("latest_post");
                                for (int i = 0; i < jsonArray_recent_post.length(); i++) {
                                    JSONObject obj = jsonArray_recent_post.getJSONObject(i);

                                    String id = obj.getString("id");
                                    String title = obj.getString("title");
                                    String category_name = obj.getString("category_name");
                                    String post_type = obj.getString("post_type");
                                    String content = obj.getString("content");
                                    String image_default = "";
                                    String embed_code = "";
                                    if (post_type.contentEquals("post")) {     ///Post

                                        if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                            image_default = obj.getString("image_url");
                                        } else {
                                            image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                            embed_code = "";
                                        }
                                    } else {    ////Videos
                                        if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                            image_default = obj.getString("video_image_url");
                                        } else {
                                            image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                        }
                                        embed_code = obj.getString("video_embed_code");
                                    }
                                    String created_at = obj.getString("created_at");
                                    String comment_count = obj.getString("comment_count");
                                    String hit = obj.getString("hit");

                                    arrayList_recent.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                                }
////////////////////
                                JSONArray jsonArray_popular_post = jsonOBJ.getJSONArray("popular_posts");
                                for (int i = 0; i < jsonArray_popular_post.length(); i++) {
                                    JSONObject obj = jsonArray_popular_post.getJSONObject(i);

                                    String id = obj.getString("id");
                                    String title = obj.getString("title");
                                    String category_name = obj.getString("category_name");
                                    String post_type = obj.getString("post_type");
                                    String content = obj.getString("content");
                                    String image_default = "";
                                    String embed_code = "";
                                    if (post_type.contentEquals("post")) {     ///Post
                                        if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                            image_default = obj.getString("image_url");
                                        } else {
                                            image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                            embed_code = "";
                                        }
                                    } else {    ////Videos
                                        if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                            image_default = obj.getString("video_image_url");
                                        } else {
                                            image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                        }
                                        embed_code = obj.getString("video_embed_code");
                                    }
                                    String created_at = obj.getString("created_at");
                                    String comment_count = obj.getString("comment_count");
                                    String hit = obj.getString("hit");

                                    arrayList_popular.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                                }
                                /////////////////////////////

                                JSONArray jsonArray_random_post = jsonOBJ.getJSONArray("random_post");
                                for (int i = 0; i < jsonArray_random_post.length(); i++) {
                                    JSONObject obj = jsonArray_random_post.getJSONObject(i);

                                    String id = obj.getString("id");
                                    String title = obj.getString("title");
                                    String category_name = obj.getString("category_name");
                                    String post_type = obj.getString("post_type");
                                    String content = obj.getString("content");
                                    String image_default = "";
                                    String embed_code = "";
                                    if (post_type.contentEquals("post")) {     ///Post
                                        if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                            image_default = obj.getString("image_url");
                                        } else {
                                            image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                            embed_code = "";
                                        }
                                    } else {    ////Videos
                                        if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                            image_default = obj.getString("video_image_url");
                                        } else {
                                            image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                        }
                                        embed_code = obj.getString("video_embed_code");

                                    }
                                    String created_at = obj.getString("created_at");
                                    String comment_count = obj.getString("comment_count");
                                    String hit = obj.getString("hit");

                                    arrayList_random.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                                }
/////////////////////
                                JSONArray jsonArray_videos_post = jsonOBJ.getJSONArray("recent_videos");
                                for (int i = 0; i < jsonArray_videos_post.length(); i++) {
                                    JSONObject obj = jsonArray_videos_post.getJSONObject(i);

                                    String image_default = "";
                                    String id = obj.getString("id");
                                    String title = obj.getString("title");
                                    String category_name = obj.getString("category_name_slug");
                                    // String image_default = obj.getString("video_image_url");
                                    String post_type = obj.getString("post_type");
                                    String created_at = obj.getString("created_at");
                                    String hit = obj.getString("hit");
                                    if (obj.getString("video_image_url").contentEquals("") || obj.getString("video_image_url").isEmpty()) {
                                        image_default = Constants.IMAGE_URL + obj.getString("image_mid");
                                    } else {
                                        image_default = obj.getString("video_image_url");
                                    }
                                    String embed_code = obj.getString("video_embed_code");
                                    String comment_count = obj.getString("comment_count");
                                    String content = obj.getString("content");


                                    arrayList_videos.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                                }

///////////////////////////
                                JSONArray jsonArray_vote_recent_post = jsonOBJ.getJSONArray("homepageVote_recentPostModel");
                                for (int i = 0; i < jsonArray_vote_recent_post.length(); i++) {
                                    JSONObject obj = jsonArray_vote_recent_post.getJSONObject(i);

                                    String id = obj.getString("id");
                                    String question = obj.getString("question");
                                    String option1 = obj.getString("option1");
                                    String option2 = obj.getString("option2");
                                    String option3 = obj.getString("option3");
                                    String option4 = obj.getString("option4");
                                    String option5 = obj.getString("option5");
                                    String option6 = obj.getString("option6");
                                    String option7 = obj.getString("option7");
                                    String option8 = obj.getString("option8");
                                    String option9 = obj.getString("option9");
                                    String option10 = obj.getString("option10");

                                    arrayList_voting_poll.add(new Viewpager_items(id, question, option1, option2, option3, option4, option5, option6, option7, option8, option9, option10));
                                }

                                if (Constants.Content_Equals.contentEquals("Latest Post")) {
                                    adapter_news = new Recyclerview_Adapter_news(getActivity(), R.layout.recent_news_layout, arrayList_recent);
                                    recyclerview_post.setAdapter(adapter_news);
                                    adapter_news.notifyDataSetChanged();
                                } else if (Constants.Content_Equals.contentEquals("Popular Post")) {
                                    adapter_news = new Recyclerview_Adapter_news(getActivity(), R.layout.recent_news_layout, arrayList_popular);
                                    recyclerview_post.setAdapter(adapter_news);
                                    adapter_news.notifyDataSetChanged();
                                } else if (Constants.Content_Equals.contentEquals("Random Post")) {
                                    adapter_news = new Recyclerview_Adapter_news(getActivity(), R.layout.recent_news_layout, arrayList_random);
                                    recyclerview_post.setAdapter(adapter_news);
                                    adapter_news.notifyDataSetChanged();
                                } else if (Constants.Content_Equals.contentEquals("Top Videos")) {
                                    adapter_news = new Recyclerview_Adapter_news(getActivity(), R.layout.recent_news_layout, arrayList_videos);
                                    recyclerview_post.setAdapter(adapter_news);
                                    adapter_news.notifyDataSetChanged();
                                } else if (Constants.Content_Equals.contentEquals("Voting Poll")) {
                                    recyclerview_adapter_voting_poll = new Recyclerview_adapter_voting_poll(getActivity(), R.layout.voting_poll_item, arrayList_voting_poll);
                                    recyclerview_post.setAdapter(recyclerview_adapter_voting_poll);
                                    recyclerview_adapter_voting_poll.notifyDataSetChanged();
                                }

                                //  recyclerview_adapter_voting_poll = new Recyclerview_adapter_voting_poll(getActivity(), R.layout.voting_poll_item, arrayList_voting_poll);
                                //  recyclerview_post.setAdapter(recyclerview_adapter_voting_poll);
                                //  recyclerview_adapter_voting_poll.notifyDataSetChanged();

                            } else {
                               /*String error = ojs.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);*/
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        mSwipeRefreshLayout.setRefreshing(false);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mSwipeRefreshLayout.setRefreshing(false);

                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
    }

    @Override
    public void onClick(View view) {
        if (view == Iv_back) {
            ((MainActivity) fragmentActivity).getSupportFragmentManager().popBackStack();
            //  ((MainActivity) fragmentActivity).SetFrameVisible(false);
        }
    }

    @Override
    public void onRefresh() {
        HOME_API();
    }
}
