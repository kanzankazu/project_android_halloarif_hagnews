package id.co.halloarif.news.models.Res.UserUpdate;

public class Change_user_profile {
    public String title_success;
    public User_profile user_profile;
    public Integer status;

    public String getTitle_success() {
        return title_success;
    }

    public void setTitle_success(String title_success) {
        this.title_success = title_success;
    }

    public User_profile getUser_profile() {
        return user_profile;
    }

    public void setUser_profile(User_profile user_profile) {
        this.user_profile = user_profile;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
