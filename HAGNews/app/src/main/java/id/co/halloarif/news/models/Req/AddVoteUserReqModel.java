package id.co.halloarif.news.models.Req;

public class AddVoteUserReqModel {
    private String user_id;
    private String poll_id;
    private String voting_option;

    public AddVoteUserReqModel(String user_id, String poll_id, String voting_option) {
        this.user_id = user_id;
        this.poll_id = poll_id;
        this.voting_option = voting_option;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPoll_id() {
        return poll_id;
    }

    public void setPoll_id(String poll_id) {
        this.poll_id = poll_id;
    }

    public String getVoting_option() {
        return voting_option;
    }

    public void setVoting_option(String voting_option) {
        this.voting_option = voting_option;
    }
}
