package id.co.halloarif.news.activities_fragments.afold.frag_old;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afold.activity_old.MainActivity;
import id.co.halloarif.news.activities_fragments.afold.activity_old.Open_Post_Activity;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Featured_Adapter;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recycler_adapter_banner;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_Adapter_Videos;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_Adapter_news;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_adapter_voting_poll;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_tags_adapter;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.utils.LinePagerIndicatorDecoration;
import id.co.halloarif.news.utils.Recycleritemclicklistener;
import id.co.halloarif.news.utils.ShimmerFrameLayout;

public class HomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final String LOG_TAG = "CheckNetworkStatus";
    RecyclerView recyclerview_featured, recyclerview_recentpost, recyclerview_Popular_view, recyclerview_random_post, recyclerview_top_videos, recyclerview_voting_poll, recyclerview_tags;
    ArrayList<Viewpager_items> arraylist_feature = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_recent = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_popular = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_random = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_videos = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_voting_poll = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_tags = new ArrayList<>();
    Featured_Adapter featured_Adapter;
    Recyclerview_adapter_voting_poll recyclerview_adapter_voting_poll;
    Recyclerview_tags_adapter recyclerview_tags_adapter;
    TextView text_breaking_news;
    Recycler_adapter_banner Recyclervideo_adapter;
    Recyclerview_Adapter_news recyclerview_adapter_news, adapter_news_latest;
    Recyclerview_Adapter_Videos recyclerview_adapter_videos;
    FragmentActivity fragmentActivity;
    String breaking_news_id;
    SwipeRefreshLayout mSwipeRefreshLayout;
    ArrayList<Viewpager_items> arrayList_sub_heading = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_all_post = new ArrayList<>();
    private ShimmerFrameLayout mShimmerViewContainer;
    private NetworkChangeReceiver receiver;
    private boolean isConnected = false;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        initComponent(view);
        initParam();
        initSession();
        initContent();
        initListener();

        return view;
    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_blue_dark);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        if (mShimmerViewContainer != null) {
            mShimmerViewContainer.setDuration(1500);
            mShimmerViewContainer.setIntensity(0.35f);
            mShimmerViewContainer.setMaskShape(ShimmerFrameLayout.MaskShape.RADIAL);
            mShimmerViewContainer.startShimmerAnimation();
        }

        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkChangeReceiver();
        fragmentActivity.registerReceiver(receiver, filter);

        /*try {
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                    HOME_API();
                }
            });
        } catch (NullPointerException e) {
            Log.e("Error", "Error");
        }*/
    }

    private void initListener() {
        recyclerview_featured.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Viewpager_items model = arraylist_feature.get(position);
                        CLICK_METHOD(model);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                })
        );

        recyclerview_recentpost.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Viewpager_items model = arrayList_recent.get(position);
                        CLICK_METHOD(model);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                })
        );

        recyclerview_Popular_view.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Viewpager_items model = arrayList_popular.get(position);
                        CLICK_METHOD(model);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                })
        );

        recyclerview_random_post.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Viewpager_items model = arrayList_random.get(position);
                        CLICK_METHOD(model);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                })
        );

        recyclerview_tags.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Constants.CHECK_OPEN_FRAG = "Sell";
                        Constants.Tag_slug = arrayList_tags.get(position).getTag_slug();
                        Constants.Cat_Name = arrayList_tags.get(position).getTag();
                        ((MainActivity) fragmentActivity).CreateFragment(new Tags_Post_Fragment());
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                })
        );

        recyclerview_top_videos.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        Viewpager_items model = arrayList_videos.get(position);
                        String video_id = model.getId();
                        String embed_url = model.getEmbed_code();
                        String video_title = model.getTitle();
                        String created_at = model.getCreated_at();
                        String video_views = model.getHit();
                        String content = model.getContent();

                        ((MainActivity) fragmentActivity).draggableView.setVisibility(View.VISIBLE);
                        ((MainActivity) fragmentActivity).draggableView.maximize();
                        ((MainActivity) fragmentActivity).Call_VIDEO_Details_API(video_id);
                        ((MainActivity) fragmentActivity).CAll_API_ALL_COMMENTS(video_id);
                        ((MainActivity) fragmentActivity).PLAYER_INITIALIZE(embed_url);
                        ((MainActivity) fragmentActivity).setText_values(video_title, video_views, content, created_at);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                })
        );

        /*text_breaking_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (breaking_news_id != null) {
                    Intent i = new Intent(getActivity(), Open_Post_Activity.class);
                    i.putExtra("POST_ID", breaking_news_id);
                    startActivity(i);
                } else {
                    Toast.makeText(getActivity(), "No info Available", Toast.LENGTH_SHORT).show();
                }

            }
        });*/
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            Log.e("ONRESUME_CALLED", "CALLED");
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                    HOME_API();
                }
            });
        } catch (NullPointerException e) {
            Log.e("Error", "Error");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mShimmerViewContainer != null) {
            mShimmerViewContainer.stopShimmerAnimation();
        }
    }

    public void CLICK_METHOD(Viewpager_items model) {
        String post_type = model.getPost_type();
        if (post_type.contentEquals("post")) {
            Intent i = new Intent(getActivity(), Open_Post_Activity.class);
            i.putExtra("POST_ID", model.getId());
            startActivity(i);
        } else if (post_type.contentEquals("audio")) {
            ((MainActivity) fragmentActivity).CreateFragment(Audio_post_Fragment.newInstance(model.getId()));
        } else {
            String embed_url = model.getEmbed_code();
            String video_title = model.getTitle();
            String video_id = model.getId();
            String created_at = model.getCreated_at();
            String video_views = model.getHit();
            String content = model.getContent();

            ((MainActivity) fragmentActivity).draggableView.setVisibility(View.VISIBLE);
            ((MainActivity) fragmentActivity).draggableView.maximize();
            ((MainActivity) fragmentActivity).Call_VIDEO_Details_API(video_id);
            ((MainActivity) fragmentActivity).CAll_API_ALL_COMMENTS(video_id);
            ((MainActivity) fragmentActivity).PLAYER_INITIALIZE(embed_url);
            ((MainActivity) fragmentActivity).setText_values(video_title, video_views, content, created_at);
        }
    }

    private void initComponent(View view) {
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);

        text_breaking_news = view.findViewById(R.id.text_breaking_news);
        recyclerview_featured = view.findViewById(R.id.recyclerview_featured);
        recyclerview_recentpost = view.findViewById(R.id.recyclerview_recentpost);
        recyclerview_Popular_view = view.findViewById(R.id.recyclerview_Popular_view);
        recyclerview_random_post = view.findViewById(R.id.recyclerview_random_post);
        recyclerview_top_videos = view.findViewById(R.id.recyclerview_top_videos);
        recyclerview_voting_poll = view.findViewById(R.id.recyclerview_voting_poll);
        recyclerview_tags = view.findViewById(R.id.recyclerview_tags);

        text_breaking_news.setSelected(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerview_featured.setLayoutManager(linearLayoutManager);
        recyclerview_featured.setHasFixedSize(true);
        recyclerview_featured.setNestedScrollingEnabled(false);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerview_featured);
        recyclerview_featured.addItemDecoration(new LinePagerIndicatorDecoration());

        LinearLayoutManager linearLayoutManager12 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerview_recentpost.setLayoutManager(linearLayoutManager12);
        recyclerview_recentpost.setHasFixedSize(true);
        recyclerview_recentpost.setNestedScrollingEnabled(false);

        LinearLayoutManager linearLayoutManager123 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerview_Popular_view.setLayoutManager(linearLayoutManager123);
        recyclerview_Popular_view.setHasFixedSize(true);
        recyclerview_Popular_view.setNestedScrollingEnabled(false);

        LinearLayoutManager linearLayoutManager1234 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerview_random_post.setLayoutManager(linearLayoutManager1234);
        recyclerview_random_post.setHasFixedSize(true);
        recyclerview_random_post.setNestedScrollingEnabled(false);

        LinearLayoutManager linearLayoutManager12345 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerview_top_videos.setLayoutManager(linearLayoutManager12345);
        recyclerview_top_videos.setHasFixedSize(true);
        recyclerview_top_videos.setNestedScrollingEnabled(false);

        LinearLayoutManager linearLayoutManager123456 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerview_voting_poll.setLayoutManager(linearLayoutManager123456);
        recyclerview_voting_poll.setHasFixedSize(true);
        recyclerview_voting_poll.setNestedScrollingEnabled(false);
        SnapHelper snapHelper2 = new PagerSnapHelper();
        snapHelper2.attachToRecyclerView(recyclerview_voting_poll);

        LinearLayoutManager linearLayoutManager1234567 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerview_tags.setLayoutManager(linearLayoutManager1234567);
        recyclerview_tags.setHasFixedSize(true);
        recyclerview_tags.setNestedScrollingEnabled(false);

    }

    private void HOME_API() {
        final RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(fragmentActivity);

        String URL = Constants.MAIN_URL + "homepage";
        Log.e("URL_TAB_HOME", Constants.MAIN_URL + "homepage");
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("tab_DATA", "" + response.trim());
                        try {

                            arraylist_feature.clear();
                            text_breaking_news.setText("");
                            arrayList_recent.clear();
                            arrayList_popular.clear();
                            arrayList_random.clear();
                            arrayList_videos.clear();
                            arrayList_voting_poll.clear();
                            arrayList_tags.clear();
                            arrayList_sub_heading.clear();
                            arrayList_all_post.clear();

                            String title_breaking = "";

                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {

                                JSONObject jsonOBJ = ojs.getJSONObject("get_json_data");
                                /////////////////////////
                                String text = "";
                                JSONArray jsonArray_breaking = jsonOBJ.getJSONArray("breaking_news");
                                for (int i = 0; i < jsonArray_breaking.length(); i++) {
                                    JSONObject obj1 = jsonArray_breaking.getJSONObject(i);
                                    title_breaking = obj1.getString("title");
                                    breaking_news_id = obj1.getString("id");
                                    int pos = i + 1;
                                    //text = text + " " + pos + ". " + title_breaking + "  ";
                                    text = "  <a>  " + "   " + text + " <font color='red'> " + "  " + pos + ".  " + " </font> " + title_breaking + "  " + "   " + "  </a>  ";
                                    text_breaking_news.setText(Html.fromHtml(text));
                                }

                                ////////////////////
                                JSONArray jsonArray1 = jsonOBJ.getJSONArray("slider_featur");
                                for (int i = 0; i < jsonArray1.length(); i++) {
                                    JSONObject obj = jsonArray1.getJSONObject(i);

                                    String id = obj.getString("id");
                                    String title = obj.getString("title");
                                    String category_name = obj.getString("category_name_slug");
                                    String post_type = obj.getString("post_type");
                                    String content = obj.getString("content");
                                    String created_at = obj.getString("created_at");
                                    String comment_count = obj.getString("comment_count");
                                    String hit = obj.getString("hit");
                                    String image_default = "";
                                    String embed_code = "";
                                    if (post_type.contentEquals("post")) {     ///Post
                                        if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                            image_default = obj.getString("image_url");
                                        } else {
                                            image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                            embed_code = "";
                                        }

                                    } else {    ////Videos
                                        if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                            image_default = obj.getString("video_image_url");
                                        } else {
                                            image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                        }
                                        embed_code = obj.getString("video_embed_code");
                                    }

                                    arraylist_feature.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                                }
                                ////////////////////////
                                JSONArray jsonArray_recent_post = jsonOBJ.getJSONArray("latest_post");
                                for (int i = 0; i < Constants.VALUE_SHOW_ITEMS; i++) {
                                    JSONObject obj = jsonArray_recent_post.getJSONObject(i);

                                    String id = obj.getString("id");
                                    String title = obj.getString("title");
                                    String category_name = obj.getString("category_name_slug");
                                    String post_type = obj.getString("post_type");
                                    String content = obj.getString("content");
                                    String created_at = obj.getString("created_at");
                                    String comment_count = obj.getString("comment_count");
                                    String hit = obj.getString("hit");
                                    String image_default = "";
                                    String embed_code = "";

                                    if (post_type.contentEquals("post")) {     ///Post

                                        if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                            image_default = obj.getString("image_url");
                                        } else {
                                            image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                            embed_code = "";
                                        }

                                    } else {    ////Videos
                                        if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                            image_default = obj.getString("video_image_url");
                                        } else {
                                            image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                        }
                                        embed_code = obj.getString("video_embed_code");
                                    }

                                    arrayList_recent.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));

                                    //  arrayList_recent.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, comment_count));

                                }
                                ////////////////////
                                JSONArray jsonArray_popular_post = jsonOBJ.getJSONArray("popular_posts");
                                for (int i = 0; i < Constants.VALUE_SHOW_ITEMS; i++) {
                                    JSONObject obj = jsonArray_popular_post.getJSONObject(i);

                                    String id = obj.getString("id");
                                    String title = obj.getString("title");
                                    String category_name = obj.getString("category_name_slug");
                                    String post_type = obj.getString("post_type");
                                    String content = obj.getString("content");
                                    String created_at = obj.getString("created_at");
                                    String comment_count = obj.getString("comment_count");
                                    String hit = obj.getString("hit");
                                    String image_default = "";
                                    String embed_code = "";

                                    if (post_type.contentEquals("post")) {     ///Post

                                        if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                            image_default = obj.getString("image_url");
                                        } else {
                                            image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                            embed_code = "";
                                        }
                                    } else {    ////Videos
                                        if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                            image_default = obj.getString("video_image_url");
                                        } else {
                                            image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                        }
                                        embed_code = obj.getString("video_embed_code");
                                    }

                                    arrayList_popular.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                                }
                                /////////////////////////////

                                JSONArray jsonArray_random_post = jsonOBJ.getJSONArray("random_post");
                                for (int i = 0; i < Constants.VALUE_SHOW_ITEMS; i++) {
                                    JSONObject obj = jsonArray_random_post.getJSONObject(i);

                                    String id = obj.getString("id");
                                    String title = obj.getString("title");
                                    String category_name = obj.getString("category_name_slug");
                                    String post_type = obj.getString("post_type");
                                    String content = obj.getString("content");
                                    String created_at = obj.getString("created_at");
                                    String comment_count = obj.getString("comment_count");
                                    String hit = obj.getString("hit");
                                    String image_default = "";
                                    String embed_code = "";

                                    if (post_type.contentEquals("post")) {     ///Post

                                        if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                            image_default = obj.getString("image_url");
                                        } else {
                                            image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                            embed_code = "";
                                        }

                                    } else {    ////Videos
                                        if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                            image_default = obj.getString("video_image_url");
                                        } else {
                                            image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                        }
                                        embed_code = obj.getString("video_embed_code");
                                    }

                                    arrayList_random.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                                }

/////////////////////
                                JSONArray jsonArray_videos_post = jsonOBJ.getJSONArray("recent_videos");
                                for (int i = 0; i < jsonArray_videos_post.length(); i++) {
                                    JSONObject obj = jsonArray_videos_post.getJSONObject(i);

                                    String image_default = "";
                                    String id = obj.getString("id");
                                    String title = obj.getString("title");
                                    String category_name = obj.getString("category_name_slug");

                                    // image_default = obj.getString("video_image_url");
                                    String post_type = obj.getString("post_type");
                                    String created_at = obj.getString("created_at");
                                    String hit = obj.getString("hit");

                                    if (obj.getString("image_default").contentEquals("null") || obj.getString("image_default").isEmpty()) {
                                        image_default = obj.getString("video_image_url");
                                    } else {
                                        image_default = Constants.IMAGE_URL + obj.getString("image_default");
                                    }
                                    String embed_code = obj.getString("video_embed_code");
                                    String content = obj.getString("content");
                                    String comment_count = obj.getString("comment_count");

                                    arrayList_videos.add(new Viewpager_items(id, title, image_default, category_name, post_type, created_at, hit, embed_code, content, comment_count));
                                }

                                ///////////////////////////
                                JSONArray jsonArray_vote_recent_post = jsonOBJ.getJSONArray("homepageVote_recentPostModel");
                                for (int i = 0; i < jsonArray_vote_recent_post.length(); i++) {
                                    JSONObject obj = jsonArray_vote_recent_post.getJSONObject(i);

                                    String id = obj.getString("id");
                                    String question = obj.getString("question");
                                    String option1 = obj.getString("option1");
                                    String option2 = obj.getString("option2");
                                    String option3 = obj.getString("option3");
                                    String option4 = obj.getString("option4");
                                    String option5 = obj.getString("option5");
                                    String option6 = obj.getString("option6");
                                    String option7 = obj.getString("option7");
                                    String option8 = obj.getString("option8");
                                    String option9 = obj.getString("option9");
                                    String option10 = obj.getString("option10");

                                    arrayList_voting_poll.add(new Viewpager_items(id, question, option1, option2, option3, option4, option5, option6, option7, option8, option9, option10));
                                }
                                /////////////////////////////

                                JSONArray jsonArray_tags_slug_post = jsonOBJ.getJSONArray("tags_slug");
                                for (int i = 0; i < jsonArray_tags_slug_post.length(); i++) {
                                    JSONObject obj = jsonArray_tags_slug_post.getJSONObject(i);

                                    String id = obj.getString("id");
                                    String tag = obj.getString("tag");
                                    String tag_slug = obj.getString("tag_slug");
                                    String created_at = obj.getString("created_at");

                                    arrayList_tags.add(new Viewpager_items(id, tag, tag_slug, created_at, "", ""));
                                }

                                Log.e("ARRAYLIST_SIZE", "" + arraylist_feature.size() + arrayList_popular.size() + arrayList_random.size() + arrayList_recent.size());

                                if (arraylist_feature.isEmpty()) {

                                } else {
                                    featured_Adapter = new Featured_Adapter(getActivity(), R.layout.banner_layout_item, arraylist_feature);
                                    recyclerview_featured.setAdapter(featured_Adapter);
                                    featured_Adapter.notifyDataSetChanged();
                                }

                                adapter_news_latest = new Recyclerview_Adapter_news(getActivity(), R.layout.recent_news_layout, arrayList_recent);
                                recyclerview_recentpost.setAdapter(adapter_news_latest);
                                adapter_news_latest.notifyDataSetChanged();

                                recyclerview_adapter_news = new Recyclerview_Adapter_news(getActivity(), R.layout.recent_news_layout, arrayList_random);
                                recyclerview_random_post.setAdapter(recyclerview_adapter_news);
                                recyclerview_adapter_news.notifyDataSetChanged();

                                recyclerview_adapter_videos = new Recyclerview_Adapter_Videos(getActivity(), R.layout.video_item_layout, arrayList_videos);
                                recyclerview_top_videos.setAdapter(recyclerview_adapter_videos);
                                recyclerview_adapter_videos.notifyDataSetChanged();

                                Recyclervideo_adapter = new Recycler_adapter_banner(getActivity(), R.layout.banner_layout_item, arrayList_popular);
                                recyclerview_Popular_view.setAdapter(Recyclervideo_adapter);
                                Recyclervideo_adapter.notifyDataSetChanged();

                                recyclerview_adapter_voting_poll = new Recyclerview_adapter_voting_poll(getActivity(), R.layout.voting_poll_item, arrayList_voting_poll);
                                recyclerview_voting_poll.setAdapter(recyclerview_adapter_voting_poll);
                                recyclerview_adapter_voting_poll.notifyDataSetChanged();

                                recyclerview_tags_adapter = new Recyclerview_tags_adapter(getActivity(), arrayList_tags);
                                recyclerview_tags.setAdapter(recyclerview_tags_adapter);
                                recyclerview_tags_adapter.notifyDataSetChanged();

                            } else {
                                Log.e("error", "0");
                               /*String error = ojs.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);*/
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        mSwipeRefreshLayout.setRefreshing(false);
                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);

    }

    @Override
    public void onDestroy() {
        if (receiver != null) {
            fragmentActivity.unregisterReceiver(receiver);
        }
        super.onDestroy();
    }

    @Override
    public void onRefresh() {
        try {
            mSwipeRefreshLayout.setRefreshing(true);
            HOME_API();
        } catch (NullPointerException e) {
            Log.e("Error", "Error");
        }

    }

    public static class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {

            Log.v(LOG_TAG, "Receieved notification about network status");
            isNetworkAvailable(context);
        }

        private void isNetworkAvailable(Context context) {
            /*ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for (int i = 0; i < info.length; i++) {
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            if (!isConnected) {
                                Log.v(LOG_TAG, "Now you are connected to Internet!");
                                isConnected = true;

                                ((MainActivity) fragmentActivity).txt_No_internet_connection.setVisibility(View.GONE);
                                //do your processing here ---
                                //HOME_API();
                                // check_connection = true;
                            }
                            return;
                        }
                    }
                }
            }
            Log.v(LOG_TAG, "You are not connected to Internet!");
            isConnected = false;
            ((MainActivity) fragmentActivity).txt_No_internet_connection.setVisibility(View.VISIBLE);*/

        }
    }

}
