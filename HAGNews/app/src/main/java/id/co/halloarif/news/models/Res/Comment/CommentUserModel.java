package id.co.halloarif.news.models.Res.Comment;

public class CommentUserModel {
    public String user_like_count;
    public String user_reply_count;
    public String comment_id;
    public String comment_post_id;
    public String comment_user_id;
    public String user_like_comment;
    public String comment_user_image;
    public String comment_user_name;
    public String comment_parent_id;
    public String comment_content;
    public String comment_created_at;

    public String comments;
    public String error;
    public String status;

    public String getUser_like_count() {
        return user_like_count;
    }

    public void setUser_like_count(String user_like_count) {
        this.user_like_count = user_like_count;
    }

    public String getUser_reply_count() {
        return user_reply_count;
    }

    public void setUser_reply_count(String user_reply_count) {
        this.user_reply_count = user_reply_count;
    }

    public String getComment_id() {
        return comment_id;
    }

    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    public String getComment_post_id() {
        return comment_post_id;
    }

    public void setComment_post_id(String comment_post_id) {
        this.comment_post_id = comment_post_id;
    }

    public String getComment_user_id() {
        return comment_user_id;
    }

    public void setComment_user_id(String comment_user_id) {
        this.comment_user_id = comment_user_id;
    }

    public String getUser_like_comment() {
        return user_like_comment;
    }

    public void setUser_like_comment(String user_like_comment) {
        this.user_like_comment = user_like_comment;
    }

    public String getComment_user_image() {
        return comment_user_image;
    }

    public void setComment_user_image(String comment_user_image) {
        this.comment_user_image = comment_user_image;
    }

    public String getComment_user_name() {
        return comment_user_name;
    }

    public void setComment_user_name(String comment_user_name) {
        this.comment_user_name = comment_user_name;
    }

    public String getComment_parent_id() {
        return comment_parent_id;
    }

    public void setComment_parent_id(String comment_parent_id) {
        this.comment_parent_id = comment_parent_id;
    }

    public String getComment_content() {
        return comment_content;
    }

    public void setComment_content(String comment_content) {
        this.comment_content = comment_content;
    }

    public String getComment_created_at() {
        return comment_created_at;
    }

    public void setComment_created_at(String comment_created_at) {
        this.comment_created_at = comment_created_at;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
