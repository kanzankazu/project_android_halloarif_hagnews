package id.co.halloarif.news.models.Res.HomePage;

import java.util.List;

public class HomepageDataModel {

    public List<HomepageMainPostModel> slider_featur = null;
    public List<HomepageMainPostModel> breaking_news = null;
    public List<HomepageMainPostModel> latest_post = null;
    public List<HomepageMainPostModel> popular_posts = null;
    public List<HomepageMainPostModel> recommended_post = null;
    public List<HomepageMainPostModel> random_post = null;
    public String recent_videos;
    public List<HomepageTagsSlugModel> tags_slug = null;
    public List<HomepageVoteRecentPostModel> vote_recent_post = null;
    public String custom_data;

    public List<HomepageMainPostModel> getSlider_featur() {
        return slider_featur;
    }

    public void setSlider_featur(List<HomepageMainPostModel> slider_featur) {
        this.slider_featur = slider_featur;
    }

    public List<HomepageMainPostModel> getBreaking_news() {
        return breaking_news;
    }

    public void setBreaking_news(List<HomepageMainPostModel> breaking_news) {
        this.breaking_news = breaking_news;
    }

    public List<HomepageMainPostModel> getLatest_post() {
        return latest_post;
    }

    public void setLatest_post(List<HomepageMainPostModel> latest_post) {
        this.latest_post = latest_post;
    }

    public List<HomepageMainPostModel> getPopular_posts() {
        return popular_posts;
    }

    public void setPopular_posts(List<HomepageMainPostModel> popular_posts) {
        this.popular_posts = popular_posts;
    }

    public List<HomepageMainPostModel> getRecommended_post() {
        return recommended_post;
    }

    public void setRecommended_post(List<HomepageMainPostModel> recommended_post) {
        this.recommended_post = recommended_post;
    }

    public List<HomepageMainPostModel> getRandom_post() {
        return random_post;
    }

    public void setRandom_post(List<HomepageMainPostModel> random_post) {
        this.random_post = random_post;
    }

    public String getRecent_videos() {
        return recent_videos;
    }

    public void setRecent_videos(String recent_videos) {
        this.recent_videos = recent_videos;
    }

    public List<HomepageTagsSlugModel> getTags_slug() {
        return tags_slug;
    }

    public void setTags_slug(List<HomepageTagsSlugModel> tags_slug) {
        this.tags_slug = tags_slug;
    }

    public List<HomepageVoteRecentPostModel> getVote_recent_post() {
        return vote_recent_post;
    }

    public void setVote_recent_post(List<HomepageVoteRecentPostModel> vote_recent_post) {
        this.vote_recent_post = vote_recent_post;
    }

    public String getCustom_data() {
        return custom_data;
    }

    public void setCustom_data(String custom_data) {
        this.custom_data = custom_data;
    }
}
