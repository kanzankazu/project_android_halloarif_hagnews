package id.co.halloarif.news.activities_fragments.afold.frag_old;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_Adapter_news;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.utils.PinchImageView;
import id.co.halloarif.news.utils.Recycleritemclicklistener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Subcategory_Gallery_Fragment extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    TextView text_header;
    RecyclerView recyclerview_post;
    ArrayList<Viewpager_items> arrayList_all_post = new ArrayList<>();
    ImageView Iv_back;
    FragmentActivity fragmentActivity;
    SwipeRefreshLayout mSwipeRefreshLayout;
    FrameLayout frame_no_data;
    PopupWindow popupWindow_selct;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post_data, container, false);

        if (Constants.Cat_Tag_Name != null) {
            text_header.setText(Constants.Cat_Tag_Name);
        }

        initComponent(view);

        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
                // Fetching data from server
                CALL_API_Category_POST();
            }
        });

        recyclerview_post.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {


                        Viewpager_items model = arrayList_all_post.get(position);
                        String image_default2 = model.getImage_default();

                        initiatePopupWindow_image(image_default2);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                })
        );

        return view;
    }

    private void initComponent(View view) {
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_blue_dark);
        Iv_back = view.findViewById(R.id.Iv_back);
        frame_no_data = view.findViewById(R.id.frame_no_data);
        Iv_back.setOnClickListener(this);
        text_header = view.findViewById(R.id.text_header);
        recyclerview_post = view.findViewById(R.id.recyclerview_post);
        LinearLayoutManager linearLayoutManager12 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerview_post.setLayoutManager(linearLayoutManager12);
        recyclerview_post.setHasFixedSize(true);
        recyclerview_post.setNestedScrollingEnabled(false);

    }

    private void initiatePopupWindow_image(String image_default1) {
        try {
            // We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) fragmentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert inflater != null;
            View layout = inflater.inflate(R.layout.popup_imagezoom, (ViewGroup) fragmentActivity.findViewById(R.id.popup));

            RelativeLayout mainlayout = layout.findViewById(R.id.popup);

            mainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  popupWindow_selct.dismiss();
                }
            });
            ImageView close_dialog = layout.findViewById(R.id.ivPopForgetPassClose);
            close_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow_selct.dismiss();
                }
            });

            PinchImageView pinchimage = layout.findViewById(R.id.pinchimage);

            Glide.with(fragmentActivity).load(image_default1).into(pinchimage);

            pinchimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            popupWindow_selct = new PopupWindow(layout, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT, true);
            popupWindow_selct.showAtLocation(layout, Gravity.CENTER, 0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void CALL_API_Category_POST() {

        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(fragmentActivity);

        String URL = Constants.MAIN_URL + "gallery" + "/" + Constants.Tag_id;
        Log.e("URL_TAB", URL);
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("tab_DATA", "" + response.trim());
                        try {
                            arrayList_all_post.clear();
                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                // JSONObject jsonArray = ojs.getJSONObject("cat_json_data");
                                JSONArray jsonArray12 = ojs.getJSONArray("get_json_data");

                                for (int i1 = 0; i1 < jsonArray12.length(); i1++) {
                                    JSONObject oj = jsonArray12.getJSONObject(i1);
                                    String id1 = oj.getString("id");
                                    String title1 = oj.getString("title");
                                    String image_default1 = Constants.IMAGE_URL + oj.getString("path_big");
                                    String category_name1 = oj.getString("category_id");
                                    String created_at1 = oj.getString("created_at");
                                    arrayList_all_post.add(new Viewpager_items(id1, title1, image_default1, category_name1, "", created_at1, "", ""));
                                }

                                if (arrayList_all_post.isEmpty()) {
                                    Log.e("error", "empty");
                                } else {
                                    Recyclerview_Adapter_news recyclerview_adapter_news = new Recyclerview_Adapter_news(getActivity(),
                                            R.layout.recent_news_layout, arrayList_all_post);
                                    recyclerview_post.setAdapter(recyclerview_adapter_news);
                                    recyclerview_adapter_news.notifyDataSetChanged();
                                }
                            } else {
                                Log.e("error", "");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
    }

    @Override
    public void onClick(View v) {
        if (v == Iv_back) {
            (fragmentActivity).getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void onRefresh() {
        CALL_API_Category_POST();
    }
}