package id.co.halloarif.news.support;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.service.notification.StatusBarNotification;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.widget.RemoteViews;

import id.co.halloarif.news.R;

public class NotifUtil {

    public static void setNotification(Context context, @Nullable String title, @Nullable String text, int smallIcon, int largerIcon, boolean isNotCancelAble, PendingIntent pendingIntent, int NOTIFICATION_ID) {
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), largerIcon);
        setNotification(context, title, text, smallIcon, bitmap, isNotCancelAble, pendingIntent, NOTIFICATION_ID);
    }

    public static void setNotification(Context context, @Nullable String title, @Nullable String text, @Nullable int smallIcon, Bitmap largeIcon, boolean isNotCancelAble, PendingIntent pendingIntent, int NOTIFICATION_ID) {

        Notification.Builder notificationBuilder = new Notification.Builder(context);
        notificationBuilder.setContentTitle(title);
        notificationBuilder.setContentText(text);
        if (pendingIntent != null) {
            notificationBuilder.setContentIntent(pendingIntent);
        }
        //notificationBuilder.setSmallIcon(Icon.createWithBitmap(PictureUtil.createBitmapFromString("10", " KB")));
        notificationBuilder.setSmallIcon(smallIcon);
        notificationBuilder.setLargeIcon(largeIcon);
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setOngoing(isNotCancelAble);
        notificationBuilder.setPriority(Notification.PRIORITY_MAX);
        //notificationBuilder.setProgress();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static void setPingNotif(Context context, @Nullable String title, @Nullable String text, String pingSpeed , boolean isNotCancelAble, PendingIntent pendingIntent, int NOTIFICATION_ID) {

        Notification.Builder notificationBuilder = new Notification.Builder(context);
        notificationBuilder.setContentTitle(title);
        notificationBuilder.setContentText(text);
        if (pendingIntent != null) {
            notificationBuilder.setContentIntent(pendingIntent);
        }
        notificationBuilder.setSmallIcon(Icon.createWithBitmap(PictureUtil.createBitmapFromString(pingSpeed, "ms")));
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setOngoing(isNotCancelAble);
        notificationBuilder.setPriority(Notification.PRIORITY_MAX);
        //notificationBuilder.setProgress();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
    }

    /*public static void setCustomNotification(Context context, @Nullable String title, @Nullable String text, @Nullable int smallIcon, Bitmap bigIcon, boolean isNotCancelAble, PendingIntent pendingIntent, int NOTIFICATION_ID) {
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        RemoteViews mRemoteViews = new RemoteViews(context.getPackageName(), R.layout.custom_notification_small);
        mRemoteViews.setImageViewBitmap(R.id.notif_icon, bigIcon);
        mRemoteViews.setTextViewText(R.id.notif_title, title);
        mRemoteViews.setTextViewText(R.id.notif_content, text);

        int apiVersion = Build.VERSION.SDK_INT;

        if (apiVersion < Build.VERSION_CODES.HONEYCOMB) {
            Notification mNotification = new Notification(smallIcon, title, System.currentTimeMillis());
            mNotification.contentView = mRemoteViews;
            mNotification.defaults |= Notification.DEFAULT_LIGHTS;
            //mNotification.contentIntent = pendingIntent;
            //mNotification.flags |= Notification.FLAG_NO_CLEAR; //Do not clear the notification

            mNotificationManager.notify(NOTIFICATION_ID, mNotification);
        } else {
            Notification.Builder mBuilder = new Notification.Builder(context)
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(smallIcon)
                    .setContent(mRemoteViews)
                    //.setAutoCancel(isNotCancelAble) //Do not clear the notification
                    ;

            mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
        }
    }*/

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static boolean isNotificationShow1(Context context, int NOTIFICATION_ID) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        StatusBarNotification[] notifications = notificationManager.getActiveNotifications();
        for (StatusBarNotification notification : notifications) {
            return notification.getId() == NOTIFICATION_ID;
        }
        return false;
    }

    private static boolean isNotificationShow2(Context context, int NOTIFICATION_ID) {
        Intent notificationIntent = new Intent(context, context.getClass());
        PendingIntent test = PendingIntent.getActivity(context, NOTIFICATION_ID, notificationIntent, PendingIntent.FLAG_NO_CREATE);
        return test != null;
    }

    public static void clearNotification(Context context, int NOTIFICATION_ID) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
    }

    public static void clearNotificationAll(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

}
