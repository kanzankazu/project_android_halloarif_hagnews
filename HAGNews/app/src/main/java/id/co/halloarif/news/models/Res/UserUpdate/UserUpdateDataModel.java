package id.co.halloarif.news.models.Res.UserUpdate;

public class UserUpdateDataModel {
    public Change_user_profile change_user_profile;

    public Change_user_profile getChange_user_profile() {
        return change_user_profile;
    }

    public void setChange_user_profile(Change_user_profile change_user_profile) {
        this.change_user_profile = change_user_profile;
    }
}
