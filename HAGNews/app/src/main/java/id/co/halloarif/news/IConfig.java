package id.co.halloarif.news;

public interface IConfig {

    String API_BASE_URL = "http://halloarif.co.id/news/";
    String API_MAIN_URL = API_BASE_URL + "webapi/app/";

    int API_CALLER_REPORT = 121212;
    int API_CALLER_UPDATE = 123123;
    int API_CALLER_OUI = 112233;

    String SUCCESS = "success";

    String PACKAGE_NAME = App.getContext().getPackageName();
    String VERSION_NAME = BuildConfig.VERSION_NAME;
    int VERSION_CODE = BuildConfig.VERSION_CODE;

    String FEATURE = "" +
            "- save IP and show again \n" +
            "- Ip Utility :\n" +
            "   + Ping\n" +
            "   + WOL(Wake On Lan)\n" +
            "   + Port Scan\n" +
            "   + Check device in LAN/WLAN Connection\n" +
            "   + Ping Internet\n" +
            "   + NEW !! Ping Background\n" +
            " - NEW !! Widget Ping in Home Your Android (shortcut ping internet, wifi, network, tethering, phone info settings ) \n" +
            " - have MAC NIC \n" +
            "\n";

}
