package id.co.halloarif.news.activities_fragments.afnew.activity_new.logreg;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.VideoView;

import id.co.halloarif.news.ISeasonConfig;
import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afnew.activity_new.BaseHomeActivity;
import id.co.halloarif.news.support.SessionUtil;

public class SplashNewActivity extends AppCompatActivity {
    ImageView ivSplashfvbi;
    VideoView vvSplashfvbi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        initComponent();
        initParam();
        initSession();
        initContent();
        initListener();

    }

    private void initComponent() {
        vvSplashfvbi = (VideoView) findViewById(R.id.vvSplash);
        ivSplashfvbi = (ImageView) findViewById(R.id.ivSplash);
    }

    private void initParam() {

    }

    private void initSession() {
        if (SessionUtil.checkIfExist(ISeasonConfig.KEY_USER_ID)) {
            moveToMain();
        }
    }

    private void moveToMain() {
        Intent intent = new Intent(SplashNewActivity.this, BaseHomeActivity.class);
        startActivity(intent);
        finish();
    }

    private void initContent() {
        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.news);
        vvSplashfvbi.setVideoURI(uri);
        vvSplashfvbi.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
                mp.setLooping(true);
            }
        });

        //Load animation
        Animation slide_down = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);

        ivSplashfvbi.startAnimation(slide_down);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                //code here
                Intent intent = new Intent(SplashNewActivity.this, LoginNewActivity.class);
                startActivity(intent);
                //finish();
            }
        }, 2000);
    }

    private void initListener() {

    }
}
