package id.co.halloarif.news.models.Res.SubCategory;

import java.util.List;

class SubCatDataMainModel {
    public List<SubCatDataModel> cat_related_sub_cat = null;
    public List<SubCatPostModel> posts_data = null;

    public List<SubCatDataModel> getCat_related_sub_cat() {
        return cat_related_sub_cat;
    }

    public void setCat_related_sub_cat(List<SubCatDataModel> cat_related_sub_cat) {
        this.cat_related_sub_cat = cat_related_sub_cat;
    }

    public List<SubCatPostModel> getPosts_data() {
        return posts_data;
    }

    public void setPosts_data(List<SubCatPostModel> posts_data) {
        this.posts_data = posts_data;
    }
}
