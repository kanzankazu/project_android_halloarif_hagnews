package id.co.halloarif.news.activities_fragments.afold.adapters_old;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;

import id.co.halloarif.news.R;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Viewpager_items;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ST_004 on 06-02-2018.
 */

public class Featured_Adapter extends RecyclerView.Adapter<Featured_Adapter.ViewHolder> implements Filterable {
    private Context activity;
    private List<Viewpager_items> mArrayList;
    private List<Viewpager_items> mFilteredList;
    private int row;

    public Featured_Adapter(Context act, int resource, ArrayList<Viewpager_items> arrayList) {
        activity = act;
        row = resource;
        mArrayList = arrayList;
        mFilteredList = arrayList;
    }


    @Override
    public Featured_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view;
        view = LayoutInflater.from(viewGroup.getContext()).inflate(row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final Featured_Adapter.ViewHolder viewHolder, int i) {
        Viewpager_items model = mFilteredList.get(i);

        viewHolder.txt_category.setText(model.getCategory_name());

        String current_datetime = Constants.Current_Time_Zone();
        long api_datetime = Constants.getDateInMillis(model.getCreated_at());
        long current_timeZone = Constants.getDateInMillis(current_datetime);
        viewHolder.txt_date.setText(DateUtils.getRelativeTimeSpanString(api_datetime, current_timeZone, DateUtils.SECOND_IN_MILLIS));
        viewHolder.txt_title.setText(model.getTitle());
        viewHolder.txt_views.setText(model.getHit());

        Glide.with(activity)
                .load(model.getImage_default())
                .transition(new DrawableTransitionOptions().crossFade())
                .apply(RequestOptions.centerCropTransform().error(R.drawable.tallywithback))
                .into(viewHolder.thumbnail);

      /*  Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        viewHolder.linear_back.setBackgroundColor(color);*/

        //viewHolder.tv_name.setText(mFilteredList.get(i).getName());
        //viewHolder.tv_version.setText(mFilteredList.get(i).getVer());
        //viewHolder.tv_api_level.setText(mFilteredList.get(i).getApi());
    }

    @Override
    public int getItemCount() {
        return mFilteredList == null ? 0 : mFilteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    mFilteredList = mArrayList;
                } else {
                    ArrayList<Viewpager_items> filteredList = new ArrayList<>();

                    for (Viewpager_items androidVersion : mArrayList) {

                        /*if (androidVersion.getTitle().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }*/
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Viewpager_items>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_category, txt_title, txt_date, txt_views;
        ImageView thumbnail;

        ViewHolder(View view) {
            super(view);
            txt_category = (TextView) view.findViewById(R.id.txt_category);
            txt_title = (TextView) view.findViewById(R.id.txt_title);
            txt_date = (TextView) view.findViewById(R.id.txt_date);
            txt_views = (TextView) view.findViewById(R.id.txt_views);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
        }
    }

    public void setData(List<Viewpager_items> datas) {
        mFilteredList = datas;
        notifyDataSetChanged();

    }

    public void replaceData(List<Viewpager_items> datas) {
        mFilteredList.clear();
        mFilteredList.addAll(datas);
        notifyDataSetChanged();
    }

    public void addDatas(List<Viewpager_items> datas) {
        mFilteredList.addAll(datas);
        notifyItemRangeInserted(mFilteredList.size(), datas.size());
    }

    public void addDataFirst(Viewpager_items data) {
        int position = 0;
        mFilteredList.add(position, data);
        notifyItemInserted(position);
    }

    public void removeAt(int position) {
        mFilteredList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mFilteredList.size());
    }

    public void removeDataFirst() {
        int position = 0;
        mFilteredList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mFilteredList.size());
    }

}