package id.co.halloarif.news.models.Res.Category;

import java.util.ArrayList;
import java.util.List;

public class CatDataMainModel {
    public List<CatDataModel> cat_data = new ArrayList<>();
    public List<CatPostModel> posts_data = new ArrayList<>();

    public List<CatDataModel> getCat_data() {
        return cat_data;
    }

    public void setCat_data(List<CatDataModel> cat_data) {
        this.cat_data = cat_data;
    }

    public List<CatPostModel> getPosts_data() {
        return posts_data;
    }

    public void setPosts_data(List<CatPostModel> posts_data) {
        this.posts_data = posts_data;
    }
}
