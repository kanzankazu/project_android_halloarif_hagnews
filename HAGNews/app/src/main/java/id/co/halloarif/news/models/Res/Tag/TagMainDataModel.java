package id.co.halloarif.news.models.Res.Tag;

public class TagMainDataModel {
    public String id;
    public String lang_id;
    public String title;
    public String title_slug;
    public String keywords;
    public String summary;
    public String content;
    public String category_id;
    public String subcategory_id;
    public String image_big;
    public String image_default;
    public String image_slider;
    public String image_mid;
    public String image_small;
    public String hit;
    public String optional_url;
    public String need_auth;
    public String is_slider;
    public String slider_order;
    public String is_featured;
    public String featured_order;
    public String is_recommended;
    public String is_breaking;
    public String visibility;
    public String show_right_column;
    public String post_type;
    public String video_path;
    public String image_url;
    public String video_embed_code;
    public String user_id;
    public String status;
    public String feed_id;
    public String post_url;
    public String show_post_url;
    public String image_description;
    public String created_at;
    public String tag_slug_name;
    public String tags_name;
    public String username;
    public String user_slug;
    public String category_name_slug;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLang_id() {
        return lang_id;
    }

    public void setLang_id(String lang_id) {
        this.lang_id = lang_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle_slug() {
        return title_slug;
    }

    public void setTitle_slug(String title_slug) {
        this.title_slug = title_slug;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getSubcategory_id() {
        return subcategory_id;
    }

    public void setSubcategory_id(String subcategory_id) {
        this.subcategory_id = subcategory_id;
    }

    public String getImage_big() {
        return image_big;
    }

    public void setImage_big(String image_big) {
        this.image_big = image_big;
    }

    public String getImage_default() {
        return image_default;
    }

    public void setImage_default(String image_default) {
        this.image_default = image_default;
    }

    public String getImage_slider() {
        return image_slider;
    }

    public void setImage_slider(String image_slider) {
        this.image_slider = image_slider;
    }

    public String getImage_mid() {
        return image_mid;
    }

    public void setImage_mid(String image_mid) {
        this.image_mid = image_mid;
    }

    public String getImage_small() {
        return image_small;
    }

    public void setImage_small(String image_small) {
        this.image_small = image_small;
    }

    public String getHit() {
        return hit;
    }

    public void setHit(String hit) {
        this.hit = hit;
    }

    public String getOptional_url() {
        return optional_url;
    }

    public void setOptional_url(String optional_url) {
        this.optional_url = optional_url;
    }

    public String getNeed_auth() {
        return need_auth;
    }

    public void setNeed_auth(String need_auth) {
        this.need_auth = need_auth;
    }

    public String getIs_slider() {
        return is_slider;
    }

    public void setIs_slider(String is_slider) {
        this.is_slider = is_slider;
    }

    public String getSlider_order() {
        return slider_order;
    }

    public void setSlider_order(String slider_order) {
        this.slider_order = slider_order;
    }

    public String getIs_featured() {
        return is_featured;
    }

    public void setIs_featured(String is_featured) {
        this.is_featured = is_featured;
    }

    public String getFeatured_order() {
        return featured_order;
    }

    public void setFeatured_order(String featured_order) {
        this.featured_order = featured_order;
    }

    public String getIs_recommended() {
        return is_recommended;
    }

    public void setIs_recommended(String is_recommended) {
        this.is_recommended = is_recommended;
    }

    public String getIs_breaking() {
        return is_breaking;
    }

    public void setIs_breaking(String is_breaking) {
        this.is_breaking = is_breaking;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public String getShow_right_column() {
        return show_right_column;
    }

    public void setShow_right_column(String show_right_column) {
        this.show_right_column = show_right_column;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getVideo_path() {
        return video_path;
    }

    public void setVideo_path(String video_path) {
        this.video_path = video_path;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getVideo_embed_code() {
        return video_embed_code;
    }

    public void setVideo_embed_code(String video_embed_code) {
        this.video_embed_code = video_embed_code;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFeed_id() {
        return feed_id;
    }

    public void setFeed_id(String feed_id) {
        this.feed_id = feed_id;
    }

    public String getPost_url() {
        return post_url;
    }

    public void setPost_url(String post_url) {
        this.post_url = post_url;
    }

    public String getShow_post_url() {
        return show_post_url;
    }

    public void setShow_post_url(String show_post_url) {
        this.show_post_url = show_post_url;
    }

    public String getImage_description() {
        return image_description;
    }

    public void setImage_description(String image_description) {
        this.image_description = image_description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getTag_slug_name() {
        return tag_slug_name;
    }

    public void setTag_slug_name(String tag_slug_name) {
        this.tag_slug_name = tag_slug_name;
    }

    public String getTags_name() {
        return tags_name;
    }

    public void setTags_name(String tags_name) {
        this.tags_name = tags_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUser_slug() {
        return user_slug;
    }

    public void setUser_slug(String user_slug) {
        this.user_slug = user_slug;
    }

    public String getCategory_name_slug() {
        return category_name_slug;
    }

    public void setCategory_name_slug(String category_name_slug) {
        this.category_name_slug = category_name_slug;
    }
}
