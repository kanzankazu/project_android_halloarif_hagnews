package id.co.halloarif.news.activities_fragments.afold.frag_old;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mzelzoghbi.zgallery.ZGallery;
import com.mzelzoghbi.zgallery.adapters.GridImagesAdapter;
import com.mzelzoghbi.zgallery.entities.ZColor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import id.co.halloarif.news.R;
import id.co.halloarif.news.activities_fragments.afold.activity_old.MainActivity;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Recyclerview_Adapter_news;
import id.co.halloarif.news.activities_fragments.afold.adapters_old.Sub_category_Adapter;
import id.co.halloarif.news.constants.Constants;
import id.co.halloarif.news.models.Viewpager_items;
import id.co.halloarif.news.utils.Recycleritemclicklistener;

import static id.co.halloarif.news.support.SystemUtil.dpToPx;

/**
 * Created by ST_004 on 27-02-2018.
 */

public class Gallery_Fragment extends Fragment {
    RecyclerView recyclerview_sub_cat_india;
    RecyclerView recyclerview_india_post;
    LinearLayoutManager linearLayoutManager;
    ArrayList<Viewpager_items> arrayList_sub_heading = new ArrayList<>();
    ArrayList<Viewpager_items> arrayList_all_post = new ArrayList<>();
    SwipeRefreshLayout mSwipeRefreshLayout;
    TextView text_header;
    FragmentActivity fragmentActivity;
    View view1, view_snack;

    Sub_category_Adapter sub_category_adapter;
    Recyclerview_Adapter_news recyclerview_adapter_news;

    ArrayList<String> list = new ArrayList<>();
    private GridImagesAdapter adapter;

    // TODO: Rename and change types and number of parameters
    public static Gallery_Fragment newInstance() {
        Gallery_Fragment fragment = new Gallery_Fragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view1 = inflater.inflate(R.layout.fragment_india, container, false);

        initComponent(view1);
        initParam();
        initSession();
        initContent();
        initListener();
        view_snack = view1;
        return view1;
    }

    private void initComponent(View view) {
        text_header = view.findViewById(R.id.text_header);
        recyclerview_india_post = view.findViewById(R.id.recyclerview_india_post);
        recyclerview_sub_cat_india = view.findViewById(R.id.recyclerview_sub_cat_india);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
    }

    private void initParam() {

    }

    private void initSession() {

    }

    private void initContent() {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerview_adapter_news = new Recyclerview_Adapter_news(getActivity(), R.layout.gallery_item_layout, arrayList_all_post);
        recyclerview_india_post.setAdapter(recyclerview_adapter_news);
        recyclerview_india_post.setLayoutManager(mLayoutManager);
        recyclerview_india_post.addItemDecoration(new GridSpacingItemDecoration(3, dpToPx(getActivity(), 1), true));
        recyclerview_india_post.setItemAnimator(new DefaultItemAnimator());

        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        sub_category_adapter = new Sub_category_Adapter(getActivity(), R.layout.sub_category_items, arrayList_sub_heading);
        recyclerview_sub_cat_india.setAdapter(sub_category_adapter);
        recyclerview_sub_cat_india.setLayoutManager(linearLayoutManager);
        recyclerview_sub_cat_india.setItemAnimator(new DefaultItemAnimator());

        // SwipeRefreshLayout
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_blue_dark);

    }

    private void initListener() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Call_API_Gallery();
            }
        });
        recyclerview_sub_cat_india.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                // Constants.Cat_Name = Constants.SLUG_TO_OPEN;
                Viewpager_items model = arrayList_sub_heading.get(position);
                Constants.Cat_Tag_Name = model.getName_sub();
                Constants.Tag_id = model.getPager_id();
                ((MainActivity) fragmentActivity).CreateFragment(new Subcategory_Gallery_Fragment());
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        recyclerview_india_post.addOnItemTouchListener(new Recycleritemclicklistener(getActivity(), new Recycleritemclicklistener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ZGallery.with(getActivity(), list)
                        .setToolbarTitleColor(ZColor.WHITE)
                        .setGalleryBackgroundColor(ZColor.black_little_dark_trans)
                        .selectedImagePosition(position)
                        .setToolbarColorResId(R.color.colorPrimary)
                        .setTitle("Gallery")
                        .show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                    text_header.setText(Constants.TAG_TITLE);
                    Call_API_Gallery();
                }
            });
        } catch (NullPointerException e) {
            Log.e("Error", "Error");
        }
    }

    private void Call_API_Gallery() {

        String URL = Constants.MAIN_URL + "gallery/";

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("URL_GALLERY", "" + response.trim ());
                try {
                    arrayList_sub_heading.clear();
                    arrayList_all_post.clear();
                    list.clear();
                    JSONObject ojs = new JSONObject(response);
                    String abc = ojs.getString("status");
                    Log.e("Value", abc);
                    if (Integer.parseInt(abc) == 1) {
                        JSONObject jsonArray = ojs.getJSONObject("get_json_data");

                        JSONArray jsonArray1 = jsonArray.getJSONArray("cat_data");
                        for (int i = 0; i < jsonArray1.length(); i++) {
                            JSONObject obj = jsonArray1.getJSONObject(i);
                            String id = obj.getString("id");
                            String name = obj.getString("name");
                            // String name_slug = obj.getString("name_slug");

                            arrayList_sub_heading.add(new Viewpager_items(id, name, "", ""));
                        }

                        JSONArray jsonArray12 = jsonArray.getJSONArray("posts_data");
                        for (int i = 0; i < jsonArray12.length(); i++) {
                            JSONObject obj = jsonArray12.getJSONObject(i);

                            String id = obj.getString("id");
                            String title = obj.getString("title");
                            String image_default = Constants.IMAGE_URL + obj.getString("path_big");
                            // String category_name = obj.getString("category_name_slug");
                            /// String post_type = obj.getString("post_type");
                            String created_at = obj.getString("created_at");
                            // String comment_count = obj.getString("comment_count");
                            // String hit = obj.getString("hit");

                            arrayList_all_post.add(new Viewpager_items(id, title, image_default, "", "", created_at, "", ""));
                        }

                        if (arrayList_sub_heading.isEmpty()) {

                        } else {
                            sub_category_adapter.setData(arrayList_sub_heading);
                        }

                        if (arrayList_all_post.isEmpty()) {

                        } else {

                            for (int i = 0; i < arrayList_all_post.size(); i++) {
                                list.add(arrayList_all_post.get(i).getImage_default());
                            }
                            recyclerview_adapter_news.setData(arrayList_all_post);

                        }

                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mSwipeRefreshLayout.setRefreshing(false);

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mSwipeRefreshLayout.setRefreshing(false);

                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);

    }

    private void Call_API_SELECTED_GALLERY(String id) {
        RequestQueue requestQueue;
        requestQueue = Volley.newRequestQueue(getActivity());

        String URL = Constants.MAIN_URL + "gallery/" + id;
        Log.e("URL_GALLERY", Constants.MAIN_URL + "gallery/" + id);
        StringRequest strRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("URL_GALLERY", "" + response.trim());
                        try {

                            JSONObject ojs = new JSONObject(response);
                            String abc = ojs.getString("status");
                            Log.e("Value", abc);
                            if (Integer.parseInt(abc) == 1) {
                                JSONArray gallery_data = ojs.getJSONArray("gallery_json_data");
                                for (int i = 0; i < gallery_data.length(); i++) {
                                    JSONObject obj = gallery_data.getJSONObject(i);

                                    String id = obj.getString("id");
                                    String title = obj.getString("title");
                                    String image_default = Constants.IMAGE_URL + obj.getString("path_big");
                                    String category_name = obj.getString("category_name");
                                    String created_at = obj.getString("created_at");

                                }

                            } else {
                               /*String error = ojs.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                                Log.e("error", error);*/
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }) {
        };
        requestQueue.add(strRequest);

    }

    class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;

        private boolean includeEdge;

        GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }

    }
}
